﻿using CafeS60.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CafeS60.DAL
{
    public class OTallerSQL
    {
        private string connectionString;
        private SqlConnection connection;


        public OTallerSQL()
        {
            /*En CafeS60.DAL - Settings.settings*/
            ConnectionString = Properties.Resource1.CadenaConexion1;
            //@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=catalogo;Integrated Security=True";
            Connection = new SqlConnection(ConnectionString);
        }


        #region busca oferta importe totales entre dos fechas
        /// <summary>
        /// Busca todos los registros por tipo: fecha de orden o fecha de trabajo,
        /// entre las fechas inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public float valor(int i, int f, string mecanico, string objeto, string tipo, string proyecto)
        {
            float resultado = 0;
            string query = "";

            if (proyecto.Equals("OTROS"))
            {
                if (objeto.Equals("REPUESTO"))
                {
                    query += "select SUM(oferta-despl-MO) from otallergeneral ";
                }
                else if (objeto.Equals("DESPLAZAMIENTO"))
                {
                    query += "select SUM(despl) from otallergeneral ";
                }
                else if (objeto.Equals("MANO DE OBRA"))
                {
                    query += "select SUM(mo) from otallergeneral ";
                }
                else
                {
                    query += "select SUM(oferta) from otallergeneral ";
                }
            }
            else
            {
                if (objeto.Equals("REPUESTO"))
                {
                    query += "select SUM(oferta-despl-MO) from otaller ";
                }
                else if (objeto.Equals("DESPLAZAMIENTO"))
                {
                    query += "select SUM(despl) from otaller ";
                }
                else if (objeto.Equals("MANO DE OBRA"))
                {
                    query += "select SUM(mo) from otaller ";
                }
                else
                {
                    query += "select SUM(oferta) from otaller ";
                }
            }

            if (tipo.Equals("FECHA DE REALIZACION DE TRABAJO"))
            {
                query += " where f_trabajo>=@F_inicio and f_trabajo<=@F_fin ";
            }
            else
            {
                query += " where f_repa>=@F_inicio and f_repa<=@F_fin ";
            }

            //query += " and tipo like '%CON RECAMBIO%' ";

            if (proyecto.Equals("TODO") || proyecto.Equals("OTROS"))
            {
                query += "";
            }
            else
            {
                query += " and proyecto like '%" + proyecto + "%'";
            }

            if (mecanico.Equals("TODOS"))
            {
                query += "";
            }
            else
            {
                query += " and tecnico like '%" + mecanico + "%'";
            }

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("F_inicio", i));
            command.Parameters.Add(new SqlParameter("F_fin", f));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {

                var r = command.ExecuteScalar();
                if (r != DBNull.Value)
                    resultado = (float)Convert.ToDouble(r);
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return resultado;
        }
        #endregion


        #region busca lista entre dos fechas INFORME OTALLER CAFE
        /// <summary>
        /// Busca todos los registros por tipo: fecha de orden o fecha de trabajo,
        /// entre las fechas inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public List<OTaller> valores(int i, int f, string mecanico, string objeto, string tipo, string proyecto)
        {
            List<OTaller> lista = new List<OTaller>();
            string query = "";

            if (objeto.Equals("REPUESTO"))
            {
                query += "select SUM(oferta-despl-MO) from otaller ";
            }
            else if (objeto.Equals("DESPLAZAMIENTO"))
            {
                query += "select SUM(despl) from otaller ";
            }
            else if (objeto.Equals("MANO DE OBRA"))
            {
                query += "select SUM(mo) from otaller ";
            }
            else if (objeto.Equals("TODO"))
            {
                query += "select SUM(oferta) from otaller ";
            }
            else
            {
                query += "select * from otaller ";
            }


            if (tipo.Equals("FECHA DE REALIZACION DE TRABAJO"))
            {
                query += " where f_trabajo>=@F_inicio and f_trabajo<=@F_fin ";
            }
            else
            {
                query += " where f_repa>=@F_inicio and f_repa<=@F_fin ";
            }

            //query += " and tipo like '%CON RECAMBIO%' ";
            if (proyecto.Equals("TODO"))
            {
                query += "";
            }
            else
            {
                query += " and proyecto like '%" + proyecto + "%'";
            }

            if (mecanico.Equals("TODOS"))
            {
                query += " order by f_trabajo asc";
            }
            else
            {
                query += " and tecnico like '%" + mecanico + "%' order by f_trabajo asc";
            }

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("F_inicio", i));
            command.Parameters.Add(new SqlParameter("F_fin", f));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    OTaller producto = new OTaller()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        Tecnico = Convert.ToString(dr[1]),
                        Estado = Convert.ToString(dr[2]),
                        Resultado = Convert.ToString(dr[3]),
                        Proyecto = Convert.ToString(dr[4]),
                        Pds = Convert.ToString(dr[5]),
                        F_trabajo = Convert.ToInt32(dr[6]),
                        F_repa = Convert.ToInt32(dr[7]),
                        Cliente = Convert.ToString(dr[8]),
                        Provincia = Convert.ToString(dr[9]),
                        Oaveria = Convert.ToString(dr[10]),
                        Omontaje = Convert.ToString(dr[11]),
                        Odesmontaje = Convert.ToString(dr[12]),
                        Otaller = Convert.ToString(dr[13]),
                        Material = Convert.ToString(dr[14]),
                        Codmaterial = Convert.ToString(dr[15]),
                        Ax = Convert.ToString(dr[16]),
                        Sap = Convert.ToString(dr[17]),
                        Norden = Convert.ToString(dr[18]),
                        Oferta = (float)Convert.ToDouble(dr[19]),
                        Despl = (float)Convert.ToDouble(dr[20]),
                        Mo = (float)Convert.ToDouble(dr[21]),
                        Repuesto = Convert.ToString(dr[22]),
                        Cmi = Convert.ToString(dr[23]),
                        Tipo = Convert.ToString(dr[24]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(producto);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion


        #region busca lista entre dos fechas NFORME OTALLERGENERAL OTRAS
        /// <summary>
        /// Busca todos los registros por tipo: fecha de orden o fecha de trabajo,
        /// entre las fechas inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public List<OTaller> valoresGeneral(int i, int f, string mecanico, string objeto, string tipo, string proyecto)
        {
            List<OTaller> lista = new List<OTaller>();
            string query = "";

            if (objeto.Equals("REPUESTO"))
            {
                query += "select SUM(oferta-despl-MO) from otallergeneral ";
            }
            else if (objeto.Equals("DESPLAZAMIENTO"))
            {
                query += "select SUM(despl) from otallergeneral ";
            }
            else if (objeto.Equals("MANO DE OBRA"))
            {
                query += "select SUM(mo) from otallergeneral ";
            }
            else if (objeto.Equals("TODO"))
            {
                query += "select SUM(oferta) from otallergeneral ";
            }
            else
            {
                query += "select * from otallergeneral ";
            }


            if (tipo.Equals("FECHA DE REALIZACION DE TRABAJO"))
            {
                query += " where f_trabajo>=@F_inicio and f_trabajo<=@F_fin ";
            }
            else
            {
                query += " where f_repa>=@F_inicio and f_repa<=@F_fin ";
            }

            //query += " and tipo like '%CON RECAMBIO%' ";
            if (proyecto.Equals("OTROS"))
            {
                query += "";
            }
            else
            {
                query += " and proyecto like '%" + proyecto + "%'";
            }

            if (mecanico.Equals("TODOS"))
            {
                query += " order by f_trabajo asc";
            }
            else
            {
                query += " and tecnico like '%" + mecanico + "%' order by f_trabajo asc";
            }

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("F_inicio", i));
            command.Parameters.Add(new SqlParameter("F_fin", f));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    OTaller producto = new OTaller()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        Tecnico = Convert.ToString(dr[1]),
                        Estado = Convert.ToString(dr[2]),
                        Resultado = Convert.ToString(dr[3]),
                        Proyecto = Convert.ToString(dr[4]),
                        Cod_proyecto = Convert.ToString(dr[5]),
                        Pds = Convert.ToString(dr[6]),
                        F_trabajo = Convert.ToInt32(dr[7]),
                        F_repa = Convert.ToInt32(dr[8]),
                        Cliente = Convert.ToString(dr[9]),
                        Provincia = Convert.ToString(dr[10]),
                        Oaveria = Convert.ToString(dr[11]),
                        Omontaje = Convert.ToString(dr[12]),
                        Odesmontaje = Convert.ToString(dr[13]),
                        Otaller = Convert.ToString(dr[14]),
                        Material = Convert.ToString(dr[15]),
                        Codmaterial = Convert.ToString(dr[16]),
                        Ax = Convert.ToString(dr[17]),
                        Sap = Convert.ToString(dr[18]),
                        Norden = Convert.ToString(dr[19]),
                        Oferta = (float)Convert.ToDouble(dr[20]),
                        Despl = (float)Convert.ToDouble(dr[21]),
                        Mo = (float)Convert.ToDouble(dr[22]),
                        Repuesto = Convert.ToString(dr[23]),
                        Cmi = Convert.ToString(dr[24]),
                        Tipo = Convert.ToString(dr[25]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(producto);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion


        #region busca registros entre dos fechas con tipo y estado - para cerrar órdenes
        /// <summary>
        /// Busca todos los registros por tipo: fecha de orden o fecha de trabajo,
        /// entre las fechas inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public List<OTaller> busca(int i, int f, string tipo, string estado)
        {
            /*Declaramos una lista de objetos de tipo clsProducto que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            string todo = "";
            List<OTaller> lista = new List<OTaller>();
            string query;
            if (tipo.Equals("FECHA DE REALIZACION DE TRABAJO"))
            {
                query = "select * from otaller where f_trabajo>=@F_inicio and f_trabajo<=@F_fin";
            }
            else if (tipo.Equals("FECHA DE CREACION DE ORDEN"))
            {
                query = "select * from otaller where f_repa>=@F_inicio and f_repa<=@F_fin";
            }
            else
            {
                query = "select * from otaller";
                todo = "TODO";
            }


            if (todo.Equals("TODO"))
            {
                query += " where estado like '%" + estado + "%'";
            }
            else
            {
                query += " and estado like '%" + estado + "%'";
            }
            /*
            if (todo.Equals("TODO"))
            {
                if (estado.Equals("RECHAZADO"))
                {
                    query += " where estado like '%" + estado + "%'";
                }
                else if (estado.Equals("ACEPTADO"))
                {
                    query += " where estado like '%" + estado + "%'";
                }
                else
                {
                    query += "";
                }
            }
            else
            {
                if (estado.Equals("RECHAZADO"))
                {
                    query += " and estado like '%" + estado + "%'";
                }
                else if (estado.Equals("ACEPTADO"))
                {
                    query += " and estado like '%" + estado + "%'";
                }
                else
                {
                    query += "";
                }
            }
            */

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("F_inicio", i));
            command.Parameters.Add(new SqlParameter("F_fin", f));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    OTaller producto = new OTaller()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        Tecnico = Convert.ToString(dr[1]),
                        Estado = Convert.ToString(dr[2]),
                        Resultado = Convert.ToString(dr[3]),
                        Proyecto = Convert.ToString(dr[4]),
                        Pds = Convert.ToString(dr[5]),
                        F_trabajo = Convert.ToInt32(dr[6]),
                        F_repa = Convert.ToInt32(dr[7]),
                        Cliente = Convert.ToString(dr[8]),
                        Provincia = Convert.ToString(dr[9]),
                        Oaveria = Convert.ToString(dr[10]),
                        Omontaje = Convert.ToString(dr[11]),
                        Odesmontaje = Convert.ToString(dr[12]),
                        Otaller = Convert.ToString(dr[13]),
                        Material = Convert.ToString(dr[14]),
                        Codmaterial = Convert.ToString(dr[15]),
                        Ax = Convert.ToString(dr[16]),
                        Sap = Convert.ToString(dr[17]),
                        Norden = Convert.ToString(dr[18]),
                        Oferta = (float)Convert.ToDouble(dr[19]),
                        Despl = (float)Convert.ToDouble(dr[20]),
                        Mo = (float)Convert.ToDouble(dr[21]),
                        Repuesto = Convert.ToString(dr[22]),
                        Cmi = Convert.ToString(dr[23]),
                        Tipo = Convert.ToString(dr[24]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(producto);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion


        #region busca registros entre dos fechas con tipo, estado y resultado - para visualizar órdenes
        /// <summary>
        /// Busca todos los registros por tipo: fecha de orden o fecha de trabajo,
        /// entre las fechas inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public List<OTaller> busca(int i, int f, string tipo, string estado, string resultado)
        {
            /*Declaramos una lista de objetos de tipo clsProducto que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            string todo = "";
            List<OTaller> lista = new List<OTaller>();
            string query;
            if (tipo.Equals("FECHA DE REALIZACION DE TRABAJO"))
            {
                query = "select * from otaller where f_trabajo>=@F_inicio and f_trabajo<=@F_fin";
            }
            else if (tipo.Equals("FECHA DE CREACION DE ORDEN"))
            {
                query = "select * from otaller where f_repa>=@F_inicio and f_repa<=@F_fin";
            }
            else
            {
                query = "select * from otaller";
                todo = "TODO";
            }


            if (todo.Equals("TODO"))
            {
                query += " where estado like '%" + estado + "%'";
                if (estado.Equals("CERRAD"))
                {
                    query += " and resultado like '%" + resultado + "%'";
                }
            }
            else
            {
                query += " and estado like '%" + estado + "%'";
                if (estado.Equals("CERRAD"))
                {
                    query += " and resultado like '%" + resultado + "%'";
                }
            }

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("F_inicio", i));
            command.Parameters.Add(new SqlParameter("F_fin", f));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    OTaller producto = new OTaller()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        Tecnico = Convert.ToString(dr[1]),
                        Estado = Convert.ToString(dr[2]),
                        Resultado = Convert.ToString(dr[3]),
                        Proyecto = Convert.ToString(dr[4]),
                        Pds = Convert.ToString(dr[5]),
                        F_trabajo = Convert.ToInt32(dr[6]),
                        F_repa = Convert.ToInt32(dr[7]),
                        Cliente = Convert.ToString(dr[8]),
                        Provincia = Convert.ToString(dr[9]),
                        Oaveria = Convert.ToString(dr[10]),
                        Omontaje = Convert.ToString(dr[11]),
                        Odesmontaje = Convert.ToString(dr[12]),
                        Otaller = Convert.ToString(dr[13]),
                        Material = Convert.ToString(dr[14]),
                        Codmaterial = Convert.ToString(dr[15]),
                        Ax = Convert.ToString(dr[16]),
                        Sap = Convert.ToString(dr[17]),
                        Norden = Convert.ToString(dr[18]),
                        Oferta = (float)Convert.ToDouble(dr[19]),
                        Despl = (float)Convert.ToDouble(dr[20]),
                        Mo = (float)Convert.ToDouble(dr[21]),
                        Repuesto = Convert.ToString(dr[22]),
                        Cmi = Convert.ToString(dr[23]),
                        Tipo = Convert.ToString(dr[24]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(producto);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion


        #region busca registros entre dos fechas
        /// <summary>
        /// Busca todos los registros por tipo: fecha de orden o fecha de trabajo,
        /// entre las fechas inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public List<OTaller> busca(int i, int f, string tipo)
        {
            /*Declaramos una lista de objetos de tipo clsProducto que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<OTaller> lista = new List<OTaller>();
            string query;
            if (tipo.Equals("FECHA DE REALIZACION DE TRABAJO"))
            {
                query = "select * from otaller where f_trabajo>=@F_inicio and f_trabajo<=@F_fin";
            }
            else if(tipo.Equals("FECHA DE CREACION DE ORDEN"))
            {
                query = "select * from otaller where f_repa>=@F_inicio and f_repa<=@F_fin";
            }
            else
            {
                query = "select * from otaller";
            }
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("F_inicio", i));
            command.Parameters.Add(new SqlParameter("F_fin", f));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    OTaller producto = new OTaller()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        Tecnico = Convert.ToString(dr[1]),
                        Estado = Convert.ToString(dr[2]),
                        Resultado = Convert.ToString(dr[3]),
                        Proyecto = Convert.ToString(dr[4]),
                        Pds = Convert.ToString(dr[5]),
                        F_trabajo = Convert.ToInt32(dr[6]),
                        F_repa = Convert.ToInt32(dr[7]),
                        Cliente = Convert.ToString(dr[8]),
                        Provincia = Convert.ToString(dr[9]),
                        Oaveria = Convert.ToString(dr[10]),
                        Omontaje = Convert.ToString(dr[11]),
                        Odesmontaje = Convert.ToString(dr[12]),
                        Otaller = Convert.ToString(dr[13]),
                        Material = Convert.ToString(dr[14]),
                        Codmaterial = Convert.ToString(dr[15]),
                        Ax = Convert.ToString(dr[16]),
                        Sap = Convert.ToString(dr[17]),
                        Norden = Convert.ToString(dr[18]),
                        Oferta = (float)Convert.ToDouble(dr[19]),
                        Despl = (float)Convert.ToDouble(dr[20]),
                        Mo = (float)Convert.ToDouble(dr[21]),
                        Repuesto = Convert.ToString(dr[22]),
                        Cmi = Convert.ToString(dr[23]),
                        Tipo = Convert.ToString(dr[24]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(producto);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        } 
        #endregion


        #region insertar un registro
        /// <summary>
        /// Insertar un nuevo botellero reparado en la base de datos
        /// <param name="producto">Tiene la información del botellero a registrar</param>
        /// </summary>
        /// <param name="producto"></param>
        /// <returns>Si 'resultado' es distinto de 0 -> OK, 
        /// sino, Si 'resultado'==0 -> ERROR</returns>
        public int inserta(OTaller producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into otaller " +
                "values(@Tecnico,@Estado,@Resultado,@Proyecto,@Pds,@F_trabajo,@F_repa,@Cliente,@Provincia," +
                "       @Oaveria,@Omontaje,@Odesmontaje,@Otaller,@Material," +
                "       @Codmaterial,@Ax,@Sap,@Norden,@Oferta,@Despl,@Mo,@Repuesto,@Cmi,@Tipo);";

        /*Abre una conexión*/
        Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            command.Parameters.Add(new SqlParameter("Tecnico", producto.Tecnico));
            command.Parameters.Add(new SqlParameter("Estado", producto.Estado)); 
            command.Parameters.Add(new SqlParameter("Resultado", producto.Resultado));
            command.Parameters.Add(new SqlParameter("Proyecto", producto.Proyecto));
            command.Parameters.Add(new SqlParameter("Pds", producto.Pds));
            command.Parameters.Add(new SqlParameter("F_trabajo", producto.F_trabajo));
            command.Parameters.Add(new SqlParameter("F_repa", producto.F_repa));
            command.Parameters.Add(new SqlParameter("Cliente", producto.Cliente));
            command.Parameters.Add(new SqlParameter("Provincia", producto.Provincia));
            command.Parameters.Add(new SqlParameter("Oaveria", producto.Oaveria));
            command.Parameters.Add(new SqlParameter("Omontaje", producto.Omontaje));
            command.Parameters.Add(new SqlParameter("Odesmontaje", producto.Odesmontaje));
            command.Parameters.Add(new SqlParameter("Otaller", producto.Otaller));
            command.Parameters.Add(new SqlParameter("Material", producto.Material));
            command.Parameters.Add(new SqlParameter("Codmaterial", producto.Codmaterial));
            command.Parameters.Add(new SqlParameter("Ax", producto.Ax));
            command.Parameters.Add(new SqlParameter("Sap", producto.Sap));
            command.Parameters.Add(new SqlParameter("Norden", producto.Norden));
            command.Parameters.Add(new SqlParameter("Oferta", producto.Oferta));
            command.Parameters.Add(new SqlParameter("Despl", producto.Despl));
            command.Parameters.Add(new SqlParameter("Mo", producto.Mo));
            command.Parameters.Add(new SqlParameter("Repuesto", producto.Repuesto));
            command.Parameters.Add(new SqlParameter("Cmi", producto.Cmi));
            command.Parameters.Add(new SqlParameter("Tipo", producto.Tipo));
            try
            {
                resultado = command.ExecuteNonQuery();
                if (resultado != 0) MessageBox.Show("SQL: GUARDADO OK");
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR SQL: " + ex);
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion


        #region insertar un registro especial
        /// <summary>
        /// Insertar un nuevo botellero reparado en la base de datos
        /// <param name="producto">Tiene la información del botellero a registrar</param>
        /// </summary>
        /// <param name="producto"></param>
        /// <returns>Si 'resultado' es distinto de 0 -> OK, 
        /// sino, Si 'resultado'==0 -> ERROR</returns>
        public int inserta2(OTaller producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into otallergeneral " +
                "values(@Tecnico,@Estado,@Resultado,@Proyecto,@Cod_proyecto,@Pds,@F_trabajo,@F_repa,@Cliente,@Provincia," +
                "       @Oaveria,@Omontaje,@Odesmontaje,@Otaller,@Material," +
                "       @Codmaterial,@Ax,@Sap,@Norden,@Oferta,@Despl,@Mo,@Repuesto,@Cmi,@Tipo);";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            command.Parameters.Add(new SqlParameter("Tecnico", producto.Tecnico));
            command.Parameters.Add(new SqlParameter("Estado", producto.Estado));
            command.Parameters.Add(new SqlParameter("Resultado", producto.Resultado));
            command.Parameters.Add(new SqlParameter("Proyecto", producto.Proyecto));
            command.Parameters.Add(new SqlParameter("Cod_proyecto", producto.Cod_proyecto));
            command.Parameters.Add(new SqlParameter("Pds", producto.Pds));
            command.Parameters.Add(new SqlParameter("F_trabajo", producto.F_trabajo));
            command.Parameters.Add(new SqlParameter("F_repa", producto.F_repa));
            command.Parameters.Add(new SqlParameter("Cliente", producto.Cliente));
            command.Parameters.Add(new SqlParameter("Provincia", producto.Provincia));
            command.Parameters.Add(new SqlParameter("Oaveria", producto.Oaveria));
            command.Parameters.Add(new SqlParameter("Omontaje", producto.Omontaje));
            command.Parameters.Add(new SqlParameter("Odesmontaje", producto.Odesmontaje));
            command.Parameters.Add(new SqlParameter("Otaller", producto.Otaller));
            command.Parameters.Add(new SqlParameter("Material", producto.Material));
            command.Parameters.Add(new SqlParameter("Codmaterial", producto.Codmaterial));
            command.Parameters.Add(new SqlParameter("Ax", producto.Ax));
            command.Parameters.Add(new SqlParameter("Sap", producto.Sap));
            command.Parameters.Add(new SqlParameter("Norden", producto.Norden));
            command.Parameters.Add(new SqlParameter("Oferta", producto.Oferta));
            command.Parameters.Add(new SqlParameter("Despl", producto.Despl));
            command.Parameters.Add(new SqlParameter("Mo", producto.Mo));
            command.Parameters.Add(new SqlParameter("Repuesto", producto.Repuesto));
            command.Parameters.Add(new SqlParameter("Cmi", producto.Cmi));
            command.Parameters.Add(new SqlParameter("Tipo", producto.Tipo));
            try
            {
                resultado = command.ExecuteNonQuery();
                if (resultado != 0) MessageBox.Show("SQL: GUARDADO OK");
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR SQL: " + ex);
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion


        #region busca si existe un registro por su id cafe
        /// <summary>
        /// Busca todos los registros por tipo: fecha de orden o fecha de trabajo,
        /// entre las fechas inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public int existe(int Id)
        {
            int resultado = 0;
            string query =  "select COUNT(*) from otaller where id=@ID";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("ID", Id));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {

                var r = command.ExecuteScalar();
                if (r != DBNull.Value)
                    resultado = Convert.ToInt32(r);
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return resultado;
        }
        #endregion


        #region busca si existe un registro por su id general
        /// <summary>
        /// Busca todos los registros por tipo: fecha de orden o fecha de trabajo,
        /// entre las fechas inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public int existe2(int Id)
        {
            int resultado = 0;
            string query = "select COUNT(*) from otallergeneral where id=@ID";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("ID", Id));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {

                var r = command.ExecuteScalar();
                if (r != DBNull.Value)
                    resultado = Convert.ToInt32(r);
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return resultado;
        }
        #endregion


        #region busca si existe un registro por su NUMERO DE ORDEN cafe
        /// <summary>
        /// Busca todos los registros por tipo: fecha de orden o fecha de trabajo,
        /// entre las fechas inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public int existe(string Id)
        {
            int resultado = 0;
            string query = "select COUNT(*) from otaller where norden like '%" + Id + "%'";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //command.Parameters.Add(new SqlParameter("ID", Id));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {

                var r = command.ExecuteScalar();
                if (r != DBNull.Value)
                    resultado = Convert.ToInt32(r);
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return resultado;
        }
        #endregion


        #region busca si existe un registro por su NUMERO DE ORDEN general
        /// <summary>
        /// Busca todos los registros por tipo: fecha de orden o fecha de trabajo,
        /// entre las fechas inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public int existe2(string Id)
        {
            int resultado = 0;
            string query = "select COUNT(*) from otallergeneral where norden like '%" + Id + "%'";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //command.Parameters.Add(new SqlParameter("ID", Id));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {

                var r = command.ExecuteScalar();
                if (r != DBNull.Value)
                    resultado = Convert.ToInt32(r);
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return resultado;
        }
        #endregion


        #region busca una orden por su ID cafe
        /// <summary>
        /// Devuelve un Objeto OTaller
        /// </summary>
        public OTaller obtener_id(int idOrden)
        {
            OTaller producto = new OTaller();
            producto = null;
            string query = "select * from otaller where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("ID", idOrden));
            SqlDataAdapter da = new SqlDataAdapter();

            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);

                DataRow dr = ds.Tables[0].Rows[0];
                //Instanciamos al objeto cls_Tarjeta para llenar sus propiedades
                producto = new OTaller()
                {
                    Id = Convert.ToInt32(dr[0]),
                    Tecnico = Convert.ToString(dr[1]),
                    Estado = Convert.ToString(dr[2]),
                    Resultado = Convert.ToString(dr[3]),
                    Proyecto = Convert.ToString(dr[4]),
                    Pds = Convert.ToString(dr[5]),
                    F_trabajo = Convert.ToInt32(dr[6]),
                    F_repa = Convert.ToInt32(dr[7]),
                    Cliente = Convert.ToString(dr[8]),
                    Provincia = Convert.ToString(dr[9]),
                    Oaveria = Convert.ToString(dr[10]),
                    Omontaje = Convert.ToString(dr[11]),
                    Odesmontaje = Convert.ToString(dr[12]),
                    Otaller = Convert.ToString(dr[13]),
                    Material = Convert.ToString(dr[14]),
                    Codmaterial = Convert.ToString(dr[15]),
                    Ax = Convert.ToString(dr[16]),
                    Sap = Convert.ToString(dr[17]),
                    Norden = Convert.ToString(dr[18]),
                    Oferta = (float)Convert.ToDouble(dr[19]),
                    Despl = (float)Convert.ToDouble(dr[20]),
                    Mo = (float)Convert.ToDouble(dr[21]),
                    Repuesto = Convert.ToString(dr[22]),
                    Cmi = Convert.ToString(dr[23]),
                    Tipo = Convert.ToString(dr[24]),
                };

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                Connection.Close();
            }

            return producto;
        }
        #endregion


        #region busca una orden por su ID general
        /// <summary>
        /// Devuelve un Objeto OTaller
        /// </summary>
        public OTaller obtener_id2(int idOrden)
        {
            OTaller producto = new OTaller();
            producto = null;
            string query = "select * from otallergeneral where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("ID", idOrden));
            SqlDataAdapter da = new SqlDataAdapter();

            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);

                DataRow dr = ds.Tables[0].Rows[0];
                //Instanciamos al objeto cls_Tarjeta para llenar sus propiedades
                producto = new OTaller()
                {
                    Id = Convert.ToInt32(dr[0]),
                    Tecnico = Convert.ToString(dr[1]),
                    Estado = Convert.ToString(dr[2]),
                    Resultado = Convert.ToString(dr[3]),
                    Proyecto = Convert.ToString(dr[4]),
                    Cod_proyecto = Convert.ToString(dr[5]),
                    Pds = Convert.ToString(dr[6]),
                    F_trabajo = Convert.ToInt32(dr[7]),
                    F_repa = Convert.ToInt32(dr[8]),
                    Cliente = Convert.ToString(dr[9]),
                    Provincia = Convert.ToString(dr[10]),
                    Oaveria = Convert.ToString(dr[11]),
                    Omontaje = Convert.ToString(dr[12]),
                    Odesmontaje = Convert.ToString(dr[13]),
                    Otaller = Convert.ToString(dr[14]),
                    Material = Convert.ToString(dr[15]),
                    Codmaterial = Convert.ToString(dr[16]),
                    Ax = Convert.ToString(dr[17]),
                    Sap = Convert.ToString(dr[18]),
                    Norden = Convert.ToString(dr[19]),
                    Oferta = (float)Convert.ToDouble(dr[20]),
                    Despl = (float)Convert.ToDouble(dr[21]),
                    Mo = (float)Convert.ToDouble(dr[22]),
                    Repuesto = Convert.ToString(dr[23]),
                    Cmi = Convert.ToString(dr[24]),
                    Tipo = Convert.ToString(dr[25]),
                };

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                Connection.Close();
            }

            return producto;
        }
        #endregion


        #region busca una orden por su NUMERO DE ORDEN cafe
        /// <summary>
        /// Devuelve un Objeto OTaller
        /// </summary>
        public OTaller obtener_orden(string idOrden)
        {
            OTaller producto = new OTaller();
            producto = null;
            string query = "select * from otaller where norden like '%" + idOrden + "%'";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            //command.Parameters.Add(new SqlParameter("ID", idOrden));
            SqlDataAdapter da = new SqlDataAdapter();

            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);

                DataRow dr = ds.Tables[0].Rows[0];
                //Instanciamos al objeto cls_Tarjeta para llenar sus propiedades
                producto = new OTaller()
                {
                    Id = Convert.ToInt32(dr[0]),
                    Tecnico = Convert.ToString(dr[1]),
                    Estado = Convert.ToString(dr[2]),
                    Resultado = Convert.ToString(dr[3]),
                    Proyecto = Convert.ToString(dr[4]),
                    Pds = Convert.ToString(dr[5]),
                    F_trabajo = Convert.ToInt32(dr[6]),
                    F_repa = Convert.ToInt32(dr[7]),
                    Cliente = Convert.ToString(dr[8]),
                    Provincia = Convert.ToString(dr[9]),
                    Oaveria = Convert.ToString(dr[10]),
                    Omontaje = Convert.ToString(dr[11]),
                    Odesmontaje = Convert.ToString(dr[12]),
                    Otaller = Convert.ToString(dr[13]),
                    Material = Convert.ToString(dr[14]),
                    Codmaterial = Convert.ToString(dr[15]),
                    Ax = Convert.ToString(dr[16]),
                    Sap = Convert.ToString(dr[17]),
                    Norden = Convert.ToString(dr[18]),
                    Oferta = (float)Convert.ToDouble(dr[19]),
                    Despl = (float)Convert.ToDouble(dr[20]),
                    Mo = (float)Convert.ToDouble(dr[21]),
                    Repuesto = Convert.ToString(dr[22]),
                    Cmi = Convert.ToString(dr[23]),
                    Tipo = Convert.ToString(dr[24]),
                };

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                Connection.Close();
            }

            return producto;
        }
        #endregion


        #region busca una orden por su NUMERO DE ORDEN general
        /// <summary>
        /// Devuelve un Objeto OTaller
        /// </summary>
        public OTaller obtener_orden2(string idOrden)
        {
            OTaller producto = new OTaller();
            producto = null;
            string query = "select * from otallergeneral where norden like '%" + idOrden + "%'";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            //command.Parameters.Add(new SqlParameter("ID", idOrden));
            SqlDataAdapter da = new SqlDataAdapter();

            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);

                DataRow dr = ds.Tables[0].Rows[0];
                //Instanciamos al objeto cls_Tarjeta para llenar sus propiedades
                producto = new OTaller()
                {
                    Id = Convert.ToInt32(dr[0]),
                    Tecnico = Convert.ToString(dr[1]),
                    Estado = Convert.ToString(dr[2]),
                    Resultado = Convert.ToString(dr[3]),
                    Proyecto = Convert.ToString(dr[4]),
                    Cod_proyecto = Convert.ToString(dr[5]),
                    Pds = Convert.ToString(dr[6]),
                    F_trabajo = Convert.ToInt32(dr[7]),
                    F_repa = Convert.ToInt32(dr[8]),
                    Cliente = Convert.ToString(dr[9]),
                    Provincia = Convert.ToString(dr[10]),
                    Oaveria = Convert.ToString(dr[11]),
                    Omontaje = Convert.ToString(dr[12]),
                    Odesmontaje = Convert.ToString(dr[13]),
                    Otaller = Convert.ToString(dr[14]),
                    Material = Convert.ToString(dr[15]),
                    Codmaterial = Convert.ToString(dr[16]),
                    Ax = Convert.ToString(dr[17]),
                    Sap = Convert.ToString(dr[18]),
                    Norden = Convert.ToString(dr[19]),
                    Oferta = (float)Convert.ToDouble(dr[20]),
                    Despl = (float)Convert.ToDouble(dr[21]),
                    Mo = (float)Convert.ToDouble(dr[22]),
                    Repuesto = Convert.ToString(dr[23]),
                    Cmi = Convert.ToString(dr[24]),
                    Tipo = Convert.ToString(dr[25]),
                };

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                Connection.Close();
            }

            return producto;
        }
        #endregion


        #region pone una orden como CERRADA
        /// Cambiar a cerrada
        public void cerrar(int idorden)
        {
            //Declaramos nuestra consulta de Acción Sql
            string query = "update otaller set estado='CERRADO' where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("ID", idorden));
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.Close();
            }

        }
        #endregion


        #region pone una orden como ABIERTA
        /// Cambiar a cerrada
        public void abrir(int idorden)
        {
            //Declaramos nuestra consulta de Acción Sql
            string query = "update otaller set estado='ABIERTA' where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("ID", idorden));
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.Close();
            }

        }
        #endregion


        #region pone una orden como ACEPTADA
        /// Cambiar a cerrada
        public void aceptar(int idorden)
        {
            //Declaramos nuestra consulta de Acción Sql
            string query = "update otaller set resultado='ACEPTADO' where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("ID", idorden));
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.Close();
            }

        }
        #endregion


        #region pone una orden como RECHAZADA
        /// Cambiar a cerrada
        public void rechazar(int idorden)
        {
            //Declaramos nuestra consulta de Acción Sql
            string query = "update otaller set resultado='RECHAZADO' where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("ID", idorden));
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.Close();
            }

        }
        #endregion


        #region pone una orden como EN TRATAMIENTO
        /// Cambiar a cerrada
        public void tratar(int idorden)
        {
            //Declaramos nuestra consulta de Acción Sql
            string query = "update otaller set resultado='TRATAMIENTO' where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("ID", idorden));
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.Close();
            }

        }
        #endregion


        #region DESCRIPTORES DE ACCESO
        public string ConnectionString
        { get; set; }

        public SqlConnection Connection
        { get; set; }
        #endregion
    }
}
