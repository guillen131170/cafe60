﻿using CafeS60.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CafeS60.DAL
{
    public class AlmEqSeriadoSQL
    {
        #region ATRIBUTOS
        private string connectionString;
        private SqlConnection connection;
        #endregion


        #region CONSTRUCTORES
        public AlmEqSeriadoSQL()
        {
            /*En CafeS60.DAL - Settings.settings*/
            ConnectionString = Properties.Resource1.CadenaConexion1;
            //@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=catalogo;Integrated Security=True";
            Connection = new SqlConnection(ConnectionString);
        }
        #endregion


        #region METODOS
        #region insertar un registro
        /// <summary>
        /// Insertar un nuevo equipo en la base de datos
        /// <param name="producto">Tiene la información del equipo a registrar</param>
        /// </summary>
        /// <param name="producto"></param>
        /// <returns>Si 'resultado' es distinto de 0 -> OK, 
        /// sino, Si 'resultado'==0 -> ERROR</returns>
        public int inserta(CafentoEquipos producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into cafentoequipos " +
                "values(@NombreMaterial,@CodigoMaterial,@CodSAP,@CodAX," +
                "       @Origen,@ODSEntrada,@Ubicacion,@Destino,@ODSSalida," +
                "       @Estado,@Stock);";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            command.Parameters.Add(new SqlParameter("NombreMaterial", producto.NombreMaterial));
            command.Parameters.Add(new SqlParameter("CodigoMaterial", producto.CodigoMaterial));
            command.Parameters.Add(new SqlParameter("CodSAP", producto.CodSAP));
            command.Parameters.Add(new SqlParameter("CodAX", producto.CodAX));
            command.Parameters.Add(new SqlParameter("Origen", producto.Origen));
            command.Parameters.Add(new SqlParameter("ODSEntrada", producto.ODSEntrada));
            command.Parameters.Add(new SqlParameter("Ubicacion", producto.Ubicacion));
            command.Parameters.Add(new SqlParameter("Destino", producto.Destino));
            command.Parameters.Add(new SqlParameter("ODSSalida", producto.ODSSalida));
            command.Parameters.Add(new SqlParameter("Estado", producto.Estado));
            command.Parameters.Add(new SqlParameter("Stock", producto.Stock));
            try
            {
                resultado = command.ExecuteNonQuery();
                if (resultado != 0) MessageBox.Show("SQL: GUARDADO OK");
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR SQL: " + ex);
                throw ex;
            }
            finally { Connection.Close(); }
            return resultado;
        }
        #endregion

        #region busca si existe un registro por su ns ax
        /// <summary>
        /// Busca un registro
        /// </summary>
        /// <returns>retorna 0 si no existe, otro valor => registro encontrado</returns>
        public int existeNsAX(string Id)
        {
            int resultado = 0;
            string query = "select COUNT(*) from cafentoequipos where CodAX like '%" + Id + "%'";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {

                var r = command.ExecuteScalar();
                if (r != DBNull.Value)
                    resultado = Convert.ToInt32(r);
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return resultado;
        }
        #endregion

        #region busca si existe un registro por su ns sap
        /// <summary>
        /// Busca un registro
        /// </summary>
        /// <returns>retorna 0 si no existe, otro valor => registro encontrado</returns>
        public int existeNsSAP(string Id)
        {
            int resultado = 0;
            string query = 
                "select COUNT(*) from cafentoequipos where CodSAP like '%" + Id + "%'";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {

                var r = command.ExecuteScalar();
                if (r != DBNull.Value)
                    resultado = Convert.ToInt32(r);
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return resultado;
        }
        #endregion

        #region busca si existe un registro por su código de material sap
        /// <summary>
        /// Busca un registro
        /// </summary>
        /// <returns>retorna 0 si no existe, otro valor => registro encontrado</returns>
        public int existeMaterial(string Id)
        {
            int resultado = 0;
            string query = 
                "select COUNT(*) from cafentoequipos where nombrematerial like '%" + Id + "%'";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {

                var r = command.ExecuteScalar();
                if (r != DBNull.Value)
                    resultado = Convert.ToInt32(r);
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return resultado;
        }
        #endregion

        #region buscar una orden por su ID cafe
        /// <summary>
        /// Devuelve un Objeto OTaller
        /// </summary>
        public CafentoEquipos obtenerRegistro(string Id)
        {
            CafentoEquipos producto = new CafentoEquipos();
            producto = null;
            string query = "select * from cafentoequipos where " +
                                    " CodAX          like '%" + Id + "%' or " +
                                    " CodSAP         like '%" + Id + "%' or " +
                                    " nombrematerial like '%" + Id + "%'";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);

                DataRow dr = ds.Tables[0].Rows[0];
                //Instanciamos al objeto cls_Tarjeta para llenar sus propiedades
                producto = new CafentoEquipos()
                {
                    Id = Convert.ToInt32(dr[0]),
                    NombreMaterial = Convert.ToString(dr[1]),
                    CodigoMaterial = Convert.ToString(dr[2]),
                    CodSAP = Convert.ToString(dr[3]),
                    CodAX = Convert.ToString(dr[4]),
                    Origen = Convert.ToString(dr[5]),
                    ODSEntrada = Convert.ToString(dr[6]),
                    Ubicacion = Convert.ToString(dr[7]),
                    Destino = Convert.ToString(dr[8]),
                    ODSSalida = Convert.ToString(dr[9]),
                    Estado = Convert.ToString(dr[10]),
                    Stock = Convert.ToInt32(dr[11]),
                };

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                Connection.Close();
            }

            return producto;
        }
        #endregion

        #region modificar un registro
        /// <summary>
        /// Modifica un nuevo equipo en la base de datos
        /// <param name="producto">Tiene la información del equipo a modificar</param>
        /// </summary>
        /// <param name="producto"></param>
        /// <returns>Si 'resultado' es distinto de 0 -> OK, 
        /// sino, Si 'resultado'==0 -> ERROR</returns>
        public int modificar(CafentoEquipos producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "update cafentoequipos " +
                " set nombrematerial = @NombreMaterial, CodigoMaterial = @CodigoMaterial," +
                "     CodSAP = @CodSAP, CodAX = @CodAX," +
                "     Origen = @Origen, ODSEntrada = @ODSEntrada, Ubicacion = @Ubicacion," +
                "     Destino = @Destino, ODSSalida = @ODSSalida," +
                "     Estado = @Estado, Stock = @Stock " +
                " where id = Id;";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            command.Parameters.Add(new SqlParameter("Id", producto.Id));
            command.Parameters.Add(new SqlParameter("NombreMaterial", producto.NombreMaterial));
            command.Parameters.Add(new SqlParameter("CodigoMaterial", producto.CodigoMaterial));
            command.Parameters.Add(new SqlParameter("CodSAP", producto.CodSAP));
            command.Parameters.Add(new SqlParameter("CodAX", producto.CodAX));
            command.Parameters.Add(new SqlParameter("Origen", producto.Origen));
            command.Parameters.Add(new SqlParameter("ODSEntrada", producto.ODSEntrada));
            command.Parameters.Add(new SqlParameter("Ubicacion", producto.Ubicacion));
            command.Parameters.Add(new SqlParameter("Destino", producto.Destino));
            command.Parameters.Add(new SqlParameter("ODSSalida", producto.ODSSalida));
            command.Parameters.Add(new SqlParameter("Estado", producto.Estado));
            command.Parameters.Add(new SqlParameter("Stock", producto.Stock));
            try
            {
                resultado = command.ExecuteNonQuery();
                if (resultado != 0) MessageBox.Show("SQL: GUARDADO OK");
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR SQL: " + ex);
                throw ex;
            }
            finally { Connection.Close(); }
            return resultado;
        }
        #endregion

        #region busca registros entre dos fechas
        /// <summary>
        /// Busca todos los registros
        /// </summary>
        /// <returns>Lista de registros</returns>
        public List<CafentoEquipos> busca()
        {
            /*Declaramos una lista de objetos de tipo clsProducto que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<CafentoEquipos> lista = new List<CafentoEquipos>();
            string query = "select * from cafentoequipos where Stock>0";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    CafentoEquipos producto = new CafentoEquipos()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        NombreMaterial = Convert.ToString(dr[1]),
                        CodigoMaterial = Convert.ToString(dr[2]),
                        CodSAP = Convert.ToString(dr[3]),
                        CodAX = Convert.ToString(dr[4]),
                        Origen = Convert.ToString(dr[5]),
                        ODSEntrada = Convert.ToString(dr[6]),
                        Ubicacion = Convert.ToString(dr[7]),
                        Destino = Convert.ToString(dr[8]),
                        ODSSalida = Convert.ToString(dr[9]),
                        Estado = Convert.ToString(dr[10]),
                        Stock = Convert.ToInt32(dr[11]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(producto);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion
        #endregion


        #region DESCRIPTORES DE ACCESO
        public string ConnectionString
        { get; set; }

        public SqlConnection Connection
        { get; set; }
        #endregion
    }
}
