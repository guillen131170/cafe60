﻿using CafeS60.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafeS60.DAL
{
    public class ProductSQL
    {
        private string connectionString;
        private SqlConnection connection;

        public ProductSQL()
        {
            /*En CafeS60.DAL - Settings.settings*/
            ConnectionString = Properties.Resource1.CadenaConexion1;
            //@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=catalogo;Integrated Security=True";
            Connection = new SqlConnection(ConnectionString);
        }


        #region todas las consultas sql
        #region listar todos los registros
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<Product> obtenerTodos(int modo)
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<Product> lista = new List<Product>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from producto";
            switch (modo)
            {
                case 0:
                    query = "select * from producto where active = 0";
                    break;
                case 1:
                    query = "select * from producto where active = 1";
                    break;
                default:
                    query = "select * from producto";
                    break;
            }
            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    Product producto = new Product()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Code = item["Code"].ToString(),
                        Name = item["Name"].ToString(),
                        Description = item["Description"].ToString(),
                        Price = Convert.ToInt32(item["Price"]),
                        Stock = Convert.ToInt32(item["Stock"]),
                        Family = item["Family"].ToString(),
                        Reference = item["Reference"].ToString(),
                        Img = item["Img"].ToString(),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion

        #region listar todos los registros con filtro
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<Product> obtenerTodosFiltrado(int modo, string nombre)
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<Product> lista = new List<Product>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from producto";
            switch (modo)
            {
                case 0:
                    query = "select * from producto where active = 0 and name LIKE '%' + @NOMBRE + '%'";
                    break;
                case 1:
                    query = "select * from producto where active = 1 and name LIKE '%' + @NOMBRE + '%'";
                    break;
                case 2:
                    query = "select * from producto where name LIKE '%' + @NOMBRE + '%'";
                    break;
                default:
                    query = "select * from producto where name LIKE '%' + @NOMBRE + '%'";
                    break;
            }
            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.AddWithValue("@NOMBRE", nombre);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    Product producto = new Product()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Code = item["Code"].ToString(),
                        Name = item["Name"].ToString(),
                        Description = item["Description"].ToString(),
                        Price = Convert.ToInt32(item["Price"]),
                        Stock = Convert.ToInt32(item["Stock"]),
                        Family = item["Family"].ToString(),
                        Reference = item["Reference"].ToString(),
                        Img = item["Img"].ToString(),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion

        #region listar todos los registros con filtro
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<Product> obtenerTodosStock(int modo, string nombre)
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<Product> lista = new List<Product>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from producto";
            switch (modo)
            {
                case 0:
                    query = "select * from producto where active = 0 and name LIKE '%' + @NOMBRE + '%'";
                    break;
                case 1:
                    query = "select * from producto where active = 1 and name LIKE '%' + @NOMBRE + '%'";
                    break;
                case 2:
                    query = "select * from producto where name LIKE '%' + @NOMBRE + '%'";
                    break;
                default:
                    query = "select * from producto where name LIKE '%' + @NOMBRE + '%'";
                    break;
            }
            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.AddWithValue("@NOMBRE", nombre);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    Product producto = new Product()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Code = item["Code"].ToString(),
                        Name = item["Name"].ToString(),
                        Description = item["Description"].ToString(),
                        Price = Convert.ToInt32(item["Price"]),
                        Stock = Convert.ToInt32(item["Stock"]),
                        Family = item["Family"].ToString(),
                        Reference = item["Reference"].ToString(),
                        Img = item["Img"].ToString(),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion

        #region devuelve el stock por su id
        /// <summary>
        /// Devuelve el stock por su id
        /// </summary>  
        public int obtenerStock(int id)
        {
            /**/
            int resultado = 0;

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select stock from producto where id LIKE @ID";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.AddWithValue("@ID", id);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                resultado = (Int32)command.ExecuteScalar();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return resultado;
        }
        #endregion

        #region insertar un registro
        /// <summary>
        /// Insertar un nuevo producto es la base de datos
        /// <param name="producto">Tiene la información del producto a registrar</param>
        /// </summary> 
        public int inserta(Product producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into producto " +
                "values(@Code,@Name,@Description,@Price,@Stock," +
                "       @Family, @Reference, @Img); ";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            command.Parameters.Add(new SqlParameter("Code", producto.Code));
            command.Parameters.Add(new SqlParameter("Name", producto.Name));
            command.Parameters.Add(new SqlParameter("Description", producto.Description));
            command.Parameters.Add(new SqlParameter("Price", producto.Price));
            command.Parameters.Add(new SqlParameter("Stock", producto.Stock));
            command.Parameters.Add(new SqlParameter("Family", producto.Family));
            command.Parameters.Add(new SqlParameter("Reference", producto.Reference));
            command.Parameters.Add(new SqlParameter("Img", producto.Img));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
             return resultado;
        }
        #endregion

        #region modificar un registro
        /// <summary>
        /// Modificar un producto es la base de datos
        /// <param name="producto">Tiene la información del producto a registrar</param>
        /// </summary> 
        public int modifica(int id, Product producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "update producto " +
                "set code=@Code, name=@Name, description=@Description, price=@Price, stock=@Stock," +
                "    family=@Family, reference=@Reference, img=@Img " +
                "where id=@id; ";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("Id", id));
            command.Parameters.Add(new SqlParameter("Code", producto.Code));
            command.Parameters.Add(new SqlParameter("Name", producto.Name));
            command.Parameters.Add(new SqlParameter("Description", producto.Description));
            command.Parameters.Add(new SqlParameter("Price", producto.Price));
            command.Parameters.Add(new SqlParameter("Stock", producto.Stock));
            command.Parameters.Add(new SqlParameter("Family", producto.Family));
            command.Parameters.Add(new SqlParameter("Reference", producto.Reference));
            command.Parameters.Add(new SqlParameter("Img", producto.Img));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion

        #region modificar un stock
        /// <summary>
        /// Modificar un producto es la base de datos
        /// <param name="producto">Tiene la información del producto a registrar</param>
        /// </summary> 
        public int stock(int id, string code, string name, int stock)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "update producto " +
                "set stock=@Stock " +
                " where id=@Id or code=@Code or name=@Name; ";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("Id", id));
            command.Parameters.Add(new SqlParameter("Code", code));
            command.Parameters.Add(new SqlParameter("Name", name));
            command.Parameters.Add(new SqlParameter("Stock", stock));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion

        #region buscar un registro duplicado por código o nombre
        /// <summary>
        /// Devuelve el resultado si 2 códigos o nombres de un producto son iguales
        /// </summary>  
        /// modo: 0 - ok
        ///       1 - duplicado
        public bool productoDuplicado(string co, string na)
        {
            bool resultado = true;

            //Cadena para la consulta sql a la base de datos
            string query = "select * from producto where code LIKE '%{0}%' AND name LIKE '%{1}%'";

            try
            {
                //Abrimos la conexion a la Base de datos
                Connection.Open();
                /*Ejecuta la consulta sql*/
                SqlCommand command = new SqlCommand(query, Connection);

                //command.CommandType = CommandType.StoredProcedure;
                if (Convert.ToInt32(command.ExecuteScalar()) > 0)
                {
                    resultado = false;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return resultado;
        }
        #endregion

        #region listar todos los registros
        /// <summary>
        /// Devuelve la lista de todas las entradas
        /// </summary>  
        public List<Move> obtenerEntradas()
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<Move> lista = new List<Move>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from entrada";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    Move entrada = new Move()
                    {
                        Id = Convert.ToInt32(item["id"]),
                        Product = item["product"].ToString(),
                        Type = item["typein"].ToString(),
                        Date = Convert.ToDateTime(item["datein"]),
                        Supplier = item["supplier"].ToString(),
                        Units = Convert.ToInt32(item["units"]),
                    };
                    lista.Add(entrada);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de entradas   
            return lista;
        }
        #endregion
        #endregion


        #region DESCRIPTORES DE ACCESO
        public string ConnectionString
        { get; set; }

        public SqlConnection Connection
        { get; set; } 
        #endregion


        #region OBSOLETO
        /*Devuelve todos los registros en un List de objetos (Product)*/
        public List<Product> listarList2()
        {
            List<Product> lista = new List<Product>();
            object[] resultado;
            //Abrimos la conexion a la Base de datos
            Connection.Open();

            //Creamos una variable que contendra la consulta a ejecutar
            String action = "SELECT * FROM producto";

            //Creamos un comeando del tipo SqlCommand y le pasamos la variable que contiene
            //la consulta y la conexion
            using (SqlCommand cmd = new SqlCommand(action, Connection))
            {
                //Rellenamos la lista
                SqlDataReader reader = cmd.ExecuteReader();

                //resultado.Add(reader);
                resultado = new object[reader.FieldCount];
            }

            foreach (var item in resultado)
            {
                lista.Add((Product)item);
            }
            Connection.Close();
            return lista;
        }

        /*Devuelve todos los registros en un DataTable*/
        public DataTable listarDT()
        {
            //Creamos un objeto DataTable que contendra los daos recuperados por el DataAdapter
            DataTable dt = new DataTable();
            //Abrimos la conexion a la Base de datos
            Connection.Open();

            //Creamos una variable que contendra la consulta a ejecutar
            String action = "SELECT * FROM producto";

            //Creamos un comeando del tipo SqlCommand y le pasamos la variable que contiene
            //la consulta y la conexion
            using (SqlCommand cmd = new SqlCommand(action, Connection))
            {
                //Creamos un objeto DataAdapter este objeto se encarga de abrir la conexion a la Bd
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                //Llenamos el objeto DataTable con los datos recuperados por el DataAdapter
                da.Fill(dt);
            }

            return dt;
        }

        /*Devuelve todos los registros en un List*/
        public List<Product> listarList1()
        {
            //Creamos un objeto DataTable que contendra los daos recuperados por el DataAdapter
            DataTable dt = new DataTable();
            List<Product> lista = new List<Product>();
            //Abrimos la conexion a la Base de datos
            Connection.Open();
            //Creamos una variable que contendra la consulta a ejecutar
            String action = "SELECT * FROM producto";

            //Creamos un comeando del tipo SqlCommand y le pasamos la variable que contiene
            //la consulta y la conexion
            using (SqlCommand cmd = new SqlCommand(action, Connection))
            {
                //Creamos un objeto DataAdapter este objeto se encarga de abrir la conexion a la Bd
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                //Llenamos el objeto DataTable con los datos recuperados por el DataAdapter
                da.Fill(dt);
            }
            foreach (DataRow item in dt.Rows)
            {
                Product producto = new Product();
                producto.Id = Convert.ToInt32(item["Id"]);
                producto.Code = item["Code"].ToString();
                producto.Name = item["Name"].ToString();
                producto.Description = item["Description"].ToString();
                producto.Price = Convert.ToInt32(item["Price"]);
                producto.Stock = Convert.ToInt32(item["Stock"]);
                producto.Reference = item["Reference"].ToString();
                producto.Img = item["Img"].ToString();
                lista.Add(producto);
            }
            return lista;
        }
        #endregion
    }
}
