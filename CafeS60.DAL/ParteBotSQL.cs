﻿using CafeS60.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CafeS60.DAL
{
    public class ParteBotSQL
    {
        private string connectionString;
        private SqlConnection connection;


        public ParteBotSQL()
        {
            /*En CafeS60.DAL - Settings.settings*/
            ConnectionString = Properties.Resource1.CadenaConexion1;
                //@"Data Source=FRANCISCO-PC\SQLEXPRESS;Initial Catalog=catalogo;Integrated Security=True";
            Connection = new SqlConnection(ConnectionString);
        }

        public void pruebasql()
        {
            try
            {
                MessageBox.Show("abriendo");
                Connection.Open();
                MessageBox.Show("cerrando");
                Connection.Close();
            }
            catch
            {
                MessageBox.Show("error");
                throw;
            }
            finally
            {
                MessageBox.Show("");
            }
        }


        #region insertar un registro
        /// <summary>
        /// Insertar un nuevo botellero reparado en la base de datos
        /// <param name="producto">Tiene la información del botellero a registrar</param>
        /// </summary>
        /// <param name="producto"></param>
        /// <returns>Si 'resultado' es distinto de 0 -> OK, 
        /// sino, Si 'resultado'==0 -> ERROR</returns>
        public int inserta(ParteBotellero producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into disBotParte " +
                "values(@F_repa,@Mo,@Origen,@Ns,@Fabricante," +
                "       @Marca, @Gas, @Modelo,@Estado,@Img,@Tecnico,@Anyo,@F_entrega," +
                "       @Resultado,@Orden,@Material,@Codigo,@SAP,@Oferta); ";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            command.Parameters.Add(new SqlParameter("F_repa", producto.F_repa));
            command.Parameters.Add(new SqlParameter("Mo", producto.Mo));
            command.Parameters.Add(new SqlParameter("Origen", producto.Origen));
            command.Parameters.Add(new SqlParameter("Ns", producto.Ns));
            command.Parameters.Add(new SqlParameter("Fabricante", producto.Fabricante));
            command.Parameters.Add(new SqlParameter("Marca", producto.Marca));
            command.Parameters.Add(new SqlParameter("Gas", producto.Gas));
            command.Parameters.Add(new SqlParameter("Modelo", producto.Modelo));
            command.Parameters.Add(new SqlParameter("Estado", producto.Estado));
            command.Parameters.Add(new SqlParameter("Img", producto.Img));
            command.Parameters.Add(new SqlParameter("Tecnico", producto.Tecnico));
            command.Parameters.Add(new SqlParameter("Anyo", producto.Anyo));
            command.Parameters.Add(new SqlParameter("F_entrega", producto.F_entrega));
            command.Parameters.Add(new SqlParameter("Material", producto.Material));
            command.Parameters.Add(new SqlParameter("Codigo", producto.Codigo));
            command.Parameters.Add(new SqlParameter("SAP", producto.SAP));
            command.Parameters.Add(new SqlParameter("Oferta", producto.Oferta));
            command.Parameters.Add(new SqlParameter("Orden", producto.Orden));
            command.Parameters.Add(new SqlParameter("Resultado", producto.Resultado));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {             
                MessageBox.Show("ERROR SQL: " + ex);
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion


        #region busca registros entre dos fechas
        /// <summary>
        /// Busca todos los registros por tipo: fecha de orden o fecha de trabajo,
        /// entre las fechas inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public List<ParteBotellero> busca(int i, int f)
        {
            /*Declaramos una lista de objetos de tipo clsProducto que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<ParteBotellero> lista = new List<ParteBotellero>();
            string query = "select * from disBotParte where f_repa>=@F_inicio and f_repa<=@F_fin";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("F_inicio", i));
            command.Parameters.Add(new SqlParameter("F_fin", f));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    ParteBotellero producto = new ParteBotellero()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        F_repa = Convert.ToInt32(dr[1]),
                        Mo = Convert.ToString(dr[2]),
                        Origen = Convert.ToString(dr[3]),
                        Ns = Convert.ToString(dr[4]),
                        Fabricante = Convert.ToString(dr[5]),
                        Marca = Convert.ToString(dr[6]),
                        Gas = Convert.ToString(dr[7]),
                        Modelo = Convert.ToString(dr[8]),
                        Estado = Convert.ToString(dr[9]),
                        Img = Convert.ToString(dr[10]),
                        Tecnico = Convert.ToString(dr[11]),
                        Anyo = Convert.ToInt32(dr[12]),
                        F_entrega = Convert.ToInt32(dr[13]),
                        Resultado = Convert.ToString(dr[14]),
                        Orden = Convert.ToString(dr[15]),
                        Material = Convert.ToString(dr[16]),
                        Codigo = Convert.ToString(dr[17]),
                        SAP = Convert.ToString(dr[18]),
                        Oferta = (float)Convert.ToDouble(dr[19]),

                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(producto);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion


        #region busca registros entre dos fechas
        /// <summary>
        /// Busca todos los registros por tipo: fecha de orden o fecha de trabajo,
        /// entre las fechas inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public List<ParteBotellero> busca(int i, int f, string tipo)
        {
            /*Declaramos una lista de objetos de tipo clsProducto que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<ParteBotellero> lista = new List<ParteBotellero>();
            string query = "select * from disBotParte where f_repa>=@F_inicio and f_repa<=@F_fin";
            if (tipo.Equals("REPARADO"))
            {
                query += " and estado like '%REPARADO%'";
            }
            else if (tipo.Equals("ENTREGADO"))
            {
                query += " and estado like '%ENTREGADO%'";
            }
            else
            {
                query += "";
            }

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("F_inicio", i));
            command.Parameters.Add(new SqlParameter("F_fin", f));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    ParteBotellero producto = new ParteBotellero()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        F_repa = Convert.ToInt32(dr[1]),
                        Mo = Convert.ToString(dr[2]),
                        Origen = Convert.ToString(dr[3]),
                        Ns = Convert.ToString(dr[4]),
                        Fabricante = Convert.ToString(dr[5]),
                        Marca = Convert.ToString(dr[6]),
                        Gas = Convert.ToString(dr[7]),
                        Modelo = Convert.ToString(dr[8]),
                        Estado = Convert.ToString(dr[9]),
                        Img = Convert.ToString(dr[10]),
                        Tecnico = Convert.ToString(dr[11]),
                        Anyo = Convert.ToInt32(dr[12]),
                        F_entrega = Convert.ToInt32(dr[13]),
                        Resultado = Convert.ToString(dr[14]),
                        Orden = Convert.ToString(dr[15]),
                        Material = Convert.ToString(dr[16]),
                        Codigo = Convert.ToString(dr[17]),
                        SAP = Convert.ToString(dr[18]),
                        Oferta = (float)Convert.ToDouble(dr[19]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(producto);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion


        #region busca registros entre dos fechas
        /// <summary>
        /// Busca todos los registros por tipo: fecha de orden o fecha de trabajo,
        /// entre las fechas inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public List<ParteBotellero> busca(int i, int f, string tipo, string tipofecha)
        {
            /*Declaramos una lista de objetos de tipo clsProducto que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<ParteBotellero> lista = new List<ParteBotellero>();
            string query = "";
            if (tipofecha.Equals("FECHA DE SALIDA"))
            {
                query += "select * from disBotParte where f_entrega>=@F_inicio and f_entrega<=@F_fin";
            }
            else
            {
                query += "select * from disBotParte where f_repa>=@F_inicio and f_repa<=@F_fin";
            }
 
            if (tipo.Equals("REPARADO"))
            {
                query += " and estado like '%REPARADO%'";
            }
            else if (tipo.Equals("ENTREGADO"))
            {
                query += " and estado like '%ENTREGADO%'";
            }
            else
            {
                query += "";
            }

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("F_inicio", i));
            command.Parameters.Add(new SqlParameter("F_fin", f));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    ParteBotellero producto = new ParteBotellero()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        F_repa = Convert.ToInt32(dr[1]),
                        Mo = Convert.ToString(dr[2]),
                        Origen = Convert.ToString(dr[3]),
                        Ns = Convert.ToString(dr[4]),
                        Fabricante = Convert.ToString(dr[5]),
                        Marca = Convert.ToString(dr[6]),
                        Gas = Convert.ToString(dr[7]),
                        Modelo = Convert.ToString(dr[8]),
                        Estado = Convert.ToString(dr[9]),
                        Img = Convert.ToString(dr[10]),
                        Tecnico = Convert.ToString(dr[11]),
                        Anyo = Convert.ToInt32(dr[12]),
                        F_entrega = Convert.ToInt32(dr[13]),
                        Resultado = Convert.ToString(dr[14]),
                        Orden = Convert.ToString(dr[15]),
                        Material = Convert.ToString(dr[16]),
                        Codigo = Convert.ToString(dr[17]),
                        SAP = Convert.ToString(dr[18]),
                        Oferta = (float)Convert.ToDouble(dr[19]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(producto);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion


        #region DESCRIPTORES DE ACCESO
        public string ConnectionString
        { get; set; }

        public SqlConnection Connection
        { get; set; }
        #endregion
    }
}
