﻿using CafeS60.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CafeS60.DAL
{
    public class SalidaDistriBotSQL
    {
        private string connectionString;
        private SqlConnection connection;


        public SalidaDistriBotSQL()
        {
            /*En CafeS60.DAL - Settings.settings*/
            ConnectionString = Properties.Resource1.CadenaConexion1;
            //@"Data Source=FRANCISCO-PC\SQLEXPRESS;Initial Catalog=catalogo;Integrated Security=True";
            Connection = new SqlConnection(ConnectionString);
        }


        #region insertar un registro
        /// <summary>
        /// Insertar una nueva salida de botelleros reparados en la base de datos
        /// <param name="producto">Tiene la información de la salida a registrar</param>
        /// </summary>
        /// <param name="producto"></param>
        /// <returns>Si 'resultado' es distinto de 0 -> OK, 
        /// sino, Si 'resultado'==0 -> ERROR</returns>
        public int inserta(SalidaDistriBot producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into disBotParteSalida " +
                "values(@Fecha,@Ns,@Destino,@Origen,@Transportista,@Cantidad); ";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            command.Parameters.Add(new SqlParameter("Fecha", producto.Fecha));
            command.Parameters.Add(new SqlParameter("Ns", producto.Ns));
            command.Parameters.Add(new SqlParameter("Destino", producto.Destino));
            command.Parameters.Add(new SqlParameter("Origen", producto.Origen));
            command.Parameters.Add(new SqlParameter("Transportista", producto.Transportista));
            command.Parameters.Add(new SqlParameter("Cantidad", producto.Cantidad));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR SQL: " + ex);
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion


        #region busca registros entre dos fechas
        /// <summary>
        /// Busca todos los registros por tipo: fecha de orden o fecha de trabajo,
        /// entre las fechas inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public List<ParteBotellero> busca(int i, int f, string tipo)
        {
            /*Declaramos una lista de objetos de tipo clsProducto que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<ParteBotellero> lista = new List<ParteBotellero>();
            string query = "select * from disBotParte where f_repa>=@F_inicio and f_repa<=@F_fin";
            if (tipo.Equals("REPARADO"))
            {
                query += " and estado like '%REPARADO%'";
            }
            else if (tipo.Equals("ENTREGADO"))
            {
                query += " and estado like '%ENTREGADO%'";
            }
            else
            {
                query += "";
            }

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("F_inicio", i));
            command.Parameters.Add(new SqlParameter("F_fin", f));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    ParteBotellero producto = new ParteBotellero()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        F_repa = Convert.ToInt32(dr[1]),
                        Mo = Convert.ToString(dr[2]),
                        Origen = Convert.ToString(dr[3]),
                        Ns = Convert.ToString(dr[4]),
                        Fabricante = Convert.ToString(dr[5]),
                        Marca = Convert.ToString(dr[6]),
                        Gas = Convert.ToString(dr[7]),
                        Modelo = Convert.ToString(dr[8]),
                        Estado = Convert.ToString(dr[9]),
                        Img = Convert.ToString(dr[10]),
                        Tecnico = Convert.ToString(dr[11]),
                        Anyo = Convert.ToInt32(dr[12]),
                        F_entrega = Convert.ToInt32(dr[13]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(producto);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion


        #region DESCRIPTORES DE ACCESO
        public string ConnectionString
        { get; set; }

        public SqlConnection Connection
        { get; set; }
        #endregion
    }
}
