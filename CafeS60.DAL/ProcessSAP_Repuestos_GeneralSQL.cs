﻿using CafeS60.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CafeS60.DAL
{
    public class ProcessSAP_Repuestos_GeneralSQL
    {
        private string connectionString;
        private SqlConnection connection;

        public ProcessSAP_Repuestos_GeneralSQL()
        {
            /*En CafeS60.DAL - Settings.settings*/
            ConnectionString = Properties.Resource1.CadenaConexion1;
            //@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=catalogo;Integrated Security=True";
            Connection = new SqlConnection(ConnectionString);
        }

        #region todas las consultas sql
        #region insertar un registro
        /// <summary>
        /// Insertar un nuevo artículo en la base de datos
        /// <param name="producto">Tiene la información del artículo a registrar</param>
        /// </summary>
        /// <param name="producto"></param>
        /// <returns>Si 'resultado' es distinto de 0 -> OK, 
        /// sino, Si 'resultado'==0 -> ERROR</returns>
        public int inserta(SAP_Repuestos_General producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into sap_repuestos_general " +
                "values(@Codesap,@Codeax,@Name,0);";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            command.Parameters.Add(new SqlParameter("Codeax", producto.Code_ax));
            command.Parameters.Add(new SqlParameter("Codesap", producto.Code_sap));
            command.Parameters.Add(new SqlParameter("Name", producto.Name));
            try
            {
                resultado = command.ExecuteNonQuery();
                if (resultado != 0) MessageBox.Show("SQL: GUARDADO OK");
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR SQL: " + ex);
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion

        #region busca si existe un registro por su id
        /// <summary>
        /// Busca todos los registros por tipo: fecha de orden o fecha de trabajo,
        /// entre las fechas inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public int existeId(int Id)
        {
            int resultado = 0;
            string query = "select COUNT(*) from sap_repuestos_general where id=@ID";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("ID", Id));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {

                var r = command.ExecuteScalar();
                if (r != DBNull.Value)
                    resultado = Convert.ToInt32(r);
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return resultado;
        }
        #endregion
        #region busca una orden por su ID
        /// <summary>
        /// Devuelve un Objeto OTaller
        /// </summary>
        public SAP_Repuestos_General obtenerId(int idOrden)
        {
            SAP_Repuestos_General producto = new SAP_Repuestos_General();
            producto = null;
            string query = "select * from sap_repuestos_general where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("ID", idOrden));
            SqlDataAdapter da = new SqlDataAdapter();

            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);

                DataRow dr = ds.Tables[0].Rows[0];
                //Instanciamos al objeto cls_Tarjeta para llenar sus propiedades
                producto = new SAP_Repuestos_General()
                {
                    Id = Convert.ToInt32(dr[0]),
                    Code_sap = Convert.ToString(dr[1]),
                    Code_ax = Convert.ToString(dr[2]),
                    Name = Convert.ToString(dr[3]),
                    Price = (float)Convert.ToDouble(dr[4]),
                };

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                Connection.Close();
            }

            return producto;
        }
        #endregion

        #region busca si existe un registro por su COD SAP
        /// <summary>
        /// Busca todos los registros por tipo: fecha de orden o fecha de trabajo,
        /// entre las fechas inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public int existeSap(string sap)
        {
            int resultado = 0;
            string query = "select COUNT(*) from sap_repuestos_general where codesap=@Sap";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("Sap", sap));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {

                var r = command.ExecuteScalar();
                if (r != DBNull.Value)
                    resultado = Convert.ToInt32(r);
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return resultado;
        }
        #endregion
        #region busca una orden por su COD SAP
        /// <summary>
        /// Devuelve un Objeto OTaller
        /// </summary>
        public SAP_Repuestos_General obtenerSap(string sap)
        {
            SAP_Repuestos_General producto = new SAP_Repuestos_General();
            producto = null;
            string query = "select * from sap_repuestos_general where codesap=@Sap";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("Sap", sap));
            SqlDataAdapter da = new SqlDataAdapter();

            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);

                DataRow dr = ds.Tables[0].Rows[0];
                //Instanciamos al objeto cls_Tarjeta para llenar sus propiedades
                producto = new SAP_Repuestos_General()
                {
                    Id = Convert.ToInt32(dr[0]),
                    Code_sap = Convert.ToString(dr[1]),
                    Code_ax = Convert.ToString(dr[2]),
                    Name = Convert.ToString(dr[3]),
                    Price = (float)Convert.ToDouble(dr[4]),
                };

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                Connection.Close();
            }

            return producto;
        }
        #endregion

        #region busca si existe un registro por su COD SAP
        /// <summary>
        /// Busca todos los registros por tipo: fecha de orden o fecha de trabajo,
        /// entre las fechas inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public int existeAx(string ax)
        {
            int resultado = 0;
            string query = "select COUNT(*) from sap_repuestos_general where codeax=@Ax";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("Ax", ax));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {

                var r = command.ExecuteScalar();
                if (r != DBNull.Value)
                    resultado = Convert.ToInt32(r);
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return resultado;
        }
        #endregion
        #region busca una orden por su COD SAP
        /// <summary>
        /// Devuelve un Objeto OTaller
        /// </summary>
        public SAP_Repuestos_General obtenerAx(string ax)
        {
            SAP_Repuestos_General producto = new SAP_Repuestos_General();
            producto = null;
            string query = "select * from sap_repuestos_general where codeax=@Ax";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("Ax", ax));
            SqlDataAdapter da = new SqlDataAdapter();

            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);

                DataRow dr = ds.Tables[0].Rows[0];
                //Instanciamos al objeto cls_Tarjeta para llenar sus propiedades
                producto = new SAP_Repuestos_General()
                {
                    Id = Convert.ToInt32(dr[0]),
                    Code_sap = Convert.ToString(dr[1]),
                    Code_ax = Convert.ToString(dr[2]),
                    Name = Convert.ToString(dr[3]),
                    Price = (float)Convert.ToDouble(dr[4]),
                };

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                Connection.Close();
            }

            return producto;
        }
        #endregion

        #region listar todos los registros
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<SAP_Repuestos_General> obtenerTodos()
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<SAP_Repuestos_General> lista = new List<SAP_Repuestos_General>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from sap_repuestos_general";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    SAP_Repuestos_General producto = new SAP_Repuestos_General()
                    {
                        Id = Convert.ToInt32(item["id"]),
                        Code_sap = item["codesap"].ToString(),
                        Code_ax = item["codeax"].ToString(),
                        Name = item["name"].ToString(),
                        Price = (float)Convert.ToDouble(item["price"]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion

        #region listar todos los registros con un LIKE NAME
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<SAP_Repuestos_General> obtenerTodosNombre(string nombre)
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<SAP_Repuestos_General> lista = new List<SAP_Repuestos_General>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from sap_repuestos_general " +
                "where name LIKE '%" + nombre + "%'";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    SAP_Repuestos_General producto = new SAP_Repuestos_General()
                    {
                        Id = Convert.ToInt32(item["id"]),
                        Code_sap = item["codesap"].ToString(),
                        Code_ax = item["codeax"].ToString(),
                        Name = item["name"].ToString(),
                        Price = (float)Convert.ToDouble(item["price"]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion

        #region listar todos los registros con un LIKE CODE SAP
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<SAP_Repuestos_General> obtenerTodosSap(string nombre)
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<SAP_Repuestos_General> lista = new List<SAP_Repuestos_General>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from sap_repuestos_general " +
                "where codesap LIKE '%" + nombre + "%'";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    SAP_Repuestos_General producto = new SAP_Repuestos_General()
                    {
                        Id = Convert.ToInt32(item["id"]),
                        Code_sap = item["codesap"].ToString(),
                        Code_ax = item["codeax"].ToString(),
                        Name = item["name"].ToString(),
                        Price = (float)Convert.ToDouble(item["price"]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion

        #region CAMBIA PRECIO DE UN REPUESTO
        public void cambiarPrecio(int idorden, float precio)
        {
            //Declaramos nuestra consulta de Acción Sql
            string query = "update sap_repuestos_general set price=@PRICE where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("ID", idorden));
            command.Parameters.Add(new SqlParameter("PRICE", precio));
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.Close();
            }

        }
        #endregion
        #endregion


        #region DESCRIPTORES DE ACCESO
        public string ConnectionString
        { get; set; }

        public SqlConnection Connection
        { get; set; } 
        #endregion
    }
}
