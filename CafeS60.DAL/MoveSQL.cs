﻿using CafeS60.CORE;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafeS60.DAL
{
    public class MoveSQL
    {
        private string connectionString;
        private SqlConnection connection;

        public MoveSQL()
        {
            /*En CafeS60.DAL - Settings.settings*/
            ConnectionString = Properties.Resource1.CadenaConexion1;
            //@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=catalogo;Integrated Security=True";
            Connection = new SqlConnection(ConnectionString);
        }




        #region insertar una entrada
        /// <summary>
        /// Insertar una nueva entrada en la base de datos
        /// <param name="entrada">Tiene la información de la entrada a registrar</param>
        /// </summary> 
        public int insertaE(int entrada, string proveedor, DateTime fecha, string producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into entrada " +
                "values(@Producto,'entrada',@Fecha,@Proveedor,@Entrada); ";

        /*Abre una conexión*/
        Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            command.Parameters.Add(new SqlParameter("Entrada", entrada));
            command.Parameters.Add(new SqlParameter("Proveedor", proveedor));
            command.Parameters.Add(new SqlParameter("Fecha", fecha));
            command.Parameters.Add(new SqlParameter("Producto", producto));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion

        #region insertar una salida
        /// <summary>
        /// Insertar una nueva salida en la base de datos
        /// <param name="salida">Tiene la información de la salida a registrar</param>
        /// </summary> 
        public int insertaS(int salida, string destino, DateTime fecha, string producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into salida " +
                "values(@Producto,'salida',@Fecha,@Destino,@Salida); ";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            command.Parameters.Add(new SqlParameter("Salida", salida));
            command.Parameters.Add(new SqlParameter("Destino", destino));
            command.Parameters.Add(new SqlParameter("Fecha", fecha));
            command.Parameters.Add(new SqlParameter("Producto", producto));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion




        #region DESCRIPTORES DE ACCESO
        public string ConnectionString
        { get; set; }

        public SqlConnection Connection
        { get; set; }
        #endregion
    }
}
