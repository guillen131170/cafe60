﻿using CafeS60.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CafeS60.DAL
{
    public class VacacionesSQL
    {
        private string connectionString;
        private SqlConnection connection;


        public VacacionesSQL()
        {
            /*En CafeS60.DAL - Settings.settings*/
            ConnectionString = Properties.Resource1.CadenaConexion1;
            //@"Data Source=FRANCISCO-PC\SQLEXPRESS;Initial Catalog=catalogo;Integrated Security=True";
            Connection = new SqlConnection(ConnectionString);
        }


        #region insertar un registro
        /// <summary>
        /// Insertar un movimiento de vacaciones
        /// <param name="producto">Tiene la información de la salida a registrar</param>
        /// </summary>
        /// <param name="producto"></param>
        /// <returns>Si 'resultado' es distinto de 0 -> OK, 
        /// sino, Si 'resultado'==0 -> ERROR</returns>
        public int inserta(Vacaciones producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into vacaciones " +
                "values(@Name,@Ndias_anteriores,@Ndias_actuales,@Ndias_acumulados,@Ndias_cogidos," +
                "@Saldo,@Description,@Fecha,@Ndias_abonados); ";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            command.Parameters.Add(new SqlParameter("Name", producto.Name));
            command.Parameters.Add(new SqlParameter("Ndias_anteriores", producto.Ndias_anteriores));
            command.Parameters.Add(new SqlParameter("Ndias_actuales", producto.Ndias_actuales));
            command.Parameters.Add(new SqlParameter("Ndias_acumulados", producto.Ndias_acumulados));
            command.Parameters.Add(new SqlParameter("Ndias_abonados", producto.Ndias_abonados));
            command.Parameters.Add(new SqlParameter("Ndias_cogidos", producto.Ndias_cogidos));
            command.Parameters.Add(new SqlParameter("Saldo", producto.Saldo));
            command.Parameters.Add(new SqlParameter("Description", producto.Description));
            command.Parameters.Add(new SqlParameter("Fecha", producto.Fecha));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR SQL: " + ex);
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion


        #region busca movimientos por nombre
        /// <summary>
        /// Devuelve una lista de movimientos de vacaciones
        /// </summary>
        public List<Vacaciones> busca(int i, int f, string nombre)
        {
            List<Vacaciones> lista = new List<Vacaciones>();
            string query = "select * from vacaciones where fecha>=@F_inicio and fecha<=@F_fin and name like '%" + nombre + "%';";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("Nombre", nombre));
            command.Parameters.Add(new SqlParameter("F_inicio", i));
            command.Parameters.Add(new SqlParameter("F_fin", f));
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    Vacaciones producto = new Vacaciones()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        Name = Convert.ToString(dr[1]),
                        Ndias_anteriores = Convert.ToInt32(dr[2]),
                        Ndias_actuales = Convert.ToInt32(dr[3]),
                        Ndias_acumulados = Convert.ToInt32(dr[4]),
                        Ndias_cogidos = Convert.ToInt32(dr[5]),
                        Saldo = Convert.ToInt32(dr[6]),
                        Description = Convert.ToString(dr[7]),
                        Fecha = Convert.ToInt32(dr[8]),
                        Ndias_abonados = Convert.ToInt32(dr[9])
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(producto);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion


        #region busca movimientos por nombre
        /// <summary>
        /// Devuelve una lista de movimientos de vacaciones
        /// </summary>
        public List<Vacaciones> busca(string nombre)
        {
            List<Vacaciones> lista = new List<Vacaciones>();
            string query = "select * from vacaciones where name=@Nombre";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("Nombre", nombre));
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    Vacaciones producto = new Vacaciones()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        Name = Convert.ToString(dr[1]),
                        Ndias_anteriores = Convert.ToInt32(dr[2]),
                        Ndias_actuales = Convert.ToInt32(dr[3]),
                        Ndias_acumulados = Convert.ToInt32(dr[4]),
                        Ndias_cogidos = Convert.ToInt32(dr[5]),
                        Saldo = Convert.ToInt32(dr[6]),
                        Description = Convert.ToString(dr[7]),
                        Fecha = Convert.ToInt32(dr[8]),
                        Ndias_abonados = Convert.ToInt32(dr[9])
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(producto);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion


        #region busca último movimiento por nombre
        /// <summary>
        /// Devuelve el último movimiento de vacaciones de un técnico
        /// </summary>
        public Vacaciones buscaPorNombre(string nombre)
        {
            Vacaciones producto = new Vacaciones();
            List<Vacaciones> lista = new List<Vacaciones>();
            string query = "select * from vacaciones where name=@Nombre";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("Nombre", nombre));
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    producto = new Vacaciones()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        Name = Convert.ToString(dr[1]),
                        Ndias_anteriores = Convert.ToInt32(dr[2]),
                        Ndias_actuales = Convert.ToInt32(dr[3]),
                        Ndias_acumulados = Convert.ToInt32(dr[4]),
                        Ndias_cogidos = Convert.ToInt32(dr[5]),
                        Saldo = Convert.ToInt32(dr[6]),
                        Description = Convert.ToString(dr[7]),
                        Fecha = Convert.ToInt32(dr[8]),
                        Ndias_abonados = Convert.ToInt32(dr[9])
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(producto);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return producto;
        }
        #endregion


        #region DESCRIPTORES DE ACCESO
        public string ConnectionString
        { get; set; }

        public SqlConnection Connection
        { get; set; }
        #endregion
    }
}
