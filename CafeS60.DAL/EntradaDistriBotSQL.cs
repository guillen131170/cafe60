﻿using CafeS60.CORE;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CafeS60.DAL
{
    public class EntradaDistriBotSQL
    {
        private string connectionString;
        private SqlConnection connection;


        public EntradaDistriBotSQL()
        {
            /*En CafeS60.DAL - Settings.settings*/
            ConnectionString = Properties.Resource1.CadenaConexion1;
            //@"Data Source=FRANCISCO-PC\SQLEXPRESS;Initial Catalog=catalogo;Integrated Security=True";
            Connection = new SqlConnection(ConnectionString);
        }


        #region insertar un registro
        /// <summary>
        /// Insertar una nueva salida de botelleros reparados en la base de datos
        /// <param name="producto">Tiene la información de la salida a registrar</param>
        /// </summary>
        /// <param name="producto"></param>
        /// <returns>Si 'resultado' es distinto de 0 -> OK, 
        /// sino, Si 'resultado'==0 -> ERROR</returns>
        public int inserta(EntradaDistriBot producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into disBotParteEntrada " +
                "values(@Fecha,@Ns,@Destino,@Origen,@Transportista,@Cantidad); ";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            command.Parameters.Add(new SqlParameter("Fecha", producto.Fecha));
            command.Parameters.Add(new SqlParameter("Ns", producto.Ns));
            command.Parameters.Add(new SqlParameter("Destino", producto.Destino));
            command.Parameters.Add(new SqlParameter("Origen", producto.Origen));
            command.Parameters.Add(new SqlParameter("Transportista", producto.Transportista));
            command.Parameters.Add(new SqlParameter("Cantidad", producto.Cantidad));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR SQL: " + ex);
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion


        #region DESCRIPTORES DE ACCESO
        public string ConnectionString
        { get; set; }

        public SqlConnection Connection
        { get; set; }
        #endregion
    }
}
