﻿using CafeS60.APPLICATION;
using CafeS60.CORE;
using CafeS60.DAL;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CafeS60.INTERFACE
{
    public partial class Main : Form
    {
        public List<Product> listaProductos;
        public int numReg;
        public int pReg;
        public string tipoOrdenTaller = "";

        public List<Move> listaEntradas;
        public int numRegEntradas;
        public int pRegEntradas;

        #region Inicio y carga
        public Main()
        {
            InitializeComponent();
            listaProductos = new List<Product>();
            numReg = 0;
            pReg = 0;
    }

        private void Main_Load(object sender, EventArgs e)
        {
            panelcafe.Appearance = TabAppearance.FlatButtons;
            panelcafe.ItemSize = new Size(0, 1);
            panelcafe.SizeMode = TabSizeMode.Fixed;

            panelcafe.SelectedTab = inicio;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, inicio);
        }
        #endregion



        #region Menú tooltips
        #region Lista de productos catalogados
        private void catalogadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessProduct consulta = new ProcessProduct();           

            panelcafe.SelectedTab = vercafe;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, vercafe);

            listaProductos = consulta.ObtenerRegistrosCatalogados();
            numReg = listaProductos.Count;
            pReg = 0;
            if (numReg > 0)
            {
                rellenarCampos(listaProductos[pReg]);
                indiceregistros.Text = (pReg + 1).ToString() + " de " + numReg.ToString();
            }
            else
            {
                indiceregistros.Text = "0 de 0";
                MessageBox.Show("No hay registros");
            }
        }
        #endregion

        #region Lista de productos descatalogados
        private void descatalogadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessProduct consulta = new ProcessProduct();

            panelcafe.SelectedTab = vercafe;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, vercafe);

            listaProductos = consulta.ObtenerRegistrosDescatalogados();
            numReg = listaProductos.Count;
            pReg = 0;
            if (numReg > 0)
            {
                rellenarCampos(listaProductos[pReg]);
                indiceregistros.Text = (pReg + 1).ToString() + " de " + numReg.ToString();
            }
            else
            {
                indiceregistros.Text = "0 de 0";
                MessageBox.Show("No hay registros");
            }
        }
        #endregion

        #region Lista de todos los productos catalogados y descatalogados
        private void todosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessProduct consulta = new ProcessProduct();

            panelcafe.SelectedTab = vercafe;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, vercafe);

            listaProductos = consulta.ObtenerRegistrosTodos();
            numReg = listaProductos.Count;
            pReg = 0;
            if (numReg > 0)
            {
                rellenarCampos(listaProductos[pReg]);
                indiceregistros.Text = (pReg + 1).ToString() + " de " + numReg.ToString();
            }
            else
            {
                indiceregistros.Text = "0 de 0";
                MessageBox.Show("No hay registros");
            }
        }
        #endregion

        #region Insertar producto nuevo
        private void crearRegistroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = insertarcafe;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, insertarcafe);

            limpiarCampos();
            ProcessProduct consulta = new ProcessProduct();
            id.Text = (consulta.ObtenerRegistrosTodos().Count+1).ToString();
        }
        #endregion

        #region Modificar producto
        private void modificarRegistroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = insertarcafe;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, modificacafe);

            limpiarCamposUpdate();
            ProcessProduct consulta = new ProcessProduct();
            listaProductos = consulta.ObtenerRegistrosTodos();
            numReg = listaProductos.Count;
            pReg = 0;
            if (numReg > 0)
            {
                rellenarCamposUpdate(listaProductos[pReg]);
                indiceregistrosUpdate.Text = (pReg + 1).ToString() + " de " + numReg.ToString();
            }
            else
            {
                indiceregistrosUpdate.Text = "0 de 0";
                MessageBox.Show("No hay registros");
            }
        }
        #endregion

        #region Menú - Entrada - Salida
        private void entradaSalidaToolStripMenuItem_Click(object sender, EventArgs e){}
        private void nuevaEntradaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = entradacafe;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, entradacafe);

            limpiarCamposStock();
            entradabuscador.Text = "";
            ProcessProduct consulta = new ProcessProduct();
            listaProductos = consulta.ObtenerRegistrosTodos();
            numReg = listaProductos.Count;
            pReg = 0;


            if (numReg > 0)
            {
                rellenarCamposStock(listaProductos[pReg]);
                entradaindice.Text = (pReg + 1).ToString() + " de " + numReg.ToString();
            }
            else
            {
                entradaindice.Text = "0 de 0";
                MessageBox.Show("No hay registros");
            }
        }
        private void buscarEntradasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = veres;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, veres);

            limpiarVerEntradas();
            entradabuscador.Text = "";
            ProcessProduct consulta = new ProcessProduct();
            listaEntradas = consulta.ObtenerEntradasTodo();
            numRegEntradas = listaEntradas.Count;
            pRegEntradas = 0;

            if (numRegEntradas > 0)
            {
                rellenarCamposEntradas(listaEntradas[pRegEntradas]);
                verentradaindice.Text = (pRegEntradas + 1).ToString() + " de " + numRegEntradas.ToString();
            }
            else
            {
                verentradaindice.Text = "0 de 0";
                MessageBox.Show("No hay registros");
            }
        }

        private void salidaToolStripMenuItem_Click(object sender, EventArgs e)
        {}
        private void nuevaSalidaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = salidacafe;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, salidacafe);

            limpiarCamposSalida();
            salidabuscador.Text = "";
            ProcessProduct consulta = new ProcessProduct();
            listaProductos = consulta.ObtenerRegistrosTodos();
            numReg = listaProductos.Count;
            pReg = 0;


            if (numReg > 0)
            {
                rellenarCamposSalida(listaProductos[pReg]);
                salidaindice.Text = (pReg + 1).ToString() + " de " + numReg.ToString();
            }
            else
            {
                salidaindice.Text = "0 de 0";
                MessageBox.Show("No hay registros");
            }
        }
        private void buscarSalidasToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        #endregion

        #region DISTRIBUCIÓN BOTELLERO PARTE CREAR
        private void nuevoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = paDisBotParteCrear;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, paDisBotParteCrear);
            ottipo.SelectedIndex = 0;
        }
        #endregion       

        #region DISTRIBUCION BOTELLERO ENTRADA CREAR
        private void crearToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = DisBotEnt;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, DisBotEnt);
        }
        #endregion

        #region DISTRIBUCION BOTELLERO SALIDA CREAR
        private void crearToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = DisBotSal;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, DisBotSal);
        } 
        #endregion

        #region DISTRIBUCION BOTELLERO IMPRIMIR LISTA DE BOTELLEROS
        private void imprimirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListaBotConBorde.Checked = true;
            ListaBotTodo.Checked = true;
            panelcafe.SelectedTab = DisBotLista;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, DisBotLista);
            comboIBotTipo.SelectedIndex = 0;
        } 
        #endregion

        #region SALIR APLICACIÓN
        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
            Application.Exit();
        }
        #endregion

        #region REPUESTOS SAP- BUSCAR REPUESTO SAP
        private void buscarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = tabPage1;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, tabPage1);
            RepCafCodSAPBuscar.Text = "";
            RepCafCodAXBuscar.Text = "";
            RepCafIdBuscar.Text = "";
            RepCafNombreBuscar.Text = "";
            RepCafPrecio.Text = "";
            RepCafRepEncontrado.Text = "";
            comboBusNom.Items.Clear();
            comboBusCod.Items.Clear();
            comboBusPre.Items.Clear();
            comboTipo.SelectedIndex = 0;
        }

        private void buscarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = tabPage1;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, tabPage1);
            RepCafCodSAPBuscar.Text = "";
            RepCafCodAXBuscar.Text = "";
            RepCafIdBuscar.Text = "";
            RepCafNombreBuscar.Text = "";
            RepCafPrecio.Text = "";
            RepCafRepEncontrado.Text = "";
            comboBusNom.Items.Clear();
            comboBusCod.Items.Clear();
            comboBusPre.Items.Clear();
            comboTipo.SelectedIndex = 1;
        }
        #endregion

        #region ORDEN TALLER - CREAR NUEVA ORDEN TALLER
        private void crearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = OTaller;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, OTaller);
            ottipo.SelectedIndex = 0;
            otmo.Text = "39,45";
            panel24.Visible = false;
            button3.Visible = true;
            otallerimprimir.Visible = true;
            groupBox11.Visible = true;
            otproyecto.Text = "";
            otcodproyecto.Text = "";
            otproyecto.Enabled = true;
            otid.Text = "";
            //panel13.Enabled = true;
        }
        #endregion

        #region ORDEN TALLER - INFORME BÁSICO
        private void básicoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = IBOTaller;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, IBOTaller);
            groupBox8.Enabled = true;
            groupBox9.Enabled = true;
            groupBox8.Visible = true;
            groupBox9.Visible = true;
            radioButtonAceptado.Checked = true;
            radioButtonAbierto.Checked = true;
            comboIBOTaller.SelectedIndex = 0;
        }

        private void cerrarAceptadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = IBOTaller;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, IBOTaller);
            groupBox8.Enabled = false;
            groupBox9.Enabled = false;
            groupBox8.Visible = false;
            groupBox9.Visible = false;
            radioButtonAceptado.Checked = true;
            radioButtonAbierto.Checked = true;
            comboIBOTaller.SelectedIndex = 0;
        }

        private void cerrarRechazadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = IBOTaller;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, IBOTaller);
            groupBox8.Enabled = false;
            groupBox9.Enabled = false;
            groupBox8.Visible = false;
            groupBox9.Visible = false;
            radioButtonRechazado.Checked = true;
            radioButtonAbierto.Checked = true;
            comboIBOTaller.SelectedIndex = 0;
        }
        #endregion

        #region ORDEN TALLER - INFORME RESUMENES
        private void resúmenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = IROTaller;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, IROTaller);
            radioIRResumen.Checked = true;
            comboIROTallerTipo.SelectedIndex = 0;
            comboIROTallerTecnico.SelectedIndex = 0;
            comboIROTallerObjeto.SelectedIndex = 1;
            comboIROTallerProyecto.SelectedIndex = 0;
        }
        #endregion

        #region ORDEN TALLER CAFE - BUSCAR ORDEN POR ID
        private void verToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = OTaller;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, OTaller);
            ottipo.SelectedIndex = 0;
            otmo.Text = "39,45";
            button3.Visible = false;
            otallerimprimir.Visible = false;
            panel24.Visible = true;
            groupBox11.Visible = false;
            tipoOrdenTaller = "";
            //panel13.Enabled = false;
        } 
        #endregion

        #region VACACIONES - INFORME
        private void informeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = VacInf;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, VacInf);
            vacInfNombre.SelectedIndex = 0;
        }
        #endregion

        #region ORDEN TALLER HEINEKEN STC - CREAR OREDEN
        private void heinekenStcToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, OTaller);
            ottipo.SelectedIndex = 0;
            otmo.Text = "0";
            panel24.Visible = false;
            button3.Visible = true;
            otallerimprimir.Visible = true;
            groupBox11.Visible = true;
            otproyecto.Text = "HEI STC";
            otcodproyecto.Text = "001";
            otproyecto.Enabled = false;
        } 
        #endregion

        #region ORDEN TALLER HEINEKEN EVENTO - CREAR OREDEN
        private void heinekenEventoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, OTaller);
            ottipo.SelectedIndex = 0;
            otmo.Text = "0";
            panel24.Visible = false;
            button3.Visible = true;
            otallerimprimir.Visible = true;
            groupBox11.Visible = true;
            otproyecto.Text = "HEN EVENTO";
            otcodproyecto.Text = "002";
            otproyecto.Enabled = false;
        } 
        #endregion

        #region ORDEN TALLER BERLY - CREAR OREDEN
        private void berlysToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, OTaller);
            ottipo.SelectedIndex = 0;
            otmo.Text = "0";
            panel24.Visible = false;
            button3.Visible = true;
            otallerimprimir.Visible = true;
            groupBox11.Visible = true;
            otproyecto.Text = "BERLYS";
            otcodproyecto.Text = "273";
            otproyecto.Enabled = false;
        }
        #endregion

        #region ORDEN TALLER DANONE VENDING - CREAR ORDEN
        private void danoneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = OTaller;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, OTaller);
            ottipo.SelectedIndex = 0;
            otmo.Text = "0";
            panel24.Visible = false;
            button3.Visible = true;
            otallerimprimir.Visible = true;
            groupBox11.Visible = true;
            otproyecto.Text = "DANONE VENDING";
            otcodproyecto.Text = "365";
            otproyecto.Enabled = false;
        }
        #endregion

        #region ORDEN TALLER DANONE WATERS - CREAR ORDEN
        private void danoneWaterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = OTaller;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, OTaller);
            ottipo.SelectedIndex = 0;
            otmo.Text = "0";
            panel24.Visible = false;
            button3.Visible = true;
            otallerimprimir.Visible = true;
            groupBox11.Visible = true;
            otproyecto.Text = "DANONE WATERS";
            otcodproyecto.Text = "348";
            otproyecto.Enabled = false;
        } 
        #endregion

        #region ORDEN TALLER IKEA - CREAR OREDEN
        private void ikeaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = OTaller;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, OTaller);
            ottipo.SelectedIndex = 0;
            otmo.Text = "0";
            panel24.Visible = false;
            button3.Visible = true;
            otallerimprimir.Visible = true;
            groupBox11.Visible = true;
            otproyecto.Text = "IKEA";
            otcodproyecto.Text = "351";
            otproyecto.Enabled = false;
        }
        #endregion

        #region NO USADO - ORDEN TALLER CORECO - CREAR ORDEN
        private void corecoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            return;
        }
        #endregion

        #region ORDEN TALLER GARANTIA CORECO - CREAR ORDEN
        private void corecoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = OTaller;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, OTaller);
            ottipo.SelectedIndex = 0;
            otmo.Text = "0";
            panel24.Visible = false;
            button3.Visible = true;
            otallerimprimir.Visible = true;
            groupBox11.Visible = true;
            otproyecto.Text = "CORECO";
            otcodproyecto.Text = "035";
            otproyecto.Enabled = false;
        }
        #endregion

        #region ORDEN TALLER GARANTIA SCHWEPPES - CREAR ORDEN
        private void plazaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = OTaller;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, OTaller);
            ottipo.SelectedIndex = 0;
            otmo.Text = "0";
            panel24.Visible = false;
            button3.Visible = true;
            otallerimprimir.Visible = true;
            groupBox11.Visible = true;
            otproyecto.Text = "SCHWEPPES";
            otcodproyecto.Text = "004";
            otproyecto.Enabled = false;
        } 
        #endregion

        #region ORDEN TALLER GENERAL - BUSCAR ORDEN POR ID
        private void buscarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = OTaller;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, OTaller);
            ottipo.SelectedIndex = 0;
            otmo.Text = "0";
            button3.Visible = false;
            otallerimprimir.Visible = false;
            panel24.Visible = true;
            groupBox11.Visible = false;
            tipoOrdenTaller = "GENERAL";
        }
        #endregion

        #region ORDEN TALLER GENERAL - INFORME RESUMENES
        private void resumenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = IROTaller;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, IROTaller);
            radioIRResumen.Checked = true;
            comboIROTallerTipo.SelectedIndex = 0;
            comboIROTallerTecnico.SelectedIndex = 0;
            comboIROTallerObjeto.SelectedIndex = 1;
            comboIROTallerProyecto.SelectedIndex = 0;
        } 
        #endregion

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = VacCrear;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, VacCrear);
            vacCreNombre.SelectedIndex = 0;
            vacCreDAnteriores.Text = "0";
            vacCreDActuales.Text = "0";
            vacCreDCogidos.Text = "0";
            vacCreDDados.Text = "0";
            vacCreFecha.Text = "";
            vacCreGuardar.Enabled = true;
            vacCreGuardar.Visible = true;
            groupBox7.Enabled = false;
            vacCreDescripcion.Enabled = false;
            vacCreFecha.Enabled = false;
            crearToolStripMenuItem3Busca();
        }

        #region VACACIONES - CREAR MOVIMIENTO
        private void crearToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            {
                panelcafe.SelectedTab = VacCrear;
                foreach (TabPage item in panelcafe.TabPages)
                {
                    item.Parent = null;
                }
                panelcafe.TabPages.Insert(0, VacCrear);
                vacCreNombre.SelectedIndex = 0;
                vacCreDAnteriores.Text = "0";
                vacCreDActuales.Text = "0";
                vacCreDCogidos.Text = "0";
                vacCreDDados.Text = "0";
                vacCreFecha.Text = "";
                vacCreGuardar.Enabled = true;
                vacCreGuardar.Visible = true;
                groupBox7.Enabled = true;
                vacCreDescripcion.Enabled = true;
                vacCreFecha.Enabled = true;
                crearToolStripMenuItem3Busca();
            }
        }
        public void crearToolStripMenuItem3Busca()
        {
            Vacaciones p = new Vacaciones();
            ProcessVacaciones proceso = new ProcessVacaciones();
            p = proceso.BuscarRegistroPorNombre(vacCreNombre.SelectedItem.ToString());

            if (p != null)
            {
                textInicio.Text = p.Saldo.ToString();
                vacCreDAnteriores.Text = p.Ndias_anteriores.ToString();
                vacCreDActuales.Text = p.Ndias_actuales.ToString();
                vacCreDAcumulados.Text = p.Ndias_acumulados.ToString();
                vacCreDSaldo.Text = p.Saldo.ToString();
                if (groupBox7.Enabled == false)
                {
                    vacCreFecha.Text = p.Fecha.ToString();
                    vacCreGuardar.Enabled = false;
                    vacCreGuardar.Visible = false;
                }
                else
                {
                    vacCreFecha.Text = "";
                    vacCreGuardar.Enabled = true;
                    vacCreGuardar.Visible = true;
                }
            }
            else
            {
                textInicio.Text = "0";
                vacCreDAnteriores.Text = "0";
                vacCreDActuales.Text = "0";
                vacCreDAcumulados.Text = "0";
                vacCreDSaldo.Text = "0";
            }
        }
        #endregion

        #region ALMACEN - EQUIPOS SERIADOS - CAFE - CAFENTO - ENTRADA
        private void crearEquipoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = AlmEqSe;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, AlmEqSe);
            eqTitulo.Text = "Entradas EQUIPO CAFENTO";
            panel27.Visible = false;
            BotAlEqSeInforme.Visible = false;
            BotAlEqSeModificar.Visible = false;
            BotAlEqSe.Visible = true;
        }
        #endregion

        #region ALMACEN - EQUIPOS SERIADOS - CAFE - CAFENTO - BUSCAR
        private void verEquipoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = AlmEqSe;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, AlmEqSe);
            eqTitulo.Text = "Entradas EQUIPO CAFENTO";
            panel27.Visible = true;
            BotAlEqSeInforme.Visible = false;
            BotAlEqSeModificar.Visible = false;
            BotAlEqSe.Visible = false;
        }
        #endregion

        #region ALMACEN - EQUIPOS SERIADOS - CAFE - CAFENTO - MODIFICAR
        private void modificarEquipoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = AlmEqSe;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, AlmEqSe);
            eqTitulo.Text = "Entradas EQUIPO CAFENTO";
            panel27.Visible = true;
            BotAlEqSeInforme.Visible = false;
            BotAlEqSeModificar.Visible = true;
            BotAlEqSe.Visible = false;
        }
        #endregion

        #region ALMACEN - EQUIPOS SERIADOS - CAFE - CAFENTO - IMPRIMIR
        private void informeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            panelcafe.SelectedTab = AlmEqSe;
            foreach (TabPage item in panelcafe.TabPages)
            {
                item.Parent = null;
            }
            panelcafe.TabPages.Insert(0, AlmEqSe);
            eqTitulo.Text = "Entradas EQUIPO CAFENTO";
            panel27.Visible = true;
            BotAlEqSeInforme.Visible = true;
            BotAlEqSeModificar.Visible = false;
            BotAlEqSe.Visible = false;
        } 
        #endregion

        #region ¿?
        private void salidacafe_Click(object sender, EventArgs e) { }  
        private void cafentoToolStripMenuItem_Click(object sender, EventArgs e) { }
        #endregion
        #endregion


        #region Teclado
        #region Código
        private void code_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((name.TextLength == 0) && (e.KeyChar == (char)Keys.Space))
            {
                MessageBox.Show("No se permiten espacios al inicio", "Advertencia",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
                return;
            }

            if (!(char.IsLetter(e.KeyChar)) &&
                !(char.IsDigit(e.KeyChar)) &&
                 (e.KeyChar != (char)Keys.Back) &&
                 (e.KeyChar != (char)Keys.Space))
            {
                MessageBox.Show("Sólo letras y números", "Advertencia",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
                return;
            }
        }
        #endregion

        #region Nombre
        private void name_KeyPress(object sender, KeyPressEventArgs e)
        {
            /*if ((name.TextLength == 0) && (e.KeyChar == (char)Keys.Space))
            {
                MessageBox.Show("No se permiten espacios al inicio", "Advertencia",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
                return;
            }

            if (!(char.IsLetter(e.KeyChar)) &&
                !(char.IsDigit(e.KeyChar)) &&
                 (e.KeyChar != (char)Keys.Back) &&
                 (e.KeyChar != (char)Keys.Space))
            {
                MessageBox.Show("Sólo letras y números", "Advertencia",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
                return;
            }*/
        }
        #endregion

        #region Stock
        private void stock_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((name.TextLength == 0) && (e.KeyChar == (char)Keys.Space))
            {
                MessageBox.Show("No se permiten espacios al inicio", "Advertencia",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
                return;
            }

            if (!(char.IsDigit(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Sólo números", "Advertencia",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
                return;
            }
        }
        #endregion

        #region stockentrada
        private void stockentrada_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((stockentrada.TextLength == 0) && (e.KeyChar == (char)Keys.Space))
            {
                MessageBox.Show("No se permiten espacios al inicio", "Advertencia",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
                return;
            }

            if (!(char.IsDigit(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Sólo números", "Advertencia",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
                return;
            }
        }
        #endregion

        #region stocksalida
        private void stocksalida_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if ((stocksalida.TextLength == 0) && (e.KeyChar == (char)Keys.Space))
            {
                MessageBox.Show("No se permiten espacios al inicio", "Advertencia",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
                return;
            }

            if (!(char.IsDigit(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Sólo números", "Advertencia",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
                return;
            }
        } 
        #endregion

        #region Precio
        private void price_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((name.TextLength == 0) && (e.KeyChar == (char)Keys.Space))
            {
                MessageBox.Show("No se permiten espacios al inicio", "Advertencia",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
                return;
            }

            if (!(char.IsDigit(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && !(e.KeyChar != '.') && !(e.KeyChar != ','))
            {
                MessageBox.Show("Sólo números", "Advertencia",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
                return;
            }
        }
        #endregion 

        private void pricedecimal_KeyPress(object sender, KeyPressEventArgs e)
        {
        }

        private void textcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((textcode.TextLength == 0) && (e.KeyChar == (char)Keys.Space))
            {
                MessageBox.Show("No se permiten espacios al inicio", "Advertencia",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
                return;
            }

            if (!(char.IsLetter(e.KeyChar)) &&
                !(char.IsDigit(e.KeyChar)) &&
                 (e.KeyChar != (char)Keys.Back) &&
                 (e.KeyChar != (char)Keys.Space))
            {
                MessageBox.Show("Sólo letras y números", "Advertencia",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
                return;
            }
        }

        private void textname_KeyPress(object sender, KeyPressEventArgs e)
        {/*
            if ((textname.TextLength == 0) && (e.KeyChar == (char)Keys.Space))
            {
                MessageBox.Show("No se permiten espacios al inicio", "Advertencia",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
                return;
            }

            if (!(char.IsLetter(e.KeyChar)) &&
                !(char.IsDigit(e.KeyChar)) &&
                 (e.KeyChar != (char)Keys.Back) &&
                 (e.KeyChar != (char)Keys.Space))
            {
                MessageBox.Show("Sólo letras y números", "Advertencia",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
                return;
            }*/
        }

        private void textprice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((textprice.TextLength == 0) && (e.KeyChar == (char)Keys.Space))
            {
                MessageBox.Show("No se permiten espacios al inicio", "Advertencia",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
                return;
            }

            if (!(char.IsDigit(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Sólo números", "Advertencia",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
                return;
            }
        }

        private void textpricedecimal_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void textstock_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((textstock.TextLength == 0) && (e.KeyChar == (char)Keys.Space))
            {
                MessageBox.Show("No se permiten espacios al inicio", "Advertencia",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
                return;
            }

            if (!(char.IsDigit(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Sólo números", "Advertencia",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
                return;
            }
        }
        #endregion



        #region validar datos
        /*********************************************/
        /********      VALIDAR DATOS          ********/
        /*********************************************/
        public bool validarDatos()
        {
            bool valor = true;
            string error = "";

            ProcessProduct consulta = new ProcessProduct();

            if (!consulta.comprobarDuplicados(code.Text, name.Text))
            {
                valor = false;
                error += "Código o nombre del producto duplicado, .\n";
            }

            if (code.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el código del producto.\n";
            }

            if (name.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el nombre del producto.\n";
            }

            if (price.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el precio del producto.\n";
            }

            if (stock.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el stock del producto.\n";
            }

            if (reference.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el nombre del equipo.\n";
            }

            if (!valor)
            {
                MessageBox.Show(error, "Revise los datos.",
                   MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }

            return valor;
        }

        public bool validarDatosUpdate()
        {
            bool valor = true;
            string error = "";

            ProcessProduct consulta = new ProcessProduct();

            if (!consulta.comprobarDuplicados(textcode.Text, textname.Text))
            {
                valor = false;
                error += "Código o nombre del producto duplicado, .\n";
            }

            if (textcode.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el código del producto.\n";
            }

            if (textname.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el nombre del producto.\n";
            }

            if (textprice.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el precio del producto.\n";
            }

            if (textstock.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el stock del producto.\n";
            }

            if (textreference.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el nombre del equipo.\n";
            }

            if (!valor)
            {
                MessageBox.Show(error, "Revise los datos.",
                   MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }

            return valor;
        }

        public bool validarDatosStock()
        {
            bool valor = true;
            string error = "";

            ProcessProduct consulta = new ProcessProduct();

            if (stockentrada.Text.Equals("") || (Convert.ToInt32(stockentrada.Text) <= 0))
            {
                valor = false;
                error += "Revisa las unidades de entrada.\n";
            }

            if (proveedorentrada.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el nombre del proveedor.\n";
            }

            if (!valor)
            {
                MessageBox.Show(error, "Revise los datos!",
                   MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }

            return valor;
        }

        public bool validarDatosSalida()
        {
            bool valor = true;
            string error = "";

            ProcessProduct consulta = new ProcessProduct();
            /*
            if (salidacode.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el código del producto.\n";
            }

            if (salidaname.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el nombre del producto.\n";
            }
            */
            if (stocksalida.Text.Equals("") || (Convert.ToInt32(stocksalida.Text) <= 0))
            {
                valor = false;
                error += "Revisa las unidades de salida.\n";
            }

            if (destinosalida.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el nombre del proveedor.\n";
            }

            if (!valor)
            {
                MessageBox.Show(error, "Revise los datos!",
                   MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }

            return valor;
        }

        public bool validarDatosDisBotParte()
        {
            bool valor = true;
            string error = "";

            ProcessParteBot consulta = new ProcessParteBot();

            if (botmo.Text.Equals(""))
            {
                valor = false;
                error += "Revisa los repuestos del botellero.\n";
            }

            if (botorigen.Text.Equals(""))
            {
                valor = false;
                error += "Revisa la procedencia del botellero.\n";
            }

            if (botns.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el ns  del botellero.\n";
            }

            if (botfabricante.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el fabricante del botellero.\n";
            }

            if (botmodelo.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el modelo del botellero.\n";
            }

            if (botecnico.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el tecnico que hizo la reparación del botellero.\n";
            }
            MessageBox.Show(error);
            return valor;
        }

        public bool validarDatosOTaller()
        {
            bool valor = true;
            string error = "";

            ProcessOTaller consulta = new ProcessOTaller();

            if (otproyecto.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el proyecto.\n";
            }

            if (otf2.Text.Equals(""))
            {
                valor = false;
                error += "Revisa la fecha de creación de orden.\n";
            }

            if (otmaterial1.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el material.\n";
            }

            if (otmaterial2.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el código de material.\n";
            }

            if (otorden.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el número de orden.\n";
            }

            if (otoferta.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el precio de oferta.\n";
            }

            if (otoferta.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el repuesto y mano de obra.\n";
            }
            MessageBox.Show(error);
            return valor;
        }

        public bool validarDatosDisBotSalida()
        {
            bool valor = true;
            string error = "";

            ProcessOTaller consulta = new ProcessOTaller();

            if (salbotcantidad.Text.Equals(""))
            {
                valor = false;
                error += "Revisa la cantidad.\n";
            }

            if (salbotdestino.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el destino.\n";
            }

            if (salbotfecha.Text.Equals(""))
            {
                valor = false;
                error += "Revisa la fecha.\n";
            }

            if (salbotorigen.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el origen.\n";
            }

            if (salbotns.Text.Equals(""))
            {
                valor = false;
                error += "Revisa los números de serie.\n";
            }

            MessageBox.Show(error);
            return valor;
        }

        public bool validarDatosDisBotEntrada()
        {
            bool valor = true;
            string error = "";

            ProcessOTaller consulta = new ProcessOTaller();

            if (entbotcantidad.Text.Equals(""))
            {
                valor = false;
                error += "Revisa la cantidad.\n";
            }

            if (entbotdestino.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el destino.\n";
            }

            if (entbotfecha.Text.Equals(""))
            {
                valor = false;
                error += "Revisa la fecha.\n";
            }

            if (entbotorigen.Text.Equals(""))
            {
                valor = false;
                error += "Revisa el origen.\n";
            }

            if (entbotns.Text.Equals(""))
            {
                valor = false;
                error += "Revisa los números de serie.\n";
            }

            MessageBox.Show(error);
            return valor;
        }

        public bool validarDatosVacacionesCrear()
        {
            bool valor = true;
            string error = "";

            if (vacCreNombre.Text.Equals(""))
            {
                valor = false;
                error += "Revisa nombre.\n";
            }

            if (vacCreDAnteriores.Text.Equals(""))
            {
                valor = false;
                error += "Revisa dias anterioes.\n";
            }

            if (vacCreDActuales.Text.Equals(""))
            {
                valor = false;
                error += "Revisa dias actuales.\n";
            }

            if (vacCreDAcumulados.Text.Equals(""))
            {
                valor = false;
                error += "Revisa dias acumulados.\n";
            }

            if (vacCreDCogidos.Text.Equals(""))
            {
                valor = false;
                error += "Revisa dias cogidos.\n";
            }

            if (vacCreDSaldo.Text.Equals(""))
            {
                valor = false;
                error += "Revisa saldo.\n";
            }

            if (vacCreDescripcion.Text.Equals(""))
            {
                valor = false;
                error += "Revisa descripción.\n";
            }

            if (vacCreFecha.Text.Equals(""))
            {
                valor = false;
                error += "Revisa fecha.\n";
            }
            /*
            if (Convert.ToInt32(vacCreDCogidos.Text) > Convert.ToInt32(vacCreDAcumulados.Text))
            {
                valor = false; 
                error += "Revisa dias cogidos.\n";
            }
            */
            MessageBox.Show(error);
            return valor;
        }

        public bool validarDatosAlEqSeCa()
        {
            bool valor = true;
            string error = "";

            if (eqcafentnombre.Text.Equals(""))
            {
                valor = false;
                error += "Nombre de mterial\n";
            }

            if (eqcafentmaterial.Text.Equals(""))
            {
                valor = false;
                error += "Código de material\n";
            }

            if (eqcafentsap.Text.Equals(""))
            {
                valor = false;
                error += "Nº de serie sap\n";
            }

            if (eqcafentax.Text.Equals(""))
            {
                valor = false;
                error += "Nº de serie ax\n";
            }

            if (eqcafentstock.Text.Equals(""))
            {
                valor = false;
                error += "Stock\n";
            }

            if (!valor)
            {
                MessageBox.Show(error, "Revise los datos.",
                   MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }

            return valor;
        }
        #endregion



        #region limpiar campos
        /*********************************************/
        /********      LIMPIAR CAMPOS         ********/
        /*********************************************/
        public void limpiarCampos()
        {
            id.Text = "";
            code.Text = "";
            name.Text = "";
            description.Text = "";
            price.Text = "0";
            stock.Text = "0";
            reference.Text = "";
            indiceregistros.Text = "0 de 0";
        }

        public void limpiarCamposUpdate()
        {
            textid.Text = "";
            textcode.Text = "";
            textname.Text = "";
            textdescription.Text = "";
            textprice.Text = "0";
            textstock.Text = "0";
            textreference.Text = "";
            indiceregistrosUpdate.Text = "0 de 0";
        }

        public void limpiarCamposStock()
        {
            entradaid.Text = "";
            entradacode.Text = "";
            entradaname.Text = "";
            entradastock.Text = "0";
            stockentrada.Text = "";
            //entradabuscador.Text = "";
            entradaindice.Text = "0 de 0";
        }

        public void limpiarCamposSalida()
        {
            salidaid.Text = "";
            salidacode.Text = "";
            salidaname.Text = "";
            salidastock.Text = "0";
            stocksalida.Text = "";
            salidaindice.Text = "0 de 0";
        }

        public void limpiarVerEntradas()
        {
            verentradaid.Text = "";
            verentradatipo.Text = "";
            verentradaproducto.Text = "";
            verentradaunidades.Text = "0";
            verentradaproveedor.Text = "";
            //entradabuscador.Text = "";
            //verentradaindice.Text = "0 de 0";
        }
        #endregion



        /*********************************************/
        /********    OBTENER LOS PRODUCTOS    ********/
        /*********************************************/
        public List<Product> obtenerProductos()
        {
            ProcessProduct consulta = new ProcessProduct();
            return consulta.ObtenerRegistrosCatalogados();
        }


        #region rellenar campos
        /*********************************************/
        /********      RELLENAR CAMPOS         ********/
        /*********************************************/
        public void rellenarCampos(Product p)
        {
            textBoxid.Text = p.Id.ToString();
            textBoxcode.Text = p.Code;
            textBoxname.Text = p.Name;
            textBoxdescription.Text = p.Description;
            textBoxprice.Text = p.Price.ToString();
            textBoxstock.Text = p.Stock.ToString();
            textBoxfamily.Text = p.Family;
            textBoxmachine.Text = p.Reference;
            textBoximg.Text = p.Img;
        }

        public void rellenarCamposUpdate(Product p)
        {
            textid.Text = p.Id.ToString();
            textcode.Text = p.Code;
            textname.Text = p.Name;
            textdescription.Text = p.Description;
            textBoxprice.Text = p.Price.ToString();
            textstock.Text = p.Stock.ToString(); ;
            textcafe.Text = p.Family;
            textreference.Text = p.Reference;
            textimg.Text = p.Img;
        }

        public void rellenarCamposStock(Product p)
        {
            entradaid.Text = p.Id.ToString();
            entradacode.Text = p.Code;
            entradaname.Text = p.Name;
            entradastock.Text = p.Stock.ToString(); ;
        }

        public void rellenarCamposSalida(Product p)
        {
            salidaid.Text = p.Id.ToString();
            salidacode.Text = p.Code;
            salidaname.Text = p.Name;
            salidastock.Text = p.Stock.ToString(); ;
        }

        public void rellenarCamposEntradas(Move p)
        {
            verentradaid.Text = p.Id.ToString();
            verentradatipo.Text = p.Type;
            verentradaproducto.Text = p.Product;
            verentradaunidades.Text = p.Units.ToString();
            verentradaproveedor.Text = p.Supplier;
            dateentrada.Value = p.Date;
        }
        #endregion


        #region CAFE
        #region ANTERIOR - SIGUIENTE VER CATÁLOGO
        private void siguiente_Click(object sender, EventArgs e)
        {
            if ((listaProductos.Count > 0) && (numReg > 0))
            {
                if (pReg < numReg - 1)
                {
                    pReg++;
                    rellenarCampos(listaProductos[pReg]);
                    indiceregistros.Text = (pReg + 1).ToString() + " de " + numReg.ToString();
                }
                else
                {
                    MessageBox.Show("No hay más registros");
                }
            }
            else
            {
                MessageBox.Show("No hay registros");
            }
        }

        private void anterior_Click(object sender, EventArgs e)
        {
            if ((listaProductos.Count > 0) && (numReg > 0))
            {
                if (pReg > 0)
                {
                    pReg--;
                    rellenarCampos(listaProductos[pReg]);
                    indiceregistros.Text = (pReg + 1).ToString() + " de " + numReg.ToString();
                }
                else
                {
                    MessageBox.Show("No hay más registros");
                }
            }
            else
            {
                MessageBox.Show("No hay registros");
            }
        }
        #endregion

        #region UPDATE REGISTER
        private void modificar_Click(object sender, EventArgs e)
        {
            //Si los datos de los campos para el registro están correctos

            if (validarDatosUpdate())
            {
                /*
                DialogResult result = new DialogResult();
                Form mensaje = new MessageBoxVehiculosRegistrar(textBox1.Text.ToString());
                result = mensaje.ShowDialog();
                */

                //if (result == DialogResult.OK)
                {
                    Product p = new Product();
                    p.Code = textcode.Text;
                    p.Name = textname.Text;
                    p.Description = textdescription.Text;
                    p.Price = (float)Convert.ToDouble(textprice.Text);
                    p.Stock = Convert.ToInt32(textstock.Text);
                    p.Family = textcafe.Text;
                    p.Reference = textreference.Text;
                    p.Img = textimg.Text;

                    ProcessProduct proceso = new ProcessProduct();
                    if (!proceso.modificarRegistrosTodos(Convert.ToInt32(textid.Text), p))
                    {
                        MessageBox.Show("¡Error al guardar los datos!\n", "Advertencia",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        limpiarCamposUpdate();
                        MessageBox.Show("¡Cambio realizado correcto!\n", "Información",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }
        #endregion

        #region INSERT REGISTER
        private void guardarcafe_Click(object sender, EventArgs e)
        {
            //Si los datos de los campos para el registro están correctos

            if (validarDatos())
            {
                /*
                DialogResult result = new DialogResult();
                Form mensaje = new MessageBoxVehiculosRegistrar(textBox1.Text.ToString());
                result = mensaje.ShowDialog();
                */

                //if (result == DialogResult.OK)
                {
                    Product p = new Product();
                    p.Code = code.Text;
                    p.Name = name.Text;
                    p.Description = description.Text;
                    p.Price = (float)Convert.ToDouble(price.Text);
                    p.Stock = Convert.ToInt32(stock.Text);
                    p.Family = family.Text;
                    p.Reference = reference.Text;
                    p.Img = img.Text;

                    ProcessProduct proceso = new ProcessProduct();
                    if (!proceso.insertarRegistrosTodos(p))
                    {
                        MessageBox.Show("¡Error al guardar los datos!\n", "Advertencia",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        limpiarCampos();
                        MessageBox.Show("¡Guardado correcto!\n", "Información",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }
        #endregion

        #region UPDATE INPUT-OUTPUT
        private void stockguardar_Click(object sender, EventArgs e)
        {
            //Si los datos de los campos para el registro están correctos

            if (validarDatosStock())
            {
                string _code = entradacode.Text;
                string _name = entradaname.Text;
                int _stock = Convert.ToInt32(entradastock.Text);
                int _id = Convert.ToInt32(entradaid.Text);
                int entrada = Convert.ToInt32(stockentrada.Text);
                string proveedor = proveedorentrada.Text;
                DateTime fecha = Convert.ToDateTime(dateentrada.Text);
                string producto = entradaname.Text;

                ProcessProduct proceso1 = new ProcessProduct();
                if (!proceso1.stockRegistrosTodos(_id, _code, _name, _stock))
                {
                    MessageBox.Show("¡Error al guardar los datos!\n", "Advertencia",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    ProcessMove proceso2 = new ProcessMove();
                    if (!proceso2.insertarEntrada(entrada, proveedor, fecha, producto))
                    {
                        MessageBox.Show("¡Error al guardar los datos!\n", "Advertencia",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        //limpiarCamposStock();
                        ProcessProduct consulta = new ProcessProduct();
                        listaProductos = consulta.ObtenerRegistrosTodos();
                        stockentrada.Text = "";
                        proveedorentrada.Text = "";
                        MessageBox.Show("¡Cambio realizado correcto!\n", "Información",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void salidaguardar_Click(object sender, EventArgs e)
        {
            if (validarDatosSalida())
            {
                string _code = salidacode.Text;
                string _name = salidaname.Text;
                int _stock = Convert.ToInt32(salidastock.Text);
                int _id = Convert.ToInt32(salidaid.Text);
                int salida = Convert.ToInt32(stocksalida.Text);
                string destino = destinosalida.Text;
                DateTime fecha = Convert.ToDateTime(datesalida.Text);
                string producto = salidaname.Text;

                ProcessProduct proceso1 = new ProcessProduct();
                if (!proceso1.stockRegistrosTodos(_id, _code, _name, _stock))
                {
                    MessageBox.Show("¡Error al guardar los datos!\n", "Advertencia",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    ProcessMove proceso2 = new ProcessMove();
                    if (!proceso2.insertarSalida(salida, destino, fecha, producto))
                    {
                        MessageBox.Show("¡Error al guardar los datos!\n", "Advertencia",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        //limpiarCamposSalida();
                        ProcessProduct consulta = new ProcessProduct();
                        listaProductos = consulta.ObtenerRegistrosTodos();
                        stocksalida.Text = "";
                        destinosalida.Text = "";
                        MessageBox.Show("¡Cambio realizado correcto!\n", "Información",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }
        #endregion

        #region buscador modificar-entrada-salida
        private void buscador_TextChanged(object sender, EventArgs e)
        {
            limpiarCamposUpdate();
            ProcessProduct consulta = new ProcessProduct();
            listaProductos = consulta.ObtenerRegistrosTodosFiltrado(buscador.Text);
            numReg = listaProductos.Count;

            pReg = 0;
            if (numReg > 0)
            {
                rellenarCamposUpdate(listaProductos[pReg]);
                indiceregistrosUpdate.Text = (pReg + 1).ToString() + " de " + numReg.ToString();
            }
            else
            {
                indiceregistrosUpdate.Text = "0 de 0";
                MessageBox.Show("No hay registros");
            }
        }

        private void stockbuscador_TextChanged(object sender, EventArgs e)
        {
            limpiarCamposStock();
            ProcessProduct consulta = new ProcessProduct();
            listaProductos = consulta.ObtenerRegistrosTodosStock(entradabuscador.Text);
            numReg = listaProductos.Count;

            pReg = 0;
            if (numReg > 0)
            {
                rellenarCamposStock(listaProductos[pReg]);
                entradaindice.Text = (pReg + 1).ToString() + " de " + numReg.ToString();
            }
            else
            {
                entradaindice.Text = "0 de 0";
                MessageBox.Show("No hay registros");
            }
        }

        private void salidabuscador_TextChanged(object sender, EventArgs e)
        {
            limpiarCamposSalida();
            ProcessProduct consulta = new ProcessProduct();
            listaProductos = consulta.ObtenerRegistrosTodosStock(salidabuscador.Text);
            numReg = listaProductos.Count;

            pReg = 0;
            if (numReg > 0)
            {
                rellenarCamposSalida(listaProductos[pReg]);
                salidaindice.Text = (pReg + 1).ToString() + " de " + numReg.ToString();
            }
            else
            {
                salidaindice.Text = "0 de 0";
                MessageBox.Show("No hay registros");
            }
        }
        #endregion

        #region ANTERIOR - SIGUIENTE UPDATE - MODIFICACIÓN
        private void anteriorUpdate_Click(object sender, EventArgs e)
        {
            if ((listaProductos.Count > 0) && (numReg > 0))
            {
                if (pReg > 0)
                {
                    pReg--;
                    rellenarCamposUpdate(listaProductos[pReg]);
                    indiceregistrosUpdate.Text = (pReg + 1).ToString() + " de " + numReg.ToString();
                }
                else
                {
                    MessageBox.Show("No hay más registros");
                }
            }
            else
            {
                MessageBox.Show("No hay registros");
            }
        }

        private void siguienteUpdate_Click(object sender, EventArgs e)
        {
            if ((listaProductos.Count > 0) && (numReg > 0))
            {
                if (pReg < numReg - 1)
                {
                    pReg++;
                    rellenarCamposUpdate(listaProductos[pReg]);
                    indiceregistrosUpdate.Text = (pReg + 1).ToString() + " de " + numReg.ToString();
                }
                else
                {
                    MessageBox.Show("No hay más registros");
                }
            }
            else
            {
                MessageBox.Show("No hay registros");
            }
        }
        #endregion

        #region ANTERIOR - SIGUIENTE STOCK - ENTRADA/SALIDA
        private void stockanterior_Click(object sender, EventArgs e)
        {
            if ((listaProductos.Count > 0) && (numReg > 0))
            {
                if (pReg > 0)
                {
                    pReg--;
                    rellenarCamposStock(listaProductos[pReg]);
                    entradaindice.Text = (pReg + 1).ToString() + " de " + numReg.ToString();
                    //entradabuscador.Text = "";
                    stockentrada.Text = "";
                }
                else
                {
                    MessageBox.Show("No hay más registros");
                }
            }
            else
            {
                MessageBox.Show("No hay registros");
            }
        }

        private void stocksiguiente_Click(object sender, EventArgs e)
        {
            if ((listaProductos.Count > 0) && (numReg > 0))
            {
                if (pReg < numReg - 1)
                {
                    pReg++;
                    rellenarCamposStock(listaProductos[pReg]);
                    entradaindice.Text = (pReg + 1).ToString() + " de " + numReg.ToString();
                    //entradabuscador.Text = "";
                    stockentrada.Text = "";
                }
                else
                {
                    MessageBox.Show("No hay más registros");
                }
            }
            else
            {
                MessageBox.Show("No hay registros");
            }
        }

        private void salidaanterior_Click(object sender, EventArgs e)
        {
            if ((listaProductos.Count > 0) && (numReg > 0))
            {
                if (pReg > 0)
                {
                    pReg--;
                    rellenarCamposSalida(listaProductos[pReg]);
                    salidaindice.Text = (pReg + 1).ToString() + " de " + numReg.ToString();
                    //entradabuscador.Text = "";
                    stocksalida.Text = "";
                }
                else
                {
                    MessageBox.Show("No hay más registros");
                }
            }
            else
            {
                MessageBox.Show("No hay registros");
            }
        }

        private void salidasiguiente_Click(object sender, EventArgs e)
        {
            if ((listaProductos.Count > 0) && (numReg > 0))
            {
                if (pReg < numReg - 1)
                {
                    pReg++;
                    rellenarCamposSalida(listaProductos[pReg]);
                    salidaindice.Text = (pReg + 1).ToString() + " de " + numReg.ToString();
                    //entradabuscador.Text = "";
                    stocksalida.Text = "";
                }
                else
                {
                    MessageBox.Show("No hay más registros");
                }
            }
            else
            {
                MessageBox.Show("No hay registros");
            }
        }
        #endregion

        #region botones de entrada y salida material
        private void stockentrada_TextChanged(object sender, EventArgs e)
        {
            if (listaProductos.Count > 0)
            {
                if (!stockentrada.Text.Equals(""))
                {
                    entradastock.Text = (listaProductos[pReg].Stock + Convert.ToInt32(stockentrada.Text)).ToString();
                }
                else
                {
                    entradastock.Text = listaProductos[pReg].Stock.ToString();
                }
            }
        }

        private void salidasalida_TextChanged(object sender, EventArgs e)
        {
            if (listaProductos.Count > 0)
            {
                if (!stocksalida.Text.Equals(""))
                {
                    if (listaProductos[pReg].Stock > Convert.ToInt32(stocksalida.Text))
                    {
                        salidastock.Text = (listaProductos[pReg].Stock - Convert.ToInt32(stocksalida.Text)).ToString();
                    }
                    else
                    {
                        salidastock.Text = "0";
                    }
                }
                else
                {
                    salidastock.Text = listaProductos[pReg].Stock.ToString();
                }
            }
        }






        #endregion 

        private void verentradaanterior_Click(object sender, EventArgs e)
        {
            if ((listaEntradas.Count > 0) && (numRegEntradas > 0))
            {
                if (pRegEntradas > 0)
                {
                    pRegEntradas--;
                    rellenarCamposEntradas(listaEntradas[pRegEntradas]);
                    verentradaindice.Text = (pRegEntradas + 1).ToString() + " de " + numRegEntradas.ToString();
                    //entradabuscador.Text = "";
                    //stockentrada.Text = "";
                }
                else
                {
                    MessageBox.Show("No hay más registros");
                }
            }
            else
            {
                MessageBox.Show("No hay registros");
            }
        }

        private void verentradasiguiente_Click(object sender, EventArgs e)
        {
            if ((listaEntradas.Count > 0) && (numRegEntradas > 0))
            {
                if (pRegEntradas < numRegEntradas - 1)
                {
                    pRegEntradas++;
                    rellenarCamposEntradas(listaEntradas[pRegEntradas]);
                    verentradaindice.Text = (pRegEntradas + 1).ToString() + " de " + numRegEntradas.ToString();
                    //entradabuscador.Text = "";
                    //stockentrada.Text = "";
                }
                else
                {
                    MessageBox.Show("No hay más registros");
                }
            }
            else
            {
                MessageBox.Show("No hay registros");
            }
        }
        #endregion


        #region DISTRIBUCIÓN BOTELLEROS
        #region GUARDAR PARTE NUEVO DE BOTELLERO REPARADO
        private void botguardar_Click(object sender, EventArgs e)
        {
            if (validarDatosDisBotParte())
            {
                ParteBotellero p = new ParteBotellero();
                p.F_repa = Convert.ToInt32(botfecha.Text);
                p.Mo = botmo.Text;
                p.Origen = botorigen.Text;
                p.Ns = botns.Text;
                p.Fabricante = botfabricante.Text;
                p.Marca = botmarca.Text;
                p.Gas = botgas.Text;
                p.Modelo = botmodelo.Text;
                p.Estado = botestado.Text;
                p.Img = botimagen.Text;
                p.Tecnico = botecnico.Text;
                p.Anyo = Convert.ToInt32(botanyo.Text);
                p.F_entrega = 0;
                p.Material = botmaterial.Text;
                p.Codigo = botcodigo.Text;
                p.SAP = botsap.Text;
                p.Oferta = (float)Convert.ToDouble(botoferta.Text);
                p.Orden = botorden.Text;
                p.Resultado = "TRATAMIENTO";

                ProcessParteBot proceso = new ProcessParteBot();
                if (!proceso.insertarRegistro(p))
                {
                    MessageBox.Show("¡Error al guardar los datos!\n", "Advertencia",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("¡Guardado correcto!\n", "Información",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("¡No guardado, error en base de datos!\n", "Información",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        #endregion

        #region GUARDAR SALIDA NUEVA DE BOTELLEROS
        private void salotguardar_Click(object sender, EventArgs e)
        {
            if (validarDatosDisBotSalida())
            {
                SalidaDistriBot p = new SalidaDistriBot();
                p.Fecha = Convert.ToInt32(salbotfecha.Value.ToString("yyyyMMdd"));
                p.Ns = salbotns.Text;
                p.Destino = salbotdestino.Text;
                p.Origen = salbotorigen.Text;
                p.Transportista = salbottransportista.Text;
                p.Cantidad = Convert.ToInt32(salbotcantidad.Text);

                ProcessDisriBotSalida proceso = new ProcessDisriBotSalida();
                if (!proceso.insertarRegistro(p))
                {
                    MessageBox.Show("¡Error al guardar los datos!\n", "Advertencia",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("¡Guardado correcto!\n", "Información",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("¡No guardado, error en base de datos!\n", "Información",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        #endregion

        #region GUARDAR ENTRADA NUEVA DE BOTELLEROS
        private void entbotguardar_Click(object sender, EventArgs e)
        {
            if (validarDatosDisBotEntrada())
            {
                EntradaDistriBot p = new EntradaDistriBot();
                p.Fecha = Convert.ToInt32(entbotfecha.Value.ToString("yyyyMMdd"));
                p.Ns = entbotns.Text;
                p.Destino = entbotdestino.Text;
                p.Origen = entbotorigen.Text;
                p.Transportista = entbottransportista.Text;
                p.Cantidad = Convert.ToInt32(entbotcantidad.Text);

                ProcessDisriBotEntrada proceso = new ProcessDisriBotEntrada();
                if (!proceso.insertarRegistro(p))
                {
                    MessageBox.Show("¡Error al guardar los datos!\n", "Advertencia",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("¡Guardado correcto!\n", "Información",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("¡No guardado, error en base de datos!\n", "Información",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        #endregion

        #region IMPRIMIR LISTA DE BOTELLEROS
        private void disBotListaImprimir_Click(object sender, EventArgs e)
        {
            int inicio = Convert.ToInt32(DateDisBotListaFeInicio.Value.ToString("yyyyMMdd"));
            int fin = Convert.ToInt32(DateDisBotListaFeFin.Value.ToString("yyyyMMdd"));
            if (inicio <= fin)
            {
                List<ParteBotellero> lista = new List<ParteBotellero>();
                ProcessParteBot p = new ProcessParteBot();
                string tipo = "";
                if (ListaBotReparado.Checked == true)
                    tipo = "REPARADO";
                else if (ListaBotEntregado.Checked == true)
                    tipo = "ENTREGADO";
                else tipo = "TODO";
                lista = p.InformeBasicoBotelleros(inicio, fin, tipo, comboIBotTipo.SelectedItem.ToString());
                if (lista.Count > 0)
                {
                    Document doc = new Document(PageSize.LETTER);
                    // Indicamos donde vamos a guardar el documento
                    PdfWriter writer = PdfWriter.GetInstance(doc,
                                                new FileStream(@"D:\Usuario\Desktop\documento.pdf", FileMode.Create));
                    doc.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
                    doc.Open();
                    int i = 0;
                    foreach (ParteBotellero o in lista)
                    {
                        i++;
                        float[] columnWidths = { 4, 8, 21, 14, 10, 10, 6, 6, 11 };
                        PdfPTable tblPrueba = new PdfPTable(columnWidths);
                        tblPrueba.WidthPercentage = 100;
                        tblPrueba.DefaultCell.Padding = 0;
                        if (ListaBotSinBorde.Checked == true)
                            tblPrueba.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                        tblPrueba.AddCell(i.ToString());
                        tblPrueba.AddCell(o.F_repa.ToString());
                        //tblPrueba.AddCell(" " + o.Mo);
                        tblPrueba.AddCell(" " + o.Origen);
                        tblPrueba.AddCell(" " + o.Ns);
                        tblPrueba.AddCell(" " + o.Fabricante);
                        tblPrueba.AddCell(" " + o.Modelo);
                        tblPrueba.AddCell(" " + o.Anyo.ToString());
                        tblPrueba.AddCell(" " + o.Gas);
                        tblPrueba.AddCell(" " + o.Marca);
                        //tblPrueba.AddCell(" " + o.Tecnico);
                        doc.Add(tblPrueba);
                        /*
                        //PadLeft(Int32)
                        doc.Add(new Paragraph(o.F_repa.ToString().PadLeft(9)
    //+ " " + o.Mo
    + " " + o.Origen.PadLeft(20)
    + " " + o.Fabricante.PadLeft(15)
    + " " + o.Modelo.PadLeft(15)
    + " " + o.Anyo.ToString().PadLeft(5)
    + " " + o.Gas.PadLeft(6)
    + " " + o.Marca.PadLeft(10)
    + " " + o.Ns.PadLeft(20)
    ));*/

                        /*
                        string s = "";
                        s += String.Format("{0,-9}", o.F_repa.ToString());
                        s += String.Format("{0,-20}", o.Origen);
                        s += String.Format("{0,-20}", o.Ns);
                        s += String.Format("{0,-15}", o.Fabricante);
                        s += String.Format("{0,-15}", o.Modelo);
                        s += String.Format("{0,-5}", o.Anyo.ToString());
                        s += String.Format("{0,-6}", o.Gas);
                        s += String.Format("{0,-10}", o.Marca);
                        doc.Add(new Paragraph(s));*/
                        /*
                        doc.Add(new Paragraph(o.F_repa.ToString()
                        //+ " " + o.Mo
                        + " " + o.Origen
                        + " " + o.Ns
                        + " " + o.Fabricante
                        + " " + o.Modelo
                        + " " + o.Anyo.ToString()
                        + " " + o.Gas
                        + " " + o.Marca));
                        */
                        //tblPrueba.AddCell(" " + o.Tecnico);
                        //doc.Add(Chunk.NEWLINE);
                    }
                    /*
                    foreach (ParteBotellero o in lista)
                    {
                        // Creamos una tabla que contendrá el nombre, apellido y país
                        // de nuestros visitante.
                        PdfPTable tblPrueba = new PdfPTable(9);
                        tblPrueba.WidthPercentage = 100;
                        tblPrueba.DefaultCell.Padding = 0;

                        tblPrueba.AddCell(o.F_repa.ToString());
                        tblPrueba.AddCell(" " + o.Mo);
                        tblPrueba.AddCell(" " + o.Origen);
                        tblPrueba.AddCell(" " + o.Ns);
                        tblPrueba.AddCell(" " + o.Fabricante);
                        tblPrueba.AddCell(" " + o.Modelo);
                        tblPrueba.AddCell(" " + o.Anyo.ToString());
                        tblPrueba.AddCell(" " + o.Gas);
                        tblPrueba.AddCell(" " + o.Marca);
                        //tblPrueba.AddCell(" " + o.Tecnico);
                        doc.Add(tblPrueba);
                    }*/


                    doc.Close();
                    writer.Close();

                    string pdfPath = @"D:\Usuario\Desktop\documento.pdf";
                    Process.Start(pdfPath);
                }
                else
                {
                    return;
                }
            }
            else
            {
                return;
            }
        }
        #endregion
        #endregion


        #region REPUESTOS SAP
        #region CAJA DE TEXTO 1 - BUSCAR UN REPUESTO
        private void RepCafBuscar1_Click(object sender, EventArgs e)
        {
            if (comboTipo.SelectedItem.ToString().Equals("GENERAL"))
            {
                SAP_Repuestos_General p = new SAP_Repuestos_General();
                ProcessSAP_Repuestos_General proceso = new ProcessSAP_Repuestos_General();
                if (!RepCafIdBuscar.Text.Equals(""))
                {
                    p = proceso.BuscarOrdenId(Convert.ToInt32(RepCafIdBuscar.Text));
                    if (p != null)
                    {
                        RepCafRepEncontrado.Text = p.Code_sap;
                        RepCafRepEncontrado.Text += " ";
                        RepCafRepEncontrado.Text += p.Name;
                        RepCafRepEncontrado.Text += " ";
                        RepCafRepEncontrado.Text += p.Price.ToString();
                    }
                    else MessageBox.Show("nulo");
                }
                if (!RepCafCodSAPBuscar.Text.Equals("") && p != null)
                {
                    p = proceso.BuscarOrdenSap(RepCafCodSAPBuscar.Text);
                    if (p != null)
                    {
                        RepCafRepEncontrado.Text = p.Code_sap;
                        RepCafRepEncontrado.Text += " ";
                        RepCafRepEncontrado.Text += p.Name;
                        RepCafRepEncontrado.Text += " ";
                        RepCafRepEncontrado.Text += p.Price.ToString();
                    }
                    else MessageBox.Show("nulo");
                }
                if (!RepCafCodAXBuscar.Text.Equals("") && p != null)
                {
                    p = proceso.BuscarOrdenSap(RepCafCodAXBuscar.Text);
                    if (p != null)
                    {
                        RepCafRepEncontrado.Text = p.Code_sap;
                        RepCafRepEncontrado.Text += " ";
                        RepCafRepEncontrado.Text += p.Name;
                        RepCafRepEncontrado.Text += " ";
                        RepCafRepEncontrado.Text += p.Price.ToString();
                    }
                    else MessageBox.Show("nulo");
                }
            }
            else if (comboTipo.SelectedItem.ToString().Equals("PEPSI"))
            {
                SAP_Repuestos_Pepsi p = new SAP_Repuestos_Pepsi();
                ProcessSAP_Repuestos_Pepsi proceso = new ProcessSAP_Repuestos_Pepsi();
                if (!RepCafIdBuscar.Text.Equals(""))
                {
                    p = proceso.BuscarOrdenId(Convert.ToInt32(RepCafIdBuscar.Text));
                    if (p != null)
                    {
                        RepCafRepEncontrado.Text = p.Proveedor;
                        RepCafRepEncontrado.Text += " ";
                        RepCafRepEncontrado.Text += p.Codax;
                        RepCafRepEncontrado.Text += " ";
                        RepCafRepEncontrado.Text += p.Codsap;
                        RepCafRepEncontrado.Text += " ";
                        RepCafRepEncontrado.Text += p.Fabricante;
                        RepCafRepEncontrado.Text += " ";
                        RepCafRepEncontrado.Text += p.Nombre;
                        RepCafRepEncontrado.Text += " ";
                        RepCafRepEncontrado.Text += p.Precio.ToString();
                    }
                    else MessageBox.Show("nulo");
                }
                if (!RepCafCodSAPBuscar.Text.Equals("") && p != null)
                {
                    p = proceso.BuscarOrdenSap(RepCafCodSAPBuscar.Text);
                    if (p != null)
                    {
                        RepCafRepEncontrado.Text = p.Proveedor;
                        RepCafRepEncontrado.Text += " ";
                        RepCafRepEncontrado.Text += p.Codax;
                        RepCafRepEncontrado.Text += " ";
                        RepCafRepEncontrado.Text += p.Codsap;
                        RepCafRepEncontrado.Text += " ";
                        RepCafRepEncontrado.Text += p.Fabricante;
                        RepCafRepEncontrado.Text += " ";
                        RepCafRepEncontrado.Text += p.Nombre;
                        RepCafRepEncontrado.Text += " ";
                        RepCafRepEncontrado.Text += p.Precio.ToString();
                    }
                    else MessageBox.Show("nulo");
                }
                if (!RepCafCodAXBuscar.Text.Equals("") && p != null)
                {
                    p = proceso.BuscarOrdenSap(RepCafCodAXBuscar.Text);
                    if (p != null)
                    {
                        RepCafRepEncontrado.Text = p.Proveedor;
                        RepCafRepEncontrado.Text += " ";
                        RepCafRepEncontrado.Text += p.Codax;
                        RepCafRepEncontrado.Text += " ";
                        RepCafRepEncontrado.Text += p.Codsap;
                        RepCafRepEncontrado.Text += " ";
                        RepCafRepEncontrado.Text += p.Fabricante;
                        RepCafRepEncontrado.Text += " ";
                        RepCafRepEncontrado.Text += p.Nombre;
                        RepCafRepEncontrado.Text += " ";
                        RepCafRepEncontrado.Text += p.Precio.ToString();
                    }
                    else MessageBox.Show("nulo");
                }
            }
        } 
        #endregion

        #region CAJA DE TEXTO 2 - BUSCAR LISTA REPUESTO
        private void RepCafNombreBuscar_TextChanged(object sender, EventArgs e)
        {
            if (comboTipo.SelectedItem.ToString().Equals("GENERAL"))
            {
                ProcessSAP_Repuestos_General proceso = new ProcessSAP_Repuestos_General();
                List<SAP_Repuestos_General> lista = new List<SAP_Repuestos_General>();
                if (!RepCafNombreBuscar.Text.Equals(""))
                {
                    lista = proceso.ObtenerRegistrosTodosNombre(RepCafNombreBuscar.Text);
                    if (lista.Count > 0)
                    {
                        comboBusNom.Items.Clear();
                        comboBusCod.Items.Clear();
                        comboBusPre.Items.Clear();
                        foreach (SAP_Repuestos_General o in lista)
                        {
                            comboBusNom.Items.Add(o.Name);
                            comboBusCod.Items.Add(o.Code_sap);
                            comboBusPre.Items.Add(o.Price);
                        }
                        comboBusNom.SelectedIndex = 0;
                        comboBusCod.SelectedIndex = 0;
                        comboBusPre.SelectedIndex = 0;
                    }
                    else
                    {
                        lista = proceso.ObtenerRegistrosTodosSap(RepCafNombreBuscar.Text);
                        if (lista.Count > 0)
                        {
                            comboBusNom.Items.Clear();
                            comboBusCod.Items.Clear();
                            comboBusPre.Items.Clear();
                            foreach (SAP_Repuestos_General o in lista)
                            {
                                comboBusNom.Items.Add(o.Name);
                                comboBusCod.Items.Add(o.Code_sap);
                                comboBusPre.Items.Add(o.Price);
                            }
                            comboBusNom.SelectedIndex = 0;
                            comboBusCod.SelectedIndex = 0;
                            comboBusPre.SelectedIndex = 0;
                        }
                        else
                        {
                            comboBusNom.Items.Clear();
                            comboBusCod.Items.Clear();
                            comboBusPre.Items.Clear();
                        }
                    }
                }
                else
                {
                    comboBusNom.Items.Clear();
                    comboBusCod.Items.Clear();
                    comboBusPre.Items.Clear();
                }
            }
            else if (comboTipo.SelectedItem.ToString().Equals("PEPSI"))
            {
                ProcessSAP_Repuestos_Pepsi proceso = new ProcessSAP_Repuestos_Pepsi();
                List<SAP_Repuestos_Pepsi> lista = new List<SAP_Repuestos_Pepsi>();
                if (!RepCafNombreBuscar.Text.Equals(""))
                {
                    lista = proceso.ObtenerRegistrosTodosNombre(RepCafNombreBuscar.Text);
                    if (lista.Count > 0)
                    {
                        comboBusNom.Items.Clear();
                        comboBusCod.Items.Clear();
                        comboBusPre.Items.Clear();
                        foreach (SAP_Repuestos_Pepsi o in lista)
                        {
                            comboBusNom.Items.Add(o.Nombre);
                            comboBusCod.Items.Add(o.Codsap);
                            comboBusPre.Items.Add(o.Precio);
                        }
                        comboBusNom.SelectedIndex = 0;
                        comboBusCod.SelectedIndex = 0;
                        comboBusPre.SelectedIndex = 0;
                    }
                    else
                    {
                        comboBusNom.Items.Clear();
                        comboBusCod.Items.Clear();
                        comboBusPre.Items.Clear();
                    }
                }
                else
                {
                    comboBusNom.Items.Clear();
                    comboBusCod.Items.Clear();
                    comboBusPre.Items.Clear();
                }
            }
        }  
        #endregion

        private void RepCafCambiar_Click(object sender, EventArgs e)
        {
            if (comboTipo.SelectedItem.ToString().Equals("GENERAL"))
            {
                SAP_Repuestos_General p = new SAP_Repuestos_General();
                ProcessSAP_Repuestos_General proceso = new ProcessSAP_Repuestos_General();
                if (!comboBusCod.SelectedItem.ToString().Equals("") && !RepCafPrecio.Text.Equals(""))
                {
                    p = proceso.BuscarOrdenSap(comboBusCod.SelectedItem.ToString());
                    if (p != null)
                    {
                        proceso.CambiarPrecio(p.Id, (float)Convert.ToDouble(RepCafPrecio.Text));
                        RepCafNombreBuscar.Text = "";
                        RepCafPrecio.Text = "";
                    }
                    else MessageBox.Show("error");
                }
            }
            else if (comboTipo.SelectedItem.ToString().Equals("PEPSI"))
            {
                SAP_Repuestos_Pepsi p = new SAP_Repuestos_Pepsi();
                ProcessSAP_Repuestos_Pepsi proceso = new ProcessSAP_Repuestos_Pepsi();
                if (!comboBusCod.SelectedItem.ToString().Equals("") && !RepCafPrecio.Text.Equals(""))
                {
                    p = proceso.BuscarOrdenSap(comboBusCod.SelectedItem.ToString());
                    if (p != null)
                    {
                        proceso.CambiarPrecio(p.Id, (float)Convert.ToDouble(RepCafPrecio.Text));
                        RepCafNombreBuscar.Text = "";
                        RepCafPrecio.Text = "";
                    }
                    else MessageBox.Show("error");
                }
            }
        }

        #region TECLADO Y COMBO
        private void RepCafPrecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números
            if (Char.IsDigit(e.KeyChar) || e.KeyChar == ',')
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan
                e.Handled = true;
            }
        }

        private void comboBusNom_SelectedValueChanged(object sender, EventArgs e)
        {
            comboBusCod.SelectedIndex = comboBusNom.SelectedIndex;
            comboBusPre.SelectedIndex = comboBusNom.SelectedIndex;
        }

        private void comboBusCod_SelectedValueChanged(object sender, EventArgs e)
        {
            comboBusNom.SelectedIndex = comboBusCod.SelectedIndex;
            comboBusPre.SelectedIndex = comboBusCod.SelectedIndex;
        }

        private void comboBusPre_SelectedValueChanged(object sender, EventArgs e)
        {
            comboBusNom.SelectedIndex = comboBusPre.SelectedIndex;
            comboBusCod.SelectedIndex = comboBusPre.SelectedIndex;
        } 
        #endregion
        #endregion

        #region ORDEN DE TALLER
        #region GUARDAR NUEVA ORDEN DE TALLER
        private void button3_Click_1(object sender, EventArgs e)
        {
            if (validarDatosOTaller())
            {
                OTaller p = new OTaller();
                p.Tecnico = ottecnico.Text;
                p.Estado = otestado.Text;
                p.Resultado = otresultado.Text;
                p.Proyecto = otproyecto.Text;
                p.Pds = otpds.Text;
                p.F_trabajo = Convert.ToInt32(otf1.Text);
                p.F_repa = Convert.ToInt32(otf2.Text);
                p.Cliente = otcliente.Text;
                p.Provincia = otprovincia.Text;
                p.Oaveria = otaveria.Text;
                p.Omontaje = otmontaje.Text;
                p.Odesmontaje = otdesmontaje.Text;
                p.Otaller = ottaller.Text;
                p.Material = otmaterial1.Text;
                p.Codmaterial = otmaterial2.Text;
                p.Ax = otax.Text;
                p.Sap = otsap.Text;
                p.Norden = otorden.Text;
                p.Oferta = (float)Convert.ToDouble(otoferta.Text);
                p.Despl = (float)Convert.ToDouble(otdespla.Text);
                p.Mo = (float)Convert.ToDouble(otmo.Text);
                p.Repuesto = otrepuesto.Text;
                p.Cmi = otcmi.Text;
                p.Tipo = ottipo.SelectedItem.ToString();

                ProcessOTaller proceso = new ProcessOTaller();
                if (otproyecto.Text.Equals("BERLYS") || otproyecto.Text.Equals("IKEA") || 
                    otproyecto.Text.Equals("DANONE VENDING") || otproyecto.Text.Equals("DANONE WATERS") ||
                    otproyecto.Text.Equals("HEI STC") || otproyecto.Text.Equals("HEI EVENTO") || 
                    otproyecto.Text.Equals("CORECO") || otproyecto.Text.Equals("SCHWEPPES"))
                {
                    p.Cod_proyecto = otcodproyecto.Text;
                    if (!proceso.insertarRegistro2(p))
                    {
                        MessageBox.Show("¡Error al guardar los datos!\n", "Advertencia",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("¡Guardado correcto!\n", "Información",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    if (!proceso.insertarRegistro(p))
                    {
                        MessageBox.Show("¡Error al guardar los datos!\n", "Advertencia",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("¡Guardado correcto!\n", "Información",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            else
            {
                MessageBox.Show("¡No guardado, error en base de datos!\n", "Información",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void OTbusca_repuesto_TextChanged(object sender, EventArgs e)
        {
            ProcessSAP_Repuestos_General proceso = new ProcessSAP_Repuestos_General();
            List<SAP_Repuestos_General> lista = new List<SAP_Repuestos_General>();
            if (!OTbusca_repuesto.Text.Equals(""))
            {
                lista = proceso.ObtenerRegistrosTodosNombre(OTbusca_repuesto.Text);
                if (lista.Count > 0)
                {
                    OTencuentra_repuesto.Items.Clear();
                    OTencuentra_repuestoCod.Items.Clear();
                    comboPrecio.Items.Clear();
                    foreach (SAP_Repuestos_General o in lista)
                    {
                        OTencuentra_repuesto.Items.Add(o.Name);
                        OTencuentra_repuestoCod.Items.Add(o.Code_sap);
                        comboPrecio.Items.Add(o.Price);
                    }
                    OTencuentra_repuesto.SelectedIndex = 0;
                    OTencuentra_repuestoCod.SelectedIndex = 0;
                    comboPrecio.SelectedIndex = 0;
                }
                else
                {
                    OTencuentra_repuesto.Items.Clear();
                    OTencuentra_repuestoCod.Items.Clear();
                    comboPrecio.Items.Clear();
                }
            }
            else
            {
                OTencuentra_repuesto.Items.Clear();
                OTencuentra_repuestoCod.Items.Clear();
                comboPrecio.Items.Clear();
            }
        }

        private void OTañadir_repuesto_Click(object sender, EventArgs e)
        {
            /*ProcessSAP_Repuestos_Pepsi proceso1 = new ProcessSAP_Repuestos_Pepsi();
            ProcessSAP_Repuestos_Pepsi proceso2 = new ProcessSAP_Repuestos_Pepsi();
            List<SAP_Repuestos_Pepsi> lista = new List<SAP_Repuestos_Pepsi>();
            lista = proceso1.ObtenerRegistrosTodosCopia();
            foreach (SAP_Repuestos_Pepsi item in lista)
            {
                SAP_Repuestos_Pepsi producto = new SAP_Repuestos_Pepsi()
                {
                    Proveedor = item.Proveedor.ToString(),
                    Codax = item.Codax.ToString(),
                    Codsap = item.Codsap.ToString(),
                    Fabricante = item.Fabricante.ToString(),
                    Nombre = item.Nombre.ToString(),
                    Precio = 0,
                };
                if (producto.Nombre != null && !producto.Nombre.Equals("") && 
                    !producto.Nombre.Equals("0"))
                    proceso2.insertarRegistro(producto);
            }*/

            if (OTencuentra_repuesto.Items.Count > 0 && OTencuentra_repuestoCod.Items.Count > 0 && !OTrepuesto_uds.Text.Equals(""))
            {
                otrepuesto.Text += System.Environment.NewLine;
                otrepuesto.Text += OTrepuesto_uds.Text + "X " +
                                   OTencuentra_repuestoCod.Text + " " +
                                   OTencuentra_repuesto.Text;
            }
        }

        private void OTencuentra_repuesto_SelectedValueChanged(object sender, EventArgs e)
        {
            OTencuentra_repuestoCod.SelectedIndex = OTencuentra_repuesto.SelectedIndex;
            comboPrecio.SelectedIndex = OTencuentra_repuesto.SelectedIndex;
        }

        private void OTencuentra_repuestoCod_SelectedValueChanged(object sender, EventArgs e)
        {
            OTencuentra_repuesto.SelectedIndex = OTencuentra_repuestoCod.SelectedIndex;
            comboPrecio.SelectedIndex = OTencuentra_repuestoCod.SelectedIndex;
        }

        private void comboPrecio_SelectionChangeCommitted(object sender, EventArgs e)
        {
            OTencuentra_repuesto.SelectedIndex = comboPrecio.SelectedIndex;
            OTencuentra_repuestoCod.SelectedIndex = comboPrecio.SelectedIndex;
        }

        private void OTrepuesto_uds_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan
                e.Handled = true;
            }
        }

        private void OTbusca_repuesto_KeyPress(object sender, KeyPressEventArgs e)
        {
        }
        #endregion

        #region IMPRIMIR NUEVA ORDEN TALLER
        private void otallerimprimir_Click(object sender, EventArgs e)
        {
            Document doc = new Document(PageSize.LETTER);
            // Indicamos donde vamos a guardar el documento
            PdfWriter writer = PdfWriter.GetInstance(doc,
                                        new FileStream(@"D:\Usuario\Desktop\documento.pdf", FileMode.Create));

            // Le colocamos el título y el autor
            // **Nota: Esto no será visible en el documento
            doc.AddTitle("ORDEN DE TALLER");
            doc.AddCreator("Fco Guillén");
            // Abrimos el archivo
            doc.Open();

            string ods = "";
            if (!otaveria.Text.Equals("")) ods += otaveria.Text;
            else if(!otdesmontaje.Text.Equals("")) ods += otdesmontaje.Text;
            else if (!otmontaje.Text.Equals("")) ods += otmontaje.Text;
            else if(!ottaller.Text.Equals("")) ods += " " + ottaller.Text;
            // Escribimos el encabezamiento en el documento
            doc.Add(new Paragraph(otf1.Text + " " + ods + " " +
                                otpds.Text + " " + otax.Text + " " +
                                otcliente.Text));
            doc.Add(Chunk.NEWLINE);

            // Creamos una tabla que contendrá el nombre, apellido y país
            // de nuestros visitante.
            PdfPTable tblPrueba = new PdfPTable(4);
            tblPrueba.WidthPercentage = 100;
            tblPrueba.DefaultCell.Padding = 4;

            tblPrueba.AddCell("PROYECTO" + Chunk.NEWLINE + "" + otproyecto.Text);
            tblPrueba.AddCell("PDS" + Chunk.NEWLINE + "" + otpds.Text);
            tblPrueba.AddCell("F.TRABAJO" + Chunk.NEWLINE + "" + otf1.Text);
            tblPrueba.AddCell("F.ORDEN" + Chunk.NEWLINE + "" + otf2.Text);


            tblPrueba.AddCell("CLIENTE" + Chunk.NEWLINE + "" + otcliente.Text);
            tblPrueba.AddCell("");
            tblPrueba.AddCell("PROVINCIA" + Chunk.NEWLINE + "" + otprovincia.Text);
            tblPrueba.AddCell("");

            tblPrueba.AddCell("AVERIA" + Chunk.NEWLINE + "" + otaveria.Text);
            tblPrueba.AddCell("MONTAJE" + Chunk.NEWLINE + "" + otmontaje.Text);
            tblPrueba.AddCell("DESMONTAJE" + Chunk.NEWLINE + "" + otdesmontaje.Text);
            tblPrueba.AddCell("TALLER" + Chunk.NEWLINE + "" + ottaller.Text);
            doc.Add(tblPrueba);

            tblPrueba = new PdfPTable(2);
            tblPrueba.WidthPercentage = 100;
            tblPrueba.DefaultCell.Padding = 4;
            tblPrueba.AddCell("MATERIAL" + Chunk.NEWLINE + "" + otmaterial1.Text);
            tblPrueba.AddCell("COD.MATERIAL" + Chunk.NEWLINE + "" + otmaterial2.Text);

            tblPrueba.AddCell("N.SERIE AX" + Chunk.NEWLINE + "" + otax.Text);
            tblPrueba.AddCell("N.SERIE SAP" + Chunk.NEWLINE + "" + otsap.Text);
            doc.Add(tblPrueba);

            tblPrueba = new PdfPTable(4);
            tblPrueba.WidthPercentage = 100;
            tblPrueba.DefaultCell.Padding = 4;
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("ESTADO" + Chunk.NEWLINE + "" + otresultado.Text);
            tblPrueba.AddCell("ABIERTO/CERRADO" + Chunk.NEWLINE + "" + otestado.Text);

            tblPrueba.AddCell("ORDEN" + Chunk.NEWLINE + "" + otorden.Text);
            tblPrueba.AddCell("OFERTA" + Chunk.NEWLINE + "" + otoferta.Text);
            tblPrueba.AddCell("DESPLAZAMIENTO" + Chunk.NEWLINE + "" + otdespla.Text);
            tblPrueba.AddCell("MANO DE OBRA" + Chunk.NEWLINE + "" + otmo.Text);
            doc.Add(tblPrueba);

            // Escribimos el pie en el documento
            doc.Add(Chunk.NEWLINE);
            doc.Add(Chunk.NEWLINE);
            doc.Add(new Paragraph("MANO DE OBRA, DESPLAZAMIENTO Y REPUESTOS"));
            doc.Add(Chunk.NEWLINE);
            doc.Add(new Paragraph(otrepuesto.Text));
            doc.Close();
            writer.Close();

            //string pdfPath = Path.Combine(Application.StartupPath, "archivo.pdf");
            string pdfPath = @"D:\Usuario\Desktop\documento.pdf";
            Process.Start(pdfPath);
        }
        #endregion

        #region INFORME PARA CERRAR ORDENES
        private void IBOTallerConsultar_Click(object sender, EventArgs e)
        {
            int inicio = Convert.ToInt32(DateIBOTallerInicio.Value.ToString("yyyyMMdd"));
            int fin = Convert.ToInt32(DateIBOTallerFin.Value.ToString("yyyyMMdd"));
            if (inicio <= fin)
            {
                List<OTaller> lista = new List<OTaller>();
                ProcessOTaller p = new ProcessOTaller();

                if (groupBox8.Enabled == true && groupBox8.Visible == true &&
                    groupBox9.Enabled == true && groupBox9.Visible == true)
                {
                    if (radioButtonAbierto.Checked == true)
                    {
                        lista = p.AceptadoRechazado2(inicio, fin, comboIBOTaller.SelectedItem.ToString(), "ABIERT", "");
                    }
                    else
                    {
                        if (radioButtonAceptado.Checked == true)
                        {
                            lista = p.AceptadoRechazado2(inicio, fin, comboIBOTaller.SelectedItem.ToString(), "CERRAD", "ACEPTAD");
                        }
                        else
                        {
                            lista = p.AceptadoRechazado2(inicio, fin, comboIBOTaller.SelectedItem.ToString(), "CERRAD", "RECHAZAD");
                        }
                    }
                }
                else
                {
                    lista = p.AceptadoRechazado1(inicio, fin, comboIBOTaller.SelectedItem.ToString(), "ABIERT");
                }

                if (lista.Count > 0)
                {
                    Document doc = new Document(PageSize.LETTER);
                    // Indicamos donde vamos a guardar el documento
                    PdfWriter writer = PdfWriter.GetInstance(doc,
                                                new FileStream(@"D:\Usuario\Desktop\documento.pdf", FileMode.Create));
                    doc.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
                    doc.Open();

                    foreach (OTaller o in lista)
                    {
                        // Creamos una tabla que contendrá el nombre, apellido y país
                        // de nuestros visitante.
                        float[] columnWidths = { 11, 9, 8, 11, 7, 7, 7, 40 };
                        PdfPTable tblPrueba = new PdfPTable(columnWidths);
                        tblPrueba.WidthPercentage = 100;
                        tblPrueba.DefaultCell.Padding = 0;
                        tblPrueba.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;

                        tblPrueba.AddCell(o.Proyecto);
                        tblPrueba.AddCell(" " + o.Pds);
                        tblPrueba.AddCell(" " + o.F_trabajo);
                        tblPrueba.AddCell(" " + o.Norden);
                        tblPrueba.AddCell(" " + o.Oferta);
                        tblPrueba.AddCell(" " + o.Despl);
                        tblPrueba.AddCell(" " + o.Mo);
                        tblPrueba.AddCell(" " + o.Estado);

                        doc.Add(tblPrueba);
                    }


                    doc.Close();
                    writer.Close();

                    string pdfPath = @"D:\Usuario\Desktop\documento.pdf";
                    Process.Start(pdfPath);
                }
                else
                {                
                    MessageBox.Show("No hay órdenes en la selección para modificar");
                }
            }
            else
            {
                MessageBox.Show("Fecha errónea");
                return;
            }
        }
        #endregion

        #region CERRAR Y ACEPTAR - RECHAZAR
        private void IBOTallerCerrar_Click(object sender, EventArgs e)
        {
            int inicio = Convert.ToInt32(DateIBOTallerInicio.Value.ToString("yyyyMMdd"));
            int fin = Convert.ToInt32(DateIBOTallerFin.Value.ToString("yyyyMMdd"));
            if (inicio <= fin)
            {
                List<OTaller> lista = new List<OTaller>();
                ProcessOTaller p = new ProcessOTaller();
                /*
                if (radioButtonRechazado.Checked == true)
                    lista = p.AceptadoRechazado(inicio, fin, comboIBOTaller.SelectedItem.ToString(), "RECHAZADO");
                else
                    lista = p.AceptadoRechazado(inicio, fin, comboIBOTaller.SelectedItem.ToString(), "ACEPTADO");
                */
                lista = p.AceptadoRechazado1(inicio, fin, comboIBOTaller.SelectedItem.ToString(), "ABIERT");

                if (lista.Count > 0)
                {
                    foreach (OTaller o in lista)
                    {
                        p.ConcluirOrdenes(o.Id);
                        if (radioButtonRechazado.Checked == true)
                            p.RechazarOrdenes(o.Id);
                        else
                            p.AceptarOrdenes(o.Id);
                    }
                    MessageBox.Show("Ordenes modificadas");
                }
                else
                {
                    MessageBox.Show("No hay órdenes en la selección para modificar");
                }
            }
            else
            {
                MessageBox.Show("Fecha errónea");
                return;
            }
        }
        #endregion

        #region INFORME RESUMENES
        private void IROTallerConsultar_Click(object sender, EventArgs e)
        {
            int inicio = Convert.ToInt32(dateIROTallerInicio.Value.ToString("yyyyMMdd"));
            int fin = Convert.ToInt32(dateIROTallerFin.Value.ToString("yyyyMMdd"));
            if (inicio <= fin)
            {
                if (radioIRResumen.Checked)
                {
                    resumen(inicio, fin);
                }
                else
                {
                    detalle(inicio,fin);
                }
            }
        }

        public void detalle(int inicio, int fin)
        {
            int actividades = 0;
            float auxiliar = 0;
            int dias = DateTime.DaysInMonth(2020, dateIROTallerInicio.Value.Month);
            List<OTaller> lista = new List<OTaller>();
            ProcessOTaller p = new ProcessOTaller();
            if (comboIROTallerProyecto.SelectedItem.ToString().Equals("OTROS"))
            {
                lista = p.obtenerTotalesDetalleGeneral(inicio, fin, comboIROTallerTecnico.SelectedItem.ToString(),
                                             comboIROTallerObjeto.SelectedItem.ToString(),
                                                comboIROTallerTipo.SelectedItem.ToString(),
                                                comboIROTallerProyecto.SelectedItem.ToString());
            }
            else
            {
                lista = p.obtenerTotalesDetalle(inicio, fin, comboIROTallerTecnico.SelectedItem.ToString(),
                                                             comboIROTallerObjeto.SelectedItem.ToString(),
                                                                comboIROTallerTipo.SelectedItem.ToString(),
                                                                comboIROTallerProyecto.SelectedItem.ToString());
            }

            Document doc = new Document(PageSize.LETTER);
            // Indicamos donde vamos a guardar el documento
            PdfWriter writer = PdfWriter.GetInstance(doc,
                                        new FileStream(@"D:\Usuario\Desktop\documento.pdf", FileMode.Create));
            doc.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
            doc.Open();
            doc.Add(Chunk.NEWLINE);
            doc.Add(Chunk.NEWLINE);
            doc.Add(new Paragraph(IROTallerencabezado.Text));
            if (comboIROTallerProyecto.SelectedItem.ToString().Equals("OTROS"))
            {
                //doc.Add(new Paragraph("ORDENES DE CAFE: CAFENTO - CANDELAS - SIH - DELTA - BLACKZI"));
            }
            else
            {
                doc.Add(new Paragraph("ORDENES DE CAFE: CAFENTO - CANDELAS - SIH - DELTA - BLACKZI"));
            }
            doc.Add(Chunk.NEWLINE);


            // Creamos una tabla que contendrá el nombre, apellido y país
            // de nuestros visitante.
            //5, 5, 5, 10, 4, 4, 4, 59
            float[] columnWidths = { 4, 20, 9, 8, 11, 7, 7, 7, 7, 20 };
            PdfPTable tblPrueba = new PdfPTable(columnWidths);
            tblPrueba.WidthPercentage = 100;
            tblPrueba.DefaultCell.Padding = 0;
            tblPrueba.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;

            tblPrueba.AddCell("NUM");
            //tblPrueba.AddCell("PROYECTO");
            tblPrueba.AddCell("TRABAJO");
            tblPrueba.AddCell("CLIENTE");
            tblPrueba.AddCell("FECHA");
            tblPrueba.AddCell("ORDEN");
            tblPrueba.AddCell("OFERTA");
            tblPrueba.AddCell("REPUES");
            tblPrueba.AddCell("DESPLA");
            tblPrueba.AddCell("M.OBRA");
            tblPrueba.AddCell("NOMBRE");

            int i = 1;
            int aux = 0;
            foreach (OTaller o in lista)
            {
                //if ((o.Oferta - o.Despl - o.Mo) > 0)
                if (o.Oferta > 0)
                {
                    auxiliar = o.Oferta + o.Despl + o.Mo;
                    if (auxiliar > 0)
                        actividades++;
                    tblPrueba.AddCell(i++.ToString());
                    //tblPrueba.AddCell(o.Proyecto);
                    tblPrueba.AddCell(o.Tipo);
                    tblPrueba.AddCell(o.Proyecto);
                    tblPrueba.AddCell(o.F_trabajo.ToString());
                    if (o.Norden.Equals(""))
                        tblPrueba.AddCell("SIN RECAMBIO");
                    else
                        tblPrueba.AddCell(o.Norden);
                    tblPrueba.AddCell(String.Format("{0:000.00}", Math.Round(o.Oferta, 2)).ToString());
                    if ((o.Oferta - o.Despl - o.Mo) <= 0)
                        tblPrueba.AddCell("-----");
                    else
                        tblPrueba.AddCell(String.Format("{0:00.00}", Math.Round((o.Oferta - o.Despl - o.Mo), 2)).ToString());
                    if (o.Despl == 0)
                        tblPrueba.AddCell("-----");
                    else
                        tblPrueba.AddCell(String.Format("{0:00.00}", Math.Round(o.Despl, 2)).ToString());
                    tblPrueba.AddCell(String.Format("{0:00.00}", Math.Round(o.Mo, 2)).ToString());
                    if (o.Cliente.Length > 18)
                        tblPrueba.AddCell(o.Cliente.Substring(0, 18));
                    else
                        tblPrueba.AddCell(o.Cliente);
                    aux = o.F_trabajo;
                }
            }
            float oferta = p.obtenerTotales(inicio, fin, comboIROTallerTecnico.SelectedItem.ToString(),
                                                "OFERTA",
                                                comboIROTallerTipo.SelectedItem.ToString(),
                                                            comboIROTallerProyecto.SelectedItem.ToString());
            float repuesto = p.obtenerTotales(inicio, fin, comboIROTallerTecnico.SelectedItem.ToString(),
                                            "REPUESTO",
                                            comboIROTallerTipo.SelectedItem.ToString(),
                                                            comboIROTallerProyecto.SelectedItem.ToString());
            float desplazamiento = p.obtenerTotales(inicio, fin, comboIROTallerTecnico.SelectedItem.ToString(),
                                            "DESPLAZAMIENTO",
                                            comboIROTallerTipo.SelectedItem.ToString(),
                                                            comboIROTallerProyecto.SelectedItem.ToString());
            float mo = p.obtenerTotales(inicio, fin, comboIROTallerTecnico.SelectedItem.ToString(),
                                            "MANO DE OBRA",
                                            comboIROTallerTipo.SelectedItem.ToString(),
                                                            comboIROTallerProyecto.SelectedItem.ToString());

            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");

            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");

            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("OFERTA");
            tblPrueba.AddCell("REPUES");
            tblPrueba.AddCell("DESPLA");
            tblPrueba.AddCell("M.OBRA");
            tblPrueba.AddCell("");

            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("");

            tblPrueba.AddCell("");
            tblPrueba.AddCell("TOTALES");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell(oferta.ToString());
            tblPrueba.AddCell(repuesto.ToString());
            tblPrueba.AddCell(desplazamiento.ToString());
            tblPrueba.AddCell(mo.ToString());
            tblPrueba.AddCell("");
            doc.Add(tblPrueba);

            doc.Close();
            writer.Close();

            string pdfPath = @"D:\Usuario\Desktop\documento.pdf";
            Process.Start(pdfPath);
        }


        public void resumen(int inicio, int fin)
        {
            ProcessOTaller p = new ProcessOTaller();
            float oferta = p.obtenerTotales(inicio, fin, comboIROTallerTecnico.SelectedItem.ToString(),
                                                            "OFERTA",
                                                            comboIROTallerTipo.SelectedItem.ToString(),
                                                            comboIROTallerProyecto.SelectedItem.ToString());
            float repuesto = p.obtenerTotales(inicio, fin, comboIROTallerTecnico.SelectedItem.ToString(),
                                            "REPUESTO",
                                            comboIROTallerTipo.SelectedItem.ToString(),
                                                            comboIROTallerProyecto.SelectedItem.ToString());
            float desplazamiento = p.obtenerTotales(inicio, fin, comboIROTallerTecnico.SelectedItem.ToString(),
                                            "DESPLAZAMIENTO",
                                            comboIROTallerTipo.SelectedItem.ToString(),
                                                            comboIROTallerProyecto.SelectedItem.ToString());
            float mo = p.obtenerTotales(inicio, fin, comboIROTallerTecnico.SelectedItem.ToString(),
                                            "MANO DE OBRA",
                                            comboIROTallerTipo.SelectedItem.ToString(),
                                                            comboIROTallerProyecto.SelectedItem.ToString());
            Document doc = new Document(PageSize.LETTER);
            // Indicamos donde vamos a guardar el documento
            PdfWriter writer = PdfWriter.GetInstance(doc,
                                        new FileStream(@"D:\Usuario\Desktop\documento.pdf", FileMode.Create));
            doc.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
            doc.Open();


            // Creamos una tabla que contendrá el nombre, apellido y país
            // de nuestros visitante.
            PdfPTable tblPrueba = new PdfPTable(4);
            tblPrueba.WidthPercentage = 100;
            tblPrueba.DefaultCell.Padding = 0;
            tblPrueba.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;

            tblPrueba.AddCell("OFERTA");
            tblPrueba.AddCell("REPUESTO");
            tblPrueba.AddCell("DESPLAZAMIENTO");
            tblPrueba.AddCell("MANO DE OBRA");
            //doc.Add(tblPrueba);
            tblPrueba.AddCell(oferta.ToString());
            tblPrueba.AddCell(repuesto.ToString());
            tblPrueba.AddCell(desplazamiento.ToString());
            tblPrueba.AddCell(mo.ToString());
            doc.Add(tblPrueba);

            doc.Close();
            writer.Close();

            string pdfPath = @"D:\Usuario\Desktop\documento.pdf";
            Process.Start(pdfPath);
        }
        #endregion

        #region BUSCAR ORDEN POR ID
        private void otallerbuscar_Click(object sender, EventArgs e)
        {
            OTaller p = new OTaller();
            ProcessOTaller proceso = new ProcessOTaller();
            if (!otidbuscar.Text.Equals(""))
            {
                if (tipoOrdenTaller.Equals("GENERAL"))
                {
                    p = proceso.BuscarOrdenId2(Convert.ToInt32(otidbuscar.Text));
                }
                else
                {
                    p = proceso.BuscarOrdenId(Convert.ToInt32(otidbuscar.Text));
                }
                if (p != null)
                {
                    ottecnico.Text = p.Tecnico;
                    otestado.Text = p.Estado;
                    otresultado.Text = p.Resultado;
                    otid.Text = p.Id.ToString();
                    otproyecto.Text = p.Proyecto;
                    if (tipoOrdenTaller.Equals("GENERAL"))
                        otcodproyecto.Text = p.Cod_proyecto;
                    else
                        otcodproyecto.Text = "";
                    otpds.Text = p.Pds;
                    otf1.Text = p.F_trabajo.ToString();
                    otf2.Text = p.F_repa.ToString();
                    otcliente.Text = p.Cliente;
                    otprovincia.Text = p.Provincia;
                    otaveria.Text= p.Oaveria ;
                    otmontaje.Text = p.Omontaje;
                    otdesmontaje.Text= p.Odesmontaje ;
                    ottaller.Text = p.Otaller;
                    otmaterial1.Text = p.Material;
                    otmaterial2.Text= p.Codmaterial ;
                    otax.Text = p.Ax;
                    otsap.Text = p.Sap;
                    otorden.Text = p.Norden;
                    otoferta.Text = p.Oferta.ToString();
                    otdespla.Text = p.Despl.ToString();
                    //otmo.Text  = p.Mo.ToString();
                    otrepuesto.Text = p.Repuesto;
                    otcmi.Text = p.Cmi;
                    buscarTipo(p.Tipo);
                    otmo.Text = p.Mo.ToString();
                    //ottipo.SelectedItem.ToString() = p.Tipo;
                } 
                else MessageBox.Show("nulo");
            }
            else if (!otordenbuscar.Text.Equals(""))
            {
                if (tipoOrdenTaller.Equals("GENERAL"))
                {
                    p = proceso.BuscarOrdenOrden2(otordenbuscar.Text);
                }
                else
                {
                    p = proceso.BuscarOrdenOrden(otordenbuscar.Text);
                }
                if (p != null)
                {
                    ottecnico.Text = p.Tecnico;
                    otestado.Text = p.Estado;
                    otresultado.Text = p.Resultado;
                    otid.Text = p.Id.ToString();
                    otproyecto.Text = p.Proyecto;
                    if (tipoOrdenTaller.Equals("GENERAL"))
                        otcodproyecto.Text = p.Cod_proyecto;
                    else
                        otcodproyecto.Text = "";
                    otpds.Text = p.Pds;
                    otf1.Text = p.F_trabajo.ToString();
                    otf2.Text = p.F_repa.ToString();
                    otcliente.Text = p.Cliente;
                    otprovincia.Text = p.Provincia;
                    otaveria.Text = p.Oaveria;
                    otmontaje.Text = p.Omontaje;
                    otdesmontaje.Text = p.Odesmontaje;
                    ottaller.Text = p.Otaller;
                    otmaterial1.Text = p.Material;
                    otmaterial2.Text = p.Codmaterial;
                    otax.Text = p.Ax;
                    otsap.Text = p.Sap;
                    otorden.Text = p.Norden;
                    otoferta.Text = p.Oferta.ToString();
                    otdespla.Text = p.Despl.ToString();
                    //otmo.Text = p.Mo.ToString();
                    otrepuesto.Text = p.Repuesto;
                    otcmi.Text = p.Cmi;
                    buscarTipo(p.Tipo);
                    otmo.Text = p.Mo.ToString();
                }
                else MessageBox.Show("nulo");
            }
        } 
        #endregion

        public void buscarTipo(string s)
        {
            switch (s)
            {
                case "CAF CON RECAMBIO":
                    ottipo.SelectedIndex = 0;
                    break;
                case "CAF SIN RECAMBIO":
                    ottipo.SelectedIndex = 1;
                    break;
                case "MOL CON RECAMBIO":
                    ottipo.SelectedIndex = 2;
                    break;
                case "MOL SIN RECAMBIO":
                    ottipo.SelectedIndex = 3;
                    break;
                case "PUESTA A PUNTO CAF":
                    ottipo.SelectedIndex = 4;
                    break;
                case "PUESTA A PUNTO MOL":
                    ottipo.SelectedIndex = 5;
                    break;
                case "MONTAJE":
                    ottipo.SelectedIndex = 6;
                    break;
                case "DESMONTAJE":
                    ottipo.SelectedIndex = 7;
                    break;
                case "CT MONTAJE":
                    ottipo.SelectedIndex = 8;
                    break;
                case "CT DESMONTAJE":
                    ottipo.SelectedIndex = 9;
                    break;
                case "LIMPIEZA":
                    ottipo.SelectedIndex = 10;
                    break;
                case "OTROS":
                    ottipo.SelectedIndex = 11;
                    break;
                case "CT MOL MONTAJE":
                    ottipo.SelectedIndex = 12;
                    break;
                case "CT MOL DESMONTAJE":
                    ottipo.SelectedIndex = 13;
                    break;
                case "REPARACION TALLER HORNO A":
                    ottipo.SelectedIndex = 14;
                    break;
                case "REPARACION TALLER HORNO B":
                    ottipo.SelectedIndex = 15;
                    break;
                case "REPARACION TALLER HORNO C":
                    ottipo.SelectedIndex = 16;
                    break;
                case "REPUESTOS":
                    ottipo.SelectedIndex = 17;
                    break;
                case "PUESTA A PUNTO HORNO":
                    ottipo.SelectedIndex = 18;
                    break;
                case "PUESTA A PUNTO ARCON":
                    ottipo.SelectedIndex = 19;
                    break;
                case "REPARACION TALLER ARCON":
                    ottipo.SelectedIndex = 20;
                    break;
                case "MONTAJE ARCON":
                    ottipo.SelectedIndex = 21;
                    break;
                case "DESMONTAJE ARCON":
                    ottipo.SelectedIndex = 22;
                    break;
                case "MONTAJE HORNO":
                    ottipo.SelectedIndex = 23;
                    break;
                case "DESMONTAJE HORNO":
                    ottipo.SelectedIndex = 24;
                    break;
                case "CT MONTAJE ARCON":
                    ottipo.SelectedIndex = 25;
                    break;
                case "CT DESMONTAJE ARCON":
                    ottipo.SelectedIndex = 26;
                    break;
                case "CT MONTAJE HORNO":
                    ottipo.SelectedIndex = 27;
                    break;
                case "CT DESMONTAJE HORNO":
                    ottipo.SelectedIndex = 28;
                    break;
                case "REPARACION TALLER":
                    ottipo.SelectedIndex = 29;
                    break;
                case "REPARACION GARANTIA":
                    ottipo.SelectedIndex = 30;
                    break;
                default:
                    ottipo.SelectedIndex = 0;
                    break;
            }
        }

        private void otidbuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan
                e.Handled = true;
            }
        }       

        private void ottipo_SelectedValueChanged(object sender, EventArgs e)
        {
            if (otproyecto.Text.Equals("IKEA") ||
                    otproyecto.Text.Equals("DANONE VENDING") || otproyecto.Text.Equals("DANONE WATERS") ||
                    otproyecto.Text.Equals("HEI STC") || otproyecto.Text.Equals("HEI EVENTO") || 
                    otproyecto.Text.Equals("HEI EVENTO") ||
                    otproyecto.Text.Equals("CORECO") || otproyecto.Text.Equals("SCHWEPPES"))
            { return; }
            else
            {
                switch (ottipo.SelectedItem.ToString())
                {
                    case "CAF CON RECAMBIO":
                        otmo.Text = "39,45";
                        break;
                    case "CAF SIN RECAMBIO":
                        otmo.Text = "41,36";
                        break;
                    case "MOL CON RECAMBIO":
                        otmo.Text = "29,20";
                        break;
                    case "MOL SIN RECAMBIO":
                        otmo.Text = "30,60";
                        break;
                    case "PUESTA A PUNTO CAF":
                        otmo.Text = "58";
                        break;
                    case "PUESTA A PUNTO MOL":
                        otmo.Text = "10,50";
                        break;
                    case "MONTAJE":
                        otmo.Text = "96,85";
                        break;
                    case "DESMONTAJE":
                        otmo.Text = "47,47";
                        break;
                    case "CT MONTAJE":
                        otmo.Text = "53,71";
                        break;
                    case "CT DESMONTAJE":
                        otmo.Text = "53,71";
                        break;
                    case "LIMPIEZA":
                        otmo.Text = "39,55";
                        break;
                    case "CT MOL MONTAJE":
                        otmo.Text = "15,24";
                        break;
                    case "CT MOL DESMONTAJE":
                        otmo.Text = "15,24";
                        break;
                    case "REPARACION TALLER HORNO A":
                        otmo.Text = "21,00";
                        break;
                    case "REPARACION TALLER HORNO B":
                        otmo.Text = "29,75";
                        break;
                    case "REPARACION TALLER HORNO C":
                        otmo.Text = "29,75";
                        break;
                    case "REPUESTOS":
                        otmo.Text = "0";
                        break;
                    case "PUESTA A PUNTO HORNO":
                        otmo.Text = "15,24";
                        break;
                    case "PUESTA A PUNTO ARCON":
                        otmo.Text = "15,75";
                        break;
                    case "REPARACION TALLER ARCON":
                        otmo.Text = "15,24";
                        break;
                    case "MONTAJE ARCON":
                        otmo.Text = "0";
                        break;
                    case "DESMONTAJE ARCON":
                        otmo.Text = "0";
                        break;
                    case "MONTAJE HORNO":
                        otmo.Text = "0";
                        break;
                    case "DESMONTAJE HORNO":
                        otmo.Text = "0";
                        break;
                    case "CT MONTAJE ARCON":
                        otmo.Text = "0";
                        break;
                    case "CT DESMONTAJE ARCON":
                        otmo.Text = "0";
                        break;
                    case "CT MONTAJE HORNO":
                        otmo.Text = "0";
                        break;
                    case "CT DESMONTAJE HORNO":
                        otmo.Text = "0";
                        break;
                    case "REPARACION TALLER":
                        otmo.Text = "0";
                        break;
                    case "REPARACION GARANTIA":
                        otmo.Text = "0";
                        break;
                    default:
                        otmo.Text = "0";
                        break;
                }
            }
        }
        #endregion

        #region VACACIONES
        #region CREAR MOVIMIENTO VACACIONES
        private void vacCreGuardar_Click(object sender, EventArgs e)
        {
            int auxiliar = 0;
            if (validarDatosVacacionesCrear())
            {
                if (Convert.ToInt32(vacCreDAnteriores.Text) > 0)
                {
                    if (Convert.ToInt32(vacCreDCogidos.Text) > Convert.ToInt32(vacCreDAnteriores.Text))
                    {
                        auxiliar = Convert.ToInt32(vacCreDCogidos.Text) - Convert.ToInt32(vacCreDAnteriores.Text);
                        vacCreDAnteriores.Text = "0";
                        vacCreDActuales.Text = (Convert.ToInt32(vacCreDActuales.Text) - auxiliar).ToString();
                    }
                    else
                    {
                        vacCreDAnteriores.Text = 
                            (Convert.ToInt32(vacCreDAnteriores.Text) - Convert.ToInt32(vacCreDCogidos.Text)).ToString();
                    }
                }
                else
                {
                    vacCreDActuales.Text = 
                        (Convert.ToInt32(vacCreDActuales.Text) - Convert.ToInt32(vacCreDCogidos.Text)).ToString();
                }
                Vacaciones p = new Vacaciones();
                p.Name = vacCreNombre.SelectedItem.ToString();
                p.Ndias_anteriores = Convert.ToInt32(vacCreDAnteriores.Text);
                p.Ndias_actuales = Convert.ToInt32(vacCreDActuales.Text);
                p.Ndias_acumulados = p.Ndias_anteriores + p.Ndias_actuales;
                p.Ndias_cogidos = Convert.ToInt32(vacCreDCogidos.Text);
                //p.Saldo = p.Ndias_acumulados - p.Ndias_cogidos;
                p.Saldo = p.Ndias_acumulados;
                p.Description = vacCreDescripcion.Text;
                p.Fecha = Convert.ToInt32(vacCreFecha.Text);


                ProcessVacaciones proceso = new ProcessVacaciones();
                if (!proceso.insertarRegistro(p))
                {
                    MessageBox.Show("¡Error al guardar los datos!\n", "Advertencia",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("¡Guardado correcto!\n", "Información",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("¡No guardado, error en base de datos!\n", "Información",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        #endregion

        #region INFORME VACACIONES ESPECÍFICO
        private void vacInfVer_Click(object sender, EventArgs e)
        {
            int inicio = Convert.ToInt32(vacInfFeInicio.Value.ToString("yyyyMMdd"));
            int fin = Convert.ToInt32(vacInfFeFin.Value.ToString("yyyyMMdd"));

            if (inicio <= fin)
            {
                //List<List<Vacaciones>> todos = new List<List<Vacaciones>>();
                List<Vacaciones> lista = new List<Vacaciones>();
                ProcessVacaciones p = new ProcessVacaciones();
                lista = p.InformeBasicoVacaciones(inicio, fin, vacInfNombre.SelectedItem.ToString());

                Document doc = new Document(PageSize.LETTER);
                // Indicamos donde vamos a guardar el documento
                PdfWriter writer = PdfWriter.GetInstance(doc,
                                            new FileStream(@"D:\Usuario\Desktop\documento.pdf", FileMode.Create));
                doc.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
                doc.Open();
                int contador = 1;

                // Creamos una tabla que contendrá el nombre, apellido y país
                // de nuestros visitante.
                float[] columnWidths1 = { 5, 11, 14, 14, 14, 14, 14, 14 };
                PdfPTable tblPrueba = new PdfPTable(columnWidths1);
                tblPrueba.WidthPercentage = 100;
                tblPrueba.DefaultCell.Padding = 0;
                //tblPrueba.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                tblPrueba.AddCell("ID");
                tblPrueba.AddCell("FECHA");
                tblPrueba.AddCell("AÑO");
                tblPrueba.AddCell("AÑO");
                tblPrueba.AddCell("SALDO");
                tblPrueba.AddCell("ABONO");
                tblPrueba.AddCell("CARGO");
                tblPrueba.AddCell("SALDO");

                tblPrueba.AddCell("");
                tblPrueba.AddCell("");
                tblPrueba.AddCell("ANTERIOR");
                tblPrueba.AddCell("ACTUAL");
                tblPrueba.AddCell("ACTUAL");
                tblPrueba.AddCell("");
                tblPrueba.AddCell("");
                tblPrueba.AddCell("FINAL");
                doc.Add(tblPrueba);

                foreach (Vacaciones o in lista)
                {
                    // Creamos una tabla que contendrá el nombre, apellido y país
                    // de nuestros visitante.
                    float[] columnWidths2 = { 5, 11, 14, 14, 14, 14, 14, 14 };
                    tblPrueba = new PdfPTable(columnWidths2);
                    tblPrueba.WidthPercentage = 100;
                    tblPrueba.DefaultCell.Padding = 0;
                    tblPrueba.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;

                    tblPrueba.AddCell(contador.ToString());
                    tblPrueba.AddCell(o.Fecha.ToString());
                    tblPrueba.AddCell(o.Ndias_anteriores.ToString());
                    tblPrueba.AddCell(o.Ndias_actuales.ToString());
                    tblPrueba.AddCell(o.Ndias_acumulados.ToString());
                    tblPrueba.AddCell(" ");
                    tblPrueba.AddCell(" ");
                    tblPrueba.AddCell(" ");
                    doc.Add(tblPrueba);

                    // Creamos una tabla que contendrá el nombre, apellido y país
                    // de nuestros visitante.
                    float[] columnWidths4 = { 5, 17, 13, 13, 13, 13, 13, 13 }; ;
                    tblPrueba = new PdfPTable(columnWidths2);
                    tblPrueba.WidthPercentage = 100;
                    tblPrueba.DefaultCell.Padding = 0;
                    tblPrueba.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;

                    tblPrueba.AddCell(" ");
                    tblPrueba.AddCell(" ");
                    tblPrueba.AddCell(" ");
                    tblPrueba.AddCell(" ");
                    tblPrueba.AddCell(" ");
                    tblPrueba.AddCell(o.Ndias_abonados.ToString());
                    tblPrueba.AddCell(o.Ndias_cogidos.ToString());
                    tblPrueba.AddCell(o.Saldo.ToString());
                    doc.Add(tblPrueba);

                    contador++;
                }


                doc.Close();
                writer.Close();

                string pdfPath = @"D:\Usuario\Desktop\documento.pdf";
                Process.Start(pdfPath);
            }
            else
            {
                return;
            }
        }
        #endregion

        #region INFORME VACACIONES GENERAL
        private void vacInfVerTodos_Click(object sender, EventArgs e)
        {
            int inicio = Convert.ToInt32(vacInfFeInicio.Value.ToString("yyyyMMdd"));
            int fin = Convert.ToInt32(vacInfFeFin.Value.ToString("yyyyMMdd"));

            if (inicio <= fin)
            {
                List<Vacaciones> lista = new List<Vacaciones>();
                ProcessVacaciones p = new ProcessVacaciones();


                    Document doc = new Document(PageSize.LETTER);
                    // Indicamos donde vamos a guardar el documento
                    PdfWriter writer = PdfWriter.GetInstance(doc,
                                                new FileStream(@"D:\Usuario\Desktop\documento.pdf", FileMode.Create));
                    doc.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
                    doc.Open();

                foreach (string s in vacInfNombre.Items)
                {
                    lista = p.InformeBasicoVacaciones(inicio, fin, s);
                    int contador = 1;
                    doc.Add(Chunk.NEWLINE);
                    doc.Add(new Paragraph(s));
                    // Creamos una tabla que contendrá el nombre, apellido y país
                    // de nuestros visitante.
                    float[] columnWidths1 = { 5, 11, 14, 14, 14, 14, 14, 14 };
                    PdfPTable tblPrueba = new PdfPTable(columnWidths1);
                    tblPrueba.WidthPercentage = 100;
                    tblPrueba.DefaultCell.Padding = 0;
                    //tblPrueba.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    tblPrueba.AddCell("ID");
                    tblPrueba.AddCell("FECHA");
                    tblPrueba.AddCell("AÑO");
                    tblPrueba.AddCell("AÑO");
                    tblPrueba.AddCell("SALDO");
                    tblPrueba.AddCell("ABONO");
                    tblPrueba.AddCell("CARGO");
                    tblPrueba.AddCell("SALDO");

                    tblPrueba.AddCell("");
                    tblPrueba.AddCell("");
                    tblPrueba.AddCell("ANTERIOR");
                    tblPrueba.AddCell("ACTUAL");
                    tblPrueba.AddCell("ACTUAL");
                    tblPrueba.AddCell("");
                    tblPrueba.AddCell("");
                    tblPrueba.AddCell("FINAL");
                    doc.Add(tblPrueba);

                    foreach (Vacaciones o in lista)
                    {
                        // Creamos una tabla que contendrá el nombre, apellido y país
                        // de nuestros visitante.
                        float[] columnWidths2 = { 5, 11, 14, 14, 14, 14, 14, 14 };
                        tblPrueba = new PdfPTable(columnWidths2);
                        tblPrueba.WidthPercentage = 100;
                        tblPrueba.DefaultCell.Padding = 0;
                        tblPrueba.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;

                        tblPrueba.AddCell(contador.ToString());
                        tblPrueba.AddCell(o.Fecha.ToString());
                        tblPrueba.AddCell(o.Ndias_anteriores.ToString());
                        tblPrueba.AddCell(o.Ndias_actuales.ToString());
                        tblPrueba.AddCell(o.Ndias_acumulados.ToString());
                        tblPrueba.AddCell(" ");
                        tblPrueba.AddCell(" ");
                        tblPrueba.AddCell(" ");
                        doc.Add(tblPrueba);

                        // Creamos una tabla que contendrá el nombre, apellido y país
                        // de nuestros visitante.
                        float[] columnWidths4 = { 5, 17, 13, 13, 13, 13, 13, 13 }; ;
                        tblPrueba = new PdfPTable(columnWidths2);
                        tblPrueba.WidthPercentage = 100;
                        tblPrueba.DefaultCell.Padding = 0;
                        tblPrueba.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;

                        tblPrueba.AddCell(" ");
                        tblPrueba.AddCell(" ");
                        tblPrueba.AddCell(" ");
                        tblPrueba.AddCell(" ");
                        tblPrueba.AddCell(" ");
                        tblPrueba.AddCell(o.Ndias_abonados.ToString());
                        tblPrueba.AddCell(o.Ndias_cogidos.ToString());
                        tblPrueba.AddCell(o.Saldo.ToString());
                        doc.Add(tblPrueba);

                        contador++;
                    }
                }
                    doc.Close();
                    writer.Close();

                    string pdfPath = @"D:\Usuario\Desktop\documento.pdf";
                    Process.Start(pdfPath);
            }
            else
            {
                return;
            }
        } 
        #endregion
        #endregion

        private void radioIRDetalle_CheckedChanged(object sender, EventArgs e)
        {
            if (radioIRResumen.Checked == true)
                comboIROTallerObjeto.SelectedIndex = 1;
            else comboIROTallerObjeto.SelectedIndex = 0;
        }

        private void radioIRResumen_CheckedChanged(object sender, EventArgs e)
        {
            if (radioIRDetalle.Checked == true)
                comboIROTallerObjeto.SelectedIndex = 0;
            else comboIROTallerObjeto.SelectedIndex = 1;
        }

        #region TECAS VACACIONES
        private void vacCreDAnteriores_TextChanged(object sender, EventArgs e)
        {
            if (vacCreDAnteriores.Text.Equals(""))
            {
                vacCreDAnteriores.Text = "0";
                vacCreDAcumulados.Text = (Convert.ToInt32(vacCreDAnteriores.Text) +
                                        Convert.ToInt32(vacCreDActuales.Text)).ToString();
                vacCreDSaldo.Text = (Convert.ToInt32(vacCreDAcumulados.Text) -
                                        Convert.ToInt32(vacCreDCogidos.Text)).ToString();
            }
            else
            {
                vacCreDAcumulados.Text = (Convert.ToInt32(vacCreDAnteriores.Text) +
                        Convert.ToInt32(vacCreDActuales.Text)).ToString();
                vacCreDSaldo.Text = (Convert.ToInt32(vacCreDAcumulados.Text) -
                                        Convert.ToInt32(vacCreDCogidos.Text)).ToString();
            }
        }

        private void vacCreDActuales_TextChanged(object sender, EventArgs e)
        {
            if (vacCreDActuales.Text.Equals(""))
            {
                vacCreDActuales.Text = "0";
                vacCreDAcumulados.Text = (Convert.ToInt32(vacCreDAnteriores.Text) +
                        Convert.ToInt32(vacCreDActuales.Text)).ToString();
                vacCreDSaldo.Text = (Convert.ToInt32(vacCreDAcumulados.Text) -
                        Convert.ToInt32(vacCreDCogidos.Text)).ToString();
            }
            else
            {
                vacCreDAcumulados.Text = (Convert.ToInt32(vacCreDAnteriores.Text) +
                        Convert.ToInt32(vacCreDActuales.Text)).ToString();
                vacCreDSaldo.Text = (Convert.ToInt32(vacCreDAcumulados.Text) -
                                        Convert.ToInt32(vacCreDCogidos.Text)).ToString();
            }
        }

        private void vacCreDCogidos_TextChanged(object sender, EventArgs e)
        {
            if (vacCreDCogidos.Text.Equals(""))
            {
                vacCreDCogidos.Text = "0";
                vacCreDAcumulados.Text = (Convert.ToInt32(vacCreDAnteriores.Text) +
                                            Convert.ToInt32(vacCreDActuales.Text)).ToString();
                vacCreDSaldo.Text = (Convert.ToInt32(vacCreDAcumulados.Text) -
                        Convert.ToInt32(vacCreDCogidos.Text)).ToString();
            }
            else
            {
                vacCreDAcumulados.Text = (Convert.ToInt32(vacCreDAnteriores.Text) +
                        Convert.ToInt32(vacCreDActuales.Text)).ToString();
                vacCreDSaldo.Text = (Convert.ToInt32(vacCreDAcumulados.Text) -
                                        Convert.ToInt32(vacCreDCogidos.Text)).ToString();
            }
        }

        private void vacCreDDados_TextChanged(object sender, EventArgs e)
        {
            if (vacCreDDados.Text.Equals(""))
            {
                vacCreDDados.Text = "0";
                vacCreDAcumulados.Text = (Convert.ToInt32(vacCreDAnteriores.Text) +
                                            Convert.ToInt32(vacCreDActuales.Text)).ToString();
                vacCreDSaldo.Text = (Convert.ToInt32(vacCreDAcumulados.Text) +
                        Convert.ToInt32(vacCreDDados.Text)).ToString();
            }
            else
            {
                vacCreDAcumulados.Text = (Convert.ToInt32(vacCreDAnteriores.Text) +
                        Convert.ToInt32(vacCreDActuales.Text)).ToString();
                vacCreDSaldo.Text = (Convert.ToInt32(vacCreDAcumulados.Text) +
                                        Convert.ToInt32(vacCreDDados.Text)).ToString();
            }
        }

        private void vacCreDDados_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan
                e.Handled = true;
            }
        }

        private void vacCreDAnteriores_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan
                e.Handled = true;
            }
        }

        private void vacCreDActuales_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan
                e.Handled = true;
            }
        }

        private void vacCreDCogidos_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan
                e.Handled = true;
            }
        }
        #endregion

        private void vacCreNombre_SelectedValueChanged(object sender, EventArgs e)
        {
            vacCreDAnteriores.Text = "0";
            vacCreDActuales.Text = "0";
            vacCreDCogidos.Text = "0";
            vacCreDAcumulados.Text = "0";
            vacCreDSaldo.Text = "0";
            Vacaciones p = new Vacaciones();
            ProcessVacaciones proceso = new ProcessVacaciones();
            p = proceso.BuscarRegistroPorNombre(vacCreNombre.SelectedItem.ToString());

            if (p != null)
            {
                textInicio.Text = p.Saldo.ToString();
                vacCreDAnteriores.Text = p.Ndias_anteriores.ToString();
                vacCreDActuales.Text = p.Ndias_actuales.ToString();
                vacCreDAcumulados.Text = p.Ndias_acumulados.ToString();
                vacCreDSaldo.Text = p.Saldo.ToString();
            }
            else
            {
                textInicio.Text = "0";
                vacCreDAnteriores.Text = "0";
                vacCreDActuales.Text = "0";
                vacCreDAcumulados.Text = "0";
                vacCreDSaldo.Text = "0";
            }
        }

        private void panel13_Paint(object sender, PaintEventArgs e)
        {

        }

        #region ALMACEN - EQUIPOS SERIADOS
        #region CAFE - CAFENTO
        #region GUARDAR EQUIPO SERIADO
        private void BotAlEqSe_Click(object sender, EventArgs e)
        {
            if (validarDatosAlEqSeCa())
            {
                CafentoEquipos p = new CafentoEquipos();
                p.NombreMaterial = eqcafentnombre.Text;
                p.CodigoMaterial = eqcafentmaterial.Text;
                p.CodSAP = eqcafentsap.Text;
                p.CodAX = eqcafentax.Text;
                p.Origen = eqcafentorigen.Text;
                p.ODSEntrada = eqcafententrada.Text;
                p.Ubicacion = eqcafentubicacion.Text;
                p.Destino = eqcafentdestino.Text;
                p.ODSSalida = eqcafentsalida.Text;
                p.Estado = eqcafentestado.Text;
                p.Stock = Convert.ToInt32(eqcafentstock.Text);

                ProcessAlmEqSeriado proceso = new ProcessAlmEqSeriado();

                if (!proceso.insertarRegistro(p))
                {
                    MessageBox.Show("¡Error al guardar los datos!\n", "Advertencia",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("¡Guardado correcto!\n", "Información",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("¡No guardado, error en base de datos!\n", "Información",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        #endregion

        #region BUSCAR EQUIPO POR NS AX - NS SAP - COD MATERIAL
        private void BotAlEqSeBuscar_Click(object sender, EventArgs e)
        {
            CafentoEquipos p = new CafentoEquipos();
            p = null;
            ProcessAlmEqSeriado proceso = new ProcessAlmEqSeriado();
            if (!textBuscaAx.Text.Equals(""))
                p = proceso.BuscarRegistro(textBuscaAx.Text);
            else if (!textBuscaSap.Text.Equals("") && p == null)
                p = proceso.BuscarRegistro(textBuscaSap.Text);
            else if (!textBuscaMaterial.Text.Equals("") && p == null)
                p = proceso.BuscarRegistro(textBuscaMaterial.Text);
            else return;

            if (p != null)
            {
                eqcafentid.Text = Convert.ToString(p.Id);
                eqcafentnombre.Text = p.NombreMaterial;
                eqcafentmaterial.Text = p.CodigoMaterial;
                eqcafentsap.Text = p.CodSAP;
                eqcafentax.Text = p.CodAX;
                eqcafentorigen.Text = p.Origen;
                eqcafententrada.Text = p.ODSEntrada;
                eqcafentubicacion.Text = p.Ubicacion;
                eqcafentdestino.Text = p.Destino;
                eqcafentsalida.Text = p.ODSSalida;
                eqcafentestado.Text = p.Estado;
                eqcafentstock.Text = Convert.ToString(p.Stock);
            }
            else MessageBox.Show("nulo");
        }
        #endregion

        #region MODIFICAR EQUIPO
        private void BotAlEqSeModificar_Click(object sender, EventArgs e)
        {
            if (validarDatosAlEqSeCa() && !eqcafentid.Text.Equals(""))
            {
                CafentoEquipos p = new CafentoEquipos();
                p.Id = Convert.ToInt32(eqcafentid.Text);
                p.NombreMaterial = eqcafentnombre.Text;
                p.CodigoMaterial = eqcafentmaterial.Text;
                p.CodSAP = eqcafentsap.Text;
                p.CodAX = eqcafentax.Text;
                //if (!eqcafentorigen.Text.Equals(""))
                    p.Origen = eqcafentorigen.Text;
                //if (!eqcafententrada.Text.Equals(""))
                    p.ODSEntrada = eqcafententrada.Text;
                //if (!eqcafentubicacion.Text.Equals(""))
                    p.Ubicacion = eqcafentubicacion.Text;
                //if (!eqcafentdestino.Text.Equals(""))
                    p.Destino = eqcafentdestino.Text;
                //if (!eqcafentsalida.Text.Equals(""))
                    p.ODSSalida = eqcafentsalida.Text;
                //if (!eqcafentestado.Text.Equals(""))
                    p.Estado = eqcafentestado.Text;
                p.Stock = Convert.ToInt32(eqcafentstock.Text);

                ProcessAlmEqSeriado proceso = new ProcessAlmEqSeriado();

                if (!proceso.modificarRegistro(p))
                {
                    MessageBox.Show("¡Error al guardar los cambios!\n", "Advertencia",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("¡Se han guardado los cambios!\n", "Información",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        #endregion

        #region INFORME EQ SERIADOS
        private void BotAlEqSeInforme_Click(object sender, EventArgs e)
        {
            List<CafentoEquipos> lista = new List<CafentoEquipos>();
            ProcessAlmEqSeriado p = new ProcessAlmEqSeriado();
            lista = p.InformeBasicoRegistro();

            Document doc = new Document(PageSize.LETTER);
            // Indicamos donde vamos a guardar el documento
            PdfWriter writer = PdfWriter.GetInstance(doc,
                                        new FileStream(@"D:\Usuario\Desktop\documento.pdf", FileMode.Create));
            //doc.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
            doc.Open();/*
            doc.Add(Chunk.NEWLINE);
            doc.Add(Chunk.NEWLINE);
            doc.Add(new Paragraph(IROTallerencabezado.Text));
            doc.Add(Chunk.NEWLINE);*/
            doc.Add(new Paragraph("LISTA"));
            foreach (CafentoEquipos o in lista)
            {
                doc.Add(new Paragraph("MATERIAL " + o.NombreMaterial + " ÇODIGO " + o.CodigoMaterial + " AX " + o.CodAX));
                doc.Add(new Paragraph("SAP:" + o.CodSAP ));
                doc.Add(new Paragraph("ORIGEN:" + o.Origen));
                //doc.Add(new Paragraph("ORIGEN:" + o.Estado));
                doc.Add(Chunk.NEWLINE);
            }
            doc.Close();
            writer.Close();

            string pdfPath = @"D:\Usuario\Desktop\documento.pdf";
            Process.Start(pdfPath);
        } 
        #endregion

        #region TECLA STOCK - NUMERICA
        private void eqcafentstock_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar)) { e.Handled = false; }
            else if (Char.IsControl(e.KeyChar)) { e.Handled = false; }
            else { e.Handled = true; }
        }





        #endregion

        #endregion

        #endregion


    }
}
