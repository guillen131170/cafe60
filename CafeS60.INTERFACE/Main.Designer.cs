﻿namespace CafeS60.INTERFACE
{
    partial class Main
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.inicioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cafeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verTodosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.catalogadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.descatalogadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.todosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearRegistroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarRegistroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.entradaSalidaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevaEntradaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buscarEntradasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salidaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevaSalidaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buscarSalidasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.almacenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.equiposSeriadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cafeToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.cafentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearEquipoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verEquipoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarEquipoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.informeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.latitudToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.candelasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blackziToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deltaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.distribuidoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.botellerosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.entradasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.verToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.salidasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.verToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.equiposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.borrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.editarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imprimirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oTallerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.informesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.informesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.verToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.informesToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.básicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resúmenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarAceptadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarRechazadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.personalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vacacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.informeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardiasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cafeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pepsiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cervezaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.repuestosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.caféToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buscarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pepsiToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.buscarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.oTallerGeneralToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.berlysToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.danoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.danoneWaterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ikeaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.starbucksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.heinekenStcToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.heinekenEventoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.corecoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.corecoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.scheeppesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.plazaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eventoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buscarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.panelcafe = new System.Windows.Forms.TabControl();
            this.inicio = new System.Windows.Forms.TabPage();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.label61 = new System.Windows.Forms.Label();
            this.vercafe = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureVer = new System.Windows.Forms.PictureBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.textBoxdescription = new System.Windows.Forms.TextBox();
            this.textBoxmachine = new System.Windows.Forms.TextBox();
            this.textBoximg = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label27 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.textBoxid = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.textBoxcode = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.textBoxname = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.textBoxprice = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.textBoxstock = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxfamily = new System.Windows.Forms.TextBox();
            this.indiceregistros = new System.Windows.Forms.TextBox();
            this.anterior = new System.Windows.Forms.Button();
            this.siguiente = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.insertarcafe = new System.Windows.Forms.TabPage();
            this.panelinsertarcafe = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.guardarcafe = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.id = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.code = new System.Windows.Forms.TextBox();
            this.labelotro = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.TextBox();
            this.description = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.img = new System.Windows.Forms.TextBox();
            this.price = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.reference = new System.Windows.Forms.TextBox();
            this.stock = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.family = new System.Windows.Forms.TextBox();
            this.modificacafe = new System.Windows.Forms.TabPage();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pictureModificar = new System.Windows.Forms.PictureBox();
            this.textid = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label42 = new System.Windows.Forms.Label();
            this.buscador = new System.Windows.Forms.TextBox();
            this.textcode = new System.Windows.Forms.TextBox();
            this.textname = new System.Windows.Forms.TextBox();
            this.textdescription = new System.Windows.Forms.TextBox();
            this.textprice = new System.Windows.Forms.TextBox();
            this.textstock = new System.Windows.Forms.TextBox();
            this.textcafe = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.textreference = new System.Windows.Forms.TextBox();
            this.textimg = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.indiceregistrosUpdate = new System.Windows.Forms.TextBox();
            this.anteriorUpdate = new System.Windows.Forms.Button();
            this.siguienteUpdate = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.modificar = new System.Windows.Forms.Button();
            this.entradacafe = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.pictureEntrada = new System.Windows.Forms.PictureBox();
            this.entradaname = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dateentrada = new System.Windows.Forms.DateTimePicker();
            this.proveedorentrada = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.stockentrada = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.entradaid = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label43 = new System.Windows.Forms.Label();
            this.entradabuscador = new System.Windows.Forms.TextBox();
            this.entradacode = new System.Windows.Forms.TextBox();
            this.entradastock = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.entradaindice = new System.Windows.Forms.TextBox();
            this.entradaanterior = new System.Windows.Forms.Button();
            this.entradasiguiente = new System.Windows.Forms.Button();
            this.label44 = new System.Windows.Forms.Label();
            this.entradaguardar = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.salidacafe = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.label58 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.datesalida = new System.Windows.Forms.DateTimePicker();
            this.stocksalida = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.destinosalida = new System.Windows.Forms.TextBox();
            this.salidaid = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label49 = new System.Windows.Forms.Label();
            this.salidabuscador = new System.Windows.Forms.TextBox();
            this.salidacode = new System.Windows.Forms.TextBox();
            this.salidaname = new System.Windows.Forms.TextBox();
            this.salidastock = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.salidaindice = new System.Windows.Forms.TextBox();
            this.salidaanterior = new System.Windows.Forms.Button();
            this.salidasiguiente = new System.Windows.Forms.Button();
            this.label50 = new System.Windows.Forms.Label();
            this.salidaguardar = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.veres = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label62 = new System.Windows.Forms.Label();
            this.verentradaid = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label66 = new System.Windows.Forms.Label();
            this.verentradabuscador = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.verentradatipo = new System.Windows.Forms.TextBox();
            this.verentradaproveedor = new System.Windows.Forms.TextBox();
            this.verentradaproducto = new System.Windows.Forms.TextBox();
            this.verentradaunidades = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.verentradaindice = new System.Windows.Forms.TextBox();
            this.verentradaanterior = new System.Windows.Forms.Button();
            this.verentradasiguiente = new System.Windows.Forms.Button();
            this.label70 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.paDisBotParteCrear = new System.Windows.Forms.TabPage();
            this.pajj = new System.Windows.Forms.Panel();
            this.label63 = new System.Windows.Forms.Label();
            this.botguardar = new System.Windows.Forms.Button();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.botorden = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.botoferta = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.botsap = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.botcodigo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.botmaterial = new System.Windows.Forms.TextBox();
            this.label80 = new System.Windows.Forms.Label();
            this.botanyo = new System.Windows.Forms.TextBox();
            this.label76 = new System.Windows.Forms.Label();
            this.botecnico = new System.Windows.Forms.TextBox();
            this.botpictureimagen = new System.Windows.Forms.PictureBox();
            this.botestado = new System.Windows.Forms.TextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.botfecha = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.botid = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.botns = new System.Windows.Forms.TextBox();
            this.botgas = new System.Windows.Forms.TextBox();
            this.botttttt = new System.Windows.Forms.Label();
            this.botorigen = new System.Windows.Forms.TextBox();
            this.botmo = new System.Windows.Forms.TextBox();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.botimagen = new System.Windows.Forms.TextBox();
            this.botfabricante = new System.Windows.Forms.TextBox();
            this.botmarca = new System.Windows.Forms.TextBox();
            this.botmodelo = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.OTaller = new System.Windows.Forms.TabPage();
            this.panel24 = new System.Windows.Forms.Panel();
            this.otordenbuscar = new System.Windows.Forms.TextBox();
            this.label160 = new System.Windows.Forms.Label();
            this.otallerbuscar = new System.Windows.Forms.Button();
            this.otidbuscar = new System.Windows.Forms.TextBox();
            this.label159 = new System.Windows.Forms.Label();
            this.otallerimprimir = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label82 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.otcodproyecto = new System.Windows.Forms.TextBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.comboPrecio = new System.Windows.Forms.ComboBox();
            this.OTencuentra_repuestoCod = new System.Windows.Forms.ComboBox();
            this.OTencuentra_repuesto = new System.Windows.Forms.ComboBox();
            this.OTañadir_repuesto = new System.Windows.Forms.Button();
            this.OTbusca_repuesto = new System.Windows.Forms.TextBox();
            this.label158 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.OTrepuesto_uds = new System.Windows.Forms.TextBox();
            this.ottipo = new System.Windows.Forms.ComboBox();
            this.label121 = new System.Windows.Forms.Label();
            this.otcmi = new System.Windows.Forms.TextBox();
            this.labelcmi = new System.Windows.Forms.Label();
            this.otresultado = new System.Windows.Forms.TextBox();
            this.label105 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.otmo = new System.Windows.Forms.TextBox();
            this.label103 = new System.Windows.Forms.Label();
            this.otdespla = new System.Windows.Forms.TextBox();
            this.label102 = new System.Windows.Forms.Label();
            this.ottecnico = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.otoferta = new System.Windows.Forms.TextBox();
            this.label94 = new System.Windows.Forms.Label();
            this.otorden = new System.Windows.Forms.TextBox();
            this.label101 = new System.Windows.Forms.Label();
            this.otsap = new System.Windows.Forms.TextBox();
            this.otmaterial2 = new System.Windows.Forms.TextBox();
            this.label100 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.ottaller = new System.Windows.Forms.TextBox();
            this.label99 = new System.Windows.Forms.Label();
            this.otdesmontaje = new System.Windows.Forms.TextBox();
            this.label97 = new System.Windows.Forms.Label();
            this.otprovincia = new System.Windows.Forms.TextBox();
            this.otf2 = new System.Windows.Forms.TextBox();
            this.label96 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.otmontaje = new System.Windows.Forms.TextBox();
            this.otestado = new System.Windows.Forms.TextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.otf1 = new System.Windows.Forms.TextBox();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.otid = new System.Windows.Forms.TextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.otproyecto = new System.Windows.Forms.TextBox();
            this.otaveria = new System.Windows.Forms.TextBox();
            this.label92 = new System.Windows.Forms.Label();
            this.otpds = new System.Windows.Forms.TextBox();
            this.otrepuesto = new System.Windows.Forms.TextBox();
            this.label93 = new System.Windows.Forms.Label();
            this.otcliente = new System.Windows.Forms.TextBox();
            this.otmaterial1 = new System.Windows.Forms.TextBox();
            this.otax = new System.Windows.Forms.TextBox();
            this.label95 = new System.Windows.Forms.Label();
            this.IBOTaller = new System.Windows.Forms.TabPage();
            this.panel15 = new System.Windows.Forms.Panel();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.radioButtonAbierto = new System.Windows.Forms.RadioButton();
            this.radioButtonCerrado = new System.Windows.Forms.RadioButton();
            this.IBOTallerCerrar = new System.Windows.Forms.Button();
            this.label152 = new System.Windows.Forms.Label();
            this.label153 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.radioButtonRechazado = new System.Windows.Forms.RadioButton();
            this.radioButtonAceptado = new System.Windows.Forms.RadioButton();
            this.comboIBOTaller = new System.Windows.Forms.ComboBox();
            this.IBOTallerConsultar = new System.Windows.Forms.Button();
            this.DateIBOTallerFin = new System.Windows.Forms.DateTimePicker();
            this.DateIBOTallerInicio = new System.Windows.Forms.DateTimePicker();
            this.DisBotSal = new System.Windows.Forms.TabPage();
            this.salbotimprimir = new System.Windows.Forms.Button();
            this.salotguardar = new System.Windows.Forms.Button();
            this.label106 = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.label112 = new System.Windows.Forms.Label();
            this.salbottransportista = new System.Windows.Forms.TextBox();
            this.label111 = new System.Windows.Forms.Label();
            this.salbotorigen = new System.Windows.Forms.TextBox();
            this.label110 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.salbotcantidad = new System.Windows.Forms.TextBox();
            this.salbotdestino = new System.Windows.Forms.TextBox();
            this.salbotns = new System.Windows.Forms.TextBox();
            this.salbotfecha = new System.Windows.Forms.DateTimePicker();
            this.DisBotEnt = new System.Windows.Forms.TabPage();
            this.entbotimprimir = new System.Windows.Forms.Button();
            this.entbotguardar = new System.Windows.Forms.Button();
            this.label113 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.label114 = new System.Windows.Forms.Label();
            this.entbottransportista = new System.Windows.Forms.TextBox();
            this.label115 = new System.Windows.Forms.Label();
            this.entbotorigen = new System.Windows.Forms.TextBox();
            this.label116 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.entbotcantidad = new System.Windows.Forms.TextBox();
            this.entbotdestino = new System.Windows.Forms.TextBox();
            this.entbotns = new System.Windows.Forms.TextBox();
            this.entbotfecha = new System.Windows.Forms.DateTimePicker();
            this.DisBotLista = new System.Windows.Forms.TabPage();
            this.disBotListaImprimir = new System.Windows.Forms.Button();
            this.label120 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.label156 = new System.Windows.Forms.Label();
            this.comboIBotTipo = new System.Windows.Forms.ComboBox();
            this.label155 = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.ListaBotReparado = new System.Windows.Forms.RadioButton();
            this.ListaBotTodo = new System.Windows.Forms.RadioButton();
            this.ListaBotEntregado = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.ListaBotConBorde = new System.Windows.Forms.RadioButton();
            this.ListaBotSinBorde = new System.Windows.Forms.RadioButton();
            this.DateDisBotListaFeFin = new System.Windows.Forms.DateTimePicker();
            this.DateDisBotListaFeInicio = new System.Windows.Forms.DateTimePicker();
            this.IROTaller = new System.Windows.Forms.TabPage();
            this.panel19 = new System.Windows.Forms.Panel();
            this.label151 = new System.Windows.Forms.Label();
            this.comboIROTallerProyecto = new System.Windows.Forms.ComboBox();
            this.label128 = new System.Windows.Forms.Label();
            this.IROTallerdias = new System.Windows.Forms.TextBox();
            this.label127 = new System.Windows.Forms.Label();
            this.IROTallerencabezado = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.radioIRDetalle = new System.Windows.Forms.RadioButton();
            this.radioIRResumen = new System.Windows.Forms.RadioButton();
            this.label126 = new System.Windows.Forms.Label();
            this.comboIROTallerObjeto = new System.Windows.Forms.ComboBox();
            this.label125 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.comboIROTallerTecnico = new System.Windows.Forms.ComboBox();
            this.label122 = new System.Windows.Forms.Label();
            this.comboIROTallerTipo = new System.Windows.Forms.ComboBox();
            this.IROTallerConsultar = new System.Windows.Forms.Button();
            this.dateIROTallerFin = new System.Windows.Forms.DateTimePicker();
            this.dateIROTallerInicio = new System.Windows.Forms.DateTimePicker();
            this.VacCrear = new System.Windows.Forms.TabPage();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label129 = new System.Windows.Forms.Label();
            this.vacCreGuardar = new System.Windows.Forms.Button();
            this.panel21 = new System.Windows.Forms.Panel();
            this.vacCreNombre = new System.Windows.Forms.ComboBox();
            this.label141 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label149 = new System.Windows.Forms.Label();
            this.vacCreDDados = new System.Windows.Forms.TextBox();
            this.label150 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.textInicio = new System.Windows.Forms.TextBox();
            this.label140 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.vacCreDAnteriores = new System.Windows.Forms.TextBox();
            this.vacCreDAcumulados = new System.Windows.Forms.TextBox();
            this.label139 = new System.Windows.Forms.Label();
            this.vacCreDActuales = new System.Windows.Forms.TextBox();
            this.vacCreDSaldo = new System.Windows.Forms.TextBox();
            this.label137 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.vacCreDCogidos = new System.Windows.Forms.TextBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.vacCreFecha = new System.Windows.Forms.TextBox();
            this.label133 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label138 = new System.Windows.Forms.Label();
            this.vacCreDescripcion = new System.Windows.Forms.TextBox();
            this.VacInf = new System.Windows.Forms.TabPage();
            this.panel22 = new System.Windows.Forms.Panel();
            this.vacInfVerTodos = new System.Windows.Forms.Button();
            this.label145 = new System.Windows.Forms.Label();
            this.vacInfVer = new System.Windows.Forms.Button();
            this.panel23 = new System.Windows.Forms.Panel();
            this.label147 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.vacInfFeFin = new System.Windows.Forms.DateTimePicker();
            this.vacInfFeInicio = new System.Windows.Forms.DateTimePicker();
            this.vacInfNombre = new System.Windows.Forms.ComboBox();
            this.label146 = new System.Windows.Forms.Label();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel25 = new System.Windows.Forms.Panel();
            this.comboTipo = new System.Windows.Forms.ComboBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.RepCafRepEncontrado = new System.Windows.Forms.TextBox();
            this.label164 = new System.Windows.Forms.Label();
            this.RepCafBuscar1 = new System.Windows.Forms.Button();
            this.RepCafCodAXBuscar = new System.Windows.Forms.TextBox();
            this.RepCafCodSAPBuscar = new System.Windows.Forms.TextBox();
            this.label163 = new System.Windows.Forms.Label();
            this.label162 = new System.Windows.Forms.Label();
            this.RepCafIdBuscar = new System.Windows.Forms.TextBox();
            this.label161 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.RepCafCambiar = new System.Windows.Forms.Button();
            this.label166 = new System.Windows.Forms.Label();
            this.RepCafPrecio = new System.Windows.Forms.TextBox();
            this.comboBusPre = new System.Windows.Forms.ComboBox();
            this.comboBusCod = new System.Windows.Forms.ComboBox();
            this.comboBusNom = new System.Windows.Forms.ComboBox();
            this.RepCafNombreBuscar = new System.Windows.Forms.TextBox();
            this.label165 = new System.Windows.Forms.Label();
            this.AlmEqSe = new System.Windows.Forms.TabPage();
            this.panel26 = new System.Windows.Forms.Panel();
            this.BotAlEqSeInforme = new System.Windows.Forms.Button();
            this.BotAlEqSeModificar = new System.Windows.Forms.Button();
            this.panel27 = new System.Windows.Forms.Panel();
            this.textBuscaMaterial = new System.Windows.Forms.TextBox();
            this.label181 = new System.Windows.Forms.Label();
            this.textBuscaSap = new System.Windows.Forms.TextBox();
            this.label178 = new System.Windows.Forms.Label();
            this.BotAlEqSeBuscar = new System.Windows.Forms.Button();
            this.textBuscaAx = new System.Windows.Forms.TextBox();
            this.label180 = new System.Windows.Forms.Label();
            this.BotAlEqSe = new System.Windows.Forms.Button();
            this.label179 = new System.Windows.Forms.Label();
            this.label177 = new System.Windows.Forms.Label();
            this.label176 = new System.Windows.Forms.Label();
            this.label175 = new System.Windows.Forms.Label();
            this.label174 = new System.Windows.Forms.Label();
            this.label173 = new System.Windows.Forms.Label();
            this.label172 = new System.Windows.Forms.Label();
            this.label171 = new System.Windows.Forms.Label();
            this.label170 = new System.Windows.Forms.Label();
            this.label169 = new System.Windows.Forms.Label();
            this.label167 = new System.Windows.Forms.Label();
            this.label168 = new System.Windows.Forms.Label();
            this.eqcafentstock = new System.Windows.Forms.TextBox();
            this.eqcafentestado = new System.Windows.Forms.TextBox();
            this.eqcafentsalida = new System.Windows.Forms.TextBox();
            this.eqcafentdestino = new System.Windows.Forms.TextBox();
            this.eqcafentubicacion = new System.Windows.Forms.TextBox();
            this.eqcafententrada = new System.Windows.Forms.TextBox();
            this.eqcafentorigen = new System.Windows.Forms.TextBox();
            this.eqcafentax = new System.Windows.Forms.TextBox();
            this.eqcafentsap = new System.Windows.Forms.TextBox();
            this.eqcafentmaterial = new System.Windows.Forms.TextBox();
            this.eqcafentnombre = new System.Windows.Forms.TextBox();
            this.eqcafentid = new System.Windows.Forms.TextBox();
            this.eqTitulo = new System.Windows.Forms.Label();
            this.informeToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.básicoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.resumenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.panelcafe.SuspendLayout();
            this.inicio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.vercafe.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureVer)).BeginInit();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.insertarcafe.SuspendLayout();
            this.panelinsertarcafe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel11.SuspendLayout();
            this.modificacafe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureModificar)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.entradacafe.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEntrada)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.salidacafe.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.veres.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.paDisBotParteCrear.SuspendLayout();
            this.pajj.SuspendLayout();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.botpictureimagen)).BeginInit();
            this.OTaller.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel13.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.IBOTaller.SuspendLayout();
            this.panel15.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.DisBotSal.SuspendLayout();
            this.panel16.SuspendLayout();
            this.DisBotEnt.SuspendLayout();
            this.panel17.SuspendLayout();
            this.DisBotLista.SuspendLayout();
            this.panel18.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.IROTaller.SuspendLayout();
            this.panel19.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.VacCrear.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel21.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.VacInf.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.panel25.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.AlmEqSe.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel27.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.cafeToolStripMenuItem,
            this.almacenToolStripMenuItem,
            this.distribuidoresToolStripMenuItem,
            this.oTallerToolStripMenuItem,
            this.personalToolStripMenuItem,
            this.repuestosToolStripMenuItem,
            this.oTallerGeneralToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(926, 27);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inicioToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.toolStripMenuItem1.Image = global::CafeS60.INTERFACE.Properties.Resources.doc;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(89, 23);
            this.toolStripMenuItem1.Text = "General";
            // 
            // inicioToolStripMenuItem
            // 
            this.inicioToolStripMenuItem.Image = global::CafeS60.INTERFACE.Properties.Resources.inicio;
            this.inicioToolStripMenuItem.Name = "inicioToolStripMenuItem";
            this.inicioToolStripMenuItem.Size = new System.Drawing.Size(114, 24);
            this.inicioToolStripMenuItem.Text = "Inicio";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Image = global::CafeS60.INTERFACE.Properties.Resources.cerrar2;
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(114, 24);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // cafeToolStripMenuItem
            // 
            this.cafeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.verToolStripMenuItem});
            this.cafeToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cafeToolStripMenuItem.Image")));
            this.cafeToolStripMenuItem.Name = "cafeToolStripMenuItem";
            this.cafeToolStripMenuItem.Size = new System.Drawing.Size(67, 23);
            this.cafeToolStripMenuItem.Text = "Café";
            // 
            // verToolStripMenuItem
            // 
            this.verToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.verTodosToolStripMenuItem,
            this.crearRegistroToolStripMenuItem,
            this.modificarRegistroToolStripMenuItem,
            this.entradaSalidaToolStripMenuItem,
            this.salidaToolStripMenuItem});
            this.verToolStripMenuItem.Image = global::CafeS60.INTERFACE.Properties.Resources.gestion;
            this.verToolStripMenuItem.Name = "verToolStripMenuItem";
            this.verToolStripMenuItem.Size = new System.Drawing.Size(165, 24);
            this.verToolStripMenuItem.Text = "Movimientos";
            // 
            // verTodosToolStripMenuItem
            // 
            this.verTodosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.catalogadosToolStripMenuItem,
            this.descatalogadosToolStripMenuItem,
            this.todosToolStripMenuItem});
            this.verTodosToolStripMenuItem.Image = global::CafeS60.INTERFACE.Properties.Resources.ojo2;
            this.verTodosToolStripMenuItem.Name = "verTodosToolStripMenuItem";
            this.verTodosToolStripMenuItem.Size = new System.Drawing.Size(209, 24);
            this.verTodosToolStripMenuItem.Text = "Ver productos";
            // 
            // catalogadosToolStripMenuItem
            // 
            this.catalogadosToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.catalogadosToolStripMenuItem.Image = global::CafeS60.INTERFACE.Properties.Resources.verde;
            this.catalogadosToolStripMenuItem.Name = "catalogadosToolStripMenuItem";
            this.catalogadosToolStripMenuItem.Size = new System.Drawing.Size(184, 24);
            this.catalogadosToolStripMenuItem.Text = "Catalogados";
            this.catalogadosToolStripMenuItem.Click += new System.EventHandler(this.catalogadosToolStripMenuItem_Click);
            // 
            // descatalogadosToolStripMenuItem
            // 
            this.descatalogadosToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.descatalogadosToolStripMenuItem.Image = global::CafeS60.INTERFACE.Properties.Resources.rojo;
            this.descatalogadosToolStripMenuItem.Name = "descatalogadosToolStripMenuItem";
            this.descatalogadosToolStripMenuItem.Size = new System.Drawing.Size(184, 24);
            this.descatalogadosToolStripMenuItem.Text = "Descatalogados";
            this.descatalogadosToolStripMenuItem.Click += new System.EventHandler(this.descatalogadosToolStripMenuItem_Click);
            // 
            // todosToolStripMenuItem
            // 
            this.todosToolStripMenuItem.Image = global::CafeS60.INTERFACE.Properties.Resources.negro;
            this.todosToolStripMenuItem.Name = "todosToolStripMenuItem";
            this.todosToolStripMenuItem.Size = new System.Drawing.Size(184, 24);
            this.todosToolStripMenuItem.Text = "Todos";
            this.todosToolStripMenuItem.Click += new System.EventHandler(this.todosToolStripMenuItem_Click);
            // 
            // crearRegistroToolStripMenuItem
            // 
            this.crearRegistroToolStripMenuItem.Image = global::CafeS60.INTERFACE.Properties.Resources.papel;
            this.crearRegistroToolStripMenuItem.Name = "crearRegistroToolStripMenuItem";
            this.crearRegistroToolStripMenuItem.Size = new System.Drawing.Size(209, 24);
            this.crearRegistroToolStripMenuItem.Text = "Crear producto";
            this.crearRegistroToolStripMenuItem.Click += new System.EventHandler(this.crearRegistroToolStripMenuItem_Click);
            // 
            // modificarRegistroToolStripMenuItem
            // 
            this.modificarRegistroToolStripMenuItem.Image = global::CafeS60.INTERFACE.Properties.Resources.editar1;
            this.modificarRegistroToolStripMenuItem.Name = "modificarRegistroToolStripMenuItem";
            this.modificarRegistroToolStripMenuItem.Size = new System.Drawing.Size(209, 24);
            this.modificarRegistroToolStripMenuItem.Text = "Modificar producto";
            this.modificarRegistroToolStripMenuItem.Click += new System.EventHandler(this.modificarRegistroToolStripMenuItem_Click);
            // 
            // entradaSalidaToolStripMenuItem
            // 
            this.entradaSalidaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevaEntradaToolStripMenuItem,
            this.buscarEntradasToolStripMenuItem});
            this.entradaSalidaToolStripMenuItem.Image = global::CafeS60.INTERFACE.Properties.Resources.mas;
            this.entradaSalidaToolStripMenuItem.Name = "entradaSalidaToolStripMenuItem";
            this.entradaSalidaToolStripMenuItem.Size = new System.Drawing.Size(209, 24);
            this.entradaSalidaToolStripMenuItem.Text = "Entrada";
            this.entradaSalidaToolStripMenuItem.Click += new System.EventHandler(this.entradaSalidaToolStripMenuItem_Click);
            // 
            // nuevaEntradaToolStripMenuItem
            // 
            this.nuevaEntradaToolStripMenuItem.Image = global::CafeS60.INTERFACE.Properties.Resources.new3;
            this.nuevaEntradaToolStripMenuItem.Name = "nuevaEntradaToolStripMenuItem";
            this.nuevaEntradaToolStripMenuItem.Size = new System.Drawing.Size(184, 24);
            this.nuevaEntradaToolStripMenuItem.Text = "Nueva entrada";
            this.nuevaEntradaToolStripMenuItem.Click += new System.EventHandler(this.nuevaEntradaToolStripMenuItem_Click);
            // 
            // buscarEntradasToolStripMenuItem
            // 
            this.buscarEntradasToolStripMenuItem.Image = global::CafeS60.INTERFACE.Properties.Resources.lupa;
            this.buscarEntradasToolStripMenuItem.Name = "buscarEntradasToolStripMenuItem";
            this.buscarEntradasToolStripMenuItem.Size = new System.Drawing.Size(184, 24);
            this.buscarEntradasToolStripMenuItem.Text = "Buscar entradas";
            this.buscarEntradasToolStripMenuItem.Click += new System.EventHandler(this.buscarEntradasToolStripMenuItem_Click);
            // 
            // salidaToolStripMenuItem
            // 
            this.salidaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevaSalidaToolStripMenuItem,
            this.buscarSalidasToolStripMenuItem});
            this.salidaToolStripMenuItem.Image = global::CafeS60.INTERFACE.Properties.Resources.menos;
            this.salidaToolStripMenuItem.Name = "salidaToolStripMenuItem";
            this.salidaToolStripMenuItem.Size = new System.Drawing.Size(209, 24);
            this.salidaToolStripMenuItem.Text = "Salida";
            this.salidaToolStripMenuItem.Click += new System.EventHandler(this.salidaToolStripMenuItem_Click);
            // 
            // nuevaSalidaToolStripMenuItem
            // 
            this.nuevaSalidaToolStripMenuItem.Image = global::CafeS60.INTERFACE.Properties.Resources.new3;
            this.nuevaSalidaToolStripMenuItem.Name = "nuevaSalidaToolStripMenuItem";
            this.nuevaSalidaToolStripMenuItem.Size = new System.Drawing.Size(171, 24);
            this.nuevaSalidaToolStripMenuItem.Text = "Nueva salida";
            this.nuevaSalidaToolStripMenuItem.Click += new System.EventHandler(this.nuevaSalidaToolStripMenuItem_Click);
            // 
            // buscarSalidasToolStripMenuItem
            // 
            this.buscarSalidasToolStripMenuItem.Image = global::CafeS60.INTERFACE.Properties.Resources.lupa;
            this.buscarSalidasToolStripMenuItem.Name = "buscarSalidasToolStripMenuItem";
            this.buscarSalidasToolStripMenuItem.Size = new System.Drawing.Size(171, 24);
            this.buscarSalidasToolStripMenuItem.Text = "Buscar salidas";
            this.buscarSalidasToolStripMenuItem.Click += new System.EventHandler(this.buscarSalidasToolStripMenuItem_Click);
            // 
            // almacenToolStripMenuItem
            // 
            this.almacenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.equiposSeriadosToolStripMenuItem});
            this.almacenToolStripMenuItem.Image = global::CafeS60.INTERFACE.Properties.Resources._87231;
            this.almacenToolStripMenuItem.Name = "almacenToolStripMenuItem";
            this.almacenToolStripMenuItem.Size = new System.Drawing.Size(95, 23);
            this.almacenToolStripMenuItem.Text = "Almacén";
            // 
            // equiposSeriadosToolStripMenuItem
            // 
            this.equiposSeriadosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cafeToolStripMenuItem2});
            this.equiposSeriadosToolStripMenuItem.Name = "equiposSeriadosToolStripMenuItem";
            this.equiposSeriadosToolStripMenuItem.Size = new System.Drawing.Size(190, 24);
            this.equiposSeriadosToolStripMenuItem.Text = "Equipos seriados";
            // 
            // cafeToolStripMenuItem2
            // 
            this.cafeToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cafentoToolStripMenuItem,
            this.latitudToolStripMenuItem,
            this.candelasToolStripMenuItem,
            this.blackziToolStripMenuItem,
            this.deltaToolStripMenuItem});
            this.cafeToolStripMenuItem2.Name = "cafeToolStripMenuItem2";
            this.cafeToolStripMenuItem2.Size = new System.Drawing.Size(108, 24);
            this.cafeToolStripMenuItem2.Text = "Cafe";
            // 
            // cafentoToolStripMenuItem
            // 
            this.cafentoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.crearEquipoToolStripMenuItem,
            this.verEquipoToolStripMenuItem,
            this.modificarEquipoToolStripMenuItem,
            this.informeToolStripMenuItem1});
            this.cafentoToolStripMenuItem.Name = "cafentoToolStripMenuItem";
            this.cafentoToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.cafentoToolStripMenuItem.Text = "Cafento";
            this.cafentoToolStripMenuItem.Click += new System.EventHandler(this.cafentoToolStripMenuItem_Click);
            // 
            // crearEquipoToolStripMenuItem
            // 
            this.crearEquipoToolStripMenuItem.Image = global::CafeS60.INTERFACE.Properties.Resources.new3;
            this.crearEquipoToolStripMenuItem.Name = "crearEquipoToolStripMenuItem";
            this.crearEquipoToolStripMenuItem.Size = new System.Drawing.Size(143, 24);
            this.crearEquipoToolStripMenuItem.Text = "Insertar";
            this.crearEquipoToolStripMenuItem.Click += new System.EventHandler(this.crearEquipoToolStripMenuItem_Click);
            // 
            // verEquipoToolStripMenuItem
            // 
            this.verEquipoToolStripMenuItem.Image = global::CafeS60.INTERFACE.Properties.Resources.lupa;
            this.verEquipoToolStripMenuItem.Name = "verEquipoToolStripMenuItem";
            this.verEquipoToolStripMenuItem.Size = new System.Drawing.Size(143, 24);
            this.verEquipoToolStripMenuItem.Text = "Buscar";
            this.verEquipoToolStripMenuItem.Click += new System.EventHandler(this.verEquipoToolStripMenuItem_Click);
            // 
            // modificarEquipoToolStripMenuItem
            // 
            this.modificarEquipoToolStripMenuItem.Image = global::CafeS60.INTERFACE.Properties.Resources.editar1;
            this.modificarEquipoToolStripMenuItem.Name = "modificarEquipoToolStripMenuItem";
            this.modificarEquipoToolStripMenuItem.Size = new System.Drawing.Size(143, 24);
            this.modificarEquipoToolStripMenuItem.Text = "Modificar";
            this.modificarEquipoToolStripMenuItem.Click += new System.EventHandler(this.modificarEquipoToolStripMenuItem_Click);
            // 
            // informeToolStripMenuItem1
            // 
            this.informeToolStripMenuItem1.Image = global::CafeS60.INTERFACE.Properties.Resources._94759;
            this.informeToolStripMenuItem1.Name = "informeToolStripMenuItem1";
            this.informeToolStripMenuItem1.Size = new System.Drawing.Size(143, 24);
            this.informeToolStripMenuItem1.Text = "Informe";
            this.informeToolStripMenuItem1.Click += new System.EventHandler(this.informeToolStripMenuItem1_Click);
            // 
            // latitudToolStripMenuItem
            // 
            this.latitudToolStripMenuItem.Name = "latitudToolStripMenuItem";
            this.latitudToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.latitudToolStripMenuItem.Text = "Latitud";
            // 
            // candelasToolStripMenuItem
            // 
            this.candelasToolStripMenuItem.Name = "candelasToolStripMenuItem";
            this.candelasToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.candelasToolStripMenuItem.Text = "Candelas";
            // 
            // blackziToolStripMenuItem
            // 
            this.blackziToolStripMenuItem.Name = "blackziToolStripMenuItem";
            this.blackziToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.blackziToolStripMenuItem.Text = "Blackzi";
            // 
            // deltaToolStripMenuItem
            // 
            this.deltaToolStripMenuItem.Name = "deltaToolStripMenuItem";
            this.deltaToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.deltaToolStripMenuItem.Text = "Delta";
            // 
            // distribuidoresToolStripMenuItem
            // 
            this.distribuidoresToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.botellerosToolStripMenuItem});
            this.distribuidoresToolStripMenuItem.Image = global::CafeS60.INTERFACE.Properties.Resources.entrar1;
            this.distribuidoresToolStripMenuItem.Name = "distribuidoresToolStripMenuItem";
            this.distribuidoresToolStripMenuItem.Size = new System.Drawing.Size(131, 23);
            this.distribuidoresToolStripMenuItem.Text = "Distribuidores";
            // 
            // botellerosToolStripMenuItem
            // 
            this.botellerosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.entradasToolStripMenuItem,
            this.salidasToolStripMenuItem,
            this.equiposToolStripMenuItem});
            this.botellerosToolStripMenuItem.Name = "botellerosToolStripMenuItem";
            this.botellerosToolStripMenuItem.Size = new System.Drawing.Size(146, 24);
            this.botellerosToolStripMenuItem.Text = "Botelleros";
            // 
            // entradasToolStripMenuItem
            // 
            this.entradasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.crearToolStripMenuItem2,
            this.verToolStripMenuItem4});
            this.entradasToolStripMenuItem.Name = "entradasToolStripMenuItem";
            this.entradasToolStripMenuItem.Size = new System.Drawing.Size(180, 24);
            this.entradasToolStripMenuItem.Text = "Entradas";
            // 
            // crearToolStripMenuItem2
            // 
            this.crearToolStripMenuItem2.Name = "crearToolStripMenuItem2";
            this.crearToolStripMenuItem2.Size = new System.Drawing.Size(115, 24);
            this.crearToolStripMenuItem2.Text = "Crear";
            this.crearToolStripMenuItem2.Click += new System.EventHandler(this.crearToolStripMenuItem2_Click);
            // 
            // verToolStripMenuItem4
            // 
            this.verToolStripMenuItem4.Name = "verToolStripMenuItem4";
            this.verToolStripMenuItem4.Size = new System.Drawing.Size(115, 24);
            this.verToolStripMenuItem4.Text = "Ver";
            // 
            // salidasToolStripMenuItem
            // 
            this.salidasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.crearToolStripMenuItem1,
            this.verToolStripMenuItem3});
            this.salidasToolStripMenuItem.Name = "salidasToolStripMenuItem";
            this.salidasToolStripMenuItem.Size = new System.Drawing.Size(180, 24);
            this.salidasToolStripMenuItem.Text = "Salidas";
            // 
            // crearToolStripMenuItem1
            // 
            this.crearToolStripMenuItem1.Name = "crearToolStripMenuItem1";
            this.crearToolStripMenuItem1.Size = new System.Drawing.Size(115, 24);
            this.crearToolStripMenuItem1.Text = "Crear";
            this.crearToolStripMenuItem1.Click += new System.EventHandler(this.crearToolStripMenuItem1_Click);
            // 
            // verToolStripMenuItem3
            // 
            this.verToolStripMenuItem3.Name = "verToolStripMenuItem3";
            this.verToolStripMenuItem3.Size = new System.Drawing.Size(115, 24);
            this.verToolStripMenuItem3.Text = "Ver";
            // 
            // equiposToolStripMenuItem
            // 
            this.equiposToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevoToolStripMenuItem,
            this.borrarToolStripMenuItem,
            this.verToolStripMenuItem1,
            this.editarToolStripMenuItem,
            this.imprimirToolStripMenuItem});
            this.equiposToolStripMenuItem.Name = "equiposToolStripMenuItem";
            this.equiposToolStripMenuItem.Size = new System.Drawing.Size(180, 24);
            this.equiposToolStripMenuItem.Text = "Partes de taller";
            // 
            // nuevoToolStripMenuItem
            // 
            this.nuevoToolStripMenuItem.Name = "nuevoToolStripMenuItem";
            this.nuevoToolStripMenuItem.Size = new System.Drawing.Size(137, 24);
            this.nuevoToolStripMenuItem.Text = "Crear";
            this.nuevoToolStripMenuItem.Click += new System.EventHandler(this.nuevoToolStripMenuItem_Click);
            // 
            // borrarToolStripMenuItem
            // 
            this.borrarToolStripMenuItem.Name = "borrarToolStripMenuItem";
            this.borrarToolStripMenuItem.Size = new System.Drawing.Size(137, 24);
            this.borrarToolStripMenuItem.Text = "Borrar";
            // 
            // verToolStripMenuItem1
            // 
            this.verToolStripMenuItem1.Name = "verToolStripMenuItem1";
            this.verToolStripMenuItem1.Size = new System.Drawing.Size(137, 24);
            this.verToolStripMenuItem1.Text = "Ver";
            // 
            // editarToolStripMenuItem
            // 
            this.editarToolStripMenuItem.Name = "editarToolStripMenuItem";
            this.editarToolStripMenuItem.Size = new System.Drawing.Size(137, 24);
            this.editarToolStripMenuItem.Text = "Editar";
            // 
            // imprimirToolStripMenuItem
            // 
            this.imprimirToolStripMenuItem.Name = "imprimirToolStripMenuItem";
            this.imprimirToolStripMenuItem.Size = new System.Drawing.Size(137, 24);
            this.imprimirToolStripMenuItem.Text = "Imprimir";
            this.imprimirToolStripMenuItem.Click += new System.EventHandler(this.imprimirToolStripMenuItem_Click);
            // 
            // oTallerToolStripMenuItem
            // 
            this.oTallerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.crearToolStripMenuItem,
            this.informesToolStripMenuItem,
            this.informesToolStripMenuItem1,
            this.verToolStripMenuItem2,
            this.informesToolStripMenuItem2,
            this.cerrarAceptadoToolStripMenuItem,
            this.cerrarRechazadoToolStripMenuItem});
            this.oTallerToolStripMenuItem.Image = global::CafeS60.INTERFACE.Properties.Resources.cafe;
            this.oTallerToolStripMenuItem.Name = "oTallerToolStripMenuItem";
            this.oTallerToolStripMenuItem.Size = new System.Drawing.Size(105, 23);
            this.oTallerToolStripMenuItem.Text = "TallerCafé";
            // 
            // crearToolStripMenuItem
            // 
            this.crearToolStripMenuItem.Name = "crearToolStripMenuItem";
            this.crearToolStripMenuItem.Size = new System.Drawing.Size(195, 24);
            this.crearToolStripMenuItem.Text = "Crear";
            this.crearToolStripMenuItem.Click += new System.EventHandler(this.crearToolStripMenuItem_Click);
            // 
            // informesToolStripMenuItem
            // 
            this.informesToolStripMenuItem.Name = "informesToolStripMenuItem";
            this.informesToolStripMenuItem.Size = new System.Drawing.Size(195, 24);
            this.informesToolStripMenuItem.Text = "Borrar";
            // 
            // informesToolStripMenuItem1
            // 
            this.informesToolStripMenuItem1.Name = "informesToolStripMenuItem1";
            this.informesToolStripMenuItem1.Size = new System.Drawing.Size(195, 24);
            this.informesToolStripMenuItem1.Text = "Editar";
            // 
            // verToolStripMenuItem2
            // 
            this.verToolStripMenuItem2.Name = "verToolStripMenuItem2";
            this.verToolStripMenuItem2.Size = new System.Drawing.Size(195, 24);
            this.verToolStripMenuItem2.Text = "Buscar";
            this.verToolStripMenuItem2.Click += new System.EventHandler(this.verToolStripMenuItem2_Click);
            // 
            // informesToolStripMenuItem2
            // 
            this.informesToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.básicoToolStripMenuItem,
            this.resúmenToolStripMenuItem});
            this.informesToolStripMenuItem2.Name = "informesToolStripMenuItem2";
            this.informesToolStripMenuItem2.Size = new System.Drawing.Size(195, 24);
            this.informesToolStripMenuItem2.Text = "Informes";
            // 
            // básicoToolStripMenuItem
            // 
            this.básicoToolStripMenuItem.Name = "básicoToolStripMenuItem";
            this.básicoToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.básicoToolStripMenuItem.Text = "Básico";
            this.básicoToolStripMenuItem.Click += new System.EventHandler(this.básicoToolStripMenuItem_Click);
            // 
            // resúmenToolStripMenuItem
            // 
            this.resúmenToolStripMenuItem.Name = "resúmenToolStripMenuItem";
            this.resúmenToolStripMenuItem.Size = new System.Drawing.Size(180, 24);
            this.resúmenToolStripMenuItem.Text = "Resumen";
            this.resúmenToolStripMenuItem.Click += new System.EventHandler(this.resúmenToolStripMenuItem_Click);
            // 
            // cerrarAceptadoToolStripMenuItem
            // 
            this.cerrarAceptadoToolStripMenuItem.Name = "cerrarAceptadoToolStripMenuItem";
            this.cerrarAceptadoToolStripMenuItem.Size = new System.Drawing.Size(195, 24);
            this.cerrarAceptadoToolStripMenuItem.Text = "Cerrar aceptado";
            this.cerrarAceptadoToolStripMenuItem.Click += new System.EventHandler(this.cerrarAceptadoToolStripMenuItem_Click);
            // 
            // cerrarRechazadoToolStripMenuItem
            // 
            this.cerrarRechazadoToolStripMenuItem.Name = "cerrarRechazadoToolStripMenuItem";
            this.cerrarRechazadoToolStripMenuItem.Size = new System.Drawing.Size(195, 24);
            this.cerrarRechazadoToolStripMenuItem.Text = "Cerrar rechazado";
            this.cerrarRechazadoToolStripMenuItem.Click += new System.EventHandler(this.cerrarRechazadoToolStripMenuItem_Click);
            // 
            // personalToolStripMenuItem
            // 
            this.personalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vacacionesToolStripMenuItem,
            this.guardiasToolStripMenuItem});
            this.personalToolStripMenuItem.Image = global::CafeS60.INTERFACE.Properties.Resources._126413;
            this.personalToolStripMenuItem.Name = "personalToolStripMenuItem";
            this.personalToolStripMenuItem.Size = new System.Drawing.Size(95, 23);
            this.personalToolStripMenuItem.Text = "Personal";
            // 
            // vacacionesToolStripMenuItem
            // 
            this.vacacionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.crearToolStripMenuItem3,
            this.eliminarToolStripMenuItem,
            this.editarToolStripMenuItem1,
            this.consultarToolStripMenuItem,
            this.informeToolStripMenuItem});
            this.vacacionesToolStripMenuItem.Name = "vacacionesToolStripMenuItem";
            this.vacacionesToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.vacacionesToolStripMenuItem.Text = "Vacaciones";
            // 
            // crearToolStripMenuItem3
            // 
            this.crearToolStripMenuItem3.Name = "crearToolStripMenuItem3";
            this.crearToolStripMenuItem3.Size = new System.Drawing.Size(141, 24);
            this.crearToolStripMenuItem3.Text = "Crear";
            this.crearToolStripMenuItem3.Click += new System.EventHandler(this.crearToolStripMenuItem3_Click);
            // 
            // eliminarToolStripMenuItem
            // 
            this.eliminarToolStripMenuItem.Name = "eliminarToolStripMenuItem";
            this.eliminarToolStripMenuItem.Size = new System.Drawing.Size(141, 24);
            this.eliminarToolStripMenuItem.Text = "Eliminar";
            // 
            // editarToolStripMenuItem1
            // 
            this.editarToolStripMenuItem1.Name = "editarToolStripMenuItem1";
            this.editarToolStripMenuItem1.Size = new System.Drawing.Size(141, 24);
            this.editarToolStripMenuItem1.Text = "Editar";
            // 
            // consultarToolStripMenuItem
            // 
            this.consultarToolStripMenuItem.Name = "consultarToolStripMenuItem";
            this.consultarToolStripMenuItem.Size = new System.Drawing.Size(141, 24);
            this.consultarToolStripMenuItem.Text = "Consultar";
            this.consultarToolStripMenuItem.Click += new System.EventHandler(this.consultarToolStripMenuItem_Click);
            // 
            // informeToolStripMenuItem
            // 
            this.informeToolStripMenuItem.Name = "informeToolStripMenuItem";
            this.informeToolStripMenuItem.Size = new System.Drawing.Size(141, 24);
            this.informeToolStripMenuItem.Text = "Informe";
            this.informeToolStripMenuItem.Click += new System.EventHandler(this.informeToolStripMenuItem_Click);
            // 
            // guardiasToolStripMenuItem
            // 
            this.guardiasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cafeToolStripMenuItem1,
            this.pepsiToolStripMenuItem,
            this.cervezaToolStripMenuItem});
            this.guardiasToolStripMenuItem.Name = "guardiasToolStripMenuItem";
            this.guardiasToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.guardiasToolStripMenuItem.Text = "Guardias";
            // 
            // cafeToolStripMenuItem1
            // 
            this.cafeToolStripMenuItem1.Name = "cafeToolStripMenuItem1";
            this.cafeToolStripMenuItem1.Size = new System.Drawing.Size(132, 24);
            this.cafeToolStripMenuItem1.Text = "Cafe";
            // 
            // pepsiToolStripMenuItem
            // 
            this.pepsiToolStripMenuItem.Name = "pepsiToolStripMenuItem";
            this.pepsiToolStripMenuItem.Size = new System.Drawing.Size(132, 24);
            this.pepsiToolStripMenuItem.Text = "Pepsi";
            // 
            // cervezaToolStripMenuItem
            // 
            this.cervezaToolStripMenuItem.Name = "cervezaToolStripMenuItem";
            this.cervezaToolStripMenuItem.Size = new System.Drawing.Size(132, 24);
            this.cervezaToolStripMenuItem.Text = "Cerveza";
            // 
            // repuestosToolStripMenuItem
            // 
            this.repuestosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.caféToolStripMenuItem,
            this.pepsiToolStripMenuItem1});
            this.repuestosToolStripMenuItem.Name = "repuestosToolStripMenuItem";
            this.repuestosToolStripMenuItem.Size = new System.Drawing.Size(89, 23);
            this.repuestosToolStripMenuItem.Text = "Repuestos";
            // 
            // caféToolStripMenuItem
            // 
            this.caféToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buscarToolStripMenuItem});
            this.caféToolStripMenuItem.Name = "caféToolStripMenuItem";
            this.caféToolStripMenuItem.Size = new System.Drawing.Size(114, 24);
            this.caféToolStripMenuItem.Text = "Café";
            // 
            // buscarToolStripMenuItem
            // 
            this.buscarToolStripMenuItem.Name = "buscarToolStripMenuItem";
            this.buscarToolStripMenuItem.Size = new System.Drawing.Size(122, 24);
            this.buscarToolStripMenuItem.Text = "Buscar";
            this.buscarToolStripMenuItem.Click += new System.EventHandler(this.buscarToolStripMenuItem_Click);
            // 
            // pepsiToolStripMenuItem1
            // 
            this.pepsiToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buscarToolStripMenuItem1});
            this.pepsiToolStripMenuItem1.Name = "pepsiToolStripMenuItem1";
            this.pepsiToolStripMenuItem1.Size = new System.Drawing.Size(114, 24);
            this.pepsiToolStripMenuItem1.Text = "Pepsi";
            // 
            // buscarToolStripMenuItem1
            // 
            this.buscarToolStripMenuItem1.Name = "buscarToolStripMenuItem1";
            this.buscarToolStripMenuItem1.Size = new System.Drawing.Size(122, 24);
            this.buscarToolStripMenuItem1.Text = "Buscar";
            this.buscarToolStripMenuItem1.Click += new System.EventHandler(this.buscarToolStripMenuItem1_Click);
            // 
            // oTallerGeneralToolStripMenuItem
            // 
            this.oTallerGeneralToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.crearToolStripMenuItem4,
            this.buscarToolStripMenuItem2,
            this.informeToolStripMenuItem2});
            this.oTallerGeneralToolStripMenuItem.Image = global::CafeS60.INTERFACE.Properties.Resources._122058;
            this.oTallerGeneralToolStripMenuItem.Name = "oTallerGeneralToolStripMenuItem";
            this.oTallerGeneralToolStripMenuItem.Size = new System.Drawing.Size(127, 23);
            this.oTallerGeneralToolStripMenuItem.Text = "TallerGeneral";
            // 
            // crearToolStripMenuItem4
            // 
            this.crearToolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.berlysToolStripMenuItem,
            this.danoneToolStripMenuItem,
            this.danoneWaterToolStripMenuItem,
            this.ikeaToolStripMenuItem,
            this.starbucksToolStripMenuItem,
            this.heinekenStcToolStripMenuItem,
            this.heinekenEventoToolStripMenuItem,
            this.corecoToolStripMenuItem,
            this.scheeppesToolStripMenuItem});
            this.crearToolStripMenuItem4.Name = "crearToolStripMenuItem4";
            this.crearToolStripMenuItem4.Size = new System.Drawing.Size(122, 24);
            this.crearToolStripMenuItem4.Text = "Crear";
            // 
            // berlysToolStripMenuItem
            // 
            this.berlysToolStripMenuItem.Name = "berlysToolStripMenuItem";
            this.berlysToolStripMenuItem.Size = new System.Drawing.Size(190, 24);
            this.berlysToolStripMenuItem.Text = "Berlys";
            this.berlysToolStripMenuItem.Click += new System.EventHandler(this.berlysToolStripMenuItem_Click);
            // 
            // danoneToolStripMenuItem
            // 
            this.danoneToolStripMenuItem.Name = "danoneToolStripMenuItem";
            this.danoneToolStripMenuItem.Size = new System.Drawing.Size(190, 24);
            this.danoneToolStripMenuItem.Text = "Danone Vending";
            this.danoneToolStripMenuItem.Click += new System.EventHandler(this.danoneToolStripMenuItem_Click);
            // 
            // danoneWaterToolStripMenuItem
            // 
            this.danoneWaterToolStripMenuItem.Name = "danoneWaterToolStripMenuItem";
            this.danoneWaterToolStripMenuItem.Size = new System.Drawing.Size(190, 24);
            this.danoneWaterToolStripMenuItem.Text = "Danone Waters";
            this.danoneWaterToolStripMenuItem.Click += new System.EventHandler(this.danoneWaterToolStripMenuItem_Click);
            // 
            // ikeaToolStripMenuItem
            // 
            this.ikeaToolStripMenuItem.Name = "ikeaToolStripMenuItem";
            this.ikeaToolStripMenuItem.Size = new System.Drawing.Size(190, 24);
            this.ikeaToolStripMenuItem.Text = "Ikea";
            this.ikeaToolStripMenuItem.Click += new System.EventHandler(this.ikeaToolStripMenuItem_Click);
            // 
            // starbucksToolStripMenuItem
            // 
            this.starbucksToolStripMenuItem.Name = "starbucksToolStripMenuItem";
            this.starbucksToolStripMenuItem.Size = new System.Drawing.Size(190, 24);
            this.starbucksToolStripMenuItem.Text = "Starbucks";
            // 
            // heinekenStcToolStripMenuItem
            // 
            this.heinekenStcToolStripMenuItem.Name = "heinekenStcToolStripMenuItem";
            this.heinekenStcToolStripMenuItem.Size = new System.Drawing.Size(190, 24);
            this.heinekenStcToolStripMenuItem.Text = "Heineken STC";
            this.heinekenStcToolStripMenuItem.Click += new System.EventHandler(this.heinekenStcToolStripMenuItem_Click);
            // 
            // heinekenEventoToolStripMenuItem
            // 
            this.heinekenEventoToolStripMenuItem.Name = "heinekenEventoToolStripMenuItem";
            this.heinekenEventoToolStripMenuItem.Size = new System.Drawing.Size(190, 24);
            this.heinekenEventoToolStripMenuItem.Text = "Heineken Evento";
            this.heinekenEventoToolStripMenuItem.Click += new System.EventHandler(this.heinekenEventoToolStripMenuItem_Click);
            // 
            // corecoToolStripMenuItem
            // 
            this.corecoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.corecoToolStripMenuItem1});
            this.corecoToolStripMenuItem.Name = "corecoToolStripMenuItem";
            this.corecoToolStripMenuItem.Size = new System.Drawing.Size(190, 24);
            this.corecoToolStripMenuItem.Text = "Garantía";
            this.corecoToolStripMenuItem.Click += new System.EventHandler(this.corecoToolStripMenuItem_Click);
            // 
            // corecoToolStripMenuItem1
            // 
            this.corecoToolStripMenuItem1.Name = "corecoToolStripMenuItem1";
            this.corecoToolStripMenuItem1.Size = new System.Drawing.Size(126, 24);
            this.corecoToolStripMenuItem1.Text = "Coreco";
            this.corecoToolStripMenuItem1.Click += new System.EventHandler(this.corecoToolStripMenuItem1_Click);
            // 
            // scheeppesToolStripMenuItem
            // 
            this.scheeppesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.plazaToolStripMenuItem,
            this.eventoToolStripMenuItem});
            this.scheeppesToolStripMenuItem.Name = "scheeppesToolStripMenuItem";
            this.scheeppesToolStripMenuItem.Size = new System.Drawing.Size(190, 24);
            this.scheeppesToolStripMenuItem.Text = "Schweppes";
            // 
            // plazaToolStripMenuItem
            // 
            this.plazaToolStripMenuItem.Name = "plazaToolStripMenuItem";
            this.plazaToolStripMenuItem.Size = new System.Drawing.Size(123, 24);
            this.plazaToolStripMenuItem.Text = "Plaza";
            this.plazaToolStripMenuItem.Click += new System.EventHandler(this.plazaToolStripMenuItem_Click);
            // 
            // eventoToolStripMenuItem
            // 
            this.eventoToolStripMenuItem.Name = "eventoToolStripMenuItem";
            this.eventoToolStripMenuItem.Size = new System.Drawing.Size(123, 24);
            this.eventoToolStripMenuItem.Text = "Evento";
            // 
            // buscarToolStripMenuItem2
            // 
            this.buscarToolStripMenuItem2.Name = "buscarToolStripMenuItem2";
            this.buscarToolStripMenuItem2.Size = new System.Drawing.Size(122, 24);
            this.buscarToolStripMenuItem2.Text = "Buscar";
            this.buscarToolStripMenuItem2.Click += new System.EventHandler(this.buscarToolStripMenuItem2_Click);
            // 
            // panelcafe
            // 
            this.panelcafe.Controls.Add(this.inicio);
            this.panelcafe.Controls.Add(this.vercafe);
            this.panelcafe.Controls.Add(this.insertarcafe);
            this.panelcafe.Controls.Add(this.modificacafe);
            this.panelcafe.Controls.Add(this.entradacafe);
            this.panelcafe.Controls.Add(this.salidacafe);
            this.panelcafe.Controls.Add(this.veres);
            this.panelcafe.Controls.Add(this.paDisBotParteCrear);
            this.panelcafe.Controls.Add(this.OTaller);
            this.panelcafe.Controls.Add(this.IBOTaller);
            this.panelcafe.Controls.Add(this.DisBotSal);
            this.panelcafe.Controls.Add(this.DisBotEnt);
            this.panelcafe.Controls.Add(this.DisBotLista);
            this.panelcafe.Controls.Add(this.IROTaller);
            this.panelcafe.Controls.Add(this.VacCrear);
            this.panelcafe.Controls.Add(this.VacInf);
            this.panelcafe.Controls.Add(this.tabPage1);
            this.panelcafe.Controls.Add(this.AlmEqSe);
            this.panelcafe.ItemSize = new System.Drawing.Size(42, 22);
            this.panelcafe.Location = new System.Drawing.Point(12, 30);
            this.panelcafe.Name = "panelcafe";
            this.panelcafe.SelectedIndex = 0;
            this.panelcafe.Size = new System.Drawing.Size(902, 611);
            this.panelcafe.TabIndex = 1;
            // 
            // inicio
            // 
            this.inicio.BackColor = System.Drawing.Color.CadetBlue;
            this.inicio.Controls.Add(this.pictureBox6);
            this.inicio.Controls.Add(this.label61);
            this.inicio.Location = new System.Drawing.Point(4, 26);
            this.inicio.Name = "inicio";
            this.inicio.Padding = new System.Windows.Forms.Padding(3);
            this.inicio.Size = new System.Drawing.Size(894, 581);
            this.inicio.TabIndex = 0;
            this.inicio.Text = "Inicio";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::CafeS60.INTERFACE.Properties.Resources.almacen2;
            this.pictureBox6.Location = new System.Drawing.Point(319, 83);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(256, 256);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox6.TabIndex = 1;
            this.pictureBox6.TabStop = false;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.BackColor = System.Drawing.Color.CadetBlue;
            this.label61.Font = new System.Drawing.Font("Comic Sans MS", 60F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Black;
            this.label61.Location = new System.Drawing.Point(65, 342);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(763, 111);
            this.label61.TabIndex = 0;
            this.label61.Text = "Almacén Valladolid";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // vercafe
            // 
            this.vercafe.BackColor = System.Drawing.Color.CadetBlue;
            this.vercafe.Controls.Add(this.panel3);
            this.vercafe.Controls.Add(this.indiceregistros);
            this.vercafe.Controls.Add(this.anterior);
            this.vercafe.Controls.Add(this.siguiente);
            this.vercafe.Controls.Add(this.label14);
            this.vercafe.Controls.Add(this.pictureBox5);
            this.vercafe.Location = new System.Drawing.Point(4, 26);
            this.vercafe.Name = "vercafe";
            this.vercafe.Padding = new System.Windows.Forms.Padding(3);
            this.vercafe.Size = new System.Drawing.Size(894, 581);
            this.vercafe.TabIndex = 1;
            this.vercafe.Text = "Ver";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Khaki;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.pictureVer);
            this.panel3.Controls.Add(this.panel10);
            this.panel3.Controls.Add(this.panel9);
            this.panel3.Location = new System.Drawing.Point(47, 72);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(800, 487);
            this.panel3.TabIndex = 58;
            // 
            // pictureVer
            // 
            this.pictureVer.Location = new System.Drawing.Point(618, 137);
            this.pictureVer.Name = "pictureVer";
            this.pictureVer.Size = new System.Drawing.Size(171, 210);
            this.pictureVer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureVer.TabIndex = 127;
            this.pictureVer.TabStop = false;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Beige;
            this.panel10.Controls.Add(this.textBoxdescription);
            this.panel10.Controls.Add(this.textBoxmachine);
            this.panel10.Controls.Add(this.textBoximg);
            this.panel10.Controls.Add(this.label20);
            this.panel10.Controls.Add(this.label17);
            this.panel10.Controls.Add(this.label24);
            this.panel10.Location = new System.Drawing.Point(10, 229);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(602, 219);
            this.panel10.TabIndex = 51;
            // 
            // textBoxdescription
            // 
            this.textBoxdescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxdescription.ForeColor = System.Drawing.Color.RoyalBlue;
            this.textBoxdescription.Location = new System.Drawing.Point(113, 5);
            this.textBoxdescription.MaxLength = 240;
            this.textBoxdescription.Multiline = true;
            this.textBoxdescription.Name = "textBoxdescription";
            this.textBoxdescription.ReadOnly = true;
            this.textBoxdescription.Size = new System.Drawing.Size(480, 90);
            this.textBoxdescription.TabIndex = 32;
            // 
            // textBoxmachine
            // 
            this.textBoxmachine.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxmachine.ForeColor = System.Drawing.Color.RoyalBlue;
            this.textBoxmachine.Location = new System.Drawing.Point(113, 101);
            this.textBoxmachine.MaxLength = 120;
            this.textBoxmachine.Name = "textBoxmachine";
            this.textBoxmachine.ReadOnly = true;
            this.textBoxmachine.Size = new System.Drawing.Size(480, 23);
            this.textBoxmachine.TabIndex = 36;
            // 
            // textBoximg
            // 
            this.textBoximg.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoximg.ForeColor = System.Drawing.Color.RoyalBlue;
            this.textBoximg.Location = new System.Drawing.Point(113, 130);
            this.textBoximg.MaxLength = 240;
            this.textBoximg.Name = "textBoximg";
            this.textBoximg.ReadOnly = true;
            this.textBoximg.Size = new System.Drawing.Size(480, 23);
            this.textBoximg.TabIndex = 37;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label20.Location = new System.Drawing.Point(20, 104);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(87, 17);
            this.label20.TabIndex = 47;
            this.label20.Text = "Referencia";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label17.Location = new System.Drawing.Point(47, 133);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(60, 17);
            this.label17.TabIndex = 50;
            this.label17.Text = "Imágen";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label24.Location = new System.Drawing.Point(14, 8);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(93, 17);
            this.label24.TabIndex = 43;
            this.label24.Text = "Descripción";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Beige;
            this.panel9.Controls.Add(this.label27);
            this.panel9.Controls.Add(this.label21);
            this.panel9.Controls.Add(this.textBoxid);
            this.panel9.Controls.Add(this.label22);
            this.panel9.Controls.Add(this.textBoxcode);
            this.panel9.Controls.Add(this.label23);
            this.panel9.Controls.Add(this.textBoxname);
            this.panel9.Controls.Add(this.label25);
            this.panel9.Controls.Add(this.textBoxprice);
            this.panel9.Controls.Add(this.label26);
            this.panel9.Controls.Add(this.textBoxstock);
            this.panel9.Controls.Add(this.label16);
            this.panel9.Controls.Add(this.textBoxfamily);
            this.panel9.Location = new System.Drawing.Point(10, 37);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(602, 186);
            this.panel9.TabIndex = 127;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label27.Location = new System.Drawing.Point(81, 11);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(26, 17);
            this.label27.TabIndex = 40;
            this.label27.Text = "Id.";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label21.Location = new System.Drawing.Point(48, 156);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(59, 17);
            this.label21.TabIndex = 46;
            this.label21.Text = "Familia";
            // 
            // textBoxid
            // 
            this.textBoxid.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxid.ForeColor = System.Drawing.Color.RoyalBlue;
            this.textBoxid.Location = new System.Drawing.Point(113, 8);
            this.textBoxid.MaxLength = 0;
            this.textBoxid.Name = "textBoxid";
            this.textBoxid.ReadOnly = true;
            this.textBoxid.Size = new System.Drawing.Size(60, 23);
            this.textBoxid.TabIndex = 29;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label22.Location = new System.Drawing.Point(59, 127);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(48, 17);
            this.label22.TabIndex = 45;
            this.label22.Text = "Stock";
            // 
            // textBoxcode
            // 
            this.textBoxcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxcode.ForeColor = System.Drawing.Color.RoyalBlue;
            this.textBoxcode.Location = new System.Drawing.Point(113, 37);
            this.textBoxcode.MaxLength = 20;
            this.textBoxcode.Name = "textBoxcode";
            this.textBoxcode.ReadOnly = true;
            this.textBoxcode.Size = new System.Drawing.Size(170, 23);
            this.textBoxcode.TabIndex = 30;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label23.Location = new System.Drawing.Point(53, 98);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(54, 17);
            this.label23.TabIndex = 44;
            this.label23.Text = "Precio";
            // 
            // textBoxname
            // 
            this.textBoxname.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxname.ForeColor = System.Drawing.Color.RoyalBlue;
            this.textBoxname.Location = new System.Drawing.Point(113, 66);
            this.textBoxname.MaxLength = 60;
            this.textBoxname.Name = "textBoxname";
            this.textBoxname.ReadOnly = true;
            this.textBoxname.Size = new System.Drawing.Size(480, 23);
            this.textBoxname.TabIndex = 31;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label25.Location = new System.Drawing.Point(43, 69);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(64, 17);
            this.label25.TabIndex = 42;
            this.label25.Text = "Nombre";
            // 
            // textBoxprice
            // 
            this.textBoxprice.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxprice.ForeColor = System.Drawing.Color.RoyalBlue;
            this.textBoxprice.Location = new System.Drawing.Point(113, 95);
            this.textBoxprice.MaxLength = 4;
            this.textBoxprice.Name = "textBoxprice";
            this.textBoxprice.ReadOnly = true;
            this.textBoxprice.Size = new System.Drawing.Size(90, 23);
            this.textBoxprice.TabIndex = 33;
            this.textBoxprice.Text = "0";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label26.Location = new System.Drawing.Point(49, 40);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(58, 17);
            this.label26.TabIndex = 41;
            this.label26.Text = "Código";
            // 
            // textBoxstock
            // 
            this.textBoxstock.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxstock.ForeColor = System.Drawing.Color.RoyalBlue;
            this.textBoxstock.Location = new System.Drawing.Point(113, 124);
            this.textBoxstock.MaxLength = 4;
            this.textBoxstock.Name = "textBoxstock";
            this.textBoxstock.ReadOnly = true;
            this.textBoxstock.Size = new System.Drawing.Size(90, 23);
            this.textBoxstock.TabIndex = 34;
            this.textBoxstock.Text = "0";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label16.Location = new System.Drawing.Point(209, 105);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(38, 13);
            this.label16.TabIndex = 52;
            this.label16.Text = "euros";
            // 
            // textBoxfamily
            // 
            this.textBoxfamily.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxfamily.ForeColor = System.Drawing.Color.RoyalBlue;
            this.textBoxfamily.Location = new System.Drawing.Point(113, 153);
            this.textBoxfamily.MaxLength = 0;
            this.textBoxfamily.Name = "textBoxfamily";
            this.textBoxfamily.ReadOnly = true;
            this.textBoxfamily.Size = new System.Drawing.Size(90, 23);
            this.textBoxfamily.TabIndex = 35;
            this.textBoxfamily.Text = "CAFE";
            // 
            // indiceregistros
            // 
            this.indiceregistros.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.indiceregistros.Location = new System.Drawing.Point(624, 35);
            this.indiceregistros.MaxLength = 0;
            this.indiceregistros.Name = "indiceregistros";
            this.indiceregistros.ReadOnly = true;
            this.indiceregistros.Size = new System.Drawing.Size(102, 23);
            this.indiceregistros.TabIndex = 57;
            this.indiceregistros.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // anterior
            // 
            this.anterior.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.anterior.Location = new System.Drawing.Point(503, 26);
            this.anterior.Name = "anterior";
            this.anterior.Size = new System.Drawing.Size(115, 40);
            this.anterior.TabIndex = 56;
            this.anterior.Text = "Anterior";
            this.anterior.UseVisualStyleBackColor = true;
            this.anterior.Click += new System.EventHandler(this.anterior_Click);
            // 
            // siguiente
            // 
            this.siguiente.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.siguiente.Location = new System.Drawing.Point(732, 26);
            this.siguiente.Name = "siguiente";
            this.siguiente.Size = new System.Drawing.Size(115, 40);
            this.siguiente.TabIndex = 55;
            this.siguiente.Text = "Siguiente";
            this.siguiente.UseVisualStyleBackColor = true;
            this.siguiente.Click += new System.EventHandler(this.siguiente_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(113, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(165, 29);
            this.label14.TabIndex = 54;
            this.label14.Text = "Ver Catálogo";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::CafeS60.INTERFACE.Properties.Resources.ojo2;
            this.pictureBox5.Location = new System.Drawing.Point(47, 6);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(60, 60);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 126;
            this.pictureBox5.TabStop = false;
            // 
            // insertarcafe
            // 
            this.insertarcafe.Controls.Add(this.panelinsertarcafe);
            this.insertarcafe.Location = new System.Drawing.Point(4, 26);
            this.insertarcafe.Name = "insertarcafe";
            this.insertarcafe.Padding = new System.Windows.Forms.Padding(3);
            this.insertarcafe.Size = new System.Drawing.Size(894, 581);
            this.insertarcafe.TabIndex = 2;
            this.insertarcafe.Text = "Insertar";
            this.insertarcafe.UseVisualStyleBackColor = true;
            // 
            // panelinsertarcafe
            // 
            this.panelinsertarcafe.BackColor = System.Drawing.Color.CadetBlue;
            this.panelinsertarcafe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelinsertarcafe.Controls.Add(this.pictureBox4);
            this.panelinsertarcafe.Controls.Add(this.label13);
            this.panelinsertarcafe.Controls.Add(this.guardarcafe);
            this.panelinsertarcafe.Controls.Add(this.panel5);
            this.panelinsertarcafe.Location = new System.Drawing.Point(0, 0);
            this.panelinsertarcafe.Name = "panelinsertarcafe";
            this.panelinsertarcafe.Size = new System.Drawing.Size(894, 559);
            this.panelinsertarcafe.TabIndex = 0;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::CafeS60.INTERFACE.Properties.Resources.crear3;
            this.pictureBox4.Location = new System.Drawing.Point(46, 3);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(60, 60);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 125;
            this.pictureBox4.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(112, 19);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(232, 29);
            this.label13.TabIndex = 28;
            this.label13.Text = "Registrar Producto";
            // 
            // guardarcafe
            // 
            this.guardarcafe.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guardarcafe.Location = new System.Drawing.Point(633, 494);
            this.guardarcafe.Name = "guardarcafe";
            this.guardarcafe.Size = new System.Drawing.Size(213, 45);
            this.guardarcafe.TabIndex = 24;
            this.guardarcafe.Text = "GUARDAR REGISTRO";
            this.guardarcafe.UseVisualStyleBackColor = true;
            this.guardarcafe.Click += new System.EventHandler(this.guardarcafe_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Khaki;
            this.panel5.Controls.Add(this.panel11);
            this.panel5.Location = new System.Drawing.Point(46, 69);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(800, 419);
            this.panel5.TabIndex = 29;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.Beige;
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.label1);
            this.panel11.Controls.Add(this.id);
            this.panel11.Controls.Add(this.label3);
            this.panel11.Controls.Add(this.label2);
            this.panel11.Controls.Add(this.label4);
            this.panel11.Controls.Add(this.label5);
            this.panel11.Controls.Add(this.code);
            this.panel11.Controls.Add(this.labelotro);
            this.panel11.Controls.Add(this.name);
            this.panel11.Controls.Add(this.description);
            this.panel11.Controls.Add(this.label9);
            this.panel11.Controls.Add(this.label10);
            this.panel11.Controls.Add(this.img);
            this.panel11.Controls.Add(this.price);
            this.panel11.Controls.Add(this.label8);
            this.panel11.Controls.Add(this.reference);
            this.panel11.Controls.Add(this.stock);
            this.panel11.Controls.Add(this.label7);
            this.panel11.Controls.Add(this.family);
            this.panel11.Location = new System.Drawing.Point(58, 42);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(684, 335);
            this.panel11.TabIndex = 28;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label1.Location = new System.Drawing.Point(103, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 17);
            this.label1.TabIndex = 13;
            this.label1.Text = "Id.";
            // 
            // id
            // 
            this.id.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.id.ForeColor = System.Drawing.Color.RoyalBlue;
            this.id.Location = new System.Drawing.Point(135, 17);
            this.id.MaxLength = 0;
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Size = new System.Drawing.Size(60, 23);
            this.id.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label3.Location = new System.Drawing.Point(65, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 15;
            this.label3.Text = "Nombre";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label2.Location = new System.Drawing.Point(204, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 17);
            this.label2.TabIndex = 14;
            this.label2.Text = "Código SAP";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label4.Location = new System.Drawing.Point(231, 179);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "euros";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label5.Location = new System.Drawing.Point(36, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 17);
            this.label5.TabIndex = 16;
            this.label5.Text = "Descripción";
            // 
            // code
            // 
            this.code.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.code.ForeColor = System.Drawing.Color.RoyalBlue;
            this.code.Location = new System.Drawing.Point(303, 17);
            this.code.MaxLength = 20;
            this.code.Name = "code";
            this.code.Size = new System.Drawing.Size(170, 23);
            this.code.TabIndex = 1;
            this.code.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.code_KeyPress);
            // 
            // labelotro
            // 
            this.labelotro.AutoSize = true;
            this.labelotro.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelotro.ForeColor = System.Drawing.Color.RoyalBlue;
            this.labelotro.Location = new System.Drawing.Point(75, 175);
            this.labelotro.Name = "labelotro";
            this.labelotro.Size = new System.Drawing.Size(54, 17);
            this.labelotro.TabIndex = 17;
            this.labelotro.Text = "Precio";
            // 
            // name
            // 
            this.name.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name.ForeColor = System.Drawing.Color.RoyalBlue;
            this.name.Location = new System.Drawing.Point(135, 46);
            this.name.MaxLength = 60;
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(480, 23);
            this.name.TabIndex = 2;
            this.name.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.name_KeyPress);
            // 
            // description
            // 
            this.description.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.description.ForeColor = System.Drawing.Color.RoyalBlue;
            this.description.Location = new System.Drawing.Point(135, 76);
            this.description.MaxLength = 240;
            this.description.Multiline = true;
            this.description.Name = "description";
            this.description.Size = new System.Drawing.Size(480, 90);
            this.description.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label9.Location = new System.Drawing.Point(81, 205);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 17);
            this.label9.TabIndex = 18;
            this.label9.Text = "Stock";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label10.Location = new System.Drawing.Point(69, 292);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 17);
            this.label10.TabIndex = 23;
            this.label10.Text = "Imágen";
            this.label10.Visible = false;
            // 
            // img
            // 
            this.img.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.img.ForeColor = System.Drawing.Color.RoyalBlue;
            this.img.Location = new System.Drawing.Point(135, 289);
            this.img.MaxLength = 240;
            this.img.Name = "img";
            this.img.Size = new System.Drawing.Size(480, 23);
            this.img.TabIndex = 10;
            this.img.Visible = false;
            // 
            // price
            // 
            this.price.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.price.ForeColor = System.Drawing.Color.RoyalBlue;
            this.price.Location = new System.Drawing.Point(135, 172);
            this.price.MaxLength = 8;
            this.price.Name = "price";
            this.price.Size = new System.Drawing.Size(90, 23);
            this.price.TabIndex = 4;
            this.price.Text = "0";
            this.price.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.price_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label8.Location = new System.Drawing.Point(70, 263);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 17);
            this.label8.TabIndex = 19;
            this.label8.Text = "Familia";
            // 
            // reference
            // 
            this.reference.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reference.ForeColor = System.Drawing.Color.RoyalBlue;
            this.reference.Location = new System.Drawing.Point(135, 231);
            this.reference.MaxLength = 120;
            this.reference.Name = "reference";
            this.reference.Size = new System.Drawing.Size(170, 23);
            this.reference.TabIndex = 7;
            // 
            // stock
            // 
            this.stock.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stock.ForeColor = System.Drawing.Color.RoyalBlue;
            this.stock.Location = new System.Drawing.Point(135, 202);
            this.stock.MaxLength = 4;
            this.stock.Name = "stock";
            this.stock.Size = new System.Drawing.Size(90, 23);
            this.stock.TabIndex = 5;
            this.stock.Text = "0";
            this.stock.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.stock_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label7.Location = new System.Drawing.Point(42, 234);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 17);
            this.label7.TabIndex = 20;
            this.label7.Text = "Referencia";
            // 
            // family
            // 
            this.family.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.family.ForeColor = System.Drawing.Color.RoyalBlue;
            this.family.Location = new System.Drawing.Point(135, 260);
            this.family.MaxLength = 0;
            this.family.Name = "family";
            this.family.ReadOnly = true;
            this.family.Size = new System.Drawing.Size(90, 23);
            this.family.TabIndex = 6;
            this.family.Text = "CAFE";
            // 
            // modificacafe
            // 
            this.modificacafe.BackColor = System.Drawing.Color.CadetBlue;
            this.modificacafe.Controls.Add(this.pictureBox3);
            this.modificacafe.Controls.Add(this.panel6);
            this.modificacafe.Controls.Add(this.indiceregistrosUpdate);
            this.modificacafe.Controls.Add(this.anteriorUpdate);
            this.modificacafe.Controls.Add(this.siguienteUpdate);
            this.modificacafe.Controls.Add(this.label28);
            this.modificacafe.Controls.Add(this.modificar);
            this.modificacafe.Location = new System.Drawing.Point(4, 26);
            this.modificacafe.Name = "modificacafe";
            this.modificacafe.Padding = new System.Windows.Forms.Padding(3);
            this.modificacafe.Size = new System.Drawing.Size(894, 581);
            this.modificacafe.TabIndex = 3;
            this.modificacafe.Text = "Modificar";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::CafeS60.INTERFACE.Properties.Resources.papel;
            this.pictureBox3.Location = new System.Drawing.Point(47, 6);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(60, 60);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 124;
            this.pictureBox3.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Khaki;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.pictureModificar);
            this.panel6.Controls.Add(this.textid);
            this.panel6.Controls.Add(this.groupBox1);
            this.panel6.Controls.Add(this.textcode);
            this.panel6.Controls.Add(this.textname);
            this.panel6.Controls.Add(this.textdescription);
            this.panel6.Controls.Add(this.textprice);
            this.panel6.Controls.Add(this.textstock);
            this.panel6.Controls.Add(this.textcafe);
            this.panel6.Controls.Add(this.label30);
            this.panel6.Controls.Add(this.textreference);
            this.panel6.Controls.Add(this.textimg);
            this.panel6.Controls.Add(this.label31);
            this.panel6.Controls.Add(this.label41);
            this.panel6.Controls.Add(this.label40);
            this.panel6.Controls.Add(this.label34);
            this.panel6.Controls.Add(this.label39);
            this.panel6.Controls.Add(this.label35);
            this.panel6.Controls.Add(this.label38);
            this.panel6.Controls.Add(this.label36);
            this.panel6.Controls.Add(this.label37);
            this.panel6.Location = new System.Drawing.Point(47, 72);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(800, 419);
            this.panel6.TabIndex = 64;
            // 
            // pictureModificar
            // 
            this.pictureModificar.Location = new System.Drawing.Point(606, 113);
            this.pictureModificar.Name = "pictureModificar";
            this.pictureModificar.Size = new System.Drawing.Size(171, 210);
            this.pictureModificar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureModificar.TabIndex = 128;
            this.pictureModificar.TabStop = false;
            // 
            // textid
            // 
            this.textid.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textid.Location = new System.Drawing.Point(120, 50);
            this.textid.MaxLength = 0;
            this.textid.Name = "textid";
            this.textid.ReadOnly = true;
            this.textid.Size = new System.Drawing.Size(60, 23);
            this.textid.TabIndex = 29;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label42);
            this.groupBox1.Controls.Add(this.buscador);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.groupBox1.Location = new System.Drawing.Point(296, 40);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(481, 63);
            this.groupBox1.TabIndex = 63;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "BUSCADOR POR NOMBRE";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label42.Location = new System.Drawing.Point(29, 21);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(71, 34);
            this.label42.TabIndex = 62;
            this.label42.Text = "Introduce\r\nel nombre";
            // 
            // buscador
            // 
            this.buscador.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buscador.Location = new System.Drawing.Point(106, 32);
            this.buscador.MaxLength = 20;
            this.buscador.Name = "buscador";
            this.buscador.Size = new System.Drawing.Size(345, 23);
            this.buscador.TabIndex = 61;
            this.buscador.TextChanged += new System.EventHandler(this.buscador_TextChanged);
            // 
            // textcode
            // 
            this.textcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textcode.Location = new System.Drawing.Point(120, 80);
            this.textcode.MaxLength = 20;
            this.textcode.Name = "textcode";
            this.textcode.Size = new System.Drawing.Size(170, 23);
            this.textcode.TabIndex = 30;
            this.textcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textcode_KeyPress);
            // 
            // textname
            // 
            this.textname.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textname.Location = new System.Drawing.Point(120, 110);
            this.textname.MaxLength = 60;
            this.textname.Name = "textname";
            this.textname.Size = new System.Drawing.Size(480, 23);
            this.textname.TabIndex = 31;
            this.textname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textname_KeyPress);
            // 
            // textdescription
            // 
            this.textdescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textdescription.Location = new System.Drawing.Point(120, 140);
            this.textdescription.MaxLength = 240;
            this.textdescription.Multiline = true;
            this.textdescription.Name = "textdescription";
            this.textdescription.Size = new System.Drawing.Size(480, 90);
            this.textdescription.TabIndex = 32;
            // 
            // textprice
            // 
            this.textprice.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textprice.Location = new System.Drawing.Point(120, 236);
            this.textprice.MaxLength = 4;
            this.textprice.Name = "textprice";
            this.textprice.Size = new System.Drawing.Size(90, 23);
            this.textprice.TabIndex = 33;
            this.textprice.Text = "0";
            this.textprice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textprice_KeyPress);
            // 
            // textstock
            // 
            this.textstock.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textstock.Location = new System.Drawing.Point(120, 266);
            this.textstock.MaxLength = 4;
            this.textstock.Name = "textstock";
            this.textstock.Size = new System.Drawing.Size(90, 23);
            this.textstock.TabIndex = 34;
            this.textstock.Text = "0";
            this.textstock.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textstock_KeyPress);
            // 
            // textcafe
            // 
            this.textcafe.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textcafe.Location = new System.Drawing.Point(120, 324);
            this.textcafe.MaxLength = 0;
            this.textcafe.Name = "textcafe";
            this.textcafe.ReadOnly = true;
            this.textcafe.Size = new System.Drawing.Size(90, 23);
            this.textcafe.TabIndex = 35;
            this.textcafe.Text = "CAFE";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label30.Location = new System.Drawing.Point(216, 246);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(33, 13);
            this.label30.TabIndex = 53;
            this.label30.Text = "euros";
            // 
            // textreference
            // 
            this.textreference.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textreference.Location = new System.Drawing.Point(120, 295);
            this.textreference.MaxLength = 120;
            this.textreference.Name = "textreference";
            this.textreference.Size = new System.Drawing.Size(240, 23);
            this.textreference.TabIndex = 36;
            // 
            // textimg
            // 
            this.textimg.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textimg.Location = new System.Drawing.Point(120, 354);
            this.textimg.MaxLength = 240;
            this.textimg.Name = "textimg";
            this.textimg.Size = new System.Drawing.Size(480, 23);
            this.textimg.TabIndex = 37;
            this.textimg.Visible = false;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label31.Location = new System.Drawing.Point(54, 357);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(60, 17);
            this.label31.TabIndex = 50;
            this.label31.Text = "Imágen";
            this.label31.Visible = false;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label41.Location = new System.Drawing.Point(88, 53);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(26, 17);
            this.label41.TabIndex = 40;
            this.label41.Text = "Id.";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label40.Location = new System.Drawing.Point(21, 83);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(93, 17);
            this.label40.TabIndex = 41;
            this.label40.Text = "Código SAP";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label34.Location = new System.Drawing.Point(27, 298);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(87, 17);
            this.label34.TabIndex = 47;
            this.label34.Text = "Referencia";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label39.Location = new System.Drawing.Point(50, 113);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(64, 17);
            this.label39.TabIndex = 42;
            this.label39.Text = "Nombre";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label35.Location = new System.Drawing.Point(55, 327);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(59, 17);
            this.label35.TabIndex = 46;
            this.label35.Text = "Familia";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label38.Location = new System.Drawing.Point(21, 143);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(93, 17);
            this.label38.TabIndex = 43;
            this.label38.Text = "Descripción";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label36.Location = new System.Drawing.Point(62, 269);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(48, 17);
            this.label36.TabIndex = 45;
            this.label36.Text = "Stock";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label37.Location = new System.Drawing.Point(60, 239);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(54, 17);
            this.label37.TabIndex = 44;
            this.label37.Text = "Precio";
            // 
            // indiceregistrosUpdate
            // 
            this.indiceregistrosUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.indiceregistrosUpdate.Location = new System.Drawing.Point(624, 35);
            this.indiceregistrosUpdate.MaxLength = 0;
            this.indiceregistrosUpdate.Name = "indiceregistrosUpdate";
            this.indiceregistrosUpdate.ReadOnly = true;
            this.indiceregistrosUpdate.Size = new System.Drawing.Size(102, 23);
            this.indiceregistrosUpdate.TabIndex = 60;
            this.indiceregistrosUpdate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // anteriorUpdate
            // 
            this.anteriorUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.anteriorUpdate.Location = new System.Drawing.Point(503, 26);
            this.anteriorUpdate.Name = "anteriorUpdate";
            this.anteriorUpdate.Size = new System.Drawing.Size(115, 40);
            this.anteriorUpdate.TabIndex = 59;
            this.anteriorUpdate.Text = "Anterior";
            this.anteriorUpdate.UseVisualStyleBackColor = true;
            this.anteriorUpdate.Click += new System.EventHandler(this.anteriorUpdate_Click);
            // 
            // siguienteUpdate
            // 
            this.siguienteUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.siguienteUpdate.Location = new System.Drawing.Point(732, 26);
            this.siguienteUpdate.Name = "siguienteUpdate";
            this.siguienteUpdate.Size = new System.Drawing.Size(115, 40);
            this.siguienteUpdate.TabIndex = 58;
            this.siguienteUpdate.Text = "Siguiente";
            this.siguienteUpdate.UseVisualStyleBackColor = true;
            this.siguienteUpdate.Click += new System.EventHandler(this.siguienteUpdate_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(113, 22);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(233, 29);
            this.label28.TabIndex = 55;
            this.label28.Text = "Modificar Producto";
            // 
            // modificar
            // 
            this.modificar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modificar.Location = new System.Drawing.Point(634, 497);
            this.modificar.Name = "modificar";
            this.modificar.Size = new System.Drawing.Size(213, 45);
            this.modificar.TabIndex = 51;
            this.modificar.Text = "GUARDAR CAMBIOS";
            this.modificar.UseVisualStyleBackColor = true;
            this.modificar.Click += new System.EventHandler(this.modificar_Click);
            // 
            // entradacafe
            // 
            this.entradacafe.BackColor = System.Drawing.Color.CadetBlue;
            this.entradacafe.Controls.Add(this.panel7);
            this.entradacafe.Controls.Add(this.entradaindice);
            this.entradacafe.Controls.Add(this.entradaanterior);
            this.entradacafe.Controls.Add(this.entradasiguiente);
            this.entradacafe.Controls.Add(this.label44);
            this.entradacafe.Controls.Add(this.entradaguardar);
            this.entradacafe.Controls.Add(this.pictureBox1);
            this.entradacafe.Location = new System.Drawing.Point(4, 26);
            this.entradacafe.Name = "entradacafe";
            this.entradacafe.Padding = new System.Windows.Forms.Padding(3);
            this.entradacafe.Size = new System.Drawing.Size(894, 581);
            this.entradacafe.TabIndex = 4;
            this.entradacafe.Text = "Entrada";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Khaki;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.pictureEntrada);
            this.panel7.Controls.Add(this.entradaname);
            this.panel7.Controls.Add(this.panel2);
            this.panel7.Controls.Add(this.entradaid);
            this.panel7.Controls.Add(this.groupBox2);
            this.panel7.Controls.Add(this.entradacode);
            this.panel7.Controls.Add(this.entradastock);
            this.panel7.Controls.Add(this.label57);
            this.panel7.Controls.Add(this.label56);
            this.panel7.Controls.Add(this.label55);
            this.panel7.Controls.Add(this.label52);
            this.panel7.Location = new System.Drawing.Point(47, 72);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(800, 419);
            this.panel7.TabIndex = 123;
            // 
            // pictureEntrada
            // 
            this.pictureEntrada.Location = new System.Drawing.Point(592, 154);
            this.pictureEntrada.Name = "pictureEntrada";
            this.pictureEntrada.Size = new System.Drawing.Size(171, 210);
            this.pictureEntrada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureEntrada.TabIndex = 128;
            this.pictureEntrada.TabStop = false;
            // 
            // entradaname
            // 
            this.entradaname.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.entradaname.Location = new System.Drawing.Point(106, 154);
            this.entradaname.MaxLength = 60;
            this.entradaname.Name = "entradaname";
            this.entradaname.ReadOnly = true;
            this.entradaname.Size = new System.Drawing.Size(480, 23);
            this.entradaname.TabIndex = 66;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Beige;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.dateentrada);
            this.panel2.Controls.Add(this.proveedorentrada);
            this.panel2.Controls.Add(this.label60);
            this.panel2.Controls.Add(this.stockentrada);
            this.panel2.Controls.Add(this.label59);
            this.panel2.Controls.Add(this.label45);
            this.panel2.Location = new System.Drawing.Point(39, 212);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(547, 119);
            this.panel2.TabIndex = 122;
            // 
            // dateentrada
            // 
            this.dateentrada.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateentrada.Location = new System.Drawing.Point(199, 42);
            this.dateentrada.Name = "dateentrada";
            this.dateentrada.Size = new System.Drawing.Size(332, 23);
            this.dateentrada.TabIndex = 122;
            // 
            // proveedorentrada
            // 
            this.proveedorentrada.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.proveedorentrada.Location = new System.Drawing.Point(103, 71);
            this.proveedorentrada.MaxLength = 60;
            this.proveedorentrada.Name = "proveedorentrada";
            this.proveedorentrada.Size = new System.Drawing.Size(428, 23);
            this.proveedorentrada.TabIndex = 119;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label60.Location = new System.Drawing.Point(25, 45);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(72, 17);
            this.label60.TabIndex = 121;
            this.label60.Text = "Cantidad";
            // 
            // stockentrada
            // 
            this.stockentrada.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stockentrada.Location = new System.Drawing.Point(103, 42);
            this.stockentrada.MaxLength = 4;
            this.stockentrada.Name = "stockentrada";
            this.stockentrada.Size = new System.Drawing.Size(90, 23);
            this.stockentrada.TabIndex = 95;
            this.stockentrada.Text = "0";
            this.stockentrada.TextChanged += new System.EventHandler(this.stockentrada_TextChanged);
            this.stockentrada.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.stockentrada_KeyPress);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label59.Location = new System.Drawing.Point(14, 74);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(83, 17);
            this.label59.TabIndex = 120;
            this.label59.Text = "Proveedor";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label45.Location = new System.Drawing.Point(100, 22);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(251, 17);
            this.label45.TabIndex = 97;
            this.label45.Text = "Entrada de Material por unidades";
            // 
            // entradaid
            // 
            this.entradaid.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.entradaid.Location = new System.Drawing.Point(106, 94);
            this.entradaid.MaxLength = 0;
            this.entradaid.Name = "entradaid";
            this.entradaid.ReadOnly = true;
            this.entradaid.Size = new System.Drawing.Size(60, 23);
            this.entradaid.TabIndex = 64;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label43);
            this.groupBox2.Controls.Add(this.entradabuscador);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.groupBox2.Location = new System.Drawing.Point(282, 85);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(481, 63);
            this.groupBox2.TabIndex = 94;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "BUSCADOR POR NOMBRE";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(29, 21);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(71, 34);
            this.label43.TabIndex = 62;
            this.label43.Text = "Introduce\r\nel nombre";
            // 
            // entradabuscador
            // 
            this.entradabuscador.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.entradabuscador.Location = new System.Drawing.Point(106, 32);
            this.entradabuscador.MaxLength = 20;
            this.entradabuscador.Name = "entradabuscador";
            this.entradabuscador.Size = new System.Drawing.Size(337, 23);
            this.entradabuscador.TabIndex = 61;
            this.entradabuscador.TextChanged += new System.EventHandler(this.stockbuscador_TextChanged);
            // 
            // entradacode
            // 
            this.entradacode.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.entradacode.Location = new System.Drawing.Point(106, 124);
            this.entradacode.MaxLength = 20;
            this.entradacode.Name = "entradacode";
            this.entradacode.ReadOnly = true;
            this.entradacode.Size = new System.Drawing.Size(170, 23);
            this.entradacode.TabIndex = 65;
            // 
            // entradastock
            // 
            this.entradastock.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.entradastock.Location = new System.Drawing.Point(106, 183);
            this.entradastock.MaxLength = 4;
            this.entradastock.Name = "entradastock";
            this.entradastock.ReadOnly = true;
            this.entradastock.Size = new System.Drawing.Size(90, 23);
            this.entradastock.TabIndex = 69;
            this.entradastock.Text = "0";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label57.Location = new System.Drawing.Point(74, 97);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(26, 17);
            this.label57.TabIndex = 75;
            this.label57.Text = "Id.";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label56.Location = new System.Drawing.Point(42, 127);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(58, 17);
            this.label56.TabIndex = 76;
            this.label56.Text = "Código";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label55.Location = new System.Drawing.Point(36, 157);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(64, 17);
            this.label55.TabIndex = 77;
            this.label55.Text = "Nombre";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label52.Location = new System.Drawing.Point(52, 186);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(48, 17);
            this.label52.TabIndex = 80;
            this.label52.Text = "Stock";
            // 
            // entradaindice
            // 
            this.entradaindice.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.entradaindice.Location = new System.Drawing.Point(624, 35);
            this.entradaindice.MaxLength = 0;
            this.entradaindice.Name = "entradaindice";
            this.entradaindice.ReadOnly = true;
            this.entradaindice.Size = new System.Drawing.Size(102, 23);
            this.entradaindice.TabIndex = 93;
            this.entradaindice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // entradaanterior
            // 
            this.entradaanterior.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.entradaanterior.Location = new System.Drawing.Point(503, 26);
            this.entradaanterior.Name = "entradaanterior";
            this.entradaanterior.Size = new System.Drawing.Size(115, 40);
            this.entradaanterior.TabIndex = 92;
            this.entradaanterior.Text = "Anterior";
            this.entradaanterior.UseVisualStyleBackColor = true;
            this.entradaanterior.Click += new System.EventHandler(this.stockanterior_Click);
            // 
            // entradasiguiente
            // 
            this.entradasiguiente.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.entradasiguiente.Location = new System.Drawing.Point(732, 26);
            this.entradasiguiente.Name = "entradasiguiente";
            this.entradasiguiente.Size = new System.Drawing.Size(115, 40);
            this.entradasiguiente.TabIndex = 91;
            this.entradasiguiente.Text = "Siguiente";
            this.entradasiguiente.UseVisualStyleBackColor = true;
            this.entradasiguiente.Click += new System.EventHandler(this.stocksiguiente_Click);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.White;
            this.label44.Location = new System.Drawing.Point(113, 22);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(204, 29);
            this.label44.TabIndex = 90;
            this.label44.Text = "Entrada Material";
            // 
            // entradaguardar
            // 
            this.entradaguardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.entradaguardar.Location = new System.Drawing.Point(634, 497);
            this.entradaguardar.Name = "entradaguardar";
            this.entradaguardar.Size = new System.Drawing.Size(213, 45);
            this.entradaguardar.TabIndex = 86;
            this.entradaguardar.Text = "GUARDAR CAMBIOS";
            this.entradaguardar.UseVisualStyleBackColor = true;
            this.entradaguardar.Click += new System.EventHandler(this.stockguardar_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CafeS60.INTERFACE.Properties.Resources.entrar1;
            this.pictureBox1.Location = new System.Drawing.Point(47, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(60, 60);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 123;
            this.pictureBox1.TabStop = false;
            // 
            // salidacafe
            // 
            this.salidacafe.BackColor = System.Drawing.Color.CadetBlue;
            this.salidacafe.Controls.Add(this.panel8);
            this.salidacafe.Controls.Add(this.salidaindice);
            this.salidacafe.Controls.Add(this.salidaanterior);
            this.salidacafe.Controls.Add(this.salidasiguiente);
            this.salidacafe.Controls.Add(this.label50);
            this.salidacafe.Controls.Add(this.salidaguardar);
            this.salidacafe.Controls.Add(this.pictureBox2);
            this.salidacafe.Location = new System.Drawing.Point(4, 26);
            this.salidacafe.Name = "salidacafe";
            this.salidacafe.Padding = new System.Windows.Forms.Padding(3);
            this.salidacafe.Size = new System.Drawing.Size(894, 581);
            this.salidacafe.TabIndex = 5;
            this.salidacafe.Text = "Salida";
            this.salidacafe.Click += new System.EventHandler(this.salidacafe_Click);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Khaki;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.pictureBox7);
            this.panel8.Controls.Add(this.label58);
            this.panel8.Controls.Add(this.panel1);
            this.panel8.Controls.Add(this.salidaid);
            this.panel8.Controls.Add(this.groupBox3);
            this.panel8.Controls.Add(this.salidacode);
            this.panel8.Controls.Add(this.salidaname);
            this.panel8.Controls.Add(this.salidastock);
            this.panel8.Controls.Add(this.label54);
            this.panel8.Controls.Add(this.label53);
            this.panel8.Controls.Add(this.label51);
            this.panel8.Location = new System.Drawing.Point(47, 72);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(800, 419);
            this.panel8.TabIndex = 121;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Location = new System.Drawing.Point(592, 155);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(171, 210);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 128;
            this.pictureBox7.TabStop = false;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label58.Location = new System.Drawing.Point(74, 98);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(26, 17);
            this.label58.TabIndex = 103;
            this.label58.Text = "Id.";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Beige;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.datesalida);
            this.panel1.Controls.Add(this.stocksalida);
            this.panel1.Controls.Add(this.label48);
            this.panel1.Controls.Add(this.label47);
            this.panel1.Controls.Add(this.label46);
            this.panel1.Controls.Add(this.destinosalida);
            this.panel1.Location = new System.Drawing.Point(39, 213);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(547, 119);
            this.panel1.TabIndex = 120;
            // 
            // datesalida
            // 
            this.datesalida.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datesalida.Location = new System.Drawing.Point(192, 42);
            this.datesalida.Name = "datesalida";
            this.datesalida.Size = new System.Drawing.Size(335, 23);
            this.datesalida.TabIndex = 121;
            // 
            // stocksalida
            // 
            this.stocksalida.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stocksalida.Location = new System.Drawing.Point(96, 42);
            this.stocksalida.MaxLength = 4;
            this.stocksalida.Name = "stocksalida";
            this.stocksalida.Size = new System.Drawing.Size(90, 23);
            this.stocksalida.TabIndex = 114;
            this.stocksalida.Text = "0";
            this.stocksalida.TextChanged += new System.EventHandler(this.salidasalida_TextChanged);
            this.stocksalida.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.stocksalida_KeyPress_1);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label48.Location = new System.Drawing.Point(18, 45);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(72, 17);
            this.label48.TabIndex = 119;
            this.label48.Text = "Cantidad";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label47.Location = new System.Drawing.Point(93, 22);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(239, 17);
            this.label47.TabIndex = 116;
            this.label47.Text = "Salida de Material por unidades";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label46.Location = new System.Drawing.Point(27, 74);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(63, 17);
            this.label46.TabIndex = 118;
            this.label46.Text = "Destino";
            // 
            // destinosalida
            // 
            this.destinosalida.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.destinosalida.Location = new System.Drawing.Point(96, 71);
            this.destinosalida.MaxLength = 60;
            this.destinosalida.Name = "destinosalida";
            this.destinosalida.Size = new System.Drawing.Size(431, 23);
            this.destinosalida.TabIndex = 117;
            // 
            // salidaid
            // 
            this.salidaid.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salidaid.Location = new System.Drawing.Point(106, 95);
            this.salidaid.MaxLength = 0;
            this.salidaid.Name = "salidaid";
            this.salidaid.ReadOnly = true;
            this.salidaid.Size = new System.Drawing.Size(60, 23);
            this.salidaid.TabIndex = 99;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label49);
            this.groupBox3.Controls.Add(this.salidabuscador);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.RoyalBlue;
            this.groupBox3.Location = new System.Drawing.Point(282, 85);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(481, 63);
            this.groupBox3.TabIndex = 112;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "BUSCADOR POR NOMBRE";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(29, 21);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(71, 34);
            this.label49.TabIndex = 62;
            this.label49.Text = "Introduce\r\nel nombre";
            // 
            // salidabuscador
            // 
            this.salidabuscador.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salidabuscador.Location = new System.Drawing.Point(106, 32);
            this.salidabuscador.MaxLength = 20;
            this.salidabuscador.Name = "salidabuscador";
            this.salidabuscador.Size = new System.Drawing.Size(349, 23);
            this.salidabuscador.TabIndex = 61;
            this.salidabuscador.TextChanged += new System.EventHandler(this.salidabuscador_TextChanged);
            // 
            // salidacode
            // 
            this.salidacode.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salidacode.Location = new System.Drawing.Point(106, 125);
            this.salidacode.MaxLength = 20;
            this.salidacode.Name = "salidacode";
            this.salidacode.ReadOnly = true;
            this.salidacode.Size = new System.Drawing.Size(170, 23);
            this.salidacode.TabIndex = 100;
            // 
            // salidaname
            // 
            this.salidaname.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salidaname.Location = new System.Drawing.Point(106, 155);
            this.salidaname.MaxLength = 60;
            this.salidaname.Name = "salidaname";
            this.salidaname.ReadOnly = true;
            this.salidaname.Size = new System.Drawing.Size(480, 23);
            this.salidaname.TabIndex = 101;
            // 
            // salidastock
            // 
            this.salidastock.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salidastock.Location = new System.Drawing.Point(106, 184);
            this.salidastock.MaxLength = 4;
            this.salidastock.Name = "salidastock";
            this.salidastock.ReadOnly = true;
            this.salidastock.Size = new System.Drawing.Size(90, 23);
            this.salidastock.TabIndex = 102;
            this.salidastock.Text = "0";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label54.Location = new System.Drawing.Point(42, 128);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(58, 17);
            this.label54.TabIndex = 104;
            this.label54.Text = "Código";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label53.Location = new System.Drawing.Point(36, 158);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(64, 17);
            this.label53.TabIndex = 105;
            this.label53.Text = "Nombre";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label51.Location = new System.Drawing.Point(52, 187);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(48, 17);
            this.label51.TabIndex = 106;
            this.label51.Text = "Stock";
            // 
            // salidaindice
            // 
            this.salidaindice.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salidaindice.Location = new System.Drawing.Point(624, 35);
            this.salidaindice.MaxLength = 0;
            this.salidaindice.Name = "salidaindice";
            this.salidaindice.ReadOnly = true;
            this.salidaindice.Size = new System.Drawing.Size(102, 23);
            this.salidaindice.TabIndex = 111;
            this.salidaindice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // salidaanterior
            // 
            this.salidaanterior.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salidaanterior.Location = new System.Drawing.Point(503, 26);
            this.salidaanterior.Name = "salidaanterior";
            this.salidaanterior.Size = new System.Drawing.Size(115, 40);
            this.salidaanterior.TabIndex = 110;
            this.salidaanterior.Text = "Anterior";
            this.salidaanterior.UseVisualStyleBackColor = true;
            this.salidaanterior.Click += new System.EventHandler(this.salidaanterior_Click);
            // 
            // salidasiguiente
            // 
            this.salidasiguiente.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salidasiguiente.Location = new System.Drawing.Point(732, 26);
            this.salidasiguiente.Name = "salidasiguiente";
            this.salidasiguiente.Size = new System.Drawing.Size(115, 40);
            this.salidasiguiente.TabIndex = 109;
            this.salidasiguiente.Text = "Siguiente";
            this.salidasiguiente.UseVisualStyleBackColor = true;
            this.salidasiguiente.Click += new System.EventHandler(this.salidasiguiente_Click);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.White;
            this.label50.Location = new System.Drawing.Point(113, 22);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(188, 29);
            this.label50.TabIndex = 108;
            this.label50.Text = "Salida Material";
            // 
            // salidaguardar
            // 
            this.salidaguardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salidaguardar.Location = new System.Drawing.Point(634, 497);
            this.salidaguardar.Name = "salidaguardar";
            this.salidaguardar.Size = new System.Drawing.Size(213, 45);
            this.salidaguardar.TabIndex = 107;
            this.salidaguardar.Text = "GUARDAR CAMBIOS";
            this.salidaguardar.UseVisualStyleBackColor = true;
            this.salidaguardar.Click += new System.EventHandler(this.salidaguardar_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::CafeS60.INTERFACE.Properties.Resources.salir;
            this.pictureBox2.Location = new System.Drawing.Point(47, 6);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(60, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 124;
            this.pictureBox2.TabStop = false;
            // 
            // veres
            // 
            this.veres.BackColor = System.Drawing.Color.CadetBlue;
            this.veres.Controls.Add(this.panel4);
            this.veres.Controls.Add(this.verentradaindice);
            this.veres.Controls.Add(this.verentradaanterior);
            this.veres.Controls.Add(this.verentradasiguiente);
            this.veres.Controls.Add(this.label70);
            this.veres.Controls.Add(this.pictureBox9);
            this.veres.Location = new System.Drawing.Point(4, 26);
            this.veres.Name = "veres";
            this.veres.Padding = new System.Windows.Forms.Padding(3);
            this.veres.Size = new System.Drawing.Size(894, 581);
            this.veres.TabIndex = 6;
            this.veres.Text = "Verentrada";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Khaki;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.panel12);
            this.panel4.Location = new System.Drawing.Point(47, 72);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(800, 419);
            this.panel4.TabIndex = 129;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.Beige;
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.pictureBox8);
            this.panel12.Controls.Add(this.dateTimePicker1);
            this.panel12.Controls.Add(this.label62);
            this.panel12.Controls.Add(this.verentradaid);
            this.panel12.Controls.Add(this.groupBox4);
            this.panel12.Controls.Add(this.label65);
            this.panel12.Controls.Add(this.verentradatipo);
            this.panel12.Controls.Add(this.verentradaproveedor);
            this.panel12.Controls.Add(this.verentradaproducto);
            this.panel12.Controls.Add(this.verentradaunidades);
            this.panel12.Controls.Add(this.label69);
            this.panel12.Controls.Add(this.label67);
            this.panel12.Controls.Add(this.label68);
            this.panel12.Location = new System.Drawing.Point(17, 79);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(765, 258);
            this.panel12.TabIndex = 120;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Location = new System.Drawing.Point(580, 23);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(171, 210);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox8.TabIndex = 128;
            this.pictureBox8.TabStop = false;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Enabled = false;
            this.dateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Location = new System.Drawing.Point(190, 152);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(384, 23);
            this.dateTimePicker1.TabIndex = 121;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label62.Location = new System.Drawing.Point(62, 66);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(26, 17);
            this.label62.TabIndex = 103;
            this.label62.Text = "Id.";
            // 
            // verentradaid
            // 
            this.verentradaid.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.verentradaid.Location = new System.Drawing.Point(94, 63);
            this.verentradaid.MaxLength = 0;
            this.verentradaid.Name = "verentradaid";
            this.verentradaid.ReadOnly = true;
            this.verentradaid.Size = new System.Drawing.Size(60, 23);
            this.verentradaid.TabIndex = 99;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label66);
            this.groupBox4.Controls.Add(this.verentradabuscador);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.RoyalBlue;
            this.groupBox4.Location = new System.Drawing.Point(270, 53);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(304, 63);
            this.groupBox4.TabIndex = 112;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "BUSCADOR POR NOMBRE";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(29, 21);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(71, 34);
            this.label66.TabIndex = 62;
            this.label66.Text = "Introduce\r\nel nombre";
            // 
            // verentradabuscador
            // 
            this.verentradabuscador.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.verentradabuscador.Location = new System.Drawing.Point(106, 32);
            this.verentradabuscador.MaxLength = 20;
            this.verentradabuscador.Name = "verentradabuscador";
            this.verentradabuscador.Size = new System.Drawing.Size(170, 23);
            this.verentradabuscador.TabIndex = 61;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label65.Location = new System.Drawing.Point(5, 184);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(83, 17);
            this.label65.TabIndex = 118;
            this.label65.Text = "Proveedor";
            // 
            // verentradatipo
            // 
            this.verentradatipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.verentradatipo.Location = new System.Drawing.Point(94, 93);
            this.verentradatipo.MaxLength = 20;
            this.verentradatipo.Name = "verentradatipo";
            this.verentradatipo.ReadOnly = true;
            this.verentradatipo.Size = new System.Drawing.Size(170, 23);
            this.verentradatipo.TabIndex = 100;
            // 
            // verentradaproveedor
            // 
            this.verentradaproveedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.verentradaproveedor.Location = new System.Drawing.Point(94, 181);
            this.verentradaproveedor.MaxLength = 60;
            this.verentradaproveedor.Name = "verentradaproveedor";
            this.verentradaproveedor.ReadOnly = true;
            this.verentradaproveedor.Size = new System.Drawing.Size(480, 23);
            this.verentradaproveedor.TabIndex = 117;
            // 
            // verentradaproducto
            // 
            this.verentradaproducto.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.verentradaproducto.Location = new System.Drawing.Point(94, 123);
            this.verentradaproducto.MaxLength = 60;
            this.verentradaproducto.Name = "verentradaproducto";
            this.verentradaproducto.ReadOnly = true;
            this.verentradaproducto.Size = new System.Drawing.Size(480, 23);
            this.verentradaproducto.TabIndex = 101;
            // 
            // verentradaunidades
            // 
            this.verentradaunidades.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.verentradaunidades.Location = new System.Drawing.Point(94, 152);
            this.verentradaunidades.MaxLength = 4;
            this.verentradaunidades.Name = "verentradaunidades";
            this.verentradaunidades.ReadOnly = true;
            this.verentradaunidades.Size = new System.Drawing.Size(90, 23);
            this.verentradaunidades.TabIndex = 102;
            this.verentradaunidades.Text = "0";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label69.Location = new System.Drawing.Point(12, 155);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(76, 17);
            this.label69.TabIndex = 106;
            this.label69.Text = "Unidades";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label67.Location = new System.Drawing.Point(48, 96);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(40, 17);
            this.label67.TabIndex = 104;
            this.label67.Text = "Tipo";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label68.Location = new System.Drawing.Point(15, 126);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(73, 17);
            this.label68.TabIndex = 105;
            this.label68.Text = "Producto";
            // 
            // verentradaindice
            // 
            this.verentradaindice.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.verentradaindice.Location = new System.Drawing.Point(624, 35);
            this.verentradaindice.MaxLength = 0;
            this.verentradaindice.Name = "verentradaindice";
            this.verentradaindice.ReadOnly = true;
            this.verentradaindice.Size = new System.Drawing.Size(102, 23);
            this.verentradaindice.TabIndex = 128;
            this.verentradaindice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // verentradaanterior
            // 
            this.verentradaanterior.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.verentradaanterior.Location = new System.Drawing.Point(503, 26);
            this.verentradaanterior.Name = "verentradaanterior";
            this.verentradaanterior.Size = new System.Drawing.Size(115, 40);
            this.verentradaanterior.TabIndex = 127;
            this.verentradaanterior.Text = "Anterior";
            this.verentradaanterior.UseVisualStyleBackColor = true;
            this.verentradaanterior.Click += new System.EventHandler(this.verentradaanterior_Click);
            // 
            // verentradasiguiente
            // 
            this.verentradasiguiente.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.verentradasiguiente.Location = new System.Drawing.Point(732, 26);
            this.verentradasiguiente.Name = "verentradasiguiente";
            this.verentradasiguiente.Size = new System.Drawing.Size(115, 40);
            this.verentradasiguiente.TabIndex = 126;
            this.verentradasiguiente.Text = "Siguiente";
            this.verentradasiguiente.UseVisualStyleBackColor = true;
            this.verentradasiguiente.Click += new System.EventHandler(this.verentradasiguiente_Click);
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.White;
            this.label70.Location = new System.Drawing.Point(113, 22);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(161, 29);
            this.label70.TabIndex = 125;
            this.label70.Text = "Ver entradas";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::CafeS60.INTERFACE.Properties.Resources.ojo2;
            this.pictureBox9.Location = new System.Drawing.Point(47, 6);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(60, 60);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox9.TabIndex = 130;
            this.pictureBox9.TabStop = false;
            // 
            // paDisBotParteCrear
            // 
            this.paDisBotParteCrear.Controls.Add(this.pajj);
            this.paDisBotParteCrear.Location = new System.Drawing.Point(4, 26);
            this.paDisBotParteCrear.Name = "paDisBotParteCrear";
            this.paDisBotParteCrear.Padding = new System.Windows.Forms.Padding(3);
            this.paDisBotParteCrear.Size = new System.Drawing.Size(894, 581);
            this.paDisBotParteCrear.TabIndex = 7;
            this.paDisBotParteCrear.Text = "DisPaCrear";
            this.paDisBotParteCrear.UseVisualStyleBackColor = true;
            // 
            // pajj
            // 
            this.pajj.BackColor = System.Drawing.Color.CadetBlue;
            this.pajj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pajj.Controls.Add(this.label63);
            this.pajj.Controls.Add(this.botguardar);
            this.pajj.Controls.Add(this.panel14);
            this.pajj.Location = new System.Drawing.Point(0, 0);
            this.pajj.Name = "pajj";
            this.pajj.Size = new System.Drawing.Size(894, 562);
            this.pajj.TabIndex = 1;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.White;
            this.label63.Location = new System.Drawing.Point(17, 17);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(325, 29);
            this.label63.TabIndex = 28;
            this.label63.Text = "Parte Nuevo: BOTELLERO";
            // 
            // botguardar
            // 
            this.botguardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botguardar.Location = new System.Drawing.Point(658, 510);
            this.botguardar.Name = "botguardar";
            this.botguardar.Size = new System.Drawing.Size(213, 45);
            this.botguardar.TabIndex = 24;
            this.botguardar.Text = "GUARDAR REGISTRO";
            this.botguardar.UseVisualStyleBackColor = true;
            this.botguardar.Click += new System.EventHandler(this.botguardar_Click);
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.Khaki;
            this.panel14.Controls.Add(this.label18);
            this.panel14.Controls.Add(this.botorden);
            this.panel14.Controls.Add(this.label15);
            this.panel14.Controls.Add(this.botoferta);
            this.panel14.Controls.Add(this.label12);
            this.panel14.Controls.Add(this.botsap);
            this.panel14.Controls.Add(this.label11);
            this.panel14.Controls.Add(this.botcodigo);
            this.panel14.Controls.Add(this.label6);
            this.panel14.Controls.Add(this.botmaterial);
            this.panel14.Controls.Add(this.label80);
            this.panel14.Controls.Add(this.botanyo);
            this.panel14.Controls.Add(this.label76);
            this.panel14.Controls.Add(this.botecnico);
            this.panel14.Controls.Add(this.botpictureimagen);
            this.panel14.Controls.Add(this.botestado);
            this.panel14.Controls.Add(this.label79);
            this.panel14.Controls.Add(this.botfecha);
            this.panel14.Controls.Add(this.label74);
            this.panel14.Controls.Add(this.label71);
            this.panel14.Controls.Add(this.label64);
            this.panel14.Controls.Add(this.botid);
            this.panel14.Controls.Add(this.label72);
            this.panel14.Controls.Add(this.label73);
            this.panel14.Controls.Add(this.label75);
            this.panel14.Controls.Add(this.botns);
            this.panel14.Controls.Add(this.botgas);
            this.panel14.Controls.Add(this.botttttt);
            this.panel14.Controls.Add(this.botorigen);
            this.panel14.Controls.Add(this.botmo);
            this.panel14.Controls.Add(this.label77);
            this.panel14.Controls.Add(this.label78);
            this.panel14.Controls.Add(this.botimagen);
            this.panel14.Controls.Add(this.botfabricante);
            this.panel14.Controls.Add(this.botmarca);
            this.panel14.Controls.Add(this.botmodelo);
            this.panel14.Controls.Add(this.label81);
            this.panel14.Location = new System.Drawing.Point(22, 49);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(849, 454);
            this.panel14.TabIndex = 29;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label18.Location = new System.Drawing.Point(403, 140);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 17);
            this.label18.TabIndex = 162;
            this.label18.Text = "Orden";
            // 
            // botorden
            // 
            this.botorden.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botorden.ForeColor = System.Drawing.Color.RoyalBlue;
            this.botorden.Location = new System.Drawing.Point(462, 137);
            this.botorden.MaxLength = 30;
            this.botorden.Name = "botorden";
            this.botorden.Size = new System.Drawing.Size(173, 23);
            this.botorden.TabIndex = 161;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label15.Location = new System.Drawing.Point(641, 140);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 17);
            this.label15.TabIndex = 160;
            this.label15.Text = "Oferta";
            // 
            // botoferta
            // 
            this.botoferta.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botoferta.ForeColor = System.Drawing.Color.RoyalBlue;
            this.botoferta.Location = new System.Drawing.Point(701, 137);
            this.botoferta.MaxLength = 30;
            this.botoferta.Name = "botoferta";
            this.botoferta.Size = new System.Drawing.Size(71, 23);
            this.botoferta.TabIndex = 159;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label12.Location = new System.Drawing.Point(118, 140);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 17);
            this.label12.TabIndex = 158;
            this.label12.Text = "Nº SAP";
            // 
            // botsap
            // 
            this.botsap.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botsap.ForeColor = System.Drawing.Color.RoyalBlue;
            this.botsap.Location = new System.Drawing.Point(184, 137);
            this.botsap.MaxLength = 30;
            this.botsap.Name = "botsap";
            this.botsap.Size = new System.Drawing.Size(213, 23);
            this.botsap.TabIndex = 157;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label11.Location = new System.Drawing.Point(524, 111);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 17);
            this.label11.TabIndex = 156;
            this.label11.Text = "Código";
            // 
            // botcodigo
            // 
            this.botcodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botcodigo.ForeColor = System.Drawing.Color.RoyalBlue;
            this.botcodigo.Location = new System.Drawing.Point(588, 108);
            this.botcodigo.MaxLength = 30;
            this.botcodigo.Name = "botcodigo";
            this.botcodigo.Size = new System.Drawing.Size(184, 23);
            this.botcodigo.TabIndex = 155;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label6.Location = new System.Drawing.Point(112, 111);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 17);
            this.label6.TabIndex = 154;
            this.label6.Text = "Material";
            // 
            // botmaterial
            // 
            this.botmaterial.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botmaterial.ForeColor = System.Drawing.Color.RoyalBlue;
            this.botmaterial.Location = new System.Drawing.Point(184, 108);
            this.botmaterial.MaxLength = 30;
            this.botmaterial.Name = "botmaterial";
            this.botmaterial.Size = new System.Drawing.Size(320, 23);
            this.botmaterial.TabIndex = 153;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label80.Location = new System.Drawing.Point(323, 285);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(36, 17);
            this.label80.TabIndex = 152;
            this.label80.Text = "Año";
            // 
            // botanyo
            // 
            this.botanyo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botanyo.ForeColor = System.Drawing.Color.RoyalBlue;
            this.botanyo.Location = new System.Drawing.Point(366, 282);
            this.botanyo.MaxLength = 4;
            this.botanyo.Name = "botanyo";
            this.botanyo.Size = new System.Drawing.Size(90, 23);
            this.botanyo.TabIndex = 151;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label76.Location = new System.Drawing.Point(113, 401);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(65, 17);
            this.label76.TabIndex = 150;
            this.label76.Text = "Técnico";
            // 
            // botecnico
            // 
            this.botecnico.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botecnico.ForeColor = System.Drawing.Color.RoyalBlue;
            this.botecnico.Location = new System.Drawing.Point(184, 398);
            this.botecnico.MaxLength = 60;
            this.botecnico.Name = "botecnico";
            this.botecnico.Size = new System.Drawing.Size(272, 23);
            this.botecnico.TabIndex = 149;
            // 
            // botpictureimagen
            // 
            this.botpictureimagen.Image = global::CafeS60.INTERFACE.Properties.Resources.crear3;
            this.botpictureimagen.Location = new System.Drawing.Point(462, 166);
            this.botpictureimagen.Name = "botpictureimagen";
            this.botpictureimagen.Size = new System.Drawing.Size(310, 252);
            this.botpictureimagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.botpictureimagen.TabIndex = 148;
            this.botpictureimagen.TabStop = false;
            // 
            // botestado
            // 
            this.botestado.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botestado.ForeColor = System.Drawing.Color.RoyalBlue;
            this.botestado.Location = new System.Drawing.Point(184, 340);
            this.botestado.MaxLength = 2;
            this.botestado.Name = "botestado";
            this.botestado.ReadOnly = true;
            this.botestado.Size = new System.Drawing.Size(115, 23);
            this.botestado.TabIndex = 147;
            this.botestado.Text = "REPARADO";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label79.Location = new System.Drawing.Point(120, 343);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(58, 17);
            this.label79.TabIndex = 146;
            this.label79.Text = "Estado";
            // 
            // botfecha
            // 
            this.botfecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botfecha.ForeColor = System.Drawing.Color.RoyalBlue;
            this.botfecha.Location = new System.Drawing.Point(414, 15);
            this.botfecha.MaxLength = 8;
            this.botfecha.Name = "botfecha";
            this.botfecha.Size = new System.Drawing.Size(90, 23);
            this.botfecha.TabIndex = 145;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label74.Location = new System.Drawing.Point(250, 17);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(158, 17);
            this.label74.TabIndex = 144;
            this.label74.Text = "Fecha de reparación";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label71.Location = new System.Drawing.Point(141, 285);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(37, 17);
            this.label71.TabIndex = 143;
            this.label71.Text = "Gas";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label64.Location = new System.Drawing.Point(152, 17);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(26, 17);
            this.label64.TabIndex = 134;
            this.label64.Text = "Id.";
            // 
            // botid
            // 
            this.botid.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botid.ForeColor = System.Drawing.Color.RoyalBlue;
            this.botid.Location = new System.Drawing.Point(184, 14);
            this.botid.MaxLength = 0;
            this.botid.Name = "botid";
            this.botid.ReadOnly = true;
            this.botid.Size = new System.Drawing.Size(60, 23);
            this.botid.TabIndex = 126;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label72.Location = new System.Drawing.Point(121, 169);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(57, 17);
            this.label72.TabIndex = 136;
            this.label72.Text = "Origen";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label73.Location = new System.Drawing.Point(149, 198);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(29, 17);
            this.label73.TabIndex = 135;
            this.label73.Text = "NS";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label75.Location = new System.Drawing.Point(70, 47);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(108, 34);
            this.label75.TabIndex = 137;
            this.label75.Text = "Mano de obra\r\ny repuestos";
            // 
            // botns
            // 
            this.botns.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botns.ForeColor = System.Drawing.Color.RoyalBlue;
            this.botns.Location = new System.Drawing.Point(184, 195);
            this.botns.MaxLength = 30;
            this.botns.Name = "botns";
            this.botns.Size = new System.Drawing.Size(213, 23);
            this.botns.TabIndex = 127;
            // 
            // botgas
            // 
            this.botgas.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botgas.ForeColor = System.Drawing.Color.RoyalBlue;
            this.botgas.Location = new System.Drawing.Point(184, 282);
            this.botgas.MaxLength = 10;
            this.botgas.Name = "botgas";
            this.botgas.Size = new System.Drawing.Size(90, 23);
            this.botgas.TabIndex = 142;
            // 
            // botttttt
            // 
            this.botttttt.AutoSize = true;
            this.botttttt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botttttt.ForeColor = System.Drawing.Color.RoyalBlue;
            this.botttttt.Location = new System.Drawing.Point(93, 227);
            this.botttttt.Name = "botttttt";
            this.botttttt.Size = new System.Drawing.Size(85, 17);
            this.botttttt.TabIndex = 138;
            this.botttttt.Text = "Fabricante";
            // 
            // botorigen
            // 
            this.botorigen.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botorigen.ForeColor = System.Drawing.Color.RoyalBlue;
            this.botorigen.Location = new System.Drawing.Point(184, 166);
            this.botorigen.MaxLength = 30;
            this.botorigen.Name = "botorigen";
            this.botorigen.Size = new System.Drawing.Size(213, 23);
            this.botorigen.TabIndex = 128;
            // 
            // botmo
            // 
            this.botmo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botmo.ForeColor = System.Drawing.Color.RoyalBlue;
            this.botmo.Location = new System.Drawing.Point(184, 44);
            this.botmo.MaxLength = 240;
            this.botmo.Multiline = true;
            this.botmo.Name = "botmo";
            this.botmo.Size = new System.Drawing.Size(588, 58);
            this.botmo.TabIndex = 129;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label77.Location = new System.Drawing.Point(118, 314);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(60, 17);
            this.label77.TabIndex = 139;
            this.label77.Text = "Modelo";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label78.Location = new System.Drawing.Point(118, 372);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(60, 17);
            this.label78.TabIndex = 141;
            this.label78.Text = "Imágen";
            // 
            // botimagen
            // 
            this.botimagen.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botimagen.ForeColor = System.Drawing.Color.RoyalBlue;
            this.botimagen.Location = new System.Drawing.Point(184, 369);
            this.botimagen.MaxLength = 240;
            this.botimagen.Name = "botimagen";
            this.botimagen.Size = new System.Drawing.Size(272, 23);
            this.botimagen.TabIndex = 133;
            // 
            // botfabricante
            // 
            this.botfabricante.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botfabricante.ForeColor = System.Drawing.Color.RoyalBlue;
            this.botfabricante.Location = new System.Drawing.Point(184, 224);
            this.botfabricante.MaxLength = 12;
            this.botfabricante.Name = "botfabricante";
            this.botfabricante.Size = new System.Drawing.Size(213, 23);
            this.botfabricante.TabIndex = 130;
            // 
            // botmarca
            // 
            this.botmarca.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botmarca.ForeColor = System.Drawing.Color.RoyalBlue;
            this.botmarca.Location = new System.Drawing.Point(184, 253);
            this.botmarca.MaxLength = 12;
            this.botmarca.Name = "botmarca";
            this.botmarca.Size = new System.Drawing.Size(130, 23);
            this.botmarca.TabIndex = 132;
            // 
            // botmodelo
            // 
            this.botmodelo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botmodelo.ForeColor = System.Drawing.Color.RoyalBlue;
            this.botmodelo.Location = new System.Drawing.Point(184, 311);
            this.botmodelo.MaxLength = 60;
            this.botmodelo.Name = "botmodelo";
            this.botmodelo.Size = new System.Drawing.Size(272, 23);
            this.botmodelo.TabIndex = 131;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label81.Location = new System.Drawing.Point(126, 256);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(52, 17);
            this.label81.TabIndex = 140;
            this.label81.Text = "Marca";
            // 
            // OTaller
            // 
            this.OTaller.BackColor = System.Drawing.Color.CadetBlue;
            this.OTaller.Controls.Add(this.panel24);
            this.OTaller.Controls.Add(this.otallerimprimir);
            this.OTaller.Controls.Add(this.button3);
            this.OTaller.Controls.Add(this.label82);
            this.OTaller.Controls.Add(this.panel13);
            this.OTaller.Location = new System.Drawing.Point(4, 26);
            this.OTaller.Name = "OTaller";
            this.OTaller.Padding = new System.Windows.Forms.Padding(3);
            this.OTaller.Size = new System.Drawing.Size(894, 581);
            this.OTaller.TabIndex = 8;
            this.OTaller.Text = "O.Taller";
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.otordenbuscar);
            this.panel24.Controls.Add(this.label160);
            this.panel24.Controls.Add(this.otallerbuscar);
            this.panel24.Controls.Add(this.otidbuscar);
            this.panel24.Controls.Add(this.label159);
            this.panel24.Location = new System.Drawing.Point(209, 530);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(477, 43);
            this.panel24.TabIndex = 137;
            // 
            // otordenbuscar
            // 
            this.otordenbuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otordenbuscar.ForeColor = System.Drawing.Color.Black;
            this.otordenbuscar.Location = new System.Drawing.Point(197, 11);
            this.otordenbuscar.MaxLength = 0;
            this.otordenbuscar.Name = "otordenbuscar";
            this.otordenbuscar.Size = new System.Drawing.Size(150, 23);
            this.otordenbuscar.TabIndex = 138;
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label160.ForeColor = System.Drawing.Color.Black;
            this.label160.Location = new System.Drawing.Point(138, 14);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(53, 17);
            this.label160.TabIndex = 139;
            this.label160.Text = "ORDEN";
            // 
            // otallerbuscar
            // 
            this.otallerbuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otallerbuscar.Location = new System.Drawing.Point(353, 3);
            this.otallerbuscar.Name = "otallerbuscar";
            this.otallerbuscar.Size = new System.Drawing.Size(92, 37);
            this.otallerbuscar.TabIndex = 137;
            this.otallerbuscar.Text = "BUSCAR";
            this.otallerbuscar.UseVisualStyleBackColor = true;
            this.otallerbuscar.Click += new System.EventHandler(this.otallerbuscar_Click);
            // 
            // otidbuscar
            // 
            this.otidbuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otidbuscar.ForeColor = System.Drawing.Color.Black;
            this.otidbuscar.Location = new System.Drawing.Point(60, 11);
            this.otidbuscar.MaxLength = 0;
            this.otidbuscar.Name = "otidbuscar";
            this.otidbuscar.Size = new System.Drawing.Size(72, 23);
            this.otidbuscar.TabIndex = 135;
            this.otidbuscar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.otidbuscar_KeyPress);
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label159.ForeColor = System.Drawing.Color.Black;
            this.label159.Location = new System.Drawing.Point(32, 14);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(22, 17);
            this.label159.TabIndex = 136;
            this.label159.Text = "ID";
            // 
            // otallerimprimir
            // 
            this.otallerimprimir.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otallerimprimir.Location = new System.Drawing.Point(23, 530);
            this.otallerimprimir.Name = "otallerimprimir";
            this.otallerimprimir.Size = new System.Drawing.Size(180, 45);
            this.otallerimprimir.TabIndex = 33;
            this.otallerimprimir.Text = "IMPRIMIR REGISTRO";
            this.otallerimprimir.UseVisualStyleBackColor = true;
            this.otallerimprimir.Click += new System.EventHandler(this.otallerimprimir_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(691, 530);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(180, 45);
            this.button3.TabIndex = 32;
            this.button3.Text = "GUARDAR REGISTRO";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.ForeColor = System.Drawing.Color.Black;
            this.label82.Location = new System.Drawing.Point(20, 3);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(185, 24);
            this.label82.TabIndex = 30;
            this.label82.Text = "Crear Orden Taller";
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Silver;
            this.panel13.Controls.Add(this.otcodproyecto);
            this.panel13.Controls.Add(this.groupBox11);
            this.panel13.Controls.Add(this.ottipo);
            this.panel13.Controls.Add(this.label121);
            this.panel13.Controls.Add(this.otcmi);
            this.panel13.Controls.Add(this.labelcmi);
            this.panel13.Controls.Add(this.otresultado);
            this.panel13.Controls.Add(this.label105);
            this.panel13.Controls.Add(this.label104);
            this.panel13.Controls.Add(this.otmo);
            this.panel13.Controls.Add(this.label103);
            this.panel13.Controls.Add(this.otdespla);
            this.panel13.Controls.Add(this.label102);
            this.panel13.Controls.Add(this.ottecnico);
            this.panel13.Controls.Add(this.label84);
            this.panel13.Controls.Add(this.otoferta);
            this.panel13.Controls.Add(this.label94);
            this.panel13.Controls.Add(this.otorden);
            this.panel13.Controls.Add(this.label101);
            this.panel13.Controls.Add(this.otsap);
            this.panel13.Controls.Add(this.otmaterial2);
            this.panel13.Controls.Add(this.label100);
            this.panel13.Controls.Add(this.label98);
            this.panel13.Controls.Add(this.ottaller);
            this.panel13.Controls.Add(this.label99);
            this.panel13.Controls.Add(this.otdesmontaje);
            this.panel13.Controls.Add(this.label97);
            this.panel13.Controls.Add(this.otprovincia);
            this.panel13.Controls.Add(this.otf2);
            this.panel13.Controls.Add(this.label96);
            this.panel13.Controls.Add(this.label83);
            this.panel13.Controls.Add(this.otmontaje);
            this.panel13.Controls.Add(this.otestado);
            this.panel13.Controls.Add(this.label85);
            this.panel13.Controls.Add(this.otf1);
            this.panel13.Controls.Add(this.label86);
            this.panel13.Controls.Add(this.label87);
            this.panel13.Controls.Add(this.label88);
            this.panel13.Controls.Add(this.otid);
            this.panel13.Controls.Add(this.label89);
            this.panel13.Controls.Add(this.label90);
            this.panel13.Controls.Add(this.label91);
            this.panel13.Controls.Add(this.otproyecto);
            this.panel13.Controls.Add(this.otaveria);
            this.panel13.Controls.Add(this.label92);
            this.panel13.Controls.Add(this.otpds);
            this.panel13.Controls.Add(this.otrepuesto);
            this.panel13.Controls.Add(this.label93);
            this.panel13.Controls.Add(this.otcliente);
            this.panel13.Controls.Add(this.otmaterial1);
            this.panel13.Controls.Add(this.otax);
            this.panel13.Controls.Add(this.label95);
            this.panel13.Location = new System.Drawing.Point(23, 30);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(849, 497);
            this.panel13.TabIndex = 31;
            this.panel13.Paint += new System.Windows.Forms.PaintEventHandler(this.panel13_Paint);
            // 
            // otcodproyecto
            // 
            this.otcodproyecto.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otcodproyecto.ForeColor = System.Drawing.Color.Black;
            this.otcodproyecto.Location = new System.Drawing.Point(272, 32);
            this.otcodproyecto.MaxLength = 2;
            this.otcodproyecto.Name = "otcodproyecto";
            this.otcodproyecto.ReadOnly = true;
            this.otcodproyecto.Size = new System.Drawing.Size(58, 23);
            this.otcodproyecto.TabIndex = 188;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.comboPrecio);
            this.groupBox11.Controls.Add(this.OTencuentra_repuestoCod);
            this.groupBox11.Controls.Add(this.OTencuentra_repuesto);
            this.groupBox11.Controls.Add(this.OTañadir_repuesto);
            this.groupBox11.Controls.Add(this.OTbusca_repuesto);
            this.groupBox11.Controls.Add(this.label158);
            this.groupBox11.Controls.Add(this.label157);
            this.groupBox11.Controls.Add(this.OTrepuesto_uds);
            this.groupBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox11.Location = new System.Drawing.Point(119, 397);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(710, 97);
            this.groupBox11.TabIndex = 187;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Agregar repuestos";
            // 
            // comboPrecio
            // 
            this.comboPrecio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboPrecio.ForeColor = System.Drawing.Color.Black;
            this.comboPrecio.FormattingEnabled = true;
            this.comboPrecio.Location = new System.Drawing.Point(483, 43);
            this.comboPrecio.Name = "comboPrecio";
            this.comboPrecio.Size = new System.Drawing.Size(65, 21);
            this.comboPrecio.TabIndex = 189;
            this.comboPrecio.SelectionChangeCommitted += new System.EventHandler(this.comboPrecio_SelectionChangeCommitted);
            // 
            // OTencuentra_repuestoCod
            // 
            this.OTencuentra_repuestoCod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.OTencuentra_repuestoCod.ForeColor = System.Drawing.Color.Black;
            this.OTencuentra_repuestoCod.FormattingEnabled = true;
            this.OTencuentra_repuestoCod.Location = new System.Drawing.Point(343, 43);
            this.OTencuentra_repuestoCod.Name = "OTencuentra_repuestoCod";
            this.OTencuentra_repuestoCod.Size = new System.Drawing.Size(134, 21);
            this.OTencuentra_repuestoCod.TabIndex = 187;
            this.OTencuentra_repuestoCod.SelectedValueChanged += new System.EventHandler(this.OTencuentra_repuestoCod_SelectedValueChanged);
            // 
            // OTencuentra_repuesto
            // 
            this.OTencuentra_repuesto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.OTencuentra_repuesto.ForeColor = System.Drawing.Color.Black;
            this.OTencuentra_repuesto.FormattingEnabled = true;
            this.OTencuentra_repuesto.Location = new System.Drawing.Point(48, 43);
            this.OTencuentra_repuesto.Name = "OTencuentra_repuesto";
            this.OTencuentra_repuesto.Size = new System.Drawing.Size(289, 21);
            this.OTencuentra_repuesto.TabIndex = 183;
            this.OTencuentra_repuesto.SelectedValueChanged += new System.EventHandler(this.OTencuentra_repuesto_SelectedValueChanged);
            // 
            // OTañadir_repuesto
            // 
            this.OTañadir_repuesto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OTañadir_repuesto.Location = new System.Drawing.Point(609, 15);
            this.OTañadir_repuesto.Name = "OTañadir_repuesto";
            this.OTañadir_repuesto.Size = new System.Drawing.Size(95, 59);
            this.OTañadir_repuesto.TabIndex = 186;
            this.OTañadir_repuesto.Text = "Añadir";
            this.OTañadir_repuesto.UseVisualStyleBackColor = true;
            this.OTañadir_repuesto.Click += new System.EventHandler(this.OTañadir_repuesto_Click);
            // 
            // OTbusca_repuesto
            // 
            this.OTbusca_repuesto.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OTbusca_repuesto.ForeColor = System.Drawing.Color.Black;
            this.OTbusca_repuesto.Location = new System.Drawing.Point(119, 14);
            this.OTbusca_repuesto.MaxLength = 15;
            this.OTbusca_repuesto.Name = "OTbusca_repuesto";
            this.OTbusca_repuesto.Size = new System.Drawing.Size(136, 23);
            this.OTbusca_repuesto.TabIndex = 181;
            this.OTbusca_repuesto.TextChanged += new System.EventHandler(this.OTbusca_repuesto_TextChanged);
            this.OTbusca_repuesto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OTbusca_repuesto_KeyPress);
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label158.ForeColor = System.Drawing.Color.Black;
            this.label158.Location = new System.Drawing.Point(261, 16);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(31, 16);
            this.label158.TabIndex = 185;
            this.label158.Text = "Uds";
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label157.ForeColor = System.Drawing.Color.Black;
            this.label157.Location = new System.Drawing.Point(48, 16);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(63, 16);
            this.label157.TabIndex = 182;
            this.label157.Text = "Repuesto";
            // 
            // OTrepuesto_uds
            // 
            this.OTrepuesto_uds.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OTrepuesto_uds.ForeColor = System.Drawing.Color.Black;
            this.OTrepuesto_uds.Location = new System.Drawing.Point(298, 14);
            this.OTrepuesto_uds.MaxLength = 15;
            this.OTrepuesto_uds.Name = "OTrepuesto_uds";
            this.OTrepuesto_uds.Size = new System.Drawing.Size(35, 23);
            this.OTrepuesto_uds.TabIndex = 184;
            this.OTrepuesto_uds.Text = "1";
            this.OTrepuesto_uds.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OTrepuesto_uds_KeyPress);
            // 
            // ottipo
            // 
            this.ottipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ottipo.ForeColor = System.Drawing.Color.Black;
            this.ottipo.FormattingEnabled = true;
            this.ottipo.Items.AddRange(new object[] {
            "CAF CON RECAMBIO",
            "CAF SIN RECAMBIO",
            "MOL CON RECAMBIO",
            "MOL SIN RECAMBIO",
            "PUESTA A PUNTO CAF",
            "PUESTA A PUNTO MOL",
            "MONTAJE",
            "DESMONTAJE",
            "CT MONTAJE",
            "CT DESMONTAJE",
            "LIMPIEZA",
            "OTROS",
            "CT MOL MONTAJE",
            "CT MOL DESMONTAJE",
            "REPARACION TALLER HORNO A",
            "REPARACION TALLER HORNO B",
            "REPARACION TALLER HORNO C",
            "REPUESTOS",
            "PUESTA A PUNTO HORNO",
            "PUESTA A PUNTO ARCON",
            "REPARACION TALLER ARCON",
            "MONTAJE ARCON",
            "DESMONTAJE ARCON",
            "MONTAJE HORNO",
            "DESMONTAJE HORNO",
            "CT MONTAJE ARCON",
            "CT DESMONTAJE ARCON",
            "CT MONTAJE HORNO",
            "CT DESMONTAJE HORNO",
            "REPARACION TALLER",
            "REPARACION GARANTIA"});
            this.ottipo.Location = new System.Drawing.Point(610, 61);
            this.ottipo.Name = "ottipo";
            this.ottipo.Size = new System.Drawing.Size(219, 21);
            this.ottipo.TabIndex = 180;
            this.ottipo.SelectedValueChanged += new System.EventHandler(this.ottipo_SelectedValueChanged);
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label121.ForeColor = System.Drawing.Color.Black;
            this.label121.Location = new System.Drawing.Point(568, 64);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(36, 17);
            this.label121.TabIndex = 179;
            this.label121.Text = "Tipo";
            // 
            // otcmi
            // 
            this.otcmi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otcmi.ForeColor = System.Drawing.Color.Black;
            this.otcmi.Location = new System.Drawing.Point(459, 177);
            this.otcmi.MaxLength = 2;
            this.otcmi.Name = "otcmi";
            this.otcmi.Size = new System.Drawing.Size(35, 23);
            this.otcmi.TabIndex = 178;
            this.otcmi.Text = "NO";
            // 
            // labelcmi
            // 
            this.labelcmi.AutoSize = true;
            this.labelcmi.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelcmi.ForeColor = System.Drawing.Color.Black;
            this.labelcmi.Location = new System.Drawing.Point(419, 180);
            this.labelcmi.Name = "labelcmi";
            this.labelcmi.Size = new System.Drawing.Size(32, 17);
            this.labelcmi.TabIndex = 177;
            this.labelcmi.Text = "CMI";
            // 
            // otresultado
            // 
            this.otresultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otresultado.ForeColor = System.Drawing.Color.Black;
            this.otresultado.Location = new System.Drawing.Point(619, 3);
            this.otresultado.MaxLength = 20;
            this.otresultado.Name = "otresultado";
            this.otresultado.Size = new System.Drawing.Size(103, 23);
            this.otresultado.TabIndex = 176;
            this.otresultado.Text = "ACEPTADO";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.ForeColor = System.Drawing.Color.Black;
            this.label105.Location = new System.Drawing.Point(544, 6);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(69, 17);
            this.label105.TabIndex = 175;
            this.label105.Text = "Resultado";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.ForeColor = System.Drawing.Color.Black;
            this.label104.Location = new System.Drawing.Point(670, 180);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(54, 17);
            this.label104.TabIndex = 174;
            this.label104.Text = "M.Obra";
            // 
            // otmo
            // 
            this.otmo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otmo.ForeColor = System.Drawing.Color.Black;
            this.otmo.Location = new System.Drawing.Point(737, 177);
            this.otmo.MaxLength = 6;
            this.otmo.Name = "otmo";
            this.otmo.Size = new System.Drawing.Size(92, 23);
            this.otmo.TabIndex = 173;
            this.otmo.Text = "0";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.ForeColor = System.Drawing.Color.Black;
            this.label103.Location = new System.Drawing.Point(522, 180);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(56, 17);
            this.label103.TabIndex = 172;
            this.label103.Text = "Desplaz";
            // 
            // otdespla
            // 
            this.otdespla.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otdespla.ForeColor = System.Drawing.Color.Black;
            this.otdespla.Location = new System.Drawing.Point(594, 177);
            this.otdespla.MaxLength = 6;
            this.otdespla.Name = "otdespla";
            this.otdespla.Size = new System.Drawing.Size(70, 23);
            this.otdespla.TabIndex = 171;
            this.otdespla.Text = "0";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.ForeColor = System.Drawing.Color.Black;
            this.label102.Location = new System.Drawing.Point(58, 5);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(55, 17);
            this.label102.TabIndex = 170;
            this.label102.Text = "Técnico";
            // 
            // ottecnico
            // 
            this.ottecnico.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ottecnico.ForeColor = System.Drawing.Color.Black;
            this.ottecnico.Location = new System.Drawing.Point(119, 3);
            this.ottecnico.MaxLength = 60;
            this.ottecnico.Name = "ottecnico";
            this.ottecnico.Size = new System.Drawing.Size(236, 23);
            this.ottecnico.TabIndex = 169;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.ForeColor = System.Drawing.Color.Black;
            this.label84.Location = new System.Drawing.Point(261, 180);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(47, 17);
            this.label84.TabIndex = 168;
            this.label84.Text = "Oferta";
            // 
            // otoferta
            // 
            this.otoferta.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otoferta.ForeColor = System.Drawing.Color.Black;
            this.otoferta.Location = new System.Drawing.Point(321, 177);
            this.otoferta.MaxLength = 8;
            this.otoferta.Name = "otoferta";
            this.otoferta.Size = new System.Drawing.Size(70, 23);
            this.otoferta.TabIndex = 167;
            this.otoferta.Text = "0";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.ForeColor = System.Drawing.Color.Black;
            this.label94.Location = new System.Drawing.Point(33, 179);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(80, 17);
            this.label94.TabIndex = 166;
            this.label94.Text = "Num.Orden";
            // 
            // otorden
            // 
            this.otorden.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otorden.ForeColor = System.Drawing.Color.Black;
            this.otorden.Location = new System.Drawing.Point(119, 177);
            this.otorden.MaxLength = 15;
            this.otorden.Name = "otorden";
            this.otorden.Size = new System.Drawing.Size(136, 23);
            this.otorden.TabIndex = 165;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.ForeColor = System.Drawing.Color.Black;
            this.label101.Location = new System.Drawing.Point(465, 151);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(53, 17);
            this.label101.TabIndex = 164;
            this.label101.Text = "NS SAP";
            // 
            // otsap
            // 
            this.otsap.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otsap.ForeColor = System.Drawing.Color.Black;
            this.otsap.Location = new System.Drawing.Point(535, 148);
            this.otsap.MaxLength = 30;
            this.otsap.Name = "otsap";
            this.otsap.Size = new System.Drawing.Size(294, 23);
            this.otsap.TabIndex = 163;
            // 
            // otmaterial2
            // 
            this.otmaterial2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otmaterial2.ForeColor = System.Drawing.Color.Black;
            this.otmaterial2.Location = new System.Drawing.Point(717, 119);
            this.otmaterial2.MaxLength = 12;
            this.otmaterial2.Name = "otmaterial2";
            this.otmaterial2.Size = new System.Drawing.Size(112, 23);
            this.otmaterial2.TabIndex = 161;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.ForeColor = System.Drawing.Color.Black;
            this.label100.Location = new System.Drawing.Point(612, 122);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(87, 17);
            this.label100.TabIndex = 162;
            this.label100.Text = "Cód.Material";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.ForeColor = System.Drawing.Color.Black;
            this.label98.Location = new System.Drawing.Point(640, 93);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(43, 17);
            this.label98.TabIndex = 160;
            this.label98.Text = "Taller";
            // 
            // ottaller
            // 
            this.ottaller.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ottaller.ForeColor = System.Drawing.Color.Black;
            this.ottaller.Location = new System.Drawing.Point(696, 90);
            this.ottaller.MaxLength = 15;
            this.ottaller.Name = "ottaller";
            this.ottaller.Size = new System.Drawing.Size(133, 23);
            this.ottaller.TabIndex = 159;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.ForeColor = System.Drawing.Color.Black;
            this.label99.Location = new System.Drawing.Point(466, 93);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(51, 17);
            this.label99.TabIndex = 158;
            this.label99.Text = "Desmo";
            // 
            // otdesmontaje
            // 
            this.otdesmontaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otdesmontaje.ForeColor = System.Drawing.Color.Black;
            this.otdesmontaje.Location = new System.Drawing.Point(523, 90);
            this.otdesmontaje.MaxLength = 15;
            this.otdesmontaje.Name = "otdesmontaje";
            this.otdesmontaje.Size = new System.Drawing.Size(111, 23);
            this.otdesmontaje.TabIndex = 157;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.ForeColor = System.Drawing.Color.Black;
            this.label97.Location = new System.Drawing.Point(369, 64);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(65, 17);
            this.label97.TabIndex = 156;
            this.label97.Text = "Provincia";
            // 
            // otprovincia
            // 
            this.otprovincia.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otprovincia.ForeColor = System.Drawing.Color.Black;
            this.otprovincia.Location = new System.Drawing.Point(440, 62);
            this.otprovincia.MaxLength = 60;
            this.otprovincia.Name = "otprovincia";
            this.otprovincia.Size = new System.Drawing.Size(122, 23);
            this.otprovincia.TabIndex = 155;
            // 
            // otf2
            // 
            this.otf2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otf2.ForeColor = System.Drawing.Color.Black;
            this.otf2.Location = new System.Drawing.Point(722, 32);
            this.otf2.MaxLength = 8;
            this.otf2.Name = "otf2";
            this.otf2.Size = new System.Drawing.Size(107, 23);
            this.otf2.TabIndex = 154;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.ForeColor = System.Drawing.Color.Black;
            this.label96.Location = new System.Drawing.Point(659, 35);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(57, 17);
            this.label96.TabIndex = 153;
            this.label96.Text = "F.Orden";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.ForeColor = System.Drawing.Color.Black;
            this.label83.Location = new System.Drawing.Point(242, 93);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(59, 17);
            this.label83.TabIndex = 152;
            this.label83.Text = "Montaje";
            // 
            // otmontaje
            // 
            this.otmontaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otmontaje.ForeColor = System.Drawing.Color.Black;
            this.otmontaje.Location = new System.Drawing.Point(307, 90);
            this.otmontaje.MaxLength = 15;
            this.otmontaje.Name = "otmontaje";
            this.otmontaje.Size = new System.Drawing.Size(111, 23);
            this.otmontaje.TabIndex = 151;
            // 
            // otestado
            // 
            this.otestado.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otestado.ForeColor = System.Drawing.Color.Black;
            this.otestado.Location = new System.Drawing.Point(450, 3);
            this.otestado.MaxLength = 20;
            this.otestado.Name = "otestado";
            this.otestado.Size = new System.Drawing.Size(76, 23);
            this.otestado.TabIndex = 147;
            this.otestado.Text = "CERRADO";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.ForeColor = System.Drawing.Color.Black;
            this.label85.Location = new System.Drawing.Point(395, 6);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(49, 17);
            this.label85.TabIndex = 146;
            this.label85.Text = "Estado";
            // 
            // otf1
            // 
            this.otf1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otf1.ForeColor = System.Drawing.Color.Black;
            this.otf1.Location = new System.Drawing.Point(558, 32);
            this.otf1.MaxLength = 8;
            this.otf1.Name = "otf1";
            this.otf1.Size = new System.Drawing.Size(85, 23);
            this.otf1.TabIndex = 145;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.ForeColor = System.Drawing.Color.Black;
            this.label86.Location = new System.Drawing.Point(486, 34);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(66, 17);
            this.label86.TabIndex = 144;
            this.label86.Text = "F.Trabajo";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.ForeColor = System.Drawing.Color.Black;
            this.label87.Location = new System.Drawing.Point(66, 93);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(47, 17);
            this.label87.TabIndex = 143;
            this.label87.Text = "Avería";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.ForeColor = System.Drawing.Color.Black;
            this.label88.Location = new System.Drawing.Point(729, 6);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(22, 17);
            this.label88.TabIndex = 134;
            this.label88.Text = "ID";
            // 
            // otid
            // 
            this.otid.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otid.ForeColor = System.Drawing.Color.Black;
            this.otid.Location = new System.Drawing.Point(757, 3);
            this.otid.MaxLength = 0;
            this.otid.Name = "otid";
            this.otid.ReadOnly = true;
            this.otid.Size = new System.Drawing.Size(72, 23);
            this.otid.TabIndex = 126;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.ForeColor = System.Drawing.Color.Black;
            this.label89.Location = new System.Drawing.Point(336, 36);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(30, 17);
            this.label89.TabIndex = 136;
            this.label89.Text = "Pds";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.ForeColor = System.Drawing.Color.Black;
            this.label90.Location = new System.Drawing.Point(51, 34);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(62, 17);
            this.label90.TabIndex = 135;
            this.label90.Text = "Proyecto";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.ForeColor = System.Drawing.Color.Black;
            this.label91.Location = new System.Drawing.Point(19, 208);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(94, 34);
            this.label91.TabIndex = 137;
            this.label91.Text = "Mano de obra\r\ny repuestos";
            // 
            // otproyecto
            // 
            this.otproyecto.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otproyecto.ForeColor = System.Drawing.Color.Black;
            this.otproyecto.Location = new System.Drawing.Point(119, 32);
            this.otproyecto.MaxLength = 12;
            this.otproyecto.Name = "otproyecto";
            this.otproyecto.Size = new System.Drawing.Size(147, 23);
            this.otproyecto.TabIndex = 127;
            // 
            // otaveria
            // 
            this.otaveria.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otaveria.ForeColor = System.Drawing.Color.Black;
            this.otaveria.Location = new System.Drawing.Point(119, 90);
            this.otaveria.MaxLength = 15;
            this.otaveria.Name = "otaveria";
            this.otaveria.Size = new System.Drawing.Size(111, 23);
            this.otaveria.TabIndex = 142;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.ForeColor = System.Drawing.Color.Black;
            this.label92.Location = new System.Drawing.Point(62, 64);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(51, 17);
            this.label92.TabIndex = 138;
            this.label92.Text = "Cliente";
            // 
            // otpds
            // 
            this.otpds.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otpds.ForeColor = System.Drawing.Color.Black;
            this.otpds.Location = new System.Drawing.Point(372, 33);
            this.otpds.MaxLength = 12;
            this.otpds.Name = "otpds";
            this.otpds.Size = new System.Drawing.Size(108, 23);
            this.otpds.TabIndex = 128;
            // 
            // otrepuesto
            // 
            this.otrepuesto.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otrepuesto.ForeColor = System.Drawing.Color.Black;
            this.otrepuesto.Location = new System.Drawing.Point(119, 206);
            this.otrepuesto.MaxLength = 1024;
            this.otrepuesto.Multiline = true;
            this.otrepuesto.Name = "otrepuesto";
            this.otrepuesto.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.otrepuesto.Size = new System.Drawing.Size(710, 185);
            this.otrepuesto.TabIndex = 129;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.ForeColor = System.Drawing.Color.Black;
            this.label93.Location = new System.Drawing.Point(66, 150);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(47, 17);
            this.label93.TabIndex = 139;
            this.label93.Text = "NS AX";
            // 
            // otcliente
            // 
            this.otcliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otcliente.ForeColor = System.Drawing.Color.Black;
            this.otcliente.Location = new System.Drawing.Point(119, 61);
            this.otcliente.MaxLength = 60;
            this.otcliente.Name = "otcliente";
            this.otcliente.Size = new System.Drawing.Size(247, 23);
            this.otcliente.TabIndex = 130;
            // 
            // otmaterial1
            // 
            this.otmaterial1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otmaterial1.ForeColor = System.Drawing.Color.Black;
            this.otmaterial1.Location = new System.Drawing.Point(119, 119);
            this.otmaterial1.MaxLength = 80;
            this.otmaterial1.Name = "otmaterial1";
            this.otmaterial1.Size = new System.Drawing.Size(429, 23);
            this.otmaterial1.TabIndex = 132;
            // 
            // otax
            // 
            this.otax.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otax.ForeColor = System.Drawing.Color.Black;
            this.otax.Location = new System.Drawing.Point(119, 148);
            this.otax.MaxLength = 30;
            this.otax.Name = "otax";
            this.otax.Size = new System.Drawing.Size(272, 23);
            this.otax.TabIndex = 131;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.ForeColor = System.Drawing.Color.Black;
            this.label95.Location = new System.Drawing.Point(54, 121);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(59, 17);
            this.label95.TabIndex = 140;
            this.label95.Text = "Material";
            // 
            // IBOTaller
            // 
            this.IBOTaller.BackColor = System.Drawing.Color.CadetBlue;
            this.IBOTaller.Controls.Add(this.panel15);
            this.IBOTaller.Location = new System.Drawing.Point(4, 26);
            this.IBOTaller.Name = "IBOTaller";
            this.IBOTaller.Padding = new System.Windows.Forms.Padding(3);
            this.IBOTaller.Size = new System.Drawing.Size(894, 581);
            this.IBOTaller.TabIndex = 9;
            this.IBOTaller.Text = "I.B.O.Taller";
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Khaki;
            this.panel15.Controls.Add(this.groupBox9);
            this.panel15.Controls.Add(this.IBOTallerCerrar);
            this.panel15.Controls.Add(this.label152);
            this.panel15.Controls.Add(this.label153);
            this.panel15.Controls.Add(this.groupBox8);
            this.panel15.Controls.Add(this.comboIBOTaller);
            this.panel15.Controls.Add(this.IBOTallerConsultar);
            this.panel15.Controls.Add(this.DateIBOTallerFin);
            this.panel15.Controls.Add(this.DateIBOTallerInicio);
            this.panel15.Location = new System.Drawing.Point(22, 20);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(850, 293);
            this.panel15.TabIndex = 0;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.radioButtonAbierto);
            this.groupBox9.Controls.Add(this.radioButtonCerrado);
            this.groupBox9.Enabled = false;
            this.groupBox9.Location = new System.Drawing.Point(269, 88);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(169, 82);
            this.groupBox9.TabIndex = 183;
            this.groupBox9.TabStop = false;
            // 
            // radioButtonAbierto
            // 
            this.radioButtonAbierto.AutoSize = true;
            this.radioButtonAbierto.Location = new System.Drawing.Point(42, 21);
            this.radioButtonAbierto.Name = "radioButtonAbierto";
            this.radioButtonAbierto.Size = new System.Drawing.Size(63, 17);
            this.radioButtonAbierto.TabIndex = 1;
            this.radioButtonAbierto.TabStop = true;
            this.radioButtonAbierto.Text = "Abiertas";
            this.radioButtonAbierto.UseVisualStyleBackColor = true;
            // 
            // radioButtonCerrado
            // 
            this.radioButtonCerrado.AutoSize = true;
            this.radioButtonCerrado.Location = new System.Drawing.Point(42, 44);
            this.radioButtonCerrado.Name = "radioButtonCerrado";
            this.radioButtonCerrado.Size = new System.Drawing.Size(67, 17);
            this.radioButtonCerrado.TabIndex = 0;
            this.radioButtonCerrado.TabStop = true;
            this.radioButtonCerrado.Text = "Cerradas";
            this.radioButtonCerrado.UseVisualStyleBackColor = true;
            // 
            // IBOTallerCerrar
            // 
            this.IBOTallerCerrar.Location = new System.Drawing.Point(618, 227);
            this.IBOTallerCerrar.Name = "IBOTallerCerrar";
            this.IBOTallerCerrar.Size = new System.Drawing.Size(157, 40);
            this.IBOTallerCerrar.TabIndex = 182;
            this.IBOTallerCerrar.Text = "Concluir orden";
            this.IBOTallerCerrar.UseVisualStyleBackColor = true;
            this.IBOTallerCerrar.Click += new System.EventHandler(this.IBOTallerCerrar_Click);
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label152.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label152.Location = new System.Drawing.Point(60, 22);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(95, 17);
            this.label152.TabIndex = 181;
            this.label152.Text = "Fecha inicio";
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label153.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label153.Location = new System.Drawing.Point(60, 68);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(75, 17);
            this.label153.TabIndex = 180;
            this.label153.Text = "Fecha fin";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.radioButtonRechazado);
            this.groupBox8.Controls.Add(this.radioButtonAceptado);
            this.groupBox8.Enabled = false;
            this.groupBox8.Location = new System.Drawing.Point(444, 88);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(169, 82);
            this.groupBox8.TabIndex = 179;
            this.groupBox8.TabStop = false;
            // 
            // radioButtonRechazado
            // 
            this.radioButtonRechazado.AutoSize = true;
            this.radioButtonRechazado.Location = new System.Drawing.Point(42, 21);
            this.radioButtonRechazado.Name = "radioButtonRechazado";
            this.radioButtonRechazado.Size = new System.Drawing.Size(80, 17);
            this.radioButtonRechazado.TabIndex = 1;
            this.radioButtonRechazado.TabStop = true;
            this.radioButtonRechazado.Text = "Rechazado";
            this.radioButtonRechazado.UseVisualStyleBackColor = true;
            // 
            // radioButtonAceptado
            // 
            this.radioButtonAceptado.AutoSize = true;
            this.radioButtonAceptado.Location = new System.Drawing.Point(42, 44);
            this.radioButtonAceptado.Name = "radioButtonAceptado";
            this.radioButtonAceptado.Size = new System.Drawing.Size(71, 17);
            this.radioButtonAceptado.TabIndex = 0;
            this.radioButtonAceptado.TabStop = true;
            this.radioButtonAceptado.Text = "Aceptado";
            this.radioButtonAceptado.UseVisualStyleBackColor = true;
            // 
            // comboIBOTaller
            // 
            this.comboIBOTaller.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboIBOTaller.FormattingEnabled = true;
            this.comboIBOTaller.Items.AddRange(new object[] {
            "FECHA DE REALIZACION DE TRABAJO",
            "FECHA DE CREACION DE ORDEN"});
            this.comboIBOTaller.Location = new System.Drawing.Point(269, 41);
            this.comboIBOTaller.Name = "comboIBOTaller";
            this.comboIBOTaller.Size = new System.Drawing.Size(248, 21);
            this.comboIBOTaller.TabIndex = 3;
            // 
            // IBOTallerConsultar
            // 
            this.IBOTallerConsultar.Location = new System.Drawing.Point(143, 227);
            this.IBOTallerConsultar.Name = "IBOTallerConsultar";
            this.IBOTallerConsultar.Size = new System.Drawing.Size(157, 40);
            this.IBOTallerConsultar.TabIndex = 2;
            this.IBOTallerConsultar.Text = "Vista previa";
            this.IBOTallerConsultar.UseVisualStyleBackColor = true;
            this.IBOTallerConsultar.Click += new System.EventHandler(this.IBOTallerConsultar_Click);
            // 
            // DateIBOTallerFin
            // 
            this.DateIBOTallerFin.Location = new System.Drawing.Point(63, 88);
            this.DateIBOTallerFin.Name = "DateIBOTallerFin";
            this.DateIBOTallerFin.Size = new System.Drawing.Size(200, 20);
            this.DateIBOTallerFin.TabIndex = 1;
            // 
            // DateIBOTallerInicio
            // 
            this.DateIBOTallerInicio.Location = new System.Drawing.Point(63, 42);
            this.DateIBOTallerInicio.Name = "DateIBOTallerInicio";
            this.DateIBOTallerInicio.Size = new System.Drawing.Size(200, 20);
            this.DateIBOTallerInicio.TabIndex = 0;
            // 
            // DisBotSal
            // 
            this.DisBotSal.BackColor = System.Drawing.Color.CadetBlue;
            this.DisBotSal.Controls.Add(this.salbotimprimir);
            this.DisBotSal.Controls.Add(this.salotguardar);
            this.DisBotSal.Controls.Add(this.label106);
            this.DisBotSal.Controls.Add(this.panel16);
            this.DisBotSal.Location = new System.Drawing.Point(4, 26);
            this.DisBotSal.Name = "DisBotSal";
            this.DisBotSal.Padding = new System.Windows.Forms.Padding(3);
            this.DisBotSal.Size = new System.Drawing.Size(894, 581);
            this.DisBotSal.TabIndex = 10;
            this.DisBotSal.Text = "SalBotCrear";
            // 
            // salbotimprimir
            // 
            this.salbotimprimir.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salbotimprimir.Location = new System.Drawing.Point(6, 514);
            this.salbotimprimir.Name = "salbotimprimir";
            this.salbotimprimir.Size = new System.Drawing.Size(213, 45);
            this.salbotimprimir.TabIndex = 34;
            this.salbotimprimir.Text = "IMPRIMIR REGISTRO";
            this.salbotimprimir.UseVisualStyleBackColor = true;
            // 
            // salotguardar
            // 
            this.salotguardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salotguardar.Location = new System.Drawing.Point(675, 514);
            this.salotguardar.Name = "salotguardar";
            this.salotguardar.Size = new System.Drawing.Size(213, 45);
            this.salotguardar.TabIndex = 33;
            this.salotguardar.Text = "GUARDAR REGISTRO";
            this.salotguardar.UseVisualStyleBackColor = true;
            this.salotguardar.Click += new System.EventHandler(this.salotguardar_Click);
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.ForeColor = System.Drawing.Color.White;
            this.label106.Location = new System.Drawing.Point(39, 8);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(326, 29);
            this.label106.TabIndex = 31;
            this.label106.Text = "Nueva salida de botelleros";
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Khaki;
            this.panel16.Controls.Add(this.label112);
            this.panel16.Controls.Add(this.salbottransportista);
            this.panel16.Controls.Add(this.label111);
            this.panel16.Controls.Add(this.salbotorigen);
            this.panel16.Controls.Add(this.label110);
            this.panel16.Controls.Add(this.label109);
            this.panel16.Controls.Add(this.label108);
            this.panel16.Controls.Add(this.label107);
            this.panel16.Controls.Add(this.salbotcantidad);
            this.panel16.Controls.Add(this.salbotdestino);
            this.panel16.Controls.Add(this.salbotns);
            this.panel16.Controls.Add(this.salbotfecha);
            this.panel16.Location = new System.Drawing.Point(44, 40);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(812, 380);
            this.panel16.TabIndex = 0;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label112.Location = new System.Drawing.Point(244, 108);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(105, 17);
            this.label112.TabIndex = 146;
            this.label112.Text = "Transportista";
            // 
            // salbottransportista
            // 
            this.salbottransportista.Location = new System.Drawing.Point(355, 107);
            this.salbottransportista.Name = "salbottransportista";
            this.salbottransportista.Size = new System.Drawing.Size(179, 20);
            this.salbottransportista.TabIndex = 145;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label111.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label111.Location = new System.Drawing.Point(75, 40);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(57, 17);
            this.label111.TabIndex = 144;
            this.label111.Text = "Origen";
            // 
            // salbotorigen
            // 
            this.salbotorigen.Location = new System.Drawing.Point(138, 40);
            this.salbotorigen.Name = "salbotorigen";
            this.salbotorigen.Size = new System.Drawing.Size(100, 20);
            this.salbotorigen.TabIndex = 143;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label110.Location = new System.Drawing.Point(69, 113);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(136, 17);
            this.label110.TabIndex = 142;
            this.label110.Text = "Números de serie";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label109.Location = new System.Drawing.Point(537, 87);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(123, 17);
            this.label109.TabIndex = 141;
            this.label109.Text = "Fecha de salida";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label108.Location = new System.Drawing.Point(244, 40);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(63, 17);
            this.label108.TabIndex = 140;
            this.label108.Text = "Destino";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label107.Location = new System.Drawing.Point(60, 69);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(72, 17);
            this.label107.TabIndex = 139;
            this.label107.Text = "Cantidad";
            // 
            // salbotcantidad
            // 
            this.salbotcantidad.Location = new System.Drawing.Point(138, 66);
            this.salbotcantidad.Name = "salbotcantidad";
            this.salbotcantidad.Size = new System.Drawing.Size(100, 20);
            this.salbotcantidad.TabIndex = 132;
            // 
            // salbotdestino
            // 
            this.salbotdestino.Location = new System.Drawing.Point(313, 39);
            this.salbotdestino.Name = "salbotdestino";
            this.salbotdestino.Size = new System.Drawing.Size(100, 20);
            this.salbotdestino.TabIndex = 131;
            // 
            // salbotns
            // 
            this.salbotns.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salbotns.ForeColor = System.Drawing.Color.RoyalBlue;
            this.salbotns.Location = new System.Drawing.Point(72, 133);
            this.salbotns.MaxLength = 1024;
            this.salbotns.Multiline = true;
            this.salbotns.Name = "salbotns";
            this.salbotns.Size = new System.Drawing.Size(668, 188);
            this.salbotns.TabIndex = 130;
            // 
            // salbotfecha
            // 
            this.salbotfecha.Location = new System.Drawing.Point(540, 107);
            this.salbotfecha.Name = "salbotfecha";
            this.salbotfecha.Size = new System.Drawing.Size(200, 20);
            this.salbotfecha.TabIndex = 1;
            // 
            // DisBotEnt
            // 
            this.DisBotEnt.BackColor = System.Drawing.Color.CadetBlue;
            this.DisBotEnt.Controls.Add(this.entbotimprimir);
            this.DisBotEnt.Controls.Add(this.entbotguardar);
            this.DisBotEnt.Controls.Add(this.label113);
            this.DisBotEnt.Controls.Add(this.panel17);
            this.DisBotEnt.Location = new System.Drawing.Point(4, 26);
            this.DisBotEnt.Name = "DisBotEnt";
            this.DisBotEnt.Padding = new System.Windows.Forms.Padding(3);
            this.DisBotEnt.Size = new System.Drawing.Size(894, 581);
            this.DisBotEnt.TabIndex = 11;
            this.DisBotEnt.Text = "EntBotCrear";
            // 
            // entbotimprimir
            // 
            this.entbotimprimir.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.entbotimprimir.Location = new System.Drawing.Point(6, 514);
            this.entbotimprimir.Name = "entbotimprimir";
            this.entbotimprimir.Size = new System.Drawing.Size(213, 45);
            this.entbotimprimir.TabIndex = 34;
            this.entbotimprimir.Text = "IMPRIMIR REGISTRO";
            this.entbotimprimir.UseVisualStyleBackColor = true;
            // 
            // entbotguardar
            // 
            this.entbotguardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.entbotguardar.Location = new System.Drawing.Point(675, 514);
            this.entbotguardar.Name = "entbotguardar";
            this.entbotguardar.Size = new System.Drawing.Size(213, 45);
            this.entbotguardar.TabIndex = 33;
            this.entbotguardar.Text = "GUARDAR REGISTRO";
            this.entbotguardar.UseVisualStyleBackColor = true;
            this.entbotguardar.Click += new System.EventHandler(this.entbotguardar_Click);
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.ForeColor = System.Drawing.Color.White;
            this.label113.Location = new System.Drawing.Point(39, 8);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(344, 29);
            this.label113.TabIndex = 31;
            this.label113.Text = "Nueva entrada de botelleros";
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Khaki;
            this.panel17.Controls.Add(this.label114);
            this.panel17.Controls.Add(this.entbottransportista);
            this.panel17.Controls.Add(this.label115);
            this.panel17.Controls.Add(this.entbotorigen);
            this.panel17.Controls.Add(this.label116);
            this.panel17.Controls.Add(this.label117);
            this.panel17.Controls.Add(this.label118);
            this.panel17.Controls.Add(this.label119);
            this.panel17.Controls.Add(this.entbotcantidad);
            this.panel17.Controls.Add(this.entbotdestino);
            this.panel17.Controls.Add(this.entbotns);
            this.panel17.Controls.Add(this.entbotfecha);
            this.panel17.Location = new System.Drawing.Point(44, 40);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(812, 380);
            this.panel17.TabIndex = 0;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label114.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label114.Location = new System.Drawing.Point(244, 108);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(105, 17);
            this.label114.TabIndex = 146;
            this.label114.Text = "Transportista";
            // 
            // entbottransportista
            // 
            this.entbottransportista.Location = new System.Drawing.Point(355, 107);
            this.entbottransportista.Name = "entbottransportista";
            this.entbottransportista.Size = new System.Drawing.Size(179, 20);
            this.entbottransportista.TabIndex = 145;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label115.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label115.Location = new System.Drawing.Point(75, 40);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(57, 17);
            this.label115.TabIndex = 144;
            this.label115.Text = "Origen";
            // 
            // entbotorigen
            // 
            this.entbotorigen.Location = new System.Drawing.Point(138, 40);
            this.entbotorigen.Name = "entbotorigen";
            this.entbotorigen.Size = new System.Drawing.Size(100, 20);
            this.entbotorigen.TabIndex = 143;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label116.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label116.Location = new System.Drawing.Point(69, 113);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(136, 17);
            this.label116.TabIndex = 142;
            this.label116.Text = "Números de serie";
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label117.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label117.Location = new System.Drawing.Point(537, 87);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(123, 17);
            this.label117.TabIndex = 141;
            this.label117.Text = "Fecha de salida";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label118.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label118.Location = new System.Drawing.Point(244, 40);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(63, 17);
            this.label118.TabIndex = 140;
            this.label118.Text = "Destino";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label119.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label119.Location = new System.Drawing.Point(60, 69);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(72, 17);
            this.label119.TabIndex = 139;
            this.label119.Text = "Cantidad";
            // 
            // entbotcantidad
            // 
            this.entbotcantidad.Location = new System.Drawing.Point(138, 66);
            this.entbotcantidad.Name = "entbotcantidad";
            this.entbotcantidad.Size = new System.Drawing.Size(100, 20);
            this.entbotcantidad.TabIndex = 132;
            // 
            // entbotdestino
            // 
            this.entbotdestino.Location = new System.Drawing.Point(313, 39);
            this.entbotdestino.Name = "entbotdestino";
            this.entbotdestino.Size = new System.Drawing.Size(100, 20);
            this.entbotdestino.TabIndex = 131;
            // 
            // entbotns
            // 
            this.entbotns.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.entbotns.ForeColor = System.Drawing.Color.RoyalBlue;
            this.entbotns.Location = new System.Drawing.Point(72, 133);
            this.entbotns.MaxLength = 1024;
            this.entbotns.Multiline = true;
            this.entbotns.Name = "entbotns";
            this.entbotns.Size = new System.Drawing.Size(668, 188);
            this.entbotns.TabIndex = 130;
            // 
            // entbotfecha
            // 
            this.entbotfecha.Location = new System.Drawing.Point(540, 107);
            this.entbotfecha.Name = "entbotfecha";
            this.entbotfecha.Size = new System.Drawing.Size(200, 20);
            this.entbotfecha.TabIndex = 1;
            // 
            // DisBotLista
            // 
            this.DisBotLista.BackColor = System.Drawing.Color.CadetBlue;
            this.DisBotLista.Controls.Add(this.disBotListaImprimir);
            this.DisBotLista.Controls.Add(this.label120);
            this.DisBotLista.Controls.Add(this.panel18);
            this.DisBotLista.Location = new System.Drawing.Point(4, 26);
            this.DisBotLista.Name = "DisBotLista";
            this.DisBotLista.Padding = new System.Windows.Forms.Padding(3);
            this.DisBotLista.Size = new System.Drawing.Size(894, 581);
            this.DisBotLista.TabIndex = 12;
            this.DisBotLista.Text = "ListaBot";
            // 
            // disBotListaImprimir
            // 
            this.disBotListaImprimir.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.disBotListaImprimir.Location = new System.Drawing.Point(675, 514);
            this.disBotListaImprimir.Name = "disBotListaImprimir";
            this.disBotListaImprimir.Size = new System.Drawing.Size(213, 45);
            this.disBotListaImprimir.TabIndex = 34;
            this.disBotListaImprimir.Text = "IMPRIMIR LISTA";
            this.disBotListaImprimir.UseVisualStyleBackColor = true;
            this.disBotListaImprimir.Click += new System.EventHandler(this.disBotListaImprimir_Click);
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label120.ForeColor = System.Drawing.Color.White;
            this.label120.Location = new System.Drawing.Point(39, 8);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(326, 29);
            this.label120.TabIndex = 31;
            this.label120.Text = "Nueva salida de botelleros";
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.Khaki;
            this.panel18.Controls.Add(this.label156);
            this.panel18.Controls.Add(this.comboIBotTipo);
            this.panel18.Controls.Add(this.label155);
            this.panel18.Controls.Add(this.label154);
            this.panel18.Controls.Add(this.groupBox10);
            this.panel18.Controls.Add(this.groupBox5);
            this.panel18.Controls.Add(this.DateDisBotListaFeFin);
            this.panel18.Controls.Add(this.DateDisBotListaFeInicio);
            this.panel18.Location = new System.Drawing.Point(44, 40);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(812, 380);
            this.panel18.TabIndex = 0;
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label156.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label156.Location = new System.Drawing.Point(338, 68);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(40, 17);
            this.label156.TabIndex = 185;
            this.label156.Text = "Tipo";
            // 
            // comboIBotTipo
            // 
            this.comboIBotTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboIBotTipo.FormattingEnabled = true;
            this.comboIBotTipo.Items.AddRange(new object[] {
            "FECHA DE REPARACIÓN",
            "FECHA DE SALIDA"});
            this.comboIBotTipo.Location = new System.Drawing.Point(384, 68);
            this.comboIBotTipo.Name = "comboIBotTipo";
            this.comboIBotTipo.Size = new System.Drawing.Size(248, 21);
            this.comboIBotTipo.TabIndex = 184;
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label155.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label155.Location = new System.Drawing.Point(338, 218);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(75, 17);
            this.label155.TabIndex = 183;
            this.label155.Text = "Fecha fin";
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label154.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label154.Location = new System.Drawing.Point(338, 156);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(95, 17);
            this.label154.TabIndex = 182;
            this.label154.Text = "Fecha inicio";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.ListaBotReparado);
            this.groupBox10.Controls.Add(this.ListaBotTodo);
            this.groupBox10.Controls.Add(this.ListaBotEntregado);
            this.groupBox10.Location = new System.Drawing.Point(74, 156);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(226, 102);
            this.groupBox10.TabIndex = 7;
            this.groupBox10.TabStop = false;
            // 
            // ListaBotReparado
            // 
            this.ListaBotReparado.AutoSize = true;
            this.ListaBotReparado.Location = new System.Drawing.Point(29, 66);
            this.ListaBotReparado.Name = "ListaBotReparado";
            this.ListaBotReparado.Size = new System.Drawing.Size(169, 17);
            this.ListaBotReparado.TabIndex = 2;
            this.ListaBotReparado.TabStop = true;
            this.ListaBotReparado.Text = "REPARADO SIN ENTREGAR";
            this.ListaBotReparado.UseVisualStyleBackColor = true;
            // 
            // ListaBotTodo
            // 
            this.ListaBotTodo.AutoSize = true;
            this.ListaBotTodo.Location = new System.Drawing.Point(29, 20);
            this.ListaBotTodo.Name = "ListaBotTodo";
            this.ListaBotTodo.Size = new System.Drawing.Size(56, 17);
            this.ListaBotTodo.TabIndex = 1;
            this.ListaBotTodo.TabStop = true;
            this.ListaBotTodo.Text = "TODO";
            this.ListaBotTodo.UseVisualStyleBackColor = true;
            // 
            // ListaBotEntregado
            // 
            this.ListaBotEntregado.AutoSize = true;
            this.ListaBotEntregado.Location = new System.Drawing.Point(29, 43);
            this.ListaBotEntregado.Name = "ListaBotEntregado";
            this.ListaBotEntregado.Size = new System.Drawing.Size(166, 17);
            this.ListaBotEntregado.TabIndex = 0;
            this.ListaBotEntregado.TabStop = true;
            this.ListaBotEntregado.Text = "REPARADO Y ENTREGADO";
            this.ListaBotEntregado.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.ListaBotConBorde);
            this.groupBox5.Controls.Add(this.ListaBotSinBorde);
            this.groupBox5.Location = new System.Drawing.Point(74, 68);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(226, 82);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            // 
            // ListaBotConBorde
            // 
            this.ListaBotConBorde.AutoSize = true;
            this.ListaBotConBorde.Location = new System.Drawing.Point(29, 21);
            this.ListaBotConBorde.Name = "ListaBotConBorde";
            this.ListaBotConBorde.Size = new System.Drawing.Size(89, 17);
            this.ListaBotConBorde.TabIndex = 1;
            this.ListaBotConBorde.TabStop = true;
            this.ListaBotConBorde.Text = "CON BORDE";
            this.ListaBotConBorde.UseVisualStyleBackColor = true;
            // 
            // ListaBotSinBorde
            // 
            this.ListaBotSinBorde.AutoSize = true;
            this.ListaBotSinBorde.Location = new System.Drawing.Point(29, 44);
            this.ListaBotSinBorde.Name = "ListaBotSinBorde";
            this.ListaBotSinBorde.Size = new System.Drawing.Size(84, 17);
            this.ListaBotSinBorde.TabIndex = 0;
            this.ListaBotSinBorde.TabStop = true;
            this.ListaBotSinBorde.Text = "SIN BORDE";
            this.ListaBotSinBorde.UseVisualStyleBackColor = true;
            // 
            // DateDisBotListaFeFin
            // 
            this.DateDisBotListaFeFin.Location = new System.Drawing.Point(341, 238);
            this.DateDisBotListaFeFin.Name = "DateDisBotListaFeFin";
            this.DateDisBotListaFeFin.Size = new System.Drawing.Size(200, 20);
            this.DateDisBotListaFeFin.TabIndex = 5;
            // 
            // DateDisBotListaFeInicio
            // 
            this.DateDisBotListaFeInicio.Location = new System.Drawing.Point(341, 176);
            this.DateDisBotListaFeInicio.Name = "DateDisBotListaFeInicio";
            this.DateDisBotListaFeInicio.Size = new System.Drawing.Size(200, 20);
            this.DateDisBotListaFeInicio.TabIndex = 4;
            // 
            // IROTaller
            // 
            this.IROTaller.BackColor = System.Drawing.Color.CadetBlue;
            this.IROTaller.Controls.Add(this.panel19);
            this.IROTaller.Location = new System.Drawing.Point(4, 26);
            this.IROTaller.Name = "IROTaller";
            this.IROTaller.Padding = new System.Windows.Forms.Padding(3);
            this.IROTaller.Size = new System.Drawing.Size(894, 581);
            this.IROTaller.TabIndex = 13;
            this.IROTaller.Text = "IROTaller";
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.Khaki;
            this.panel19.Controls.Add(this.label151);
            this.panel19.Controls.Add(this.comboIROTallerProyecto);
            this.panel19.Controls.Add(this.label128);
            this.panel19.Controls.Add(this.IROTallerdias);
            this.panel19.Controls.Add(this.label127);
            this.panel19.Controls.Add(this.IROTallerencabezado);
            this.panel19.Controls.Add(this.groupBox6);
            this.panel19.Controls.Add(this.label126);
            this.panel19.Controls.Add(this.comboIROTallerObjeto);
            this.panel19.Controls.Add(this.label125);
            this.panel19.Controls.Add(this.label124);
            this.panel19.Controls.Add(this.label123);
            this.panel19.Controls.Add(this.comboIROTallerTecnico);
            this.panel19.Controls.Add(this.label122);
            this.panel19.Controls.Add(this.comboIROTallerTipo);
            this.panel19.Controls.Add(this.IROTallerConsultar);
            this.panel19.Controls.Add(this.dateIROTallerFin);
            this.panel19.Controls.Add(this.dateIROTallerInicio);
            this.panel19.Location = new System.Drawing.Point(22, 56);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(850, 293);
            this.panel19.TabIndex = 1;
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label151.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label151.Location = new System.Drawing.Point(597, 67);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(72, 17);
            this.label151.TabIndex = 184;
            this.label151.Text = "Proyecto";
            // 
            // comboIROTallerProyecto
            // 
            this.comboIROTallerProyecto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboIROTallerProyecto.FormattingEnabled = true;
            this.comboIROTallerProyecto.Items.AddRange(new object[] {
            "TODO",
            "CAFENTO",
            "CANDELAS",
            "SIH",
            "BLACKZI",
            "DELTA",
            "OTROS"});
            this.comboIROTallerProyecto.Location = new System.Drawing.Point(675, 66);
            this.comboIROTallerProyecto.Name = "comboIROTallerProyecto";
            this.comboIROTallerProyecto.Size = new System.Drawing.Size(140, 21);
            this.comboIROTallerProyecto.TabIndex = 183;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label128.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label128.Location = new System.Drawing.Point(56, 225);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(77, 17);
            this.label128.TabIndex = 182;
            this.label128.Text = "Num.Dias";
            // 
            // IROTallerdias
            // 
            this.IROTallerdias.Location = new System.Drawing.Point(139, 224);
            this.IROTallerdias.Name = "IROTallerdias";
            this.IROTallerdias.Size = new System.Drawing.Size(68, 20);
            this.IROTallerdias.TabIndex = 181;
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label127.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label127.Location = new System.Drawing.Point(36, 250);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(97, 17);
            this.label127.TabIndex = 180;
            this.label127.Text = "Encabezado";
            // 
            // IROTallerencabezado
            // 
            this.IROTallerencabezado.Location = new System.Drawing.Point(139, 250);
            this.IROTallerencabezado.Name = "IROTallerencabezado";
            this.IROTallerencabezado.Size = new System.Drawing.Size(448, 20);
            this.IROTallerencabezado.TabIndex = 179;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.radioIRDetalle);
            this.groupBox6.Controls.Add(this.radioIRResumen);
            this.groupBox6.Location = new System.Drawing.Point(399, 107);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(169, 82);
            this.groupBox6.TabIndex = 178;
            this.groupBox6.TabStop = false;
            // 
            // radioIRDetalle
            // 
            this.radioIRDetalle.AutoSize = true;
            this.radioIRDetalle.Location = new System.Drawing.Point(42, 21);
            this.radioIRDetalle.Name = "radioIRDetalle";
            this.radioIRDetalle.Size = new System.Drawing.Size(73, 17);
            this.radioIRDetalle.TabIndex = 1;
            this.radioIRDetalle.TabStop = true;
            this.radioIRDetalle.Text = "DETALLE";
            this.radioIRDetalle.UseVisualStyleBackColor = true;
            this.radioIRDetalle.CheckedChanged += new System.EventHandler(this.radioIRDetalle_CheckedChanged);
            // 
            // radioIRResumen
            // 
            this.radioIRResumen.AutoSize = true;
            this.radioIRResumen.Location = new System.Drawing.Point(42, 44);
            this.radioIRResumen.Name = "radioIRResumen";
            this.radioIRResumen.Size = new System.Drawing.Size(79, 17);
            this.radioIRResumen.TabIndex = 0;
            this.radioIRResumen.TabStop = true;
            this.radioIRResumen.Text = "RESUMEN";
            this.radioIRResumen.UseVisualStyleBackColor = true;
            this.radioIRResumen.CheckedChanged += new System.EventHandler(this.radioIRResumen_CheckedChanged);
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label126.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label126.Location = new System.Drawing.Point(336, 40);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(56, 17);
            this.label126.TabIndex = 177;
            this.label126.Text = "Objeto";
            // 
            // comboIROTallerObjeto
            // 
            this.comboIROTallerObjeto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboIROTallerObjeto.Enabled = false;
            this.comboIROTallerObjeto.FormattingEnabled = true;
            this.comboIROTallerObjeto.Items.AddRange(new object[] {
            "",
            "TODO",
            "OFERTA",
            "REPUESTO",
            "DESPLAZAMIENTO",
            "MANO DE OBRA"});
            this.comboIROTallerObjeto.Location = new System.Drawing.Point(398, 39);
            this.comboIROTallerObjeto.Name = "comboIROTallerObjeto";
            this.comboIROTallerObjeto.Size = new System.Drawing.Size(200, 21);
            this.comboIROTallerObjeto.TabIndex = 176;
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label125.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label125.Location = new System.Drawing.Point(92, 124);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(95, 17);
            this.label125.TabIndex = 175;
            this.label125.Text = "Fecha inicio";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label124.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label124.Location = new System.Drawing.Point(112, 150);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(75, 17);
            this.label124.TabIndex = 174;
            this.label124.Text = "Fecha fin";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label123.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label123.Location = new System.Drawing.Point(604, 39);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(65, 17);
            this.label123.TabIndex = 173;
            this.label123.Text = "Técnico";
            // 
            // comboIROTallerTecnico
            // 
            this.comboIROTallerTecnico.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboIROTallerTecnico.FormattingEnabled = true;
            this.comboIROTallerTecnico.Items.AddRange(new object[] {
            "TODOS",
            "MIGUEL",
            "JOSE",
            "HILARIO"});
            this.comboIROTallerTecnico.Location = new System.Drawing.Point(675, 39);
            this.comboIROTallerTecnico.Name = "comboIROTallerTecnico";
            this.comboIROTallerTecnico.Size = new System.Drawing.Size(140, 21);
            this.comboIROTallerTecnico.TabIndex = 172;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label122.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label122.Location = new System.Drawing.Point(36, 41);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(40, 17);
            this.label122.TabIndex = 171;
            this.label122.Text = "Tipo";
            // 
            // comboIROTallerTipo
            // 
            this.comboIROTallerTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboIROTallerTipo.FormattingEnabled = true;
            this.comboIROTallerTipo.Items.AddRange(new object[] {
            "FECHA DE REALIZACION DE TRABAJO",
            "FECHA DE CREACION DE ORDEN"});
            this.comboIROTallerTipo.Location = new System.Drawing.Point(82, 40);
            this.comboIROTallerTipo.Name = "comboIROTallerTipo";
            this.comboIROTallerTipo.Size = new System.Drawing.Size(248, 21);
            this.comboIROTallerTipo.TabIndex = 3;
            // 
            // IROTallerConsultar
            // 
            this.IROTallerConsultar.Location = new System.Drawing.Point(690, 250);
            this.IROTallerConsultar.Name = "IROTallerConsultar";
            this.IROTallerConsultar.Size = new System.Drawing.Size(157, 40);
            this.IROTallerConsultar.TabIndex = 2;
            this.IROTallerConsultar.Text = "Consultar";
            this.IROTallerConsultar.UseVisualStyleBackColor = true;
            this.IROTallerConsultar.Click += new System.EventHandler(this.IROTallerConsultar_Click);
            // 
            // dateIROTallerFin
            // 
            this.dateIROTallerFin.Location = new System.Drawing.Point(193, 150);
            this.dateIROTallerFin.Name = "dateIROTallerFin";
            this.dateIROTallerFin.Size = new System.Drawing.Size(200, 20);
            this.dateIROTallerFin.TabIndex = 1;
            // 
            // dateIROTallerInicio
            // 
            this.dateIROTallerInicio.Location = new System.Drawing.Point(193, 124);
            this.dateIROTallerInicio.Name = "dateIROTallerInicio";
            this.dateIROTallerInicio.Size = new System.Drawing.Size(200, 20);
            this.dateIROTallerInicio.TabIndex = 0;
            // 
            // VacCrear
            // 
            this.VacCrear.Controls.Add(this.panel20);
            this.VacCrear.Location = new System.Drawing.Point(4, 26);
            this.VacCrear.Name = "VacCrear";
            this.VacCrear.Padding = new System.Windows.Forms.Padding(3);
            this.VacCrear.Size = new System.Drawing.Size(894, 581);
            this.VacCrear.TabIndex = 14;
            this.VacCrear.Text = "VacCre";
            this.VacCrear.UseVisualStyleBackColor = true;
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.CadetBlue;
            this.panel20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel20.Controls.Add(this.label129);
            this.panel20.Controls.Add(this.vacCreGuardar);
            this.panel20.Controls.Add(this.panel21);
            this.panel20.Location = new System.Drawing.Point(0, 3);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(894, 560);
            this.panel20.TabIndex = 2;
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label129.ForeColor = System.Drawing.Color.White;
            this.label129.Location = new System.Drawing.Point(17, 17);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(325, 29);
            this.label129.TabIndex = 28;
            this.label129.Text = "Movimiento: VACACIONES";
            // 
            // vacCreGuardar
            // 
            this.vacCreGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vacCreGuardar.Location = new System.Drawing.Point(658, 510);
            this.vacCreGuardar.Name = "vacCreGuardar";
            this.vacCreGuardar.Size = new System.Drawing.Size(213, 45);
            this.vacCreGuardar.TabIndex = 24;
            this.vacCreGuardar.Text = "GUARDAR REGISTRO";
            this.vacCreGuardar.UseVisualStyleBackColor = true;
            this.vacCreGuardar.Click += new System.EventHandler(this.vacCreGuardar_Click);
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.Khaki;
            this.panel21.Controls.Add(this.vacCreNombre);
            this.panel21.Controls.Add(this.label141);
            this.panel21.Controls.Add(this.groupBox7);
            this.panel21.Controls.Add(this.pictureBox10);
            this.panel21.Controls.Add(this.vacCreFecha);
            this.panel21.Controls.Add(this.label133);
            this.panel21.Controls.Add(this.label135);
            this.panel21.Controls.Add(this.textBox12);
            this.panel21.Controls.Add(this.label138);
            this.panel21.Controls.Add(this.vacCreDescripcion);
            this.panel21.Location = new System.Drawing.Point(22, 49);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(849, 454);
            this.panel21.TabIndex = 29;
            // 
            // vacCreNombre
            // 
            this.vacCreNombre.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.vacCreNombre.FormattingEnabled = true;
            this.vacCreNombre.Items.AddRange(new object[] {
            "CARLOS GARCIA",
            "FERNANDO SASTRE",
            "FRANCSCO GUILLEN",
            "HILARIO DUVAL",
            "JAIME GONZALEZ",
            "JOSE ESTEVEZ",
            "JULIAN PAIS",
            "MANUEL GUILLEN",
            "MIGUEL SANCHIDRAN"});
            this.vacCreNombre.Location = new System.Drawing.Point(477, 36);
            this.vacCreNombre.Name = "vacCreNombre";
            this.vacCreNombre.Size = new System.Drawing.Size(298, 21);
            this.vacCreNombre.TabIndex = 151;
            this.vacCreNombre.SelectedValueChanged += new System.EventHandler(this.vacCreNombre_SelectedValueChanged);
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label141.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label141.Location = new System.Drawing.Point(407, 40);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(64, 17);
            this.label141.TabIndex = 150;
            this.label141.Text = "Nombre";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label149);
            this.groupBox7.Controls.Add(this.vacCreDDados);
            this.groupBox7.Controls.Add(this.label150);
            this.groupBox7.Controls.Add(this.label144);
            this.groupBox7.Controls.Add(this.label143);
            this.groupBox7.Controls.Add(this.textInicio);
            this.groupBox7.Controls.Add(this.label140);
            this.groupBox7.Controls.Add(this.label132);
            this.groupBox7.Controls.Add(this.label142);
            this.groupBox7.Controls.Add(this.label131);
            this.groupBox7.Controls.Add(this.label130);
            this.groupBox7.Controls.Add(this.label134);
            this.groupBox7.Controls.Add(this.vacCreDAnteriores);
            this.groupBox7.Controls.Add(this.vacCreDAcumulados);
            this.groupBox7.Controls.Add(this.label139);
            this.groupBox7.Controls.Add(this.vacCreDActuales);
            this.groupBox7.Controls.Add(this.vacCreDSaldo);
            this.groupBox7.Controls.Add(this.label137);
            this.groupBox7.Controls.Add(this.label136);
            this.groupBox7.Controls.Add(this.vacCreDCogidos);
            this.groupBox7.Location = new System.Drawing.Point(187, 163);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(270, 288);
            this.groupBox7.TabIndex = 149;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Días";
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label149.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label149.Location = new System.Drawing.Point(208, 185);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(25, 25);
            this.label149.TabIndex = 158;
            this.label149.Text = "+";
            // 
            // vacCreDDados
            // 
            this.vacCreDDados.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vacCreDDados.ForeColor = System.Drawing.Color.RoyalBlue;
            this.vacCreDDados.Location = new System.Drawing.Point(142, 187);
            this.vacCreDDados.MaxLength = 30;
            this.vacCreDDados.Name = "vacCreDDados";
            this.vacCreDDados.Size = new System.Drawing.Size(60, 23);
            this.vacCreDDados.TabIndex = 156;
            this.vacCreDDados.Text = "0";
            this.vacCreDDados.TextChanged += new System.EventHandler(this.vacCreDDados_TextChanged);
            this.vacCreDDados.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.vacCreDDados_KeyPress);
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label150.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label150.Location = new System.Drawing.Point(83, 190);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(54, 17);
            this.label150.TabIndex = 157;
            this.label150.Text = "Abono";
            // 
            // label144
            // 
            this.label144.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label144.Enabled = false;
            this.label144.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label144.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label144.Location = new System.Drawing.Point(51, 64);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(151, 2);
            this.label144.TabIndex = 155;
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label143.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label143.Location = new System.Drawing.Point(40, 41);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(96, 17);
            this.label143.TabIndex = 154;
            this.label143.Text = "Saldo inicial";
            // 
            // textInicio
            // 
            this.textInicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textInicio.ForeColor = System.Drawing.Color.RoyalBlue;
            this.textInicio.Location = new System.Drawing.Point(142, 38);
            this.textInicio.MaxLength = 10;
            this.textInicio.Name = "textInicio";
            this.textInicio.ReadOnly = true;
            this.textInicio.Size = new System.Drawing.Size(60, 23);
            this.textInicio.TabIndex = 153;
            this.textInicio.Text = "0";
            // 
            // label140
            // 
            this.label140.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label140.Enabled = false;
            this.label140.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label140.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label140.Location = new System.Drawing.Point(51, 215);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(151, 2);
            this.label140.TabIndex = 152;
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label132.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label132.Location = new System.Drawing.Point(208, 156);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(20, 25);
            this.label132.TabIndex = 151;
            this.label132.Text = "-";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label142.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label142.Location = new System.Drawing.Point(78, 161);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(51, 17);
            this.label142.TabIndex = 140;
            this.label142.Text = "Cargo";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label131.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label131.Location = new System.Drawing.Point(208, 96);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(25, 25);
            this.label131.TabIndex = 150;
            this.label131.Text = "+";
            // 
            // label130
            // 
            this.label130.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label130.Enabled = false;
            this.label130.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label130.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label130.Location = new System.Drawing.Point(51, 124);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(151, 2);
            this.label130.TabIndex = 150;
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label134.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label134.Location = new System.Drawing.Point(37, 223);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(99, 17);
            this.label134.TabIndex = 143;
            this.label134.Text = "= Saldo final";
            // 
            // vacCreDAnteriores
            // 
            this.vacCreDAnteriores.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vacCreDAnteriores.ForeColor = System.Drawing.Color.RoyalBlue;
            this.vacCreDAnteriores.Location = new System.Drawing.Point(142, 69);
            this.vacCreDAnteriores.MaxLength = 30;
            this.vacCreDAnteriores.Name = "vacCreDAnteriores";
            this.vacCreDAnteriores.ReadOnly = true;
            this.vacCreDAnteriores.Size = new System.Drawing.Size(60, 23);
            this.vacCreDAnteriores.TabIndex = 128;
            this.vacCreDAnteriores.Text = "0";
            this.vacCreDAnteriores.TextChanged += new System.EventHandler(this.vacCreDAnteriores_TextChanged);
            this.vacCreDAnteriores.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.vacCreDAnteriores_KeyPress);
            // 
            // vacCreDAcumulados
            // 
            this.vacCreDAcumulados.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vacCreDAcumulados.ForeColor = System.Drawing.Color.RoyalBlue;
            this.vacCreDAcumulados.Location = new System.Drawing.Point(142, 129);
            this.vacCreDAcumulados.MaxLength = 12;
            this.vacCreDAcumulados.Name = "vacCreDAcumulados";
            this.vacCreDAcumulados.ReadOnly = true;
            this.vacCreDAcumulados.Size = new System.Drawing.Size(60, 23);
            this.vacCreDAcumulados.TabIndex = 130;
            this.vacCreDAcumulados.Text = "0";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label139.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label139.Location = new System.Drawing.Point(49, 132);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(87, 17);
            this.label139.TabIndex = 138;
            this.label139.Text = "Acumulado";
            // 
            // vacCreDActuales
            // 
            this.vacCreDActuales.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vacCreDActuales.ForeColor = System.Drawing.Color.RoyalBlue;
            this.vacCreDActuales.Location = new System.Drawing.Point(142, 98);
            this.vacCreDActuales.MaxLength = 30;
            this.vacCreDActuales.Name = "vacCreDActuales";
            this.vacCreDActuales.ReadOnly = true;
            this.vacCreDActuales.Size = new System.Drawing.Size(60, 23);
            this.vacCreDActuales.TabIndex = 127;
            this.vacCreDActuales.Text = "0";
            this.vacCreDActuales.TextChanged += new System.EventHandler(this.vacCreDActuales_TextChanged);
            this.vacCreDActuales.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.vacCreDActuales_KeyPress);
            // 
            // vacCreDSaldo
            // 
            this.vacCreDSaldo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vacCreDSaldo.ForeColor = System.Drawing.Color.RoyalBlue;
            this.vacCreDSaldo.Location = new System.Drawing.Point(142, 220);
            this.vacCreDSaldo.MaxLength = 10;
            this.vacCreDSaldo.Name = "vacCreDSaldo";
            this.vacCreDSaldo.ReadOnly = true;
            this.vacCreDSaldo.Size = new System.Drawing.Size(60, 23);
            this.vacCreDSaldo.TabIndex = 142;
            this.vacCreDSaldo.Text = "0";
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label137.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label137.Location = new System.Drawing.Point(83, 101);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(53, 17);
            this.label137.TabIndex = 135;
            this.label137.Text = "Actual";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label136.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label136.Location = new System.Drawing.Point(70, 72);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(66, 17);
            this.label136.TabIndex = 136;
            this.label136.Text = "Anterior";
            // 
            // vacCreDCogidos
            // 
            this.vacCreDCogidos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vacCreDCogidos.ForeColor = System.Drawing.Color.RoyalBlue;
            this.vacCreDCogidos.Location = new System.Drawing.Point(142, 158);
            this.vacCreDCogidos.MaxLength = 12;
            this.vacCreDCogidos.Name = "vacCreDCogidos";
            this.vacCreDCogidos.Size = new System.Drawing.Size(60, 23);
            this.vacCreDCogidos.TabIndex = 132;
            this.vacCreDCogidos.Text = "0";
            this.vacCreDCogidos.TextChanged += new System.EventHandler(this.vacCreDCogidos_TextChanged);
            this.vacCreDCogidos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.vacCreDCogidos_KeyPress);
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::CafeS60.INTERFACE.Properties.Resources.crear3;
            this.pictureBox10.Location = new System.Drawing.Point(594, 163);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(181, 202);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox10.TabIndex = 148;
            this.pictureBox10.TabStop = false;
            // 
            // vacCreFecha
            // 
            this.vacCreFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vacCreFecha.ForeColor = System.Drawing.Color.RoyalBlue;
            this.vacCreFecha.Location = new System.Drawing.Point(311, 37);
            this.vacCreFecha.MaxLength = 8;
            this.vacCreFecha.Name = "vacCreFecha";
            this.vacCreFecha.Size = new System.Drawing.Size(90, 23);
            this.vacCreFecha.TabIndex = 145;
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label133.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label133.Location = new System.Drawing.Point(253, 40);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(52, 17);
            this.label133.TabIndex = 144;
            this.label133.Text = "Fecha";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label135.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label135.Location = new System.Drawing.Point(155, 40);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(26, 17);
            this.label135.TabIndex = 134;
            this.label135.Text = "Id.";
            // 
            // textBox12
            // 
            this.textBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox12.ForeColor = System.Drawing.Color.RoyalBlue;
            this.textBox12.Location = new System.Drawing.Point(187, 37);
            this.textBox12.MaxLength = 0;
            this.textBox12.Name = "textBox12";
            this.textBox12.ReadOnly = true;
            this.textBox12.Size = new System.Drawing.Size(60, 23);
            this.textBox12.TabIndex = 126;
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label138.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label138.Location = new System.Drawing.Point(65, 70);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(116, 17);
            this.label138.TabIndex = 137;
            this.label138.Text = "Observaciones";
            // 
            // vacCreDescripcion
            // 
            this.vacCreDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vacCreDescripcion.ForeColor = System.Drawing.Color.RoyalBlue;
            this.vacCreDescripcion.Location = new System.Drawing.Point(187, 67);
            this.vacCreDescripcion.MaxLength = 240;
            this.vacCreDescripcion.Multiline = true;
            this.vacCreDescripcion.Name = "vacCreDescripcion";
            this.vacCreDescripcion.Size = new System.Drawing.Size(588, 90);
            this.vacCreDescripcion.TabIndex = 129;
            // 
            // VacInf
            // 
            this.VacInf.Controls.Add(this.panel22);
            this.VacInf.Location = new System.Drawing.Point(4, 26);
            this.VacInf.Name = "VacInf";
            this.VacInf.Padding = new System.Windows.Forms.Padding(3);
            this.VacInf.Size = new System.Drawing.Size(894, 581);
            this.VacInf.TabIndex = 15;
            this.VacInf.Text = "VacInf";
            this.VacInf.UseVisualStyleBackColor = true;
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.CadetBlue;
            this.panel22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel22.Controls.Add(this.vacInfVerTodos);
            this.panel22.Controls.Add(this.label145);
            this.panel22.Controls.Add(this.vacInfVer);
            this.panel22.Controls.Add(this.panel23);
            this.panel22.Location = new System.Drawing.Point(0, 0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(894, 563);
            this.panel22.TabIndex = 3;
            // 
            // vacInfVerTodos
            // 
            this.vacInfVerTodos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vacInfVerTodos.Location = new System.Drawing.Point(22, 386);
            this.vacInfVerTodos.Name = "vacInfVerTodos";
            this.vacInfVerTodos.Size = new System.Drawing.Size(213, 45);
            this.vacInfVerTodos.TabIndex = 30;
            this.vacInfVerTodos.Text = "MOSTRAR TODOS";
            this.vacInfVerTodos.UseVisualStyleBackColor = true;
            this.vacInfVerTodos.Click += new System.EventHandler(this.vacInfVerTodos_Click);
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label145.ForeColor = System.Drawing.Color.White;
            this.label145.Location = new System.Drawing.Point(17, 17);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(292, 29);
            this.label145.TabIndex = 28;
            this.label145.Text = "Informes: VACACIONES";
            // 
            // vacInfVer
            // 
            this.vacInfVer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vacInfVer.Location = new System.Drawing.Point(658, 386);
            this.vacInfVer.Name = "vacInfVer";
            this.vacInfVer.Size = new System.Drawing.Size(213, 45);
            this.vacInfVer.TabIndex = 24;
            this.vacInfVer.Text = "MOSTRAR INFORME";
            this.vacInfVer.UseVisualStyleBackColor = true;
            this.vacInfVer.Click += new System.EventHandler(this.vacInfVer_Click);
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.Khaki;
            this.panel23.Controls.Add(this.label147);
            this.panel23.Controls.Add(this.label148);
            this.panel23.Controls.Add(this.vacInfFeFin);
            this.panel23.Controls.Add(this.vacInfFeInicio);
            this.panel23.Controls.Add(this.vacInfNombre);
            this.panel23.Controls.Add(this.label146);
            this.panel23.Controls.Add(this.pictureBox11);
            this.panel23.Location = new System.Drawing.Point(22, 49);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(849, 331);
            this.panel23.TabIndex = 29;
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label147.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label147.Location = new System.Drawing.Point(81, 40);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(95, 17);
            this.label147.TabIndex = 179;
            this.label147.Text = "Fecha inicio";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label148.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label148.Location = new System.Drawing.Point(101, 66);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(75, 17);
            this.label148.TabIndex = 178;
            this.label148.Text = "Fecha fin";
            // 
            // vacInfFeFin
            // 
            this.vacInfFeFin.Location = new System.Drawing.Point(182, 66);
            this.vacInfFeFin.Name = "vacInfFeFin";
            this.vacInfFeFin.Size = new System.Drawing.Size(200, 20);
            this.vacInfFeFin.TabIndex = 177;
            // 
            // vacInfFeInicio
            // 
            this.vacInfFeInicio.Location = new System.Drawing.Point(182, 40);
            this.vacInfFeInicio.Name = "vacInfFeInicio";
            this.vacInfFeInicio.Size = new System.Drawing.Size(200, 20);
            this.vacInfFeInicio.TabIndex = 176;
            // 
            // vacInfNombre
            // 
            this.vacInfNombre.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.vacInfNombre.FormattingEnabled = true;
            this.vacInfNombre.Items.AddRange(new object[] {
            "CARLOS GARCIA",
            "FERNANDO SASTRE",
            "FRANCSCO GUILLEN",
            "HILARIO DUVAL",
            "JAIME GONZALEZ",
            "JOSE ESTEVEZ",
            "JULIAN PAIS",
            "MANUEL GUILLEN",
            "MIGUEL SANCHIDRAN"});
            this.vacInfNombre.Location = new System.Drawing.Point(477, 36);
            this.vacInfNombre.Name = "vacInfNombre";
            this.vacInfNombre.Size = new System.Drawing.Size(298, 21);
            this.vacInfNombre.TabIndex = 151;
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label146.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label146.Location = new System.Drawing.Point(407, 40);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(64, 17);
            this.label146.TabIndex = 150;
            this.label146.Text = "Nombre";
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::CafeS60.INTERFACE.Properties.Resources.crear3;
            this.pictureBox11.Location = new System.Drawing.Point(201, 92);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(181, 202);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox11.TabIndex = 148;
            this.pictureBox11.TabStop = false;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.CadetBlue;
            this.tabPage1.Controls.Add(this.panel25);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(894, 581);
            this.tabPage1.TabIndex = 16;
            this.tabPage1.Text = "RepCaf";
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.Silver;
            this.panel25.Controls.Add(this.comboTipo);
            this.panel25.Controls.Add(this.groupBox13);
            this.panel25.Controls.Add(this.groupBox12);
            this.panel25.Location = new System.Drawing.Point(72, 22);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(750, 509);
            this.panel25.TabIndex = 0;
            // 
            // comboTipo
            // 
            this.comboTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboTipo.Enabled = false;
            this.comboTipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboTipo.ForeColor = System.Drawing.Color.Black;
            this.comboTipo.FormattingEnabled = true;
            this.comboTipo.Items.AddRange(new object[] {
            "GENERAL",
            "PEPSI"});
            this.comboTipo.Location = new System.Drawing.Point(34, 29);
            this.comboTipo.Name = "comboTipo";
            this.comboTipo.Size = new System.Drawing.Size(174, 21);
            this.comboTipo.TabIndex = 190;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.RepCafRepEncontrado);
            this.groupBox13.Controls.Add(this.label164);
            this.groupBox13.Controls.Add(this.RepCafBuscar1);
            this.groupBox13.Controls.Add(this.RepCafCodAXBuscar);
            this.groupBox13.Controls.Add(this.RepCafCodSAPBuscar);
            this.groupBox13.Controls.Add(this.label163);
            this.groupBox13.Controls.Add(this.label162);
            this.groupBox13.Controls.Add(this.RepCafIdBuscar);
            this.groupBox13.Controls.Add(this.label161);
            this.groupBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox13.Location = new System.Drawing.Point(34, 56);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(683, 145);
            this.groupBox13.TabIndex = 189;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Buscar repuesto por";
            // 
            // RepCafRepEncontrado
            // 
            this.RepCafRepEncontrado.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RepCafRepEncontrado.ForeColor = System.Drawing.Color.Black;
            this.RepCafRepEncontrado.Location = new System.Drawing.Point(83, 113);
            this.RepCafRepEncontrado.MaxLength = 15;
            this.RepCafRepEncontrado.Name = "RepCafRepEncontrado";
            this.RepCafRepEncontrado.Size = new System.Drawing.Size(586, 23);
            this.RepCafRepEncontrado.TabIndex = 183;
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label164.ForeColor = System.Drawing.Color.Black;
            this.label164.Location = new System.Drawing.Point(14, 116);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(63, 16);
            this.label164.TabIndex = 184;
            this.label164.Text = "Repuesto";
            // 
            // RepCafBuscar1
            // 
            this.RepCafBuscar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RepCafBuscar1.Location = new System.Drawing.Point(577, 70);
            this.RepCafBuscar1.Name = "RepCafBuscar1";
            this.RepCafBuscar1.Size = new System.Drawing.Size(92, 37);
            this.RepCafBuscar1.TabIndex = 137;
            this.RepCafBuscar1.Text = "BUSCAR";
            this.RepCafBuscar1.UseVisualStyleBackColor = true;
            this.RepCafBuscar1.Click += new System.EventHandler(this.RepCafBuscar1_Click);
            // 
            // RepCafCodAXBuscar
            // 
            this.RepCafCodAXBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RepCafCodAXBuscar.ForeColor = System.Drawing.Color.Black;
            this.RepCafCodAXBuscar.Location = new System.Drawing.Point(83, 55);
            this.RepCafCodAXBuscar.MaxLength = 0;
            this.RepCafCodAXBuscar.Name = "RepCafCodAXBuscar";
            this.RepCafCodAXBuscar.Size = new System.Drawing.Size(177, 23);
            this.RepCafCodAXBuscar.TabIndex = 140;
            // 
            // RepCafCodSAPBuscar
            // 
            this.RepCafCodSAPBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RepCafCodSAPBuscar.ForeColor = System.Drawing.Color.Black;
            this.RepCafCodSAPBuscar.Location = new System.Drawing.Point(83, 26);
            this.RepCafCodSAPBuscar.MaxLength = 0;
            this.RepCafCodSAPBuscar.Name = "RepCafCodSAPBuscar";
            this.RepCafCodSAPBuscar.Size = new System.Drawing.Size(177, 23);
            this.RepCafCodSAPBuscar.TabIndex = 138;
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label163.ForeColor = System.Drawing.Color.Black;
            this.label163.Location = new System.Drawing.Point(25, 57);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(52, 16);
            this.label163.TabIndex = 141;
            this.label163.Text = "Cod. ax";
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label162.ForeColor = System.Drawing.Color.Black;
            this.label162.Location = new System.Drawing.Point(58, 87);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(19, 16);
            this.label162.TabIndex = 136;
            this.label162.Text = "id";
            // 
            // RepCafIdBuscar
            // 
            this.RepCafIdBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RepCafIdBuscar.ForeColor = System.Drawing.Color.Black;
            this.RepCafIdBuscar.Location = new System.Drawing.Point(83, 84);
            this.RepCafIdBuscar.MaxLength = 0;
            this.RepCafIdBuscar.Name = "RepCafIdBuscar";
            this.RepCafIdBuscar.Size = new System.Drawing.Size(72, 23);
            this.RepCafIdBuscar.TabIndex = 135;
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label161.ForeColor = System.Drawing.Color.Black;
            this.label161.Location = new System.Drawing.Point(19, 29);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(60, 16);
            this.label161.TabIndex = 139;
            this.label161.Text = "Cod. sap";
            // 
            // groupBox12
            // 
            this.groupBox12.BackColor = System.Drawing.Color.Silver;
            this.groupBox12.Controls.Add(this.RepCafCambiar);
            this.groupBox12.Controls.Add(this.label166);
            this.groupBox12.Controls.Add(this.RepCafPrecio);
            this.groupBox12.Controls.Add(this.comboBusPre);
            this.groupBox12.Controls.Add(this.comboBusCod);
            this.groupBox12.Controls.Add(this.comboBusNom);
            this.groupBox12.Controls.Add(this.RepCafNombreBuscar);
            this.groupBox12.Controls.Add(this.label165);
            this.groupBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox12.Location = new System.Drawing.Point(34, 207);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(683, 154);
            this.groupBox12.TabIndex = 188;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Buscar repuesto por nombre";
            // 
            // RepCafCambiar
            // 
            this.RepCafCambiar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RepCafCambiar.Location = new System.Drawing.Point(246, 87);
            this.RepCafCambiar.Name = "RepCafCambiar";
            this.RepCafCambiar.Size = new System.Drawing.Size(92, 37);
            this.RepCafCambiar.TabIndex = 191;
            this.RepCafCambiar.Text = "CAMBIAR";
            this.RepCafCambiar.UseVisualStyleBackColor = true;
            this.RepCafCambiar.Click += new System.EventHandler(this.RepCafCambiar_Click);
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label166.ForeColor = System.Drawing.Color.Black;
            this.label166.Location = new System.Drawing.Point(70, 97);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(87, 16);
            this.label166.TabIndex = 190;
            this.label166.Text = "Nuevo precio";
            // 
            // RepCafPrecio
            // 
            this.RepCafPrecio.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RepCafPrecio.ForeColor = System.Drawing.Color.Black;
            this.RepCafPrecio.Location = new System.Drawing.Point(163, 94);
            this.RepCafPrecio.MaxLength = 0;
            this.RepCafPrecio.Name = "RepCafPrecio";
            this.RepCafPrecio.Size = new System.Drawing.Size(77, 23);
            this.RepCafPrecio.TabIndex = 189;
            this.RepCafPrecio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RepCafPrecio_KeyPress);
            // 
            // comboBusPre
            // 
            this.comboBusPre.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBusPre.ForeColor = System.Drawing.Color.Black;
            this.comboBusPre.FormattingEnabled = true;
            this.comboBusPre.Location = new System.Drawing.Point(547, 60);
            this.comboBusPre.Name = "comboBusPre";
            this.comboBusPre.Size = new System.Drawing.Size(65, 21);
            this.comboBusPre.TabIndex = 188;
            this.comboBusPre.SelectedValueChanged += new System.EventHandler(this.comboBusPre_SelectedValueChanged);
            // 
            // comboBusCod
            // 
            this.comboBusCod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBusCod.ForeColor = System.Drawing.Color.Black;
            this.comboBusCod.FormattingEnabled = true;
            this.comboBusCod.Location = new System.Drawing.Point(421, 60);
            this.comboBusCod.Name = "comboBusCod";
            this.comboBusCod.Size = new System.Drawing.Size(120, 21);
            this.comboBusCod.TabIndex = 187;
            this.comboBusCod.SelectedValueChanged += new System.EventHandler(this.comboBusCod_SelectedValueChanged);
            // 
            // comboBusNom
            // 
            this.comboBusNom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBusNom.ForeColor = System.Drawing.Color.Black;
            this.comboBusNom.FormattingEnabled = true;
            this.comboBusNom.Location = new System.Drawing.Point(73, 60);
            this.comboBusNom.Name = "comboBusNom";
            this.comboBusNom.Size = new System.Drawing.Size(342, 21);
            this.comboBusNom.TabIndex = 183;
            this.comboBusNom.SelectedValueChanged += new System.EventHandler(this.comboBusNom_SelectedValueChanged);
            // 
            // RepCafNombreBuscar
            // 
            this.RepCafNombreBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RepCafNombreBuscar.ForeColor = System.Drawing.Color.Black;
            this.RepCafNombreBuscar.Location = new System.Drawing.Point(254, 31);
            this.RepCafNombreBuscar.MaxLength = 40;
            this.RepCafNombreBuscar.Name = "RepCafNombreBuscar";
            this.RepCafNombreBuscar.Size = new System.Drawing.Size(358, 23);
            this.RepCafNombreBuscar.TabIndex = 181;
            this.RepCafNombreBuscar.TextChanged += new System.EventHandler(this.RepCafNombreBuscar_TextChanged);
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label165.ForeColor = System.Drawing.Color.Black;
            this.label165.Location = new System.Drawing.Point(70, 34);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(178, 16);
            this.label165.TabIndex = 182;
            this.label165.Text = "Repuesto (nombre o código)";
            // 
            // AlmEqSe
            // 
            this.AlmEqSe.BackColor = System.Drawing.Color.CadetBlue;
            this.AlmEqSe.Controls.Add(this.panel26);
            this.AlmEqSe.Controls.Add(this.eqTitulo);
            this.AlmEqSe.Location = new System.Drawing.Point(4, 26);
            this.AlmEqSe.Name = "AlmEqSe";
            this.AlmEqSe.Padding = new System.Windows.Forms.Padding(3);
            this.AlmEqSe.Size = new System.Drawing.Size(894, 581);
            this.AlmEqSe.TabIndex = 17;
            this.AlmEqSe.Text = "tabEq";
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.Silver;
            this.panel26.Controls.Add(this.BotAlEqSeInforme);
            this.panel26.Controls.Add(this.BotAlEqSeModificar);
            this.panel26.Controls.Add(this.panel27);
            this.panel26.Controls.Add(this.BotAlEqSe);
            this.panel26.Controls.Add(this.label179);
            this.panel26.Controls.Add(this.label177);
            this.panel26.Controls.Add(this.label176);
            this.panel26.Controls.Add(this.label175);
            this.panel26.Controls.Add(this.label174);
            this.panel26.Controls.Add(this.label173);
            this.panel26.Controls.Add(this.label172);
            this.panel26.Controls.Add(this.label171);
            this.panel26.Controls.Add(this.label170);
            this.panel26.Controls.Add(this.label169);
            this.panel26.Controls.Add(this.label167);
            this.panel26.Controls.Add(this.label168);
            this.panel26.Controls.Add(this.eqcafentstock);
            this.panel26.Controls.Add(this.eqcafentestado);
            this.panel26.Controls.Add(this.eqcafentsalida);
            this.panel26.Controls.Add(this.eqcafentdestino);
            this.panel26.Controls.Add(this.eqcafentubicacion);
            this.panel26.Controls.Add(this.eqcafententrada);
            this.panel26.Controls.Add(this.eqcafentorigen);
            this.panel26.Controls.Add(this.eqcafentax);
            this.panel26.Controls.Add(this.eqcafentsap);
            this.panel26.Controls.Add(this.eqcafentmaterial);
            this.panel26.Controls.Add(this.eqcafentnombre);
            this.panel26.Controls.Add(this.eqcafentid);
            this.panel26.Location = new System.Drawing.Point(35, 44);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(825, 476);
            this.panel26.TabIndex = 0;
            // 
            // BotAlEqSeInforme
            // 
            this.BotAlEqSeInforme.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotAlEqSeInforme.Location = new System.Drawing.Point(444, 406);
            this.BotAlEqSeInforme.Name = "BotAlEqSeInforme";
            this.BotAlEqSeInforme.Size = new System.Drawing.Size(122, 67);
            this.BotAlEqSeInforme.TabIndex = 153;
            this.BotAlEqSeInforme.Text = "SACAR\r\nINFORME";
            this.BotAlEqSeInforme.UseVisualStyleBackColor = true;
            this.BotAlEqSeInforme.Click += new System.EventHandler(this.BotAlEqSeInforme_Click);
            // 
            // BotAlEqSeModificar
            // 
            this.BotAlEqSeModificar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotAlEqSeModificar.Location = new System.Drawing.Point(572, 406);
            this.BotAlEqSeModificar.Name = "BotAlEqSeModificar";
            this.BotAlEqSeModificar.Size = new System.Drawing.Size(122, 67);
            this.BotAlEqSeModificar.TabIndex = 152;
            this.BotAlEqSeModificar.Text = "MODIFICAR\r\nEQUIPO";
            this.BotAlEqSeModificar.UseVisualStyleBackColor = true;
            this.BotAlEqSeModificar.Click += new System.EventHandler(this.BotAlEqSeModificar_Click);
            // 
            // panel27
            // 
            this.panel27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel27.Controls.Add(this.textBuscaMaterial);
            this.panel27.Controls.Add(this.label181);
            this.panel27.Controls.Add(this.textBuscaSap);
            this.panel27.Controls.Add(this.label178);
            this.panel27.Controls.Add(this.BotAlEqSeBuscar);
            this.panel27.Controls.Add(this.textBuscaAx);
            this.panel27.Controls.Add(this.label180);
            this.panel27.Location = new System.Drawing.Point(3, 362);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(384, 111);
            this.panel27.TabIndex = 151;
            // 
            // textBuscaMaterial
            // 
            this.textBuscaMaterial.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBuscaMaterial.ForeColor = System.Drawing.Color.Black;
            this.textBuscaMaterial.Location = new System.Drawing.Point(116, 73);
            this.textBuscaMaterial.MaxLength = 0;
            this.textBuscaMaterial.Name = "textBuscaMaterial";
            this.textBuscaMaterial.Size = new System.Drawing.Size(150, 23);
            this.textBuscaMaterial.TabIndex = 140;
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label181.ForeColor = System.Drawing.Color.Black;
            this.label181.Location = new System.Drawing.Point(10, 75);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(100, 17);
            this.label181.TabIndex = 141;
            this.label181.Text = "MATERIAL SAP";
            // 
            // textBuscaSap
            // 
            this.textBuscaSap.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBuscaSap.ForeColor = System.Drawing.Color.Black;
            this.textBuscaSap.Location = new System.Drawing.Point(116, 44);
            this.textBuscaSap.MaxLength = 0;
            this.textBuscaSap.Name = "textBuscaSap";
            this.textBuscaSap.Size = new System.Drawing.Size(150, 23);
            this.textBuscaSap.TabIndex = 138;
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label178.ForeColor = System.Drawing.Color.Black;
            this.label178.Location = new System.Drawing.Point(21, 46);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(89, 17);
            this.label178.TabIndex = 139;
            this.label178.Text = "Nº SERIE SAP";
            // 
            // BotAlEqSeBuscar
            // 
            this.BotAlEqSeBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotAlEqSeBuscar.Location = new System.Drawing.Point(287, 22);
            this.BotAlEqSeBuscar.Name = "BotAlEqSeBuscar";
            this.BotAlEqSeBuscar.Size = new System.Drawing.Size(92, 64);
            this.BotAlEqSeBuscar.TabIndex = 137;
            this.BotAlEqSeBuscar.Text = "BUSCAR\r\nEQUIPO";
            this.BotAlEqSeBuscar.UseVisualStyleBackColor = true;
            this.BotAlEqSeBuscar.Click += new System.EventHandler(this.BotAlEqSeBuscar_Click);
            // 
            // textBuscaAx
            // 
            this.textBuscaAx.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBuscaAx.ForeColor = System.Drawing.Color.Black;
            this.textBuscaAx.Location = new System.Drawing.Point(116, 15);
            this.textBuscaAx.MaxLength = 0;
            this.textBuscaAx.Name = "textBuscaAx";
            this.textBuscaAx.Size = new System.Drawing.Size(150, 23);
            this.textBuscaAx.TabIndex = 135;
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label180.ForeColor = System.Drawing.Color.Black;
            this.label180.Location = new System.Drawing.Point(27, 17);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(83, 17);
            this.label180.TabIndex = 136;
            this.label180.Text = "Nº SERIE AX";
            // 
            // BotAlEqSe
            // 
            this.BotAlEqSe.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotAlEqSe.Location = new System.Drawing.Point(700, 406);
            this.BotAlEqSe.Name = "BotAlEqSe";
            this.BotAlEqSe.Size = new System.Drawing.Size(122, 67);
            this.BotAlEqSe.TabIndex = 150;
            this.BotAlEqSe.Text = "GUARDAR\r\nEQUIPO";
            this.BotAlEqSe.UseVisualStyleBackColor = true;
            this.BotAlEqSe.Click += new System.EventHandler(this.BotAlEqSe_Click);
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label179.ForeColor = System.Drawing.Color.Black;
            this.label179.Location = new System.Drawing.Point(95, 303);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(39, 16);
            this.label179.TabIndex = 149;
            this.label179.Text = "Stock";
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label177.ForeColor = System.Drawing.Color.Black;
            this.label177.Location = new System.Drawing.Point(86, 277);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(48, 16);
            this.label177.TabIndex = 147;
            this.label177.Text = "Estado";
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label176.ForeColor = System.Drawing.Color.Black;
            this.label176.Location = new System.Drawing.Point(43, 251);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(91, 16);
            this.label176.TabIndex = 146;
            this.label176.Text = "ODS de salida";
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label175.ForeColor = System.Drawing.Color.Black;
            this.label175.Location = new System.Drawing.Point(78, 225);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(52, 16);
            this.label175.TabIndex = 145;
            this.label175.Text = "Destino";
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label174.ForeColor = System.Drawing.Color.Black;
            this.label174.Location = new System.Drawing.Point(69, 199);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(65, 16);
            this.label174.TabIndex = 144;
            this.label174.Text = "Ubicación";
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label173.ForeColor = System.Drawing.Color.Black;
            this.label173.Location = new System.Drawing.Point(32, 173);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(102, 16);
            this.label173.TabIndex = 143;
            this.label173.Text = "ODS de entrada";
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label172.ForeColor = System.Drawing.Color.Black;
            this.label172.Location = new System.Drawing.Point(86, 147);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(48, 16);
            this.label172.TabIndex = 142;
            this.label172.Text = "Origen";
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label171.ForeColor = System.Drawing.Color.Black;
            this.label171.Location = new System.Drawing.Point(26, 121);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(108, 16);
            this.label171.TabIndex = 141;
            this.label171.Text = "Número serie AX";
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label170.ForeColor = System.Drawing.Color.Black;
            this.label170.Location = new System.Drawing.Point(52, 95);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(82, 16);
            this.label170.TabIndex = 140;
            this.label170.Text = "Número SAP";
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label169.ForeColor = System.Drawing.Color.Black;
            this.label169.Location = new System.Drawing.Point(83, 69);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(51, 16);
            this.label169.TabIndex = 139;
            this.label169.Text = "Código";
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label167.ForeColor = System.Drawing.Color.Black;
            this.label167.Location = new System.Drawing.Point(78, 43);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(56, 16);
            this.label167.TabIndex = 138;
            this.label167.Text = "Material";
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Font = new System.Drawing.Font("Gadugi", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label168.ForeColor = System.Drawing.Color.Black;
            this.label168.Location = new System.Drawing.Point(115, 16);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(19, 16);
            this.label168.TabIndex = 137;
            this.label168.Text = "Id";
            // 
            // eqcafentstock
            // 
            this.eqcafentstock.Location = new System.Drawing.Point(140, 302);
            this.eqcafentstock.Name = "eqcafentstock";
            this.eqcafentstock.Size = new System.Drawing.Size(55, 20);
            this.eqcafentstock.TabIndex = 11;
            this.eqcafentstock.Text = "1";
            this.eqcafentstock.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.eqcafentstock_KeyPress);
            // 
            // eqcafentestado
            // 
            this.eqcafentestado.Location = new System.Drawing.Point(140, 276);
            this.eqcafentestado.Name = "eqcafentestado";
            this.eqcafentestado.Size = new System.Drawing.Size(350, 20);
            this.eqcafentestado.TabIndex = 10;
            // 
            // eqcafentsalida
            // 
            this.eqcafentsalida.Location = new System.Drawing.Point(140, 250);
            this.eqcafentsalida.Name = "eqcafentsalida";
            this.eqcafentsalida.Size = new System.Drawing.Size(132, 20);
            this.eqcafentsalida.TabIndex = 9;
            // 
            // eqcafentdestino
            // 
            this.eqcafentdestino.Location = new System.Drawing.Point(140, 224);
            this.eqcafentdestino.Name = "eqcafentdestino";
            this.eqcafentdestino.Size = new System.Drawing.Size(658, 20);
            this.eqcafentdestino.TabIndex = 8;
            // 
            // eqcafentubicacion
            // 
            this.eqcafentubicacion.Location = new System.Drawing.Point(140, 198);
            this.eqcafentubicacion.Name = "eqcafentubicacion";
            this.eqcafentubicacion.Size = new System.Drawing.Size(350, 20);
            this.eqcafentubicacion.TabIndex = 7;
            // 
            // eqcafententrada
            // 
            this.eqcafententrada.Location = new System.Drawing.Point(140, 172);
            this.eqcafententrada.Name = "eqcafententrada";
            this.eqcafententrada.Size = new System.Drawing.Size(132, 20);
            this.eqcafententrada.TabIndex = 6;
            // 
            // eqcafentorigen
            // 
            this.eqcafentorigen.Location = new System.Drawing.Point(140, 146);
            this.eqcafentorigen.Name = "eqcafentorigen";
            this.eqcafentorigen.Size = new System.Drawing.Size(658, 20);
            this.eqcafentorigen.TabIndex = 5;
            // 
            // eqcafentax
            // 
            this.eqcafentax.Location = new System.Drawing.Point(140, 120);
            this.eqcafentax.Name = "eqcafentax";
            this.eqcafentax.Size = new System.Drawing.Size(132, 20);
            this.eqcafentax.TabIndex = 4;
            // 
            // eqcafentsap
            // 
            this.eqcafentsap.Location = new System.Drawing.Point(140, 94);
            this.eqcafentsap.Name = "eqcafentsap";
            this.eqcafentsap.Size = new System.Drawing.Size(132, 20);
            this.eqcafentsap.TabIndex = 3;
            // 
            // eqcafentmaterial
            // 
            this.eqcafentmaterial.Location = new System.Drawing.Point(140, 68);
            this.eqcafentmaterial.Name = "eqcafentmaterial";
            this.eqcafentmaterial.Size = new System.Drawing.Size(132, 20);
            this.eqcafentmaterial.TabIndex = 2;
            // 
            // eqcafentnombre
            // 
            this.eqcafentnombre.Location = new System.Drawing.Point(140, 42);
            this.eqcafentnombre.Name = "eqcafentnombre";
            this.eqcafentnombre.Size = new System.Drawing.Size(658, 20);
            this.eqcafentnombre.TabIndex = 1;
            // 
            // eqcafentid
            // 
            this.eqcafentid.Location = new System.Drawing.Point(140, 16);
            this.eqcafentid.Name = "eqcafentid";
            this.eqcafentid.ReadOnly = true;
            this.eqcafentid.Size = new System.Drawing.Size(55, 20);
            this.eqcafentid.TabIndex = 0;
            // 
            // eqTitulo
            // 
            this.eqTitulo.AutoSize = true;
            this.eqTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eqTitulo.ForeColor = System.Drawing.Color.Black;
            this.eqTitulo.Location = new System.Drawing.Point(33, 17);
            this.eqTitulo.Name = "eqTitulo";
            this.eqTitulo.Size = new System.Drawing.Size(281, 24);
            this.eqTitulo.TabIndex = 31;
            this.eqTitulo.Text = "Entradas EQUIPO CAFENTO";
            // 
            // informeToolStripMenuItem2
            // 
            this.informeToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.básicoToolStripMenuItem1,
            this.resumenToolStripMenuItem});
            this.informeToolStripMenuItem2.Name = "informeToolStripMenuItem2";
            this.informeToolStripMenuItem2.Size = new System.Drawing.Size(180, 24);
            this.informeToolStripMenuItem2.Text = "Informe";
            // 
            // básicoToolStripMenuItem1
            // 
            this.básicoToolStripMenuItem1.Name = "básicoToolStripMenuItem1";
            this.básicoToolStripMenuItem1.Size = new System.Drawing.Size(180, 24);
            this.básicoToolStripMenuItem1.Text = "Básico";
            // 
            // resumenToolStripMenuItem
            // 
            this.resumenToolStripMenuItem.Name = "resumenToolStripMenuItem";
            this.resumenToolStripMenuItem.Size = new System.Drawing.Size(180, 24);
            this.resumenToolStripMenuItem.Text = "Resumen";
            this.resumenToolStripMenuItem.Click += new System.EventHandler(this.resumenToolStripMenuItem_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 653);
            this.Controls.Add(this.panelcafe);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main";
            this.Text = "Almacén - SERVICIOS60";
            this.Load += new System.EventHandler(this.Main_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panelcafe.ResumeLayout(false);
            this.inicio.ResumeLayout(false);
            this.inicio.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.vercafe.ResumeLayout(false);
            this.vercafe.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureVer)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.insertarcafe.ResumeLayout(false);
            this.panelinsertarcafe.ResumeLayout(false);
            this.panelinsertarcafe.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.modificacafe.ResumeLayout(false);
            this.modificacafe.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureModificar)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.entradacafe.ResumeLayout(false);
            this.entradacafe.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEntrada)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.salidacafe.ResumeLayout(false);
            this.salidacafe.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.veres.ResumeLayout(false);
            this.veres.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.paDisBotParteCrear.ResumeLayout(false);
            this.pajj.ResumeLayout(false);
            this.pajj.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.botpictureimagen)).EndInit();
            this.OTaller.ResumeLayout(false);
            this.OTaller.PerformLayout();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.IBOTaller.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.DisBotSal.ResumeLayout(false);
            this.DisBotSal.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.DisBotEnt.ResumeLayout(false);
            this.DisBotEnt.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.DisBotLista.ResumeLayout(false);
            this.DisBotLista.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.IROTaller.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.VacCrear.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.VacInf.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.AlmEqSe.ResumeLayout(false);
            this.AlmEqSe.PerformLayout();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cafeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verTodosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem catalogadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem descatalogadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem todosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crearRegistroToolStripMenuItem;
        private System.Windows.Forms.TabControl panelcafe;
        private System.Windows.Forms.TabPage inicio;
        private System.Windows.Forms.TabPage vercafe;
        private System.Windows.Forms.TabPage insertarcafe;
        private System.Windows.Forms.Panel panelinsertarcafe;
        private System.Windows.Forms.TextBox reference;
        private System.Windows.Forms.TextBox family;
        private System.Windows.Forms.TextBox stock;
        private System.Windows.Forms.TextBox price;
        private System.Windows.Forms.TextBox description;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.TextBox code;
        private System.Windows.Forms.TextBox id;
        private System.Windows.Forms.TextBox img;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelotro;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button guardarcafe;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBoximg;
        private System.Windows.Forms.TextBox textBoxmachine;
        private System.Windows.Forms.TextBox textBoxfamily;
        private System.Windows.Forms.TextBox textBoxstock;
        private System.Windows.Forms.TextBox textBoxprice;
        private System.Windows.Forms.TextBox textBoxdescription;
        private System.Windows.Forms.TextBox textBoxname;
        private System.Windows.Forms.TextBox textBoxcode;
        private System.Windows.Forms.TextBox textBoxid;
        private System.Windows.Forms.Button anterior;
        private System.Windows.Forms.Button siguiente;
        private System.Windows.Forms.TextBox indiceregistros;
        private System.Windows.Forms.TabPage modificacafe;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button modificar;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox textimg;
        private System.Windows.Forms.TextBox textreference;
        private System.Windows.Forms.TextBox textcafe;
        private System.Windows.Forms.TextBox textstock;
        private System.Windows.Forms.TextBox textprice;
        private System.Windows.Forms.TextBox textdescription;
        private System.Windows.Forms.TextBox textname;
        private System.Windows.Forms.TextBox textcode;
        private System.Windows.Forms.TextBox textid;
        private System.Windows.Forms.ToolStripMenuItem modificarRegistroToolStripMenuItem;
        private System.Windows.Forms.TextBox indiceregistrosUpdate;
        private System.Windows.Forms.Button anteriorUpdate;
        private System.Windows.Forms.Button siguienteUpdate;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox buscador;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabPage entradacafe;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox stockentrada;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox entradabuscador;
        private System.Windows.Forms.TextBox entradaindice;
        private System.Windows.Forms.Button entradaanterior;
        private System.Windows.Forms.Button entradasiguiente;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Button entradaguardar;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox entradastock;
        private System.Windows.Forms.TextBox entradaname;
        private System.Windows.Forms.TextBox entradacode;
        private System.Windows.Forms.TextBox entradaid;
        private System.Windows.Forms.ToolStripMenuItem entradaSalidaToolStripMenuItem;
        private System.Windows.Forms.TabPage salidacafe;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox stocksalida;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox salidabuscador;
        private System.Windows.Forms.TextBox salidaindice;
        private System.Windows.Forms.Button salidaanterior;
        private System.Windows.Forms.Button salidasiguiente;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Button salidaguardar;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox salidastock;
        private System.Windows.Forms.TextBox salidaname;
        private System.Windows.Forms.TextBox salidacode;
        private System.Windows.Forms.TextBox salidaid;
        private System.Windows.Forms.ToolStripMenuItem salidaToolStripMenuItem;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox proveedorentrada;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox destinosalida;
        private System.Windows.Forms.DateTimePicker datesalida;
        private System.Windows.Forms.DateTimePicker dateentrada;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.ToolStripMenuItem inicioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureVer;
        private System.Windows.Forms.PictureBox pictureModificar;
        private System.Windows.Forms.PictureBox pictureEntrada;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.ToolStripMenuItem buscarEntradasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buscarSalidasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevaEntradaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevaSalidaToolStripMenuItem;
        private System.Windows.Forms.TabPage veres;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox verentradaid;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox verentradabuscador;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox verentradatipo;
        private System.Windows.Forms.TextBox verentradaproveedor;
        private System.Windows.Forms.TextBox verentradaproducto;
        private System.Windows.Forms.TextBox verentradaunidades;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox verentradaindice;
        private System.Windows.Forms.Button verentradaanterior;
        private System.Windows.Forms.Button verentradasiguiente;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.ToolStripMenuItem distribuidoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem botellerosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem entradasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salidasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem equiposToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem borrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem editarToolStripMenuItem;
        private System.Windows.Forms.TabPage paDisBotParteCrear;
        private System.Windows.Forms.Panel pajj;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Button botguardar;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.PictureBox botpictureimagen;
        private System.Windows.Forms.TextBox botestado;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.TextBox botfecha;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox botid;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.TextBox botns;
        private System.Windows.Forms.TextBox botgas;
        private System.Windows.Forms.Label botttttt;
        private System.Windows.Forms.TextBox botorigen;
        private System.Windows.Forms.TextBox botmo;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.TextBox botimagen;
        private System.Windows.Forms.TextBox botfabricante;
        private System.Windows.Forms.TextBox botmarca;
        private System.Windows.Forms.TextBox botmodelo;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.TextBox botecnico;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TextBox botanyo;
        private System.Windows.Forms.TabPage OTaller;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.TextBox otmontaje;
        private System.Windows.Forms.TextBox otestado;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.TextBox otf1;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.TextBox otid;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.TextBox otproyecto;
        private System.Windows.Forms.TextBox otaveria;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.TextBox otpds;
        private System.Windows.Forms.TextBox otrepuesto;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.TextBox otcliente;
        private System.Windows.Forms.TextBox otmaterial1;
        private System.Windows.Forms.TextBox otax;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.TextBox otf2;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.TextBox ottecnico;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.TextBox otoferta;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.TextBox otorden;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.TextBox otsap;
        private System.Windows.Forms.TextBox otmaterial2;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.TextBox ottaller;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.TextBox otdesmontaje;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.TextBox otprovincia;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.TextBox otdespla;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.TextBox otmo;
        private System.Windows.Forms.ToolStripMenuItem oTallerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crearToolStripMenuItem;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button otallerimprimir;
        private System.Windows.Forms.TextBox otresultado;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.ToolStripMenuItem informesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem informesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem verToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem informesToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem básicoToolStripMenuItem;
        private System.Windows.Forms.TabPage IBOTaller;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Button IBOTallerConsultar;
        private System.Windows.Forms.DateTimePicker DateIBOTallerFin;
        private System.Windows.Forms.DateTimePicker DateIBOTallerInicio;
        private System.Windows.Forms.ComboBox comboIBOTaller;
        private System.Windows.Forms.TextBox otcmi;
        private System.Windows.Forms.Label labelcmi;
        private System.Windows.Forms.ToolStripMenuItem crearToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem verToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem crearToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem verToolStripMenuItem3;
        private System.Windows.Forms.TabPage DisBotSal;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.TextBox salbotcantidad;
        private System.Windows.Forms.TextBox salbotdestino;
        private System.Windows.Forms.TextBox salbotns;
        private System.Windows.Forms.DateTimePicker salbotfecha;
        private System.Windows.Forms.Button salbotimprimir;
        private System.Windows.Forms.Button salotguardar;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.TextBox salbottransportista;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.TextBox salbotorigen;
        private System.Windows.Forms.TabPage DisBotEnt;
        private System.Windows.Forms.Button entbotimprimir;
        private System.Windows.Forms.Button entbotguardar;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.TextBox entbottransportista;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.TextBox entbotorigen;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.TextBox entbotcantidad;
        private System.Windows.Forms.TextBox entbotdestino;
        private System.Windows.Forms.TextBox entbotns;
        private System.Windows.Forms.DateTimePicker entbotfecha;
        private System.Windows.Forms.TabPage DisBotLista;
        private System.Windows.Forms.Button disBotListaImprimir;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.DateTimePicker DateDisBotListaFeFin;
        private System.Windows.Forms.DateTimePicker DateDisBotListaFeInicio;
        private System.Windows.Forms.ToolStripMenuItem imprimirToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton ListaBotConBorde;
        private System.Windows.Forms.RadioButton ListaBotSinBorde;
        private System.Windows.Forms.ComboBox ottipo;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.TabPage IROTaller;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.ComboBox comboIROTallerTecnico;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.ComboBox comboIROTallerTipo;
        private System.Windows.Forms.Button IROTallerConsultar;
        private System.Windows.Forms.DateTimePicker dateIROTallerFin;
        private System.Windows.Forms.DateTimePicker dateIROTallerInicio;
        private System.Windows.Forms.ToolStripMenuItem resúmenToolStripMenuItem;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.ComboBox comboIROTallerObjeto;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton radioIRDetalle;
        private System.Windows.Forms.RadioButton radioIRResumen;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.TextBox IROTallerencabezado;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.TextBox IROTallerdias;
        private System.Windows.Forms.ToolStripMenuItem personalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vacacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crearToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem eliminarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem informeToolStripMenuItem;
        private System.Windows.Forms.TabPage VacCrear;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Button vacCreGuardar;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.TextBox vacCreDAnteriores;
        private System.Windows.Forms.TextBox vacCreDAcumulados;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.TextBox vacCreDActuales;
        private System.Windows.Forms.TextBox vacCreDSaldo;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.TextBox vacCreDCogidos;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.TextBox vacCreFecha;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.TextBox vacCreDescripcion;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.ComboBox vacCreNombre;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.TextBox textInicio;
        private System.Windows.Forms.TabPage VacInf;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Button vacInfVer;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.DateTimePicker vacInfFeFin;
        private System.Windows.Forms.DateTimePicker vacInfFeInicio;
        private System.Windows.Forms.ComboBox vacInfNombre;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.TextBox vacCreDDados;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.ComboBox comboIROTallerProyecto;
        private System.Windows.Forms.ToolStripMenuItem cerrarAceptadoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cerrarRechazadoToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.RadioButton radioButtonRechazado;
        private System.Windows.Forms.RadioButton radioButtonAceptado;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Button IBOTallerCerrar;
        private System.Windows.Forms.ToolStripMenuItem guardiasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cafeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pepsiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cervezaToolStripMenuItem;
        private System.Windows.Forms.Button vacInfVerTodos;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.RadioButton radioButtonAbierto;
        private System.Windows.Forms.RadioButton radioButtonCerrado;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.RadioButton ListaBotReparado;
        private System.Windows.Forms.RadioButton ListaBotTodo;
        private System.Windows.Forms.RadioButton ListaBotEntregado;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.ComboBox comboIBotTipo;
        private System.Windows.Forms.ComboBox OTencuentra_repuesto;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.TextBox OTbusca_repuesto;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.ComboBox OTencuentra_repuestoCod;
        private System.Windows.Forms.Button OTañadir_repuesto;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.TextBox OTrepuesto_uds;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Button otallerbuscar;
        private System.Windows.Forms.TextBox otidbuscar;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.TextBox otordenbuscar;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.ToolStripMenuItem repuestosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem caféToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buscarToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Button RepCafBuscar1;
        private System.Windows.Forms.TextBox RepCafCodAXBuscar;
        private System.Windows.Forms.TextBox RepCafCodSAPBuscar;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.Label label162;
        private System.Windows.Forms.TextBox RepCafIdBuscar;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.ComboBox comboBusPre;
        private System.Windows.Forms.ComboBox comboBusCod;
        private System.Windows.Forms.ComboBox comboBusNom;
        private System.Windows.Forms.TextBox RepCafNombreBuscar;
        private System.Windows.Forms.Label label165;
        private System.Windows.Forms.TextBox RepCafRepEncontrado;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.Button RepCafCambiar;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.TextBox RepCafPrecio;
        private System.Windows.Forms.ComboBox comboTipo;
        private System.Windows.Forms.ToolStripMenuItem pepsiToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem buscarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem oTallerGeneralToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crearToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem berlysToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem danoneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ikeaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem starbucksToolStripMenuItem;
        private System.Windows.Forms.TextBox otcodproyecto;
        private System.Windows.Forms.ToolStripMenuItem heinekenStcToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem heinekenEventoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem danoneWaterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem corecoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem corecoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem buscarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem scheeppesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem plazaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eventoToolStripMenuItem;
        private System.Windows.Forms.ComboBox comboPrecio;
        private System.Windows.Forms.TabPage AlmEqSe;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.TextBox eqcafentestado;
        private System.Windows.Forms.TextBox eqcafentsalida;
        private System.Windows.Forms.TextBox eqcafentdestino;
        private System.Windows.Forms.TextBox eqcafentubicacion;
        private System.Windows.Forms.TextBox eqcafententrada;
        private System.Windows.Forms.TextBox eqcafentorigen;
        private System.Windows.Forms.TextBox eqcafentax;
        private System.Windows.Forms.TextBox eqcafentsap;
        private System.Windows.Forms.TextBox eqcafentmaterial;
        private System.Windows.Forms.TextBox eqcafentnombre;
        private System.Windows.Forms.TextBox eqcafentid;
        private System.Windows.Forms.TextBox eqcafentstock;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.Label label177;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.Label label174;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.Label label170;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.Label eqTitulo;
        private System.Windows.Forms.ToolStripMenuItem almacenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem equiposSeriadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cafeToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem cafentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem latitudToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem candelasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blackziToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deltaToolStripMenuItem;
        private System.Windows.Forms.Button BotAlEqSe;
        private System.Windows.Forms.Button BotAlEqSeInforme;
        private System.Windows.Forms.Button BotAlEqSeModificar;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.TextBox textBuscaMaterial;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.TextBox textBuscaSap;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.Button BotAlEqSeBuscar;
        private System.Windows.Forms.TextBox textBuscaAx;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.ToolStripMenuItem crearEquipoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verEquipoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarEquipoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem informeToolStripMenuItem1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox botsap;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox botcodigo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox botmaterial;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox botorden;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox botoferta;
        private System.Windows.Forms.ToolStripMenuItem informeToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem básicoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem resumenToolStripMenuItem;
    }
}

