﻿using CafeS60.CORE;
using CafeS60.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafeS60.APPLICATION
{
    public class ProcessAlmEqSeriado
    {
        public bool insertarRegistro(CafentoEquipos p)
        {
            bool resultado = false;
            AlmEqSeriadoSQL proceso = new AlmEqSeriadoSQL();
            if (proceso.inserta(p) > 0) resultado = true;
            return resultado;
        }

        public CafentoEquipos BuscarRegistro(string id)
        {
            CafentoEquipos equipo = new CafentoEquipos();
            equipo = null;
            AlmEqSeriadoSQL proceso = new AlmEqSeriadoSQL();
            if ((proceso.existeNsAX(id) > 0) || (proceso.existeNsSAP(id) > 0) || 
                                                (proceso.existeMaterial(id) > 0))
                equipo = proceso.obtenerRegistro(id);
            return equipo;
        }

        public bool modificarRegistro(CafentoEquipos p)
        {
            bool resultado = false;
            AlmEqSeriadoSQL proceso = new AlmEqSeriadoSQL();
            if ((proceso.existeNsAX(p.CodAX) > 0) || (proceso.existeNsSAP(p.CodSAP) > 0) ||
                                                (proceso.existeMaterial(p.CodigoMaterial) > 0))
            {
                if (proceso.modificar(p) > 0) resultado = true;
            }
            return resultado;
        }

        public List<CafentoEquipos> InformeBasicoRegistro()
        {
            AlmEqSeriadoSQL proceso = new AlmEqSeriadoSQL();
            return proceso.busca();
        }
    }
}
