﻿using CafeS60.CORE;
using CafeS60.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafeS60.APPLICATION
{
    public class ProcessOTaller
    {
        public OTaller BuscarOrdenId(int idorden)
        {
            OTaller orden = new OTaller();
            orden = null;
            OTallerSQL proceso = new OTallerSQL();
            if (proceso.existe(idorden)>0)
                orden = proceso.obtener_id(idorden);
            return orden;
        }

        public OTaller BuscarOrdenId2(int idorden)
        {
            OTaller orden = new OTaller();
            orden = null;
            OTallerSQL proceso = new OTallerSQL();
            if (proceso.existe2(idorden) > 0)
                orden = proceso.obtener_id2(idorden);
            return orden;
        }

        public OTaller BuscarOrdenOrden(string idorden)
        {
            OTaller orden = new OTaller();
            orden = null;
            OTallerSQL proceso = new OTallerSQL();
            if (proceso.existe(idorden) > 0)
                orden = proceso.obtener_orden(idorden);
            return orden;
        }

        public OTaller BuscarOrdenOrden2(string idorden)
        {
            OTaller orden = new OTaller();
            orden = null;
            OTallerSQL proceso = new OTallerSQL();
            if (proceso.existe2(idorden) > 0)
                orden = proceso.obtener_orden2(idorden);
            return orden;
        }

        public bool insertarRegistro(OTaller p)
        {
            bool resultado = false;
            OTallerSQL proceso = new OTallerSQL();
            if (proceso.inserta(p) > 0) resultado = true;
            return resultado;
        }

        public bool insertarRegistro2(OTaller p)
        {
            bool resultado = false;
            OTallerSQL proceso = new OTallerSQL();
            if (proceso.inserta2(p) > 0) resultado = true;
            return resultado;
        }

        public List<OTaller> InformeBasicoRegistro(int fechainicio, int fechafin, string tipo)
        {
            OTallerSQL proceso = new OTallerSQL();
            return proceso.busca(fechainicio, fechafin, tipo);
        }

        public List<OTaller> AceptadoRechazado1(int fechainicio, int fechafin, string tipo, string estado)
        {
            OTallerSQL proceso = new OTallerSQL();
            return proceso.busca(fechainicio, fechafin, tipo, estado);
        }

        public List<OTaller> AceptadoRechazado2(int fechainicio, int fechafin, string tipo, string estado, string resultado)
        {
            OTallerSQL proceso = new OTallerSQL();
            return proceso.busca(fechainicio, fechafin, tipo, estado, resultado);
        }

        public void ConcluirOrdenes(int idorden)
        {
            OTallerSQL proceso = new OTallerSQL();
            proceso.cerrar(idorden);
        }

        public void AceptarOrdenes(int idorden)
        {
            OTallerSQL proceso = new OTallerSQL();
            proceso.aceptar(idorden);
        }

        public void RechazarOrdenes(int idorden)
        {
            OTallerSQL proceso = new OTallerSQL();
            proceso.rechazar(idorden);
        }

        public float obtenerTotales(int fechainicio, int fechafin, string mecanico, string objeto, string tipo, string proyecto)
        {
            OTallerSQL proceso = new OTallerSQL();
            return proceso.valor(fechainicio, fechafin, mecanico, objeto, tipo, proyecto);
        }

        public List<OTaller> obtenerTotalesDetalle(int fechainicio, int fechafin, string mecanico, string objeto, string tipo, string proyecto)
        {
            OTallerSQL proceso = new OTallerSQL();
            return proceso.valores(fechainicio, fechafin, mecanico, objeto, tipo, proyecto);
        }

        public List<OTaller> obtenerTotalesDetalleGeneral(int fechainicio, int fechafin, string mecanico, string objeto, string tipo, string proyecto)
        {
            OTallerSQL proceso = new OTallerSQL();
            return proceso.valoresGeneral(fechainicio, fechafin, mecanico, objeto, tipo, proyecto);
        }
    }
}
