﻿using CafeS60.CORE;
using CafeS60.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafeS60.APPLICATION
{
    public class ProcessProduct
    {
        public List<Product> ObtenerRegistrosTodos()
        {
            ProductSQL proceso = new ProductSQL();
            return proceso.obtenerTodos(3);
        }

        public int ObtenerStock(int id)
        {
            ProductSQL proceso = new ProductSQL();
            return proceso.obtenerStock(id);
        }

        public List<Product> ObtenerRegistrosTodosFiltrado(string n)
        {
            ProductSQL proceso = new ProductSQL();
            return proceso.obtenerTodosFiltrado(2, n);
        }

        public List<Product> ObtenerRegistrosTodosStock(string n)
        {
            ProductSQL proceso = new ProductSQL();
            return proceso.obtenerTodosStock(2, n);
        }

        public List<Product> ObtenerRegistrosCatalogados()
        {
            ProductSQL proceso = new ProductSQL();
            return proceso.obtenerTodos(1);
        }

        public List<Product> ObtenerRegistrosDescatalogados()
        {
            ProductSQL proceso = new ProductSQL();
            return proceso.obtenerTodos(0);
        }

        public bool insertarRegistrosTodos(Product p)
        {
            bool resultado = false;
            ProductSQL proceso = new ProductSQL();
            if (proceso.inserta(p) > 0) resultado = true;
            return resultado;
        }

        public bool modificarRegistrosTodos(int id, Product p)
        {
            bool resultado = false;
            ProductSQL proceso = new ProductSQL();
            if (proceso.modifica(id,p) > 0) resultado = true;
            return resultado;
        }

        public bool stockRegistrosTodos(int id, string code, string name, int stock)
        {
            bool resultado = false;
            ProductSQL proceso = new ProductSQL();
            if (proceso.stock(id, code, name, stock) > 0) resultado = true;
            return resultado;
        }

        public bool comprobarDuplicados(string c, string n)
        {
            ProductSQL proceso = new ProductSQL();
            return proceso.productoDuplicado(c, n);
        }

        public List<Move> ObtenerEntradasTodo()
        {
            ProductSQL proceso = new ProductSQL();
            return proceso.obtenerEntradas();
        }
    }
}
