﻿using CafeS60.CORE;
using CafeS60.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafeS60.APPLICATION
{
    public class ProcessMove
    {
        public bool insertarEntrada(int entrada, string proveedor, DateTime fecha, string producto)
        {
            bool resultado = false;
            MoveSQL proceso = new MoveSQL();
            if (proceso.insertaE(entrada, proveedor, fecha, producto) > 0) resultado = true;
            return resultado;
        }

        public bool insertarSalida(int entrada, string proveedor, DateTime fecha, string producto)
        {
            bool resultado = false;
            MoveSQL proceso = new MoveSQL();
            if (proceso.insertaS(entrada, proveedor, fecha, producto) > 0) resultado = true;
            return resultado;
        }
    }
}
