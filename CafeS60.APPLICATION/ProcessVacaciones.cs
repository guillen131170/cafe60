﻿using CafeS60.CORE;
using CafeS60.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafeS60.APPLICATION
{
    public class ProcessVacaciones
    {
        public bool insertarRegistro(Vacaciones p)
        {
            bool resultado = false;
            VacacionesSQL proceso = new VacacionesSQL();
            if (proceso.inserta(p) > 0) resultado = true;
            return resultado;
        }

        public List<Vacaciones> InformeBasicoVacaciones(int fechainicio, int fechafin, string nombre)
        {
            VacacionesSQL proceso = new VacacionesSQL();
            return proceso.busca(fechainicio, fechafin, nombre);
        }

        public List<Vacaciones> InformeBasicoVacaciones(string nombre)
        {
            VacacionesSQL proceso = new VacacionesSQL();
            return proceso.busca(nombre);
        }

        public Vacaciones BuscarRegistroPorNombre(string nombre)
        {
            VacacionesSQL proceso = new VacacionesSQL();
            return proceso.buscaPorNombre(nombre);
        }
    }
}
