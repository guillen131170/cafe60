﻿using CafeS60.CORE;
using CafeS60.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafeS60.APPLICATION
{
    public class ProcessSAP_Repuestos_Pepsi
    {
        public void CambiarPrecio(int idorden, float precio)
        {
            ProcessSAP_Repuestos_PepsiSQL proceso = new ProcessSAP_Repuestos_PepsiSQL();
            proceso.cambiarPrecio(idorden, precio);
        }

        public SAP_Repuestos_Pepsi BuscarOrdenId(int idorden)
        {
            SAP_Repuestos_Pepsi orden = new SAP_Repuestos_Pepsi();
            orden = null;
            ProcessSAP_Repuestos_PepsiSQL proceso = new ProcessSAP_Repuestos_PepsiSQL();
            if (proceso.existeId(idorden) > 0)
                orden = proceso.obtenerId(idorden);
            return orden;
        }

        public SAP_Repuestos_Pepsi BuscarOrdenSap(string idorden)
        {
            SAP_Repuestos_Pepsi orden = new SAP_Repuestos_Pepsi();
            orden = null;
            ProcessSAP_Repuestos_PepsiSQL proceso = new ProcessSAP_Repuestos_PepsiSQL();
            if (proceso.existeSap(idorden) > 0)
                orden = proceso.obtenerSap(idorden);
            return orden;
        }

        public SAP_Repuestos_Pepsi BuscarOrdenAx(string idorden)
        {
            SAP_Repuestos_Pepsi orden = new SAP_Repuestos_Pepsi();
            orden = null;
            ProcessSAP_Repuestos_PepsiSQL proceso = new ProcessSAP_Repuestos_PepsiSQL();
            if (proceso.existeAx(idorden) > 0)
                orden = proceso.obtenerAx(idorden);
            return orden;
        }

        public List<SAP_Repuestos_Pepsi> ObtenerRegistrosTodos()
        {
            ProcessSAP_Repuestos_PepsiSQL proceso = new ProcessSAP_Repuestos_PepsiSQL();
            return proceso.obtenerTodos();
        }

        public List<SAP_Repuestos_Pepsi> ObtenerRegistrosTodosCopia()
        {
            ProcessSAP_Repuestos_PepsiSQL proceso = new ProcessSAP_Repuestos_PepsiSQL();
            return proceso.obtenerTodosCopia();
        }

        public List<SAP_Repuestos_Pepsi> ObtenerRegistrosTodosNombre(string nombre)
        {
            ProcessSAP_Repuestos_PepsiSQL proceso = new ProcessSAP_Repuestos_PepsiSQL();
            return proceso.obtenerTodosNombre(nombre);
        }

        public bool insertarRegistro(SAP_Repuestos_Pepsi p)
        {
            bool resultado = false;
            ProcessSAP_Repuestos_PepsiSQL proceso = new ProcessSAP_Repuestos_PepsiSQL();
            if (proceso.inserta(p) > 0) resultado = true;
            return resultado;
        }
    }
}
