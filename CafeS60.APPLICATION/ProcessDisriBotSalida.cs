﻿using CafeS60.CORE;
using CafeS60.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafeS60.APPLICATION
{
    public class ProcessDisriBotSalida
    {
        public bool insertarRegistro(SalidaDistriBot p)
        {
            bool resultado = false;
            SalidaDistriBotSQL proceso = new SalidaDistriBotSQL();
            if (proceso.inserta(p) > 0) resultado = true;
            return resultado;
        }

        public List<ParteBotellero> InformeBasicoBotelleros(int fechainicio, int fechafin, string tipo)
        {
            ParteBotSQL proceso = new ParteBotSQL();
            return proceso.busca(fechainicio, fechafin, tipo);
        }
    }
}
