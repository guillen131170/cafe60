﻿using CafeS60.CORE;
using CafeS60.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafeS60.APPLICATION
{
    public class ProcessDisriBotEntrada
    {
        public bool insertarRegistro(EntradaDistriBot p)
        {
            bool resultado = false;
            EntradaDistriBotSQL proceso = new EntradaDistriBotSQL();
            if (proceso.inserta(p) > 0) resultado = true;
            return resultado;
        }
    }
}
