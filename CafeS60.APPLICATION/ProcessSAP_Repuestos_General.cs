﻿using CafeS60.CORE;
using CafeS60.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafeS60.APPLICATION
{
    public class ProcessSAP_Repuestos_General
    {
        public void CambiarPrecio(int idorden, float precio)
        {
            ProcessSAP_Repuestos_GeneralSQL proceso = new ProcessSAP_Repuestos_GeneralSQL();
            proceso.cambiarPrecio(idorden, precio);
        }

        public SAP_Repuestos_General BuscarOrdenId(int idorden)
        {
            SAP_Repuestos_General orden = new SAP_Repuestos_General();
            orden = null;
            ProcessSAP_Repuestos_GeneralSQL proceso = new ProcessSAP_Repuestos_GeneralSQL();
            if (proceso.existeId(idorden) > 0)
                orden = proceso.obtenerId(idorden);
            return orden;
        }

        public SAP_Repuestos_General BuscarOrdenSap(string idorden)
        {
            SAP_Repuestos_General orden = new SAP_Repuestos_General();
            orden = null;
            ProcessSAP_Repuestos_GeneralSQL proceso = new ProcessSAP_Repuestos_GeneralSQL();
            if (proceso.existeSap(idorden) > 0)
                orden = proceso.obtenerSap(idorden);
            return orden;
        }

        public SAP_Repuestos_General BuscarOrdenAx(string idorden)
        {
            SAP_Repuestos_General orden = new SAP_Repuestos_General();
            orden = null;
            ProcessSAP_Repuestos_GeneralSQL proceso = new ProcessSAP_Repuestos_GeneralSQL();
            if (proceso.existeAx(idorden) > 0)
                orden = proceso.obtenerAx(idorden);
            return orden;
        }

        public List<SAP_Repuestos_General> ObtenerRegistrosTodos()
        {
            ProcessSAP_Repuestos_GeneralSQL proceso = new ProcessSAP_Repuestos_GeneralSQL();
            return proceso.obtenerTodos();
        }

        public List<SAP_Repuestos_General> ObtenerRegistrosTodosSap(string nombre)
        {
            ProcessSAP_Repuestos_GeneralSQL proceso = new ProcessSAP_Repuestos_GeneralSQL();
            return proceso.obtenerTodosSap(nombre);
        }

        public List<SAP_Repuestos_General> ObtenerRegistrosTodosNombre(string nombre)
        {
            ProcessSAP_Repuestos_GeneralSQL proceso = new ProcessSAP_Repuestos_GeneralSQL();
            return proceso.obtenerTodosNombre(nombre);
        }

        public bool insertarRegistro(SAP_Repuestos_General p)
        {
            bool resultado = false;
            ProcessSAP_Repuestos_GeneralSQL proceso = new ProcessSAP_Repuestos_GeneralSQL();
            if (proceso.inserta(p) > 0) resultado = true;
            return resultado;
        }
    }
}
