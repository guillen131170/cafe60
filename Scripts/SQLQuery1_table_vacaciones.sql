USE catalogo
GO
 
/****** Object:  Table [catalogo].[vacaciones]    Script Date: 14/01/20120:48:12 PM ******/
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE vacaciones(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	name [nvarchar](60) NOT NULL,
	ndias_anteriores [int] NOT NULL,
	ndias_actuales [int] NOT NULL,
	ndias_acumulados [int] NOT NULL,
	ndias_cogidos [int] NOT NULL,
	ndias_saldo [int] NOT NULL,
	description [nvarchar](240) NULL,
	fecha int NOT NULL
	)

GO