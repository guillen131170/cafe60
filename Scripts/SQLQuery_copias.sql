USE catalogo
GO
SET IDENTITY_INSERT cafentoequipos ON
INSERT INTO cafentoequipos
(id, nombrematerial, CodigoMaterial, CodSAP, CodAX, Origen, ODSEntrada, Ubicacion, Destino, ODSSalida, Estado, Stock)
SELECT * FROM copiacatalogo.dbo.cafentoequipos
SET IDENTITY_INSERT cafentoequipos OFF


USE catalogo
GO
SELECT * INTO cafentoequipos
FROM copiacatalogo.dbo.cafentoequipos
GO