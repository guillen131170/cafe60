USE catalogo
GO
 
/****** Object:  Table [catalogo].[disBotParte]    Script Date: 14/01/20120:48:12 PM ******/
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE sap_repuestos_pepsi(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	proveedor [nvarchar](20) NULL DEFAULT '0',
	codax [nvarchar](20) NULL DEFAULT '0',
	codsap [nvarchar](20) NULL DEFAULT '0',
	fabricante [nvarchar](20) NULL DEFAULT '0',
	nombre [nvarchar](255) NULL DEFAULT '0',
	precio [float] NULL DEFAULT 0,
	)

GO