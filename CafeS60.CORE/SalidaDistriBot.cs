﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafeS60.CORE
{
    public class SalidaDistriBot
    {
        private int id;
        private int fecha;
        private string ns;
        private string destino;
        private string origen;
        private string transportista;
        private int cantidad;


        public int Id { get; set; }
        public int Fecha { get; set; }
        public string Ns { get; set; }
        public string Destino { get; set; }
        public string Origen { get; set; }
        public string Transportista { get; set; }
        public int Cantidad { get; set; }
    }
}
