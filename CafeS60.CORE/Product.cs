﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafeS60.CORE
{

    public class Product
    {
        /// <summary>
        /// 
        /// </summary>
        #region Atributes
        private int id;
        private string code;
        private string name;
        private string description;
        private float price;
        private int stock;
        private string family;
        private string reference;
        private string img;
        #endregion


        /// <summary>
        /// 
        /// </summary>
        #region Properties
        [Key]
        [Required]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [Required]
        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        [Required]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        [Required]
        public float Price
        {
            get { return price; }
            set { price = value; }
        }

        [Required]
        public int Stock
        {
            get { return stock; }
            set { stock = value; }
        }

        [Required]
        public string Family
        {
            get { return family; }
            set { family = value; }
        }

        [Required]
        public string Reference
        {
            get { return reference; }
            set { reference = value; }
        }

        public string Img
        {
            get { return img; }
            set { img = value; }
        }
        #endregion
    }

}
