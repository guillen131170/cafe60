﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafeS60.CORE
{
    public class SAP_Repuestos_General
    {
        /// <summary>
        /// 
        /// </summary>
        #region Atributes
        private int id;
        private string code_sap;
        private string code_ax;
        private string name;
        private float price;
        #endregion


        /// <summary>
        /// 
        /// </summary>
        #region Properties
        [Key]
        [Required]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [Required]
        public string Code_sap
        {
            get { return code_sap; }
            set { code_sap = value; }
        }

        public string Code_ax
        {
            get { return code_ax; }
            set { code_ax = value; }
        }

        [Required]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public float Price
        {
            get { return price; }
            set { price = value; }
        }
        #endregion
    }
}
