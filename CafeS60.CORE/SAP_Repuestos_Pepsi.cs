﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafeS60.CORE
{
    public class SAP_Repuestos_Pepsi
    {
        /// <summary>
        /// 
        /// </summary>
        #region Atributes
        private int id;
        private string proveedor;
        private string codax;
        private string codsap;
        private string fabricante;
        private string nombre;
        private float precio;
        #endregion


        /// <summary>
        /// 
        /// </summary>
        #region Properties
        [Key]
        [Required]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Proveedor
        {
            get { return proveedor; }
            set { proveedor = value; }
        }

        public string Codsap
        {
            get { return codsap; }
            set { codsap = value; }
        }

        public string Codax
        {
            get { return codax; }
            set { codax = value; }
        }

        public string Fabricante
        {
            get { return fabricante; }
            set { fabricante = value; }
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public float Precio
        {
            get { return precio; }
            set { precio = value; }
        }
        #endregion
    }
}
