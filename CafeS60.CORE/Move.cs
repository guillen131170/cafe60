﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafeS60.CORE
{
    public class Move
    {
        /// <summary>
        /// 
        /// </summary>
        #region Atributes
        private int id;
        private string product;
        private string type;
        private DateTime date;
        private string destination;
        private string supplier;
        private int units;
        #endregion


        /// <summary>
        /// 
        /// </summary>
        #region Properties
        [Key]
        [Required]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [Required]
        public string Product
        {
            get { return product; }
            set { product = value; }
        }

        [Required]
        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        [Required]
        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public string Destination
        {
            get { return destination; }
            set { destination = value; }
        }

        public string Supplier
        {
            get { return supplier; }
            set { supplier = value; }
        }

        [Required]
        public int Units
        {
            get { return units; }
            set { units = value; }
        }
        #endregion
    }
}
