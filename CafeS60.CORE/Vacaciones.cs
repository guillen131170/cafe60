﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafeS60.CORE
{
    /// <summary>
	/// 
	/// </summary>
	public class Vacaciones
    {
		private int id;
		private string name;
		private int ndias_anteriores;
		private int ndias_actuales;
		private int ndias_acumulados;
		private int ndias_abonados;
		private int ndias_cogidos;
		private int saldo;
		private string description;
		private int fecha;

        public int Id { get; set; }
		public string Name { get; set; }
		public int Ndias_anteriores { get; set; }
		public int Ndias_actuales { get; set; }
		public int Ndias_acumulados { get; set; }
		public int Ndias_abonados { get; set; }
		public int Ndias_cogidos { get; set; }
		public int Saldo { get; set; }
		public string Description { get; set; }
		public int Fecha { get; set; }
	}
}
