﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafeS60.CORE
{
    public class Repuesto_SAP_General
    {
        /// <summary>
        /// 
        /// </summary>
        #region Atributes
        private string cliente;
        private string code_sap;
        private string code_ax;
        private string name;
        private decimal price;
        #endregion


        /// <summary>
        /// 
        /// </summary>
        #region Properties
        [Key]
        [Required]
        public string Cliente
        {
            get { return cliente; }
            set { cliente = value; }
        }

        [Required]
        public string Code_sap
        {
            get { return code_sap; }
            set { code_sap = value; }
        }

        public string Code_ax
        {
            get { return code_ax; }
            set { code_ax = value; }
        }

        [Required]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public decimal Price
        {
            get { return price; }
            set { price = value; }
        }
        #endregion
    }
}
