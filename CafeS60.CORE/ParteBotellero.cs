﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafeS60.CORE
{
    public class ParteBotellero
    {
        #region Atributes
        private int id;
        private int f_repa;
        private string mo;
        private string origen;
        private string ns;
        private string fabricante;
        private string marca;
        private string gas;
        private string modelo;
        private string estado;
        private string img;
        private string tecnico;
        private int anyo;
        private int f_entrega;
        private string material;
        private string codigo;
        private string sap;
        private float oferta;
        private string orden;
        private string resultado;
        #endregion

        #region properties
        [Key]
        [Required]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public int F_repa
        {
            get { return f_repa; }
            set { f_repa = value; }
        }

        [Required]
        public string Mo
        {
            get { return mo; }
            set { mo = value; }
        }

        [Required]
        public string Origen
        {
            get { return origen; }
            set { origen = value; }
        }

        [Required]
        public string Ns
        {
            get { return ns; }
            set { ns = value; }
        }

        [Required]
        public string Fabricante
        {
            get { return fabricante; }
            set { fabricante = value; }
        }

        public string Marca
        {
            get { return marca; }
            set { marca = value; }
        }

        public string Gas
        {
            get { return gas; }
            set { gas = value; }
        }

        public string Modelo
        {
            get { return modelo; }
            set { modelo = value; }
        }

        [Required]
        public string Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        public string Img
        {
            get { return img; }
            set { img = value; }
        }

        [Required]
        public string Tecnico
        {
            get { return tecnico; }
            set { tecnico = value; }
        }

        public int Anyo
        {
            get { return anyo; }
            set { anyo = value; }
        }

        public int F_entrega
        {
            get { return f_entrega; }
            set { f_entrega = value; }
        }

        public string Material
        {
            get { return material; }
            set { material = value; }
        }

        public string Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }

        public string SAP
        {
            get { return sap; }
            set { sap = value; }
        }

        public float Oferta
        {
            get { return oferta; }
            set { oferta = value; }
        }

        public string Orden
        {
            get { return orden; }
            set { orden = value; }
        }

        public string Resultado
        {
            get { return resultado; }
            set { resultado = value; }
        }
        #endregion
    }
}
